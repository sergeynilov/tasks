<?php

namespace Modules\SqlMonitor\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class SqlMonitorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        // FROM :  file:///_wwwroot/lar/lprods/Modules/SqlMonitor/Assets/js
        // file:///_wwwroot/lar/lprods/public/modules/sqlmonitor
        $source_dir= base_path().'/Modules/SqlMonitor/Assets';
//        echo '<pre>-1 $source_dir ::'.print_r($source_dir,true).'</pre>';

        $destination_public_dir= public_path('/modules/sqlmonitor');
//        echo '<pre>-2 $destination_public_dir::'.print_r($destination_public_dir,true).'</pre>';
        $this->publishes([
            $source_dir => $destination_public_dir,
        ], 'public');

        /* // Path to the project's root folder
echo base_path();

// Path to the 'app' folder
echo app_path();

// Path to the 'public' folder
echo public_path();

//Path to the 'app/storage' folder
echo storage_path();  */


//        die("-1 XXZ");
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('sqlmonitor.php'),   ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'sqlmonitor'
        );
    }

    /**/
    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/sqlmonitor');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/sqlmonitor';
        }, \Config::get('view.paths')), [$sourcePath]), 'sqlmonitor');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/sqlmonitor');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'sqlmonitor');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'sqlmonitor');
        }
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
