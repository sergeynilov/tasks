var this_csrf_token
var this_is_inited= false
var this_skip_onchanging= false
function sqlmonitor(page, paramsArray) {  // constructor of backend Product's editor - set all params from server
     // alert( "sqlmonitor page::"+page+"   paramsArray::"+var_dump(paramsArray) )
    this_csrf_token = paramsArray.csrf_token;
} // function sqlmonitor(Params) {  constructor of backend Product's editor - set all params from server

sqlmonitor.prototype.onSqlMonitorInit = function (page, current_tab) {  // Init Editor Editor - all vars/objects init
    $('[data-toggle="tooltip"]').tooltip()
    this_is_inited= true
    sqlmonitor.reloadHistory();
    sqlmonitor.reloadLogs();
} // backendProductsEditorFuncs.prototype.onSqlMonitorInit= function(page) {  // Init Editor Editor - all vars/objects init and load sublistings

sqlmonitor.prototype.smLineInsert = function ( key_id, sql, output_result_type, info ) {
    $("#textarea_sql_statement").val(sql)
    $("#textarea_sql_statement").focus()
    $("#output_result_type").val(output_result_type)
    sqlmonitor.onChangeTextareaSqlStatement()
    activaTab("sm_entrance")
    if ( jQuery.trim(info) != "" ) {
        this_skip_onchanging= true
        $('#cbx_save_sql').prop('checked', false);
        $('#cbx_top').prop('checked', false);
        $("#textarea_sql_info").val(info)
        $("#div_textarea_sql_info").css("display","block")
        $("#div_top").css("display","block")
    }
}

sqlmonitor.prototype.smLineDelete = function ( key_id, sql, info ) {
    confirmMsg('Do you want to delete Sql statement "' + sql + '" with info "'+info+'" ?', function() {
            var href = "/sqlmonitor/delete_sm_line";
            $.ajax({
                timeout: 10000,
                async :false,
                url: href,
                type: "DELETE",
                dataType: "json",
                data: { "sql": sql, "key_id":key_id, "info" : info, "_token": this_csrf_token }
            }).done(function(result){
                // alert( "delete_sm_line  result::"+var_dump(result) )
                if (result.error_code == 0) {
                    sqlmonitor.reloadHistory();
                    alertMsg(  result.sm_line_was_deleted_message, 'Sql monitor', 'OK', ( result.sm_line_was_deleted ? 'glyphicon glyphicon-ok' : 'glyphicon glyphicon-remove' ) )
                }
                if (result.error_code > 0) {
                    alertMsg( result.error_message, 'Uploading error!', 'OK', 'glyphicon glyphicon-remove', 'downloadable_description' )
                }
            });
        }
    );
} // sqlmonitor.prototype.smLineDelete = function ( key_id, sql, info ) {

sqlmonitor.prototype.reloadLogs = function () {
    $.ajax({
        timeout: 10000,
        async :false,
        url: "/sqlmonitor/reload_logs",
        type: "GET",
        dataType: "json"
    }).done(function(result){
        // alert( "reload_logs result::"+var_dump(result) )
        if (result.error_code == 0 && result.success_message!= "" ) {
            $("#div_reload_logs").html( result.html );
        }
        if (result.error_code > 0) {
            alertMsg( result.error_message, 'Error', 'OK', 'glyphicon glyphicon-remove' )
        }
    });
}

sqlmonitor.prototype.logDelete = function (filename) {
    confirmMsg('Do you want to delete log file "' + filename + '" ?', function () {
            $.ajax({
                timeout: 10000,
                async: false,
                url: "/sqlmonitor/delete_log",
                type: "DELETE",
                dataType: "json",
                data: { "filename": filename, "_token": this_csrf_token }
            }).done(function (result) {
                // alert( "reload_logs result::"+var_dump(result) )
                if (result.error_code == 0 && result.success_message != "") {
                    sqlmonitor.reloadLogs()
                }
                if (result.error_code > 0) {
                    alertMsg(result.error_message, 'Error', 'OK', 'glyphicon glyphicon-remove')
                }
            });
        }
    );
}
sqlmonitor.prototype.logView = function (filename) {
    $.ajax({
        timeout: 10000,
        async :false,
        url: "/sqlmonitor/view_log?filename="+filename,
        type: "GET",
        dataType: "json"
    }).done(function(result){
        // alert( "reload_logs result::"+var_dump(result) )
        if (result.error_code == 0 && result.success_message!= "" ) {
            $("#div-sqlmonitor_info_title").html(result.filename)
            $("#div-sqlmonitor_info").html(result.contents)
            $("#div_show_sqlmonitor_info_modal").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        }
        if (result.error_code > 0) {
            alertMsg( result.error_message, 'Error', 'OK', 'glyphicon glyphicon-remove' )
        }
    });
}


sqlmonitor.prototype.reloadHistory = function () {
    var order_by= $("#order_by").val()
    $.ajax({
        timeout: 10000,
        async :false,
        url: "/sqlmonitor/reload_history?order_by="+order_by,
        type: "GET",
        dataType: "json"
    }).done(function(result){
        if (result.error_code == 0 && result.success_message!= "" ) {
            $("#div_reload_history").html( result.html );
        }
        if (result.error_code > 0) {
            alertMsg( result.error_message, 'Error', 'OK', 'glyphicon glyphicon-remove' )
        }
    });
}

sqlmonitor.prototype.switchRunSqlStatementLoader = function (show_attribute_items_images_loader) {
    if (!show_attribute_items_images_loader) {
        $("#div_result_rows_explain").css( "display", "none" );
        $("#div_result_rows_count").css( "display", "none" );
        $("#div_result_rows").css( "display", "none" );
        $("#div_result_rows_block_loading_image").css( "display", "block" );
    } else {
        $("#div_result_rows_explain").css( "display", "block" );
        $("#div_result_rows_count").css( "display", "block" );
        $("#div_result_rows").css( "display", "block" );
        $("#div_result_rows_block_loading_image").css( "display", "none" );
    }
}

sqlmonitor.prototype.runSqlStatement = function () {
    var sql_statement = $("#textarea_sql_statement").val()
    var sql_info = $("#textarea_sql_info").val()
    var output_result_type = $("#output_result_type").val()
    var cbx_save_sql = $("#cbx_save_sql").is(':checked')
    var cbx_top = $("#cbx_top").is(':checked')
    var href = "/sqlmonitor/run_sql_statement"
    var dataArray=  { sql_statement : sql_statement, sql_info : sql_info, save_sql : (cbx_save_sql?'Y':'N'), top : (cbx_top?'Y':'N'), "output_result_type" : output_result_type, "_token": this_csrf_token }
    // alert( "dataArray::"+var_dump(dataArray) )
    sqlmonitor.switchRunSqlStatementLoader(false)
    $.ajax({
        timeout: 10000,
        async :false,
        url: href,
        type: "POST",
        data: dataArray,
        dataType: "json"
    }).done(function(result){
        // alert( "result::"+var_dump(result) )
        sqlmonitor.switchRunSqlStatementLoader(true)
        if (result.error_code == 0 && result.success_message!= "" ) {
            $("#div_result_rows_explain").html( result.result_explain_text );
            $("#div_result_rows_count").html( "<span style='color: blue;'>Success with "+result.result_rows_count +" row(s) : </span>" );
            $("#div_result_rows").html( result.result_rows_as_text );
            if ( result.output_result_type == 'B') { // Both Result set and Explain
                $("#div_result_rows_explain_wrapper").css("display","block")
                $("#div_result_rows_wrapper").css("display","block")
            }
            if ( result.output_result_type == 'R') { // Only Result set
                $("#div_result_rows_explain_wrapper").css("display","none")
                $("#div_result_rows_wrapper").css("display","block")
            }
            if ( result.output_result_type == 'E') { // Only Explain
                $("#div_result_rows_explain_wrapper").css("display","block")
                $("#div_result_rows_wrapper").css("display","none")
            }
            $('#cbx_save_sql').prop('checked', false);
        }
        if (result.error_code > 0) {
            // alertMsg( result.error_message, 'Error', 'OK', 'glyphicon glyphicon-remove' )
            $("#div_result_rows_wrapper").css("display","block")
            $("#div_result_rows_explain").html( "" );
            $("#div_result_rows_count").html( "<span style='color: red;'>Error:</span>" );
            $("#div_result_rows").html( result.error_message );
            $('#cbx_save_sql').prop('checked', false);
        }
    });
}

sqlmonitor.prototype.showSqlmonitorInfo = function (key_id, data_type, title) {
    var href = "/sqlmonitor/get_sm_line";
    $.ajax({
        timeout: 10000,
        async :false,
        url: href,
        type: "GET",
        dataType: "json",
        data: { "key_id":key_id, "_token": this_csrf_token }
    }).done(function(result){
        if (result.error_code == 0) {
            $("#div-sqlmonitor_info_title").html(title)
            if ( data_type == 'sql' && (typeof result.SmLine.sql != "undefined" ) ) {
                $("#div-sqlmonitor_info").html(result.SmLine.sql)
            }
            if ( data_type == 'error_message' && (typeof result.SmLine.error_message != "undefined" ) ) {
                $("#div-sqlmonitor_info").html(result.SmLine.error_message)
            }
            if ( data_type == 'info' && (typeof result.SmLine.info != "undefined" ) ) {
                $("#div-sqlmonitor_info").html(result.SmLine.info)
            }

            $("#div_show_sqlmonitor_info_modal").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        }
        if (result.error_code > 0) {
            alertMsg( result.error_message, 'Uploading error!', 'OK', 'glyphicon glyphicon-remove', 'downloadable_description' )
        }
    });


}

sqlmonitor.prototype.smCopyToClipboard = function (text) {
    $("#div_input_clipboard").css("display","block")
    $("#input_clipboard").val(text)
    document.querySelector("#input_clipboard").select();
    document.execCommand('copy');
    $("#div_input_clipboard").css("display","none")
}

sqlmonitor.prototype.onChange_cbx_save_sql = function () {
    if ( this_skip_onchanging ) {
        this_skip_onchanging= false
        return
    }
    var cbx_save_sql= $("#cbx_save_sql").is(':checked')
    if ( cbx_save_sql ) {
        $("#div_textarea_sql_info").css("display","block")
        $("#div_top").css("display","block")
    } else {
        $("#div_textarea_sql_info").css("display","none")
        $("#div_top").css("display","none")
    }
}
sqlmonitor.prototype.onChangeTextareaSqlStatement = function () {
    if ( this_skip_onchanging ) {
        this_skip_onchanging= false
        return
    }
    var sql_statement= $("#textarea_sql_statement").val()
    var href = "/sqlmonitor/format_sql"
    var dataArray=  { sql_statement : sql_statement,  "_token": this_csrf_token }

    $.ajax({
        timeout: 10000,
        async :false,
        url: href,
        type: "POST",
        data: dataArray,
        dataType: "json"
    }).done(function(result){
        // alert( "result::"+var_dump(result) )
        if (result.error_code == 0 && result.success_message!= "" ) {
            $("#textarea_sql_statement").val(result.sql_statement)
        }
        if (result.error_code > 0) {
            alertMsg( result.error_message, 'Error', 'OK', 'glyphicon glyphicon-remove' )
        }
    });
}


sqlmonitor.prototype.startTracing = function () {
    $.ajax({
        timeout: 10000,
        async :false,
        url: "/sqlmonitor/start_tracing",
        type: "GET",
        data: { "_token": this_csrf_token },
        dataType: "json"
    }).done(function(result){
        // alert( "startTracing result::"+var_dump(result) )
        if (result.error_code == 0 && result.success_message!= "" ) {
        }
        if (result.error_code > 0) {
            alertMsg( "Tracing was started !", 'Tracing', 'OK', 'glyphicon glyphicon-ok' )
        }
    });

}

sqlmonitor.prototype.stopTracing = function () {
    $.ajax({
        timeout: 10000,
        async :false,
        url: "/sqlmonitor/stop_tracing",
        type: "GET",
        data: { "_token": this_csrf_token },
        dataType: "json"
    }).done(function(result){
        // alert( "stopTracing result::"+var_dump(result) )
        if (result.error_code == 0 && result.success_message!= "" ) {
            // $("#textarea_sql_lines").val ( result.traced_sql_lines )
            $("#div_textarea_sql_lines").html ( result.traced_sql_lines )
        }
        if (result.error_code > 0) {
            alertMsg( "Tracing was stopped !", 'Tracing', 'OK', 'glyphicon glyphicon-ok' )
        }
    });

}


function var_dump(oElem, from_line, till_line) {
    if ( typeof oElem == 'undefined' ) return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number')     {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if ( typeof from_line == "number" && typeof till_line == "number" ) {
        return sStr.substr( from_line, till_line );
    }
    if ( typeof from_line == "number" ) {
        return sStr.substr( from_line );
    }
    return sStr;
}

function alertMsg(content, title, confirm_button, icon, focus_field) {
    $.alert({
        title: title,
        content: content,
        icon: ( typeof icon != 'undefined' ? icon : 'fa fa-info-circle' ),
        confirmButton: ( typeof confirm_button != 'undefined' ? confirm_button : 'OK' ),
        keyboardEnabled: true,
        columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
    });
}


function confirmMsg(question, confirm_function_code, title, icon ) {
    if ( typeof title == "undefined" || jQuery.trim(title) == "" ) {
        title= 'Confirm!'
    }
    if ( typeof icon == "undefined" || jQuery.trim(icon) == "" ) {
        icon= 'glyphicon glyphicon-signal'
    }
    $.confirm({
        icon: icon,
        title: title,
        content: question,
        confirmButton: 'YES',
        cancelButton: 'Cancel',
        confirmButtonClass: 'btn-info',
        cancelButtonClass: 'btn-danger',
        keyboardEnabled: true,
        columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
        confirm: function(){
            confirm_function_code()
        }
    });
}

function activaTab(tab){
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
};
