<?php

namespace Modules\SqlMonitor\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Controller;

class SqlMonitorController extends Controller
{
    protected $data_strip_tags = true;
    protected $clear_doubled_spaces = true;
    protected $stripslashes = true;
    protected $sm_lines_filename = 'sm_user_';
    protected $order_by= 'top';
    protected $defaultRequestsArray = [];


//    public function __construct()
//    {
//        parent::__construct();
//        $this->middleware('auth');
//        $loggedUser   = Auth::user();
//        echo '<pre>$loggedUser::'.print_r($loggedUser,true).'</pre>';
//        die("-1 XXZ  loggedUser");
//        $this->middleware('guest');
//    }

        /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tab_page = "sm_entrance";
//        $tab_page = "sm_history";
//        $tab_page = "sm_tracing";
//        $tab_page = "sm_logs";
        $system_info_str= $this->getSystemInfo();
        $appParamsForJSArray= 'csrf_token : \'' . csrf_token().'\'';
        $order_by= 'top';
        $auto_start= ini_get("session.auto_start");

        $loggedUser   = Auth::user();
//        echo '<pre>$loggedUser::'.print_r($loggedUser,true).'</pre>';
        $this->middleware('guest');
//        die("-1 XXZ  loggedUser");

        return view('sqlmonitor::index',compact('tab_page', 'order_by', 'appParamsForJSArray', 'auto_start', 'system_info_str') );
    }


    public static function getSystemInfo(bool $add_system_info= true) : string
    {
        $system_info= '';
        if ( $add_system_info ) {
            $system_info= '<br>' .
                          'PHP : <b> ' . phpversion() . '</b><br>'.
                          'Apache version:<b> ' . apache_get_version() . '</b><br>'
            ;
        }

        $string = ' Laravel <b>'. app()::VERSION . '</b><br>'.
                  'DEBUG:<b>' . env('APP_DEBUG') . '</b><br>'.
                  'ENV:<b> ' . env('APP_ENV') . '</b><br>'.
                  'DB_CONNECTION:<b> ' . env('DB_CONNECTION') .'</b><br>'.
                  'Prefix:<b>' . \DB::getTablePrefix() .'</b>' . $system_info;
        $db_version= '';
        if ( env('DB_CONNECTION') == 'pgsql' ) {
            $v= DB::select(' SELECT version(); ');
            if ( !empty($v[0]->version) ) {
                $db_version = 'Db Version:<b>' . $v[0]->version . '</b>';
            }
        }
        return $string.$db_version;
    }


    public function get_sm_line() // get sql line by key_id
    {
        $loggedUser   = Auth::user();
        $user_id       = $loggedUser->id;
        $key_id        = $this->getParameter('key_id');
        $data_filename = $this->getDataFilename($user_id);
        $smLinesArray  = (array)json_decode($this->readFromFileName($data_filename));
        foreach( $smLinesArray as $next_key=>$nextSmLine ) {
            if( $nextSmLine->key_id == $key_id ) {
                if ( !empty($nextSmLine->sql) ) {
                    $nextSmLine->sql = $this->formatSql($nextSmLine->sql);
                }
                if ( !empty($nextSmLine->error_message) ) {
                    $nextSmLine->error_message = str_replace("ERROR:", '<span style="font-weight: bold;color:red;">ERROR:</span>',$nextSmLine->error_message);
                }
                return \Response::json( [ 'error_message' => '', 'error_code' => 0, 'user_id' => $user_id, 'key_id'=> $key_id, 'SmLine'=> $nextSmLine ] );
            }
        }
        return \Response::json( [ 'error_message' => 'Sm Line not found!', 'error_code' => 1, 'user_id' => $user_id, 'key_id'=> $key_id ] );
    } //public function get_sm_line() // get sql line by key_id


    public function delete_sm_line()
    {
        $loggedUser = Auth::user();
        $user_id     = $loggedUser->id;
        $key_id = $this->getParameter( 'key_id' );
        $data_filename  = $this->getDataFilename($user_id);
        $smLinesArray = (array)json_decode($this->readFromFileName($data_filename));

        $smLinesToWriteArray= [];
        foreach( $smLinesArray as $next_key=>$nextSmLine ) {
//            echo '<pre>$nextSmLine->key_id::'.print_r($nextSmLine->key_id,true).'</pre>';
            if( $nextSmLine->key_id != $key_id ) {
                $smLinesToWriteArray[]= $nextSmLine;
            }
        }
        $ret= $this->writeToFileName( json_encode($smLinesToWriteArray), $data_filename, true );
        $sm_line_was_deleted= count($smLinesToWriteArray) == ( count($smLinesArray) - 1) ;
        $sm_line_was_deleted_message= $sm_line_was_deleted ? "Sql-statement was deleted succesfully !" : "Sql-statement was not deleted !";
        return \Response::json( [ 'error_message' => '', 'error_code' => 0, 'user_id' => $user_id, 'key_id'=> $key_id, 'sm_line_was_deleted'=> $sm_line_was_deleted, 'sm_line_was_deleted_message'=> $sm_line_was_deleted_message ] );
    } // public function delete_sm_line()


    public function run_sql_statement()
    {
        $sql_statement = $this->workTextString( $this->getParameter( 'sql_statement', '' ) );
        $output_result_type = $this->getParameter( 'output_result_type', '' ); // 'B'=>'Both Result set and Explain', 'R'=>'Only Result set', 'E'=>'Only Explain'
        $result_explain_text= '';
        $resultRows= [];
        $save_sql= $this->getParameter( 'save_sql', 'N' );
        $sql_info= $this->getParameter( 'sql_info', '' );

        $top = $this->getParameter( 'top', 'N' );
        $loggedUser = Auth::user();
        $user_id= $loggedUser->id;
        $key_id =  urlencode($this->generateKey(6).'_'.time().'_'.$user_id);
        try {
            if ( $output_result_type == 'B' or $output_result_type== 'R' ) { // 'B'=>'Both Result set and Explain', 'R'=>'Only Result set'
                $resultRows = DB::select($sql_statement);
                foreach ($resultRows as $next_key => $nextRow) {
                    $resultRows[$next_key] = (array)$nextRow;
                }
            } //
            if ( $save_sql == 'Y' ) { // save sql-statement
                $retArray = $this->addSmLine(
                    $key_id,              // string $key_id
                    $sql_statement,       // string $sql
                    $sql_info,            // string $info= ''
                    $top,                 // bool $top= false
                    'Y',            // bool $valid= false
                    $output_result_type
                );
            } // if ( $save ) { // save sql-statement

            if ( $output_result_type == 'B' or $output_result_type== 'E' ) { // 'B'=>'Both Result set and Explain', 'E'=>'Only Explain'
                $resultExplainRows = DB::select('EXPLAIN ' . $sql_statement);
                if ( ! empty($resultExplainRows) and is_array($resultExplainRows)) {
                    foreach ($resultExplainRows as $next_key => $nextResultExplainRow) {
                        $arr = (array)$nextResultExplainRow;
                        if ( ! empty($arr["QUERY PLAN"])) {
                            $result_explain_text .= ($next_key + 1) . '->' . str_repeat("&nbsp;", ($next_key + 1)) . $arr["QUERY PLAN"] . '<br>';
                        }
                    }
                }
            }

            return [ 'error_message' => '', 'error_code' => 0, 'output_result_type'=> $output_result_type, 'result_explain_text'=> $result_explain_text, 'resultRows'=> $resultRows, 'result_rows_count'=> count($resultRows), 'result_rows_as_text'=> print_r($resultRows,true), 'sql_statement' => $sql_statement ];
        } catch (\Exception $e) {
            if ( $save_sql == 'Y' ) { // save sql-statement
                $retArray = $this->addSmLine(
                    $key_id,          // string $key_id
                    $sql_statement,   // string $sql
                    $sql_info,            // string $info= ''
                    $top,             // bool $top= false
                    'N',            // bool $valid= false
                    $output_result_type,
                    $e->getMessage()
                );
            } // if ( $save ) { // save sql-statement
            return [ 'error_message' => $this->workTextString($e->getMessage()), 'error_code' => 1, 'resultRows'=> null, 'sql_statement' => $sql_statement ];
        }
    }

    private function addSmLine(string $key_id, string $sql, string $info, string $top, string $valid, string $output_result_type, string $error_message= '') : array
    {
        $loggedUser = Auth::user();
        $user_id= $loggedUser->id;
        $data_filename= $this->getDataFilename($user_id);

        $smLinesArray= json_decode($this->readFromFileName($data_filename));
        $NewSqlArray= [ 'key_id'=> $key_id, 'sql'=>$sql, 'info'=> $info, 'top'=> $top, 'valid'=> $valid,  'output_result_type'=> $output_result_type, 'created_at'=>time() ];
        if (!empty($error_message)) {
            $NewSqlArray['error_message']= $this->workTextString($error_message);
        }
        $smLinesArray[]= $NewSqlArray;
        $ret= $this->writeToFileName( json_encode($smLinesArray), $data_filename, true );
        return $smLinesArray;
    }


    public function view_log()
    {
        $filename = $this->getParameter( 'filename' );
        $logs_dir = storage_path(). '/logs/';
        $ret= '';
        if ( !file_exists($logs_dir.$filename) ) {
            return [ 'error_message' => 'File "'.$logs_dir.$filename."' not found!", 'error_code' => 1, 'ret' => $ret, 'directory'=> $logs_dir.$filename ];
        }
        if ( file_exists($logs_dir.$filename) and is_dir($logs_dir.$filename) ) {
            return [ 'error_message' => '"'.$logs_dir.$filename."' is directory!", 'error_code' => 1, 'ret' => $ret, 'directory'=> $logs_dir.$filename ];
        }

        if ( file_exists($logs_dir.$filename) and !is_dir($logs_dir.$filename) ) {
            try {
                $f = $logs_dir.$filename;
                $h = fopen($f, "r");
                $contents = $this->convert_from_latin1_to_utf8_recursively( fread($h, filesize($f)) );
                $contents= str_replace(["\r\n","\r","\n"], '<br>', $contents);
                $contents= str_replace( "ERROR:", '<span style="font-weight: bold;color:red;">ERROR:</span>', $contents);
                fclose($h);
            } catch (\Exception $e) {
                return [ 'error_message' => 'Error uploading file "'.$filename." : ".$this->workTextString($e->getMessage()), 'error_code' => 0, 'ret' => $ret, 'filename'=> $filename, 'contents'=> '' ];
            }
        }
        return [ 'error_message' => '', 'error_code' => 0, 'ret' => $ret, 'filename'=> $filename, 'contents'=> $contents ];
    } //public function view_log()

    protected static function convert_from_latin1_to_utf8_recursively($dat)
    {
        if (is_string($dat))
            return utf8_encode($dat);
        if (!is_array($dat))
            return $dat;
        $ret = array();
        foreach ($dat as $i => $d)
            $ret[$i] = self::array_utf8_encode($d);
        return $ret;
    }
    public function delete_log()
    {
        $filename = $this->getParameter( 'filename' );
        $logs_dir = storage_path(). '/logs/';
        $ret= '';
        if ( !file_exists($logs_dir.$filename) ) {
            return [ 'error_message' => 'File "'.$logs_dir.$filename."' not found!", 'error_code' => 1, 'ret' => $ret, 'directory'=> $logs_dir.$filename ];
        }
        if ( file_exists($logs_dir.$filename) and !is_dir($logs_dir.$filename) ) {
             $ret= @unlink($logs_dir.$filename);
        }
        if ( file_exists($logs_dir.$filename) and is_dir($logs_dir.$filename) ) {
            $ret= $this->deleteDirectory($logs_dir.$filename);
        }
        return [ 'error_message' => '', 'error_code' => 0, 'ret' => $ret, 'filename'=> $filename ];
    } //public function delete_log()

    public function reload_logs()
    {
        $loggedUser    = Auth::user();
        $user_id        = $loggedUser->id;
        $logs_dir = storage_path(). '/logs/';
        $logFilesArray= $this->getFilesOfDirectory($logs_dir, true);
        $commonVarsArray= compact('logFilesArray' );
        $html = view( 'sqlmonitor::logs_list', $commonVarsArray )->render();
        return \Response::json([ 'error_message' => '', 'error_code' => 0, 'html'=>$html, 'logFilesArray'=>$logFilesArray, 'user_id' => $user_id ] );
    }

    public function reload_history()
    {
        $loggedUser = Auth::user();
        $user_id= $loggedUser->id;
        $this->order_by = $this->getParameter( 'order_by', 'top' );

        $data_filename= $this->getDataFilename($user_id);
        $smLinesArray= [];
        $smLinesObjects= json_decode($this->readFromFileName($data_filename));
        if ( empty($smLinesObjects) ) $smLinesObjects= [];
        foreach( $smLinesObjects as $next_key=> $nextSmLine ) {
            $nextSmLine= (array)$nextSmLine;
            $nextSmLine['valid_label'] = ( (  !empty($nextSmLine['valid']) and $nextSmLine['valid']== 'Y' )? 'Valid' : 'Error');
            $nextSmLine['top_label'] = ( ( !empty($nextSmLine['top']) and $nextSmLine['top'] == 'Y' )? 'Top' : '');
            $nextSmLine['created_at_label'] = ( !empty($nextSmLine['created_at']) ? strftime('%Y-%m-%d %H:%M',$nextSmLine['created_at']) : '');
            $smLinesArray[]= $nextSmLine;
        }
        usort( $smLinesArray, [$this, "smLinesArraySort" ] );
        $commonVarsArray= compact('smLinesArray', 'order_by', 'order_direction' );
        $html = view( 'sqlmonitor::history_list', $commonVarsArray )->render();
        return \Response::json([ 'error_message' => '', 'error_code' => 0, 'html'=>$html, 'smLinesArray'=>$smLinesArray, 'user_id' => $user_id ] );
    }


    public function format_sql()
    {
        $original_sql_statement = $this->workTextString( $this->getParameter( 'sql_statement', '' ) );
        $sql_statement= $this->formatSql($original_sql_statement,true, false);
        return [ 'error_message' => '', 'error_code' => 0, 'sql_statement' => $sql_statement, 'original_sql_statement'=> $original_sql_statement ];
    } //public function format_sql()


    public function start_tracing( Request $request )
    {
        $session_id= session_id();
        if ( empty($session_id) ) {
            session_start();
        }
        $_SESSION['sqlmonitor_tracing_on']= 1;
        return [ 'error_message' => '', 'error_code' => 0, 'sqlmonitor_tracing_on'=> ( !empty($_SESSION['sqlmonitor_tracing_on']) ? $_SESSION['sqlmonitor_tracing_on'] : '' ) ];
    } // public function start_tracing()

    public function stop_tracing( Request $request )
    {
        $sqlmonitor_tracing_lines_array= ( !empty($_SESSION['sqlmonitor_tracing_lines_array']) ? $_SESSION['sqlmonitor_tracing_lines_array'] : [] );
        $traced_sql_lines= '';
        foreach( $sqlmonitor_tracing_lines_array as $next_sql ) {
            $traced_sql_lines= $traced_sql_lines . $this->formatSql($next_sql)."\r\n"."\r\n";
        }
        if ( !empty($_SESSION['sqlmonitor_tracing_on']) ) {
            unset($_SESSION['sqlmonitor_tracing_on']);
        }
        if ( !empty($_SESSION['sqlmonitor_tracing_lines_array']) ) {
            unset($_SESSION['sqlmonitor_tracing_lines_array']);
        }
        return [ 'error_message' => '', 'error_code' => 0,
                 'sqlmonitor_tracing_on'=> ( !empty($_SESSION['sqlmonitor_tracing_on']) ? $_SESSION['sqlmonitor_tracing_on'] : '' ),
                 'traced_sql_lines'=> $traced_sql_lines ];
    } // public function stop_tracing()


    /* Submitting form string value must be worked out according to options of app */
    protected function workTextString($str) : string
    {
        if ( empty($str) ) return '';
        $str= $this->makeClearDoubledSpaces($str);
        $str= $this->makeStripTags($str);
        $str= $this->makeStripslashes($str);
        return trim($str);
    }

    public function makeStripTags(string $str)
    {
        $str= $this->data_strip_tags ? strip_tags($str) : $str;
        return $str;
    }

    protected function makeClearDoubledSpaces(string $str) : string
    {
        if ( $this->clear_doubled_spaces ) {
            return preg_replace("/(\s{2,})/ms"," ",$str);
        }
        return $str;
    }

    protected function makeStripslashes(string $str) : string
    {
        if ( $this->stripslashes ) {
            return stripslashes($str);
        }
        return $str;
    }

    protected function getParameter( $param_name, $default_value= null, $flags_array= [] ) {
        $requestsArray= $this->getRequestParamsArray();
        if ( isset($requestsArray[$param_name]) ) {
            $ret= $requestsArray[$param_name];
            if (is_array($ret)) {
                $retArray= [];
                foreach ($ret as $item) {
                    $retArray[]= empty($flags_array['skip_urldecode']) ? urldecode($item) : $item;
                }
                return $retArray;
            }
            return empty($flags_array['skip_urldecode']) ? urldecode($ret) : $ret;
        }

        if (is_array($default_value)) {
            $retArray= [];
            foreach ($default_value as $item) {
                $retArray[]= empty($flags_array['skip_urldecode']) ? urldecode($item) : $item;
            }
            return $retArray;
        }

        $default_value= empty($flags_array['skip_urldecode']) ? urldecode($default_value) : $default_value;
        return $default_value;
    }

    protected function getRequestParamsArray() : array
    {
        $requestArray= request()->all();
        foreach( $this->defaultRequestsArray as $next_default_request_key=> $next_default_request_key_value ) {
            if ( !empty($next_default_request_key) and !empty($next_default_request_key_value) )
                $requestArray[$next_default_request_key]= $next_default_request_key_value;
        }
        return $requestArray;
    }


    private function writeToFileName(string $contents, $file_name = '', $rewrite_file = false)
    {
        try {
            $fd = fopen($file_name, ($rewrite_file ? "w+" : "a+"));
            fwrite($fd, $contents);
            fclose($fd);
            return true;
        } catch (Exception $lException) {
            return false;
        }
    }


    private function readFromFileName(string $file_name) : string
    {
        if ( !file_exists($file_name) or is_dir($file_name) ) return '';
        try {
            $fd = fopen($file_name, "rb");
            $contents = fread($fd, filesize($file_name));
            fclose($fd);
            return $contents;
        } catch (Exception $lException) {
            return false;
        }
    }


    private function debToFile($contents, string  $descr_text = '', bool $is_sql = false, string $file_name= '')
    {
        return;
        try {
            if (empty($file_name))
                $file_name = '/_wwwroot/lar/lprods/storage/logs/__SqlMonitor_deb.txt';
            $fd = fopen($file_name, "a+");
            if (  is_array($contents)== 'array' ) {
                $contents= print_r($contents,true);
            }
            if ( $is_sql ) {
                fwrite($fd, '<pre>' . $descr_text . '::' . appFuncs::showFormattedSql($contents, false) . '<pre>' . chr(13));
            } else {
                fwrite($fd, '<pre>' . $descr_text . '::' . $contents . '<pre>' . chr(13));
            }
            fclose($fd);
            return true;
        } catch (Exception $lException) {
            return false;
        }
    }

    private function getDataFilename(int $user_id) : string {
        $module_path = public_path().'/modules/sqlmonitor/data';
        $this->createDir( [$module_path] );
        $data_filename= $module_path.'/'.$this->sm_lines_filename.''.$user_id.'.txt';
        return $data_filename;
    }

    protected function createDir( array $directoriesList = [], $mode = 0755)
    {
        foreach ($directoriesList as $dir) {
            if ( !file_exists($dir) ) {
                mkdir($dir, $mode );
            }
        }
    }

    function formatSql($sql, $is_break_line = true, $is_include_html = true)
    {
        $break_line = '';
        $space_char = '  ';
        if ($is_break_line) {
            if ( $is_include_html ) {
                $break_line = '<br>';
            }  else {
                $break_line = PHP_EOL;
            }
        }
        $bold_start= '';
        $bold_end= '';
        if ( $is_include_html ) {
            $bold_start= '<B>';
            $bold_end= '</B>';
        }
        $sql = ' ' . $sql . ' ';
        $left_cond= '~\b(?<![%\'])';
        $right_cond= '(?![%\'])\b~i';
        $sql = preg_replace( $left_cond . "insert[\s]+into" . $right_cond, $space_char . $space_char .$bold_start . "INSERT INTO" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "insert" . $right_cond, $space_char . $bold_start . "INSERT" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "delete" . $right_cond, $space_char . $bold_start . "DELETE" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "values" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "VALUES" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "update" . $right_cond, $space_char . $bold_start . "UPDATE" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "inner[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "INNER JOIN" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "straight[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "STRAIGHT_JOIN" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "left[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "LEFT JOIN" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "select" . $right_cond, $space_char . $bold_start . "SELECT" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "from" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "FROM" . $bold_end, $sql );
        $sql = preg_replace( $left_cond . "where" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "WHERE" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "group by" . $right_cond, $break_line . $space_char . $space_char . "GROUP BY" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "having" . $right_cond, $break_line . $space_char . $bold_start . "HAVING" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "order[\s]+by" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "ORDER BY" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "and" . $right_cond,  $space_char . $space_char . $bold_start . "AND" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "or" . $right_cond,  $space_char . $space_char . $bold_start . "OR" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "as" . $right_cond,  $space_char . $space_char . $bold_start . "AS" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "exists" . $right_cond,  $break_line . $space_char . $space_char . $bold_start . "EXISTS" . $bold_end, $sql);
        return $sql;
    } // function formatSql($sql, $is_break_line = true, $is_include_html = true)

    public function generateKey($length= 8)	{
        $alphabet = "0123456789abcdefghijklmnopqrstuvwxyz";
        $Res = '';
        for ($I = 0; $I < $length; $I++) {
            $Index = random_int(0, strlen($alphabet) - 1);
            $Res .= substr($alphabet, $Index, 1);
        }
        return $Res;
    }

    function smLinesArraySort($a,$b) {
        if ( $this->order_by == 'top' ) {
            if ( !isset($a['top']) or !isset($b['top']) ) return 1;
            if ( $a['top'] < $b['top'] ) return 1;
            if ( $a['top'] > $b['top'] ) return -1;
            if ( $a['top'] == $b['top'] ) {
                if ( !isset($a['valid']) or !isset($b['valid']) ) return -1;
                if ( $a['valid'] < $b['valid'] ) return 1;
                if ( $a['valid'] > $b['valid'] ) return -1;
                if ( $a['valid'] == $b['valid'] ) return 0;
            }
        }

        if ( $this->order_by == 'valid' ) {
            if ( !isset($a['valid']) or !isset($b['valid']) ) return 1;
            if ( $a['valid'] < $b['valid'] ) return 1;
            if ( $a['valid'] > $b['valid'] ) return -1;
            if ( $a['valid'] == $b['valid'] ) {
                if ( !isset($a['top']) or !isset($b['top']) ) return -1;
                if ( $a['top'] < $b['top'] ) return 1;
                if ( $a['top'] > $b['top'] ) return -1;
                if ( $a['top'] == $b['top'] ) return 0;
            }
        }

        if ( $this->order_by == 'created_at' ) {
            if ( !isset($a['created_at']) or !isset($b['created_at']) ) return 1;
            if ( $a['created_at'] < $b['created_at'] ) return 1;
            if ( $a['created_at'] > $b['created_at'] ) return -1;
            if ( $a['created_at'] == $b['created_at'] ) 0;
        }

//        if ( $this->order_by == 'created_at' ) {
//            if ( !isset($a['created_at']) or !isset($b['created_at']) ) return 1;
//            if ( $a['created_at'] < $b['created_at'] ) return 1;
//            if ( $a['created_at'] > $b['created_at'] ) return -1;
//            if ( $a['created_at'] == $b['created_at'] ) 0;
//        }

        if ( $this->order_by == 'info' ) {
            if ( !isset($a['info']) or !isset($b['info']) ) return -1;
            if ( $a['info'] < $b['info'] ) return -1;
            if ( $a['info'] > $b['info'] ) return 1;
            if ( $a['info'] == $b['info'] ) 0;
        }

    }

    protected function getFilesOfDirectory( string $directory_name, $show_extended_info= false ) :array
    {
        if (!file_exists($directory_name) or !is_dir($directory_name)) return [];
        $retArray= [];
        $H = OpenDir($directory_name);
        while ($next_file = readdir($H)) { // All files in dir
            if ($next_file == "." or $next_file == "..")
                continue;
            $fileInfo= [ 'filename'=> $next_file, 'file_path'=> $directory_name . $next_file ];
            if ( $show_extended_info ) {
                $file_size= @filesize($directory_name . $next_file);
                $file_size_label= $this->getFileSizeAsString($file_size);
                $fileArray = @getimagesize($directory_name . $next_file);
                $fileperms = substr(sprintf('%o', @fileperms($directory_name . $next_file) ), -4);
                $fileInfo['file_size']= $file_size;
                $fileInfo['file_size_label']= $file_size_label;
                $fileInfo['file_width']= ( !empty($fileArray[0]) ? ( $fileArray[0] ) : '' );
                $fileInfo['file_height']= ( !empty($fileArray[1]) ? ( $fileArray[1] ) : '' );
                $fileInfo['directory']= is_dir($directory_name . $next_file) ? "Directory" : "";
                $fileInfo['fileperms']= $fileperms;
            }
            $retArray[]= $fileInfo;
        }
        closedir($H);
        return $retArray;
    }

    protected function deleteDirectory( string $directory_name )
    {
        if (!file_exists($directory_name) or !is_dir($directory_name)) return true;
        $H = OpenDir($directory_name);
        while ($nextFile = readdir($H)) { // All files in dir
            if ($nextFile == "." or $nextFile == "..")
                continue;
            //appUtils::deb($directory_name . DIRECTORY_SEPARATOR . $nextFile, '$directory_name . DIRECTORY_SEPARATOR . $nextFile::');
            unlink($directory_name . DIRECTORY_SEPARATOR . $nextFile);
        }
        closedir($H);
        return rmdir($directory_name);
    }

    protected function getFileSizeAsString( string $file_size ) : string
    {
        if ((int)$file_size < 1024)
            return $file_size . ' b';
        if ((int)$file_size < 1024 * 1024)
            return floor($file_size / 1024) . ' kb';
        return floor($file_size / (1024 * 1024)) . ' mb';
    }

}

