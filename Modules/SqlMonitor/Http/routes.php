<?php
//SqlMonitorController

///_wwwroot/lar/lprods/Modules/SqlMonitor/Http/Middleware/SqlMonitorAccess.php
/// namespace Modules\SqlMonitor\Http\Controllers;

use Modules\SqlMonitor\Http\Middleware\SqlMonitorAccess;
//use App\Http\Middleware\SqlMonitorAccess;
Route::group(['middleware' => 'web', 'prefix' => 'sqlmonitor', 'namespace' => 'Modules\SqlMonitor\Http\Controllers'], function()
{
//    $this->commonVarsArray['dtparam']= time();


//
//    Route::get('admin/profile', function () {
//        //
//    })->middleware(CheckAge::class);
//    Route::get('/', 'SqlMonitorController@index');
//
    Route::get('/', 'SqlMonitorController@index')->middleware(SqlMonitorAccess::class);;
    Route::get( '/start_tracing', 'SqlMonitorController@start_tracing');
    Route::get( '/stop_tracing', 'SqlMonitorController@stop_tracing');
    Route::get( '/reload_history', 'SqlMonitorController@reload_history');
    Route::get( '/reload_logs', 'SqlMonitorController@reload_logs');
    Route::delete( '/delete_log', 'SqlMonitorController@delete_log');
    Route::get( '/view_log', 'SqlMonitorController@view_log');
    Route::delete( '/delete_sm_line', 'SqlMonitorController@delete_sm_line');
    Route::get( '/get_sm_line', 'SqlMonitorController@get_sm_line');
    Route::post( '/format_sql', 'SqlMonitorController@format_sql');
    Route::post(  '/run_sql_statement', 'SqlMonitorController@run_sql_statement');
    // http://local-lprods.com/sqlmonitor/format_sql
    Route::post('/sqlmonitor/settings/index', 'SqlMonitorController@index');
//    Route::post('/sqlmonitor/format_sql', 'SqlMonitorController@format_sql');
//    Route::post('/sqlmonitor/settings/index', 'SqlMonitorController@index');
});

//SqlMonitorController
