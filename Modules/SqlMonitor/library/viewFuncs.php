<?php namespace Modules\SqlMonitor\library {

    use Auth;
    use Carbon\Carbon;
    use App\User;
    use App\CmsItem;
    use App\Product;
    use App\ProductComment;
    use App\ProductAffiliatedUser;
    use App\Order;
    use App\Category;
    use App\Attribute;
    use App\Settings;
    use App\RgRegexpItem;
    use App\Http\Traits\funcsTrait;
    use App\library\ButtonType;

    class viewFuncs // https://laravel.com/docs/5.4/blade
    {
        use funcsTrait;

        public static function concatStr(string $str, int $max_length = 0, string $add_str = ' ...', $show_help = false, $strip_tags = true, $additive_code= '') : string
        {
            if ($strip_tags) $str = strip_tags($str);
            $ret_html = self::limitChars($str, ( !empty($max_length) ? $max_length : 100 ), $add_str);
            if ($show_help and strlen($str) > $max_length) {
//			$str = appFuncs::nl2br2($str);
//            $str = addslashes(appFuncs::nl2br2($str));
//                echo '<pre>$additive_code::'.print_r(htmlspecialchars($additive_code),true).'</pre>';
//                die("-1 XXZ");
                $ret_html .= '<i class=" a_link glyphicon glyphicon-barcode" style="font-size:larger;" '.$additive_code.' ></i>';
//            $ret_html .= '<i class=" a_link fa fa-life-bouy" style="font-size:larger;" onclick="javascript:showDownloadableDescription(9);"></i>';
//            $ret_html .= '&nbsp;<button type="button" class="btn"  style="padding:1px; border:1px dotted grey;" data-toggle="tooltip" data-placement="bottom" title="'.$str.'">+</button>';
//            echo '<pre>=========$ret_html::'.print_r($ret_html,true).'</pre>';
            }
//        echo '<pre>$ret_html::'.print_r(htmlspecialchars($ret_html),true).'</pre>';
//        die("-1 XXZ");
            return $ret_html;
        }

        /**
         * Limits a phrase to a given number of characters.
         *
         * @param   string   phrase to limit characters of
         * @param   integer  number of characters to limit to
         * @param   string   end character or entity
         * @param   boolean  enable or disable the preservation of words while limiting
         * @return  string
         */
        public static function limitChars($str, $limit = 100, $end_char = null, $preserve_words = false) : string
        {
            $end_char = ($end_char === null) ? '&#8230;' : $end_char;

            $limit = (int)$limit;

            if (trim($str) === '' OR strlen($str) <= $limit)
                return $str;

            if ($limit <= 0)
                return $end_char;

            if ($preserve_words == false) {
                return rtrim(substr($str, 0, $limit)) . $end_char;
            }
            // TO FIX AND DELETE SPACE BELOW
            preg_match('/^.{' . ($limit - 1) . '}\S* /us', $str, $matches);

            return rtrim($matches[0]) . (strlen($matches[0]) == strlen($str) ? '' : $end_char);
        }



//        public static function trim_characters($text, $length = 60, $append = '&hellip;')
//        {
//
//            $length = (int)$length;
//            $text   = trim(strip_tags($text));
//
//            if (strlen($text) > $length) {
//                $text  = substr($text, 0, $length + 1);
//                $words = preg_split("/[\s]|&nbsp;/", $text, -1, PREG_SPLIT_NO_EMPTY);
//                preg_match("/[\s]|&nbsp;/", $text, $lastchar, 0, $length);
//                if (empty($lastchar)) {
//                    array_pop($words);
//                }
//
//                $text = implode(' ', $words) . $append;
//            }
//
//            return $text;
//        }

    }
}