<h4 class="box-title">Found ( {{ count($logFilesArray) }} file(s) ) </h4>
@inject('viewFuncs', 'Modules\SqlMonitor\library\viewFuncs')

<section class="">
    @if ( count( $logFilesArray ) == 0 )
    <div class="alert alert-info">No Files Found</div>
    @else
    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-striped ">

            <thead>
            <tr>
                <th>File</th>
                <th></th>
                <th>Size</th>
                <th></th>
                <th>View</th>
                <th>Perms</th>
                <th>Delete</th>
            </tr>
            </thead>

            <tbody>
            @foreach( $logFilesArray as $nextLogFile )
            @if ( !empty($nextLogFile) )
            <tr>

                <td>
                    {{ $nextLogFile['filename'] }}
                </td>

                <td>
                    {{ $nextLogFile['directory'] }}
                </td>

                <td>
                    @if(  $nextLogFile['directory'] != "Directory" )
                    {{ $nextLogFile['file_size_label'] }}
                    @endif
                </td>

                <td>
                    @if(  !empty($nextLogFile['file_width']) and !empty($nextLogFile['file_height'])   )
                    {{ $nextLogFile['file_width'] }}x{{ $nextLogFile['file_height'] }}
                    @endif
                </td>

                <td class="icon_cell">
                    @if(  $nextLogFile['directory'] != "Directory" )
                        <a href="javascript:sqlmonitor.logView( {{ json_encode($nextLogFile['filename']) }} ); "><small>View</small></a>
                    @endif
                </td>

                <td>
                    {{ $nextLogFile['fileperms'] }}
                </td>

                <td class="icon_cell">
                    <a href="javascript:sqlmonitor.logDelete( {{ json_encode($nextLogFile['filename']) }} ); "><small>Delete</small></a>
                </td>

            </tr>
            @endif
            @endforeach
            </tbody>

        </table>

    </div>
    @endif
</section>