<div class="modal fade" id="div_show_sqlmonitor_info_modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog-full-width">
        <div class="modal-content-full-width">
            <section class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <div class="modal-title" id="div-sqlmonitor_info_title"></div>
            </section>

            <section class="modal-body-full-width">
                <form role="form" class="form-horizontal" >

                    <section class="modal-body ">
                        <form role="form" class="form-horizontal" >
                            <div id="div-sqlmonitor_info" style="padding-bottom: 50px;"></div>
                        </form>
                    </section>

                </form>
            </section>

            <section class="modal-footer">
                <div class="btn-group  pull-right editor_btn_group " role="group" aria-label="group button" style="padding: 0; margin: 0">
                    <button type="button" class=" btn btn-inverse " data-dismiss="modal" role="button"><span class="btn-label"><i class="fa fa-arrows-alt"></i></span>&nbsp;Cancel</button>
                </div>
            </section>
        </div> <!-- <div class="modal-content"> -->
    </div>
</div> <!-- div_show_sqlmonitor_info_modal -->