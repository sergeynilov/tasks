@extends('sqlmonitor::layouts.master')

@section('content')
    <script type="text/javascript" language="JavaScript">
        /*<![CDATA[*/

        var sqlmonitor = new sqlmonitor('edit', { // must be called before jQuery(document).ready(function ($) {
            <?php echo $appParamsForJSArray ?>
        });

        jQuery(document).ready(function ($) {
            sqlmonitor.onSqlMonitorInit('edit', 1)
        });

        /*]]>*/
    </script>

    <?php
    //    $sql= "select o.* , u.username as username, u.active_status as  user_active_status from pd_order as o inner join pd_ion_users as u  on u.id = o.user_id where trade_manager_user_id = 127 order by o.order_number asc limit 20 offset 0";
        $sql= "select  oi.* , p.title as product_title, p.slug as product_slug       FROM pd_order_item as oi inner join pd_product as p  on p.id = oi.product_id      WHERE oi.order_id = '35'      ORDER BY qty desc  ";
//    $sql = "";
    ?>



    <!-- Page Content -->
    <div id="page-wrapper">
        <section class="container-fluid">

            <h1>Sql monitor</h1>

            <!-- /row -->
            <div class="row p-10">
                <div class="col-sm-12">

                    <hr>
                    <section class="m-t-40">

                        <ul class="nav nav-tabs">
                            <li class="@if( $tab_page == "sm_entrance") active @endif" >
                                <a href="#sm_entrance" data-toggle="tab">Sql-Entrance</a>
                            </li>
                            <li class="@if( $tab_page == "sm_history") active @endif" >
                                <a href="#sm_history" data-toggle="tab">History</a>
                            </li>
                            <li class="@if( $tab_page == "sm_tracing") active @endif">
                                <a href="#sm_tracing" data-toggle="tab">Tracing</a>
                            </li>
                            <li class="@if( $tab_page == "sm_logs") active @endif">
                                <a href="#sm_logs" data-toggle="tab">Log files</a>
                            </li>
                        </ul>
                        
                        <div class="tab-content" id="tabs">
                            <div class="tab-pane @if( $tab_page == "sm_entrance") active @endif" id="sm_entrance">
                                <div class="form-group row" >
                                    <div class="col-xs-12 col-sm-3">
                                        <button class="btn btn-primary btn-rounded waves-effect waves-light btn-sm" onclick="javascript:sqlmonitor.runSqlStatement();" type="button" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="Run sql-statement in textarea. "><span class="btn-label"><i class="fa fa-filter"></i></span>Run
                                        </button>
                                        <input class="editable_field" value="1" id="cbx_save_sql" name="cbx_save_sql" type="checkbox"                     onchange="javascript:sqlmonitor.onChange_cbx_save_sql()">&nbsp;&nbsp;&nbsp;&nbsp;Save sql
                                    </div>

                                    <div class="col-xs-12 col-sm-9" style="padding: 5px;">
                                        <div class="col-xs-12">
                                            {{ Form::select('output_result_type', ['B'=>'Show Both Result set and Explain', 'R'=>'Show Only Result set', 'E'=>'ShowOnly Explain'], 'R', [  "id"=>"output_result_type", "class"=>"form-control editable_field select_input " ] ) }}
                                        </div>
                                    </div>

                                    <div class="col-sm-12" style="padding: 5px; display:none" id="div_textarea_sql_info">
                                        <label for="textarea_sql_info" class="col-xs-12 col-sm-3 control-label">Info
                                        </label>
                                        <div class="col-xs-12 col-sm-9">
                                            {{ Form::textarea('textarea_sql_info', '', [   "class"=>"form-control editable_field textarea_input ",  "rows"=>"4", "placeholder"=>"Enter info for the entered sql", "id"=>"textarea_sql_info"  ] ) }}
                                            <small class="form-text text-muted">Optional.</small>
                                        </div>
                                    </div>

                                    <div class="col-sm-12" style="padding: 5px; display:none" id="div_top">
                                        <label for="textarea_sql_info" class="col-xs-12 col-sm-3 control-label">Top
                                        </label>
                                        <div class="col-xs-12 col-sm-9">
                                            <input class="editable_field" value="1" id="cbx_top" name="cbx_top" type="checkbox">&nbsp;
                                        </div>
                                    </div>

                                    <div class="col-sm-12" style="padding: 5px;">
                                        <div class="col-xs-12 col-sm-3">
                                            <label for="textarea_sql_statement" class="control-label">Sql statement<span class="required"> * </span></label><br>
                                            <button class="btn btn-primary btn-rounded waves-effect waves-light btn-sm"
                                                    onclick="javascript:sqlmonitor.onChangeTextareaSqlStatement();" type="button" data-toggle="tooltip"
                                                    data-html="true"
                                                    data-placement="top" title="" data-original-title="Redraw sql-statement in human-readable way "><span class="btn-label"><i
                                                            class="fa fa-filter"></i></span>&nbsp;Redraw
                                            </button>
                                        </div>

                                        <div class="col-xs-12 col-sm-9">
                                            {{ Form::textarea('textarea_sql_statement', $sql, [   "class"=>"form-control editable_field textarea_input ",  "rows"=>"8", "placeholder"=>"Enter content text", "id"=>"textarea_sql_statement"  ] ) }}
                                        </div>
                                    </div>


                                    <div class="col-sm-12" id="div_result_rows_explain_wrapper" style="display: none; padding: 5px">
                                        <pre><div id="div_result_rows_explain" style="background-color: white; font-weight: 900; "></div></pre>
                                    </div>

                                    <div class="col-sm-12" id="div_result_rows_wrapper" style="display: none; padding: 5px">
                                        <div id="div_result_rows_count" style="background-color: white; font-weight: bold;"></div>
                                        <pre><div id="div_result_rows" style="background-color: white; white-space:pre-wrap; font-weight: 900;"></div></pre>
                                    </div>

                                    <div class="p-20 align image_outer_loading" id="div_result_rows_block_loading_image" style="display:none;">
                                        <div class="image_inner_loading">
                                            <img src="{{ Module::asset('sqlmonitor:images/big_loading.gif') }}" style="padding: 20px;">
                                        </div>
                                    </div>

                                </div>

                                <div>
                                    <a style="cursor: pointer;" onclick="javascript: $('#div_system' ).toggle( 'slow', function() { });" >
                                        System
                                    </a>
                                </div>

                                <div id="div_system" style="display: none">
                                    <small class="form-text text-muted">{!! $system_info_str !!}</small>
                                </div>

                            </div>
                            <div class="tab-pane @if( $tab_page == "sm_history") active @endif" id="sm_history">
                                <div class="form-group row" style="padding: 5px;">

                                    <div class="col-xs-9">
                                        {{ Form::select('order_by', ['top'=>'By Top/Valid', 'valid'=>'By Valid/Top', 'created_at'=>'By Created', 'info'=>'By info'], $order_by, [  "id"=>"order_by", "class"=>"form-control editable_field select_input " ] ) }}
                                    </div>
                                    <div class="col-xs-3">
                                        <button class="btn btn-primary btn-rounded waves-effect waves-light btn-sm" onclick="javascript:sqlmonitor.reloadHistory();" type="button"
                                            title=""><span class="btn-label"><i class="fa fa-filter"></i></span>Reload
                                        </button>
                                    </div>

                                    <div class="col-sm-12" id="div_reload_history_wrapper" style="display: block; padding: 5px; margin-top:20px; ">
                                        <div id="div_reload_history" style="background-color: white;"></div>
                                    </div>
                                </div>
                                <div id="div_input_clipboard" style="display:none">
                                    <input id="input_clipboard" value="" type="text" size="1" >
                                </div>
                            </div>


                            <div class="tab-pane @if( $tab_page == "sm_tracing") active @endif" id="sm_tracing">

                                <h4>Session Support must be enabled and auto_start set to On</h4>
                                @if(empty($auto_start))
                                    <h5>As <b>session.auto_start</b> is Off Tracing would not work</h5>
                                @endif

                                <div class="col-sm-6" style="padding: 10px;">
                                    <button class="btn btn-primary btn-rounded waves-effect waves-light btn-sm" onclick="javascript:sqlmonitor.startTracing();" type="button" title=""><span class="btn-label"><i class="fa fa-filter"></i></span>Start Tracing
                                    </button>
                                </div>

                                <div class="col-sm-6"  style="padding: 10px;">
                                    <button class="btn btn-primary btn-rounded waves-effect waves-light btn-sm" onclick="javascript:sqlmonitor.stopTracing();" type="button" title=""><span class="btn-label"><i class="fa fa-filter"></i></span>Stop Tracing
                                    </button>
                                </div>

                                <div class="col-sm-12">
                                    <div id="div_textarea_sql_lines"></div>
                                    <small class="form-text text-muted">Sql lines were traced.</small>

                                </div>

                            </div>


                            <div class="tab-pane @if( $tab_page == "sm_logs") active @endif" id="sm_logs">
                                <div class="col-sm-12" style="padding: 5px;">
                                    <button class="btn btn-primary btn-rounded waves-effect waves-light btn-sm" onclick="javascript:sqlmonitor.reloadLogs();" type="button"  title=""><span class="btn-label"><i class="fa fa-filter"></i></span>Reload
                                    </button>
                                </div>

                                <div class="col-sm-12" id="div_reload_logs_wrapper" style="display: block; padding: 5px">
                                    <div id="div_reload_logs" style="background-color: white;"></div>
                                </div>

                            </div>
                        </div>

                        <hr>

                    </section>

                </div>
            </div>
        </section>
    </div>

    @include('sqlmonitor::sqlmonitor_popup_dialogs')

@stop