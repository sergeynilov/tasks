<!DOCTYPE html>
<html lang="en">
<?php
//$path = module_path('SqlMonitor');
$dtparam= time();
//echo '<pre>$dtparam::'.print_r($dtparam,true).'</pre>';
//echo '<pre>$path::'.print_r($path,true).'</pre>';
//
//$assets_path= Module::getAssetsPath();
//echo '<pre>$assets_path::'.print_r($assets_path,true).'</pre>';
?>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module SqlMonitor</title>

        <link rel="stylesheet" href="{{ Module::asset('sqlmonitor:css/bootstrap-3/bootstrap.min.css') }}" />

        <link rel="stylesheet" href="{{ Module::asset('sqlmonitor:css/app.css') }}" />

        <link rel="stylesheet" href="{{ Module::asset('sqlmonitor:css/jquery-confirm.min.css') }}" />

        {{--<script src="{{ Module::asset('sqlmonitor:js/sqlmonitor.js?dt='.$dtparam) }}"></script>--}}

        <script src="{{ Module::asset('sqlmonitor:js/jquery.min.js') }}"></script>
        <script src="{{ Module::asset('sqlmonitor:js/bootstrap-3/bootstrap.min.js') }}"></script>


        <script src="{{ Module::asset('sqlmonitor:js/sqlmonitor.js?dt='.$dtparam) }}"></script>
        <script src="{{ Module::asset('sqlmonitor:js/jquery-confirm.min.js') }}"></script>

    </head>
    <body>
        @yield('content')
    </body>

    <footer>
        <div>
            {{--<a href="https://nwidart.com/laravel-modules/v1/advanced-tools/facade-methods" target="_blank">facade-methods</a>--}}
        </div>
    </footer>
</html>
