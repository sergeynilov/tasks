<h4 class="box-title">Sql statements : {{ count($smLinesArray) }} item(s) </h4>
@inject('viewFuncs', 'Modules\SqlMonitor\library\viewFuncs')

<section class="">
    @if ( count( $smLinesArray ) == 0 )
        <div class="alert alert-info">No Data Found</div>
    @else
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-striped ">

                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Sql</th>
                    <th>Info</th>
                    <th>Top</th>
                    <th>Valid</th>
                    <th>Error message</th>
                    <th>Created at</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>

                <tbody>
                @foreach( $smLinesArray as $nexSmLine )
                    @if ( !empty($nexSmLine) )
                        <tr>
                            <td class="icon_cell">
                                <a href='javascript:sqlmonitor.smLineInsert( {{ json_encode($nexSmLine['key_id']) }}, {{ json_encode($nexSmLine['sql']) }}, {{ json_encode($nexSmLine['output_result_type']) }}, {{ json_encode($nexSmLine['info']) }} ); '>Insert</a>
                            </td>

                            <td>
                                @if(!empty($nexSmLine['sql']))
                                {!! $viewFuncs::concatStr( $nexSmLine['sql'], 30, '...', true, false, ' onclick="javascript:sqlmonitor.showSqlmonitorInfo( \'' . $nexSmLine['key_id'] .'\',  \'sql\', \'Sql statement\' );" ' ) !!}&nbsp;
                                <a href="javascript:sqlmonitor.smCopyToClipboard( {{ json_encode($nexSmLine['sql']) }} ); "><small>Copy</small></a>
                                @endif
                            </td>

                            <td>
                                @if(!empty($nexSmLine['info']))
                                {!! $viewFuncs::concatStr( $nexSmLine['info'], 50, '...', true, false, ' onclick="javascript:sqlmonitor.showSqlmonitorInfo( \'' . $nexSmLine['key_id'] .'\',  \'info\', \'Info\' );" ' ) !!}&nbsp;
                                <a href="javascript:sqlmonitor.smCopyToClipboard( {{ json_encode($nexSmLine['info']) }} ); "><small>Copy</small></a>
                                @endif
                            </td>
                            
                            <td>
                                {{ $nexSmLine['top_label'] }}
                            </td>

                            <td>
                                {{ $nexSmLine['valid_label'] }}
                            </td>

                            <td>
                                @if(!empty($nexSmLine['error_message']))
                                {!! $viewFuncs::concatStr( $nexSmLine['error_message'], 50, '...', true, false, ' onclick="javascript:sqlmonitor.showSqlmonitorInfo( \'' . $nexSmLine['key_id'] .'\',  \'error_message\', \'Error message\' );" ' ) !!}&nbsp;
                                <a href="javascript:sqlmonitor.smCopyToClipboard( {{ json_encode($nexSmLine['error_message']) }} ); "><small>Copy</small></a>
                                @endif
                            </td>

                            <td nowrap>
                                {{ $nexSmLine['created_at_label'] }}
                            </td>

                            <td class="icon_cell">
                                <a href="javascript:sqlmonitor.smLineDelete( {{ json_encode($nexSmLine['key_id']) }}, {{ json_encode($nexSmLine['sql']) }}, {{ json_encode($nexSmLine['info']) }} ); ">Delete</a>
                            </td>

                        </tr>
                    @endif
                @endforeach
                </tbody>

            </table>

        </div>
    @endif
</section>
