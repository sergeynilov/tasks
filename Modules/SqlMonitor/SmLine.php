<?php

namespace SqlMonitor;
//namespace Modules\SqlMonitor\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Validator;
use DB;
use Auth;
use Carbon\Carbon;
//use App\Http\Traits\funcsTrait;
//use App\MyAppModel;
//use App\Settings;
//use App\User;
//use App\library\ListingReturnData;
//use App\library\PgDataType;


/* Content Manage System rows, used for email templates', pages of content and blog articles */
class SmLine extends Model
{
    /* 
CREATE TABLE public.pd_sm_line (
	id int4 NOT NULL DEFAULT nextval('pd_sm_line_id_seq'::regclass),
	sql text NOT NULL,
	info varchar(255) NULL,
	top bool NULL DEFAULT false,
	valid bool NULL DEFAULT false,
	key_id varchar(50) NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT ind_pd_sm_line_unique UNIQUE (info),
	CONSTRAINT pd_sm_line_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;
CREATE INDEX ind_sm_line_key_id_top ON public.pd_sm_line (key_id,top) ;
CREATE INDEX ind_sm_line_key_id_valid ON public.pd_sm_line (key_id,valid) ;
CREATE INDEX ind_sm_line_created_at ON public.pd_sm_line (created_at) ;

ALTER SEQUENCE pd_sm_line_id_seq OWNED BY pd_sm_line.id; */
    protected $table = 'sm_line';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $sm_lineImagePropsArray = [];
    private static $sm_lineTopValueArray = Array('N' => 'Not Top', 'Y' => 'Is Top');
    private static $sm_lineValidValueArray = Array('N' => 'Not Valid', 'Y' => 'Is Valid');

//    use funcsTrait;

    private static $fieldsMappingArray = [ // which fields must be converted from postgresql format(usually bool) type when updating/retrieving data
        [ 'field'=>"top", "field_type"=> "bool", "values" => [ ['client_value'=>'Y', 'server_value'=>'TRUE', 'has_single_quote'=>false ],
            [ 'client_value'=>'N', 'server_value'=>'FALSE', 'has_single_quote'=>false ] ] ],
        [ 'field'=>"valid", "field_type"=> "bool", "values" => [ ['client_value'=>'Y', 'server_value'=>'TRUE', 'has_single_quote'=>false ],
            [ 'client_value'=>'N', 'server_value'=>'FALSE', 'has_single_quote'=>false ] ] ],
    ];

    /* return array of key value/label pairs based on self::$sm_lineTopValueArray - db enum key values/labels implementation */
    public static function getSmLineTopValueArray(bool $key_value= true) :array
    {
        $resArray = [];
        foreach (self::$sm_lineTopValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'value' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$sm_lineTopValueArray - db enum key values/labels implementation */
    public static function getSmLineTopLabel(string $page_type) : string
    {
        if (!empty(self::$sm_lineTopValueArray[$page_type])) {
            return self::$sm_lineTopValueArray[$page_type];
        }
        return '';
    }

    /* return array of key value/label pairs based on self::$sm_lineTopValueArray - db enum key values/labels implementation */
    public static function getSmLineValidValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$sm_lineValidValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'value' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$sm_lineTopValueArray - db enum key values/labels implementation */
    public static function getSmLineValidLabel(string $published) : string
    {
        if (!empty(self::$sm_lineValidValueArray[$published])) {
            return self::$sm_lineValidValueArray[$published];
        }
        return '';
    }


    /* return data array by keys id/info based on filters/ordering... */
    public static function getSmLinesSelectionList( bool $key_return= true,  array $filtersArray = array(), string $order_by = 's.top', string $order_direction = 'asc') : array
    {
        $sm_linesList = SmLine::getSmLinesList(ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction);
        $resArray = [];
        foreach ($sm_linesList as $nextSmLine) {
            if ( $key_return ) {
                $resArray[] = [ 'key'=>$nextSmLine->info, 'value'=> $nextSmLine->id ];
            } else {
                $resArray[$nextSmLine->id] = $nextSmLine->info;
            }
        }
        return $resArray;
    }

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getSmLinesList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 's.top'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $sm_line_table_name= with(new SmLine)->getTableName();
        $quoteModel= SmLine::from(  \DB::raw(DB::getTablePrefix().$sm_line_table_name.' as s' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 's.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['info'])) {
//            if ( empty($filtersArray['in_content']) ) {
                $quoteModel->whereRaw( SmLine::pgStrLower('info', false, false) . ' like ' . SmLine::pgStrLower( $filtersArray['info'], true,true ));
//            } else {
//                $quoteModel->whereRaw( ' ( '.SmLine::pgStrLower('title', false, false) . ' like ' . SmLine::pgStrLower( $filtersArray['title'], true,true ) . ' OR ' . SmLine::pgStrLower('content', false, false) . ' like ' . SmLine::pgStrLower( $filtersArray['title'], true,true ) .
//                                       ' OR ' . SmLine::pgStrLower('short_descr', false, false) . ' like ' . SmLine::pgStrLower( $filtersArray['title'], true,true ) . ' ) ');
//            }
        }

        /*
CREATE TABLE public.pd_sm_line (
id int4 NOT NULL DEFAULT nextval('pd_sm_line_id_seq'::regclass),
sql text NOT NULL,
info varchar(255) NULL,
top bool NULL DEFAULT false,
valid bool NULL DEFAULT false,
key_id varchar(50) NULL,
created_at timestamp NOT NULL DEFAULT now(),
CONSTRAINT ind_pd_sm_line_unique UNIQUE (info),
CONSTRAINT pd_sm_line_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
) ;
CREATE INDEX ind_sm_line_key_id_top ON public.pd_sm_line (key_id,top) ;
CREATE INDEX ind_sm_line_key_id_valid ON public.pd_sm_line (key_id,valid) ;
CREATE INDEX ind_sm_line_created_at ON public.pd_sm_line (created_at) ;

ALTER SEQUENCE pd_sm_line_id_seq OWNED BY pd_sm_line.id; */

        if (!empty($filtersArray['top'])) {
            $quoteModel->where( 'top', '=', $filtersArray['top'] );
        }

        if (!empty($filtersArray['valid'])) {
            $quoteModel->where( 'valid', '=', $filtersArray['valid'] );
        }

        if (!empty($filtersArray['key_id'])) {
            $quoteModel->where( 'key_id', '=', $filtersArray['key_id'] );
        }

        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( 'user_id', '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "s.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "s.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }
//        if ( !empty($filtersArray['show_username'])  ) { // need to join in select sql username and user_active_status field of author of cms item
//            $ion_users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
//            $additive_fields_for_select .= ', u.username as username, u.active_status as  user_active_status' ;
//            $quoteModel->join( \DB::raw($ion_users_table_name . ' as u '), \DB::raw('u.id'), '=', \DB::raw('cms.user_id') );
//        } // if ( !empty($filtersArray['show_username'])  ) { // need to join in select sql username and user_active_status field of author of cms item

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
        $items_per_page= with(new SmLine)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new AttributeItem)->isPositiveNumeric($page_param) ) ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $sm_linesList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $sm_linesList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $sm_linesList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $sm_linesList as $next_key=>$nextSmLine ) { /* map all retrieved data from server using self::$fieldsMappingArray definitions */
            $sm_linesList[$next_key]= MyAppModel::makeRetreivingFieldsMapping($nextSmLine, self::$fieldsMappingArray);
        }
        return $sm_linesList;

    } // public static function getSmLinesList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if (!empty($additiveParams['show_username']) ) { // need to join in select sql username and user_active_status field of author of cms item
            $is_row_retrieved= true;
            $additive_fields_for_select= "";
            $fields_for_select= 's.*';
            $sm_line_table_name= with(new SmLine)->getTableName();
            $quoteModel= SmLine::from(  \DB::raw(DB::getTablePrefix().$sm_line_table_name.' as cms' ));
            $quoteModel->where( \DB::raw('cms.id'), '=', $id );
            $ion_users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u.username as username, u.active_status as  user_active_status' ;
            $quoteModel->join( \DB::raw($ion_users_table_name . ' as u '), \DB::raw('u.id'), '=', \DB::raw('cms.user_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $sm_lineRows = $quoteModel->get();
            if (!empty($sm_lineRows[0]) and get_class($sm_lineRows[0]) == 'App\SmLine' ) {
                $sm_line= $sm_lineRows[0];
            }
        } // if (!empty($additiveParams['show_username']) ) { // need to join in select sql username and user_active_status field of author of cms item

        if ( !$is_row_retrieved ) {
            $sm_line = SmLine::find($id);
        }

        if (empty($sm_line)) return false;
        if (!empty($additiveParams['show_file_info']) and !empty($sm_line->img)) { // in sm_lineImagePropsArray return image properties(url, path, size ...)
            $sm_line_img = SmLine::getSmLineDir($id) . $sm_line->img;
            if ( file_exists($sm_line_img) ) {
                $image             = $sm_line->img;
                $image_path        = SmLine::getSmLinePath( $id, $image );
                $image_url         = SmLine::getSmLineUrl( $id, $image );

                $imagePropsArray        = [ 'image'=> $image, 'image_path'=> $image_path, 'image_url'=> $image_url ];
                $previewSizeArray= with(new SmLine)->getImageShowSize($sm_line_img, self::$img_preview_width, self::$img_preview_height );
                if ( !empty($previewSizeArray['width']) ) {
                    $imagePropsArray['preview_width']= $previewSizeArray['width'];
                    $imagePropsArray['preview_height']= $previewSizeArray['height'];
                }
                $sm_lineImgProps= SmLine::getImageProps( $image_path, $imagePropsArray );
                if ( !empty($sm_lineImgProps) ) {
                    $sm_line->setSmLineImagePropsAttribute($sm_lineImgProps);
                }
            }
        } // if (!empty($additiveParams['show_file_info']) and !empty($sm_line->img)) { // in sm_lineImagePropsArray return image properties(url, path, size ...)

        /* map all retrieved data from server using self::$fieldsMappingArray definitions */
        return MyAppModel::makeRetreivingFieldsMapping($sm_line, self::$fieldsMappingArray);
    } // public function getRowById( int $id, array $additiveParams= [] )


    public static function updateSmLine(int $sm_line_id, array $dataArray, bool $run_validation= true) : array
    {
        $is_insert= true;
        if( $run_validation ) { // data must be validated before saving
            if ( !empty($sm_line_id) and with(new SmLine)->isPositiveNumeric($sm_line_id) ) {
                $sm_line= SmLine::find($sm_line_id);
                if ( empty($sm_line) ) { // if have id of existing object it would be updated
                    return [ 'error_code' => 1, 'errorsList' => ["Cms Item # '".$sm_line_id."' not found" ], 'success_message' => "", 'sm_line_id' => '' ];
                }
                $existing_image_dir= SmLine::getSmLineDir($sm_line_id);
                $is_insert= false;
            }
            $additional_title_validation_rule= '';// 'check_sm_line_unique_by_title:'.( !empty($sm_line_id)?$sm_line_id:'');
            $rules = [
                'sql'        =>  'required',
                'info'       =>  'max:255|'.$additional_title_validation_rule,
                'top'        =>  'required|in:'.with(new RgRegexpItem)->getValueLabelKeys( self::$sm_lineTopValueArray),
                'valid'      =>  'required|in:'.with(new RgRegexpItem)->getValueLabelKeys( self::$sm_lineValidValueArray),
                'user_id'    =>  'required|exists:'.( with(new User)->getTableName() ).',id',
                'key_id'     =>  'required:',
            ];
            /* 	id int4 NOT NULL DEFAULT nextval('pd_sm_line_id_seq'::regclass),
	        "sql" text NOT NULL,
        	info varchar(255) NULL,
	top bool NULL DEFAULT false,
	valid bool NULL DEFAULT false,
	        user_id int4 NOT NULL,
        	key_id varchar(50) NULL,
	created_at timestamp NOT NULL DEFAULT now(), */
            $validator = Validator::make( $dataArray , $rules);
            if ($validator->fails()) { // if error on validation return errors list
                $errorsList = SmLine::getErrorsList($validator);
                return [ 'error_code' => 1, 'errorsList' => $errorsList, 'success_message' => "", 'sm_line_id' => '' ];
            }
        } // if( $run_validation ) { // data must be validated before saving

        /* map all data for saving on server using self::$fieldsMappingArray definitions */
        $dataArray= SmLine::makeSavingFieldsMapping( $dataArray, self::$fieldsMappingArray );

        $uploaded_sm_line_img_name= '';
        if (request()->hasFile('sm_line_img') ) { // there is image uploaded
            $uploadingSmLineImg        = request()->file('sm_line_img');
            $uploaded_sm_line_img_name = Category::checkValidImgName($uploadingSmLineImg->getClientOriginalName(), SmLine::$img_filename_max_length, true);
        }
        $updateValuesArray= [ /* Array for saving with data and data type */
            [ 'field_name'=> 'p_id', 'value'=> $sm_line_id, 'type'=> 'N', 'is_array'=> false ],
            [ 'field_name'=> 'p_title', 'value'=> MyAppModel::pgEscape($dataArray['title']), 'type'=> PgDataType::STRING, 'is_array'=> false ],
            [ 'field_name'=> 'p_alias', 'value'=> MyAppModel::pgEscape( !empty($dataArray['alias']) ? $dataArray['alias'] : '' ), 'type'=> PgDataType::STRING, 'is_array'=> false ],
            [ 'field_name'=> 'p_page_type', 'value'=> $dataArray['page_type'], 'type'=> PgDataType::STRING, 'is_array'=> false ],
            [ 'field_name'=> 'p_short_descr', 'value'=> MyAppModel::pgEscape( !empty($dataArray['short_descr'])?$dataArray['short_descr']:''), 'type'=> PgDataType::STRING, 'is_array'=> false ],
            [ 'field_name'=> 'p_content', 'value'=> MyAppModel::pgEscape($dataArray['content']), 'type'=> PgDataType::STRING, 'is_array'=> false ],
            [ 'field_name'=> 'p_img', 'value'=> MyAppModel::pgEscape($uploaded_sm_line_img_name), 'type'=> PgDataType::STRING, 'is_array'=> false ],
            [ 'field_name'=> 'p_user_id', 'value'=> $dataArray['user_id'], 'type'=> PgDataType::NUMERIC, 'is_array'=> false ],
            [ 'field_name'=> 'p_published', 'value'=> $dataArray['published'], 'type'=> PgDataType::BOOLEAN, 'is_array'=> false ],
            [ 'field_name'=> 'p_mailchimp_template_id', 'value'=> !empty($dataArray['mailchimp_template_id']) ? $dataArray['mailchimp_template_id'] : '', 'type'=> PgDataType::STRING, 'is_array'=> false ],
        ];
        if ( !empty($dataArray['created_at']) ) { /* if created_at field is supplied, ex. in import */
            $updateValuesArray[]= [ 'field_name'=> 'p_created_at', 'value'=> $dataArray['created_at'], 'type'=> PgDataType::STRING, 'is_array'=> false ];
        }

        /* run postgresql updating function with provided parameters in $updateValuesArray and id returned */
        $sm_line_id= MyAppModel::runPgFunction($updateValuesArray, DB::getTablePrefix()."update_sm_line");

        if ( $sm_line_id ) {
            if (request()->hasFile('sm_line_img') and empty($dataArray['cbx_clear_image']) )
            { // there is image uploaded
                if ( empty($sm_line) ) {
                    $sm_line = SmLine::find($sm_line_id);
                    if ( empty($sm_line) ) { // if have id of existing object it would be updated
                        return [ 'error_code' => 1, 'errorsList' => ["Cms Item # '".$sm_line_id."' not found" ], 'success_message' => "", 'sm_line_id' => '' ];
                    }
                }
                $sm_line_image_dir= SmLine::getSmLineDir($sm_line_id);
                $all_sm_line_image_paths= SmLine::getAllSmLinePaths($sm_line_id);
                with(new SmLine)->createDir($all_sm_line_image_paths);
                $uploaded_sm_line_img_name= SmLine::checkValidImgName( $uploadingSmLineImg->getClientOriginalName(), SmLine::$img_filename_max_length, true );
                $uploadingSmLineImg->move($sm_line_image_dir, public_path($sm_line_image_dir . $uploaded_sm_line_img_name));
            }
            if (!empty($dataArray['cbx_clear_image']) ) { // images must be removed as cleared checked!
                if ( file_exists($existing_image_dir) ) {
                    with(new SmLine)->deleteDirectory( $existing_image_dir );
                }
            } // if (empty($dataArray['cbx_clear_image']) ) {  // images must be removed as cleared checked!

            return [ 'error_code' => 0, 'errorsList' => [], 'success_message' => "Cms item '".$dataArray['title']."' was ".($is_insert?"created":"updated"), 'sm_line_id' => $sm_line_id ];
        }
        return [ 'error_code' => 1, 'errorsList' => ["Error ".($is_insert?"creating":"updating")." cms item '".$dataArray['title']."' " ], 'success_message' => "", 'sm_line_id' => '' ];
    } // public static function updateSmLine`(int $sm_line_id, array $dataArray, bool $run_validation= true) : array


    public static function deleteSmLine(int $sm_line_id, bool $run_validation= true) : array
    {
        if( $run_validation ) { // data must be validated before deleted
            $sm_line = SmLine::find( $sm_line_id );
            if ( empty( $sm_line ) ) {
                return [ 'error_code'      => 1,
                         'errorsList'     => [ "Cms Item # '" . $sm_line_id . "' not found" ],
                         'success_message' => "",
                         'sm_line_id'   => ''
                ];
            }
        } // if( $run_validation ) { // data must be validated before deleted

        $ret= MyAppModel::runPgFunction($sm_line_id, DB::getTablePrefix()."delete_sm_line");
        $sm_line_dir= SmLine::getSmLineDir($sm_line_id, false);
        $ret= with(new SmLine)->deleteDirectory( $sm_line_dir );
        if ( $ret ) { // Deleting was successful
            return [ 'error_code' => 0, 'errorsList' => [], 'success_message' => "SmLine '".$sm_line->title."' was deleted ", 'sm_line_id' => $sm_line_id ];
        }
        return [ 'error_code' => 1, 'errorsList' => ["Error deleting sm_line '".$sm_line->title."' " ], 'success_message' => "", 'sm_line_id' => '' ];  // Deleting was NOT successful
    }


    /* check if provided title is unique for sm_line.title field */
    public static function getSimilarSmLineByTitle( string $title, int $id= null, $return_count = false )
    {
        $quoteModel = SmLine::whereRaw( SmLine::pgStrLower('title', false, false) .' = '. SmLine::pgStrLower(SmLine::pgEscape($title), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    /* get Body of content by title of Content*/
    public static function getBodyContentByAlias($alias, $constantsArray = [], $is_content = true)
    {
        $sm_lineRow = SmLine::whereRaw( SmLine::pgStrLower('alias', false, false) .' = '. SmLine::pgStrLower($alias, true,false) )->get();
        if ( !empty($sm_lineRow[0]->content) ) {
            $text = $is_content ? $sm_lineRow[0]->content : $sm_lineRow[0]->title;
            foreach ($constantsArray as $key => $value) {
                $pattern = '/\[([\s]*' . $key . '[\s]*)\]/xsi';
                $text = preg_replace($pattern, $value, $text);
            }
            return $text;
        }
        return '';
    }

    public function getSmLineIdByAlias($alias)
    {
        $sm_lineRow = SmLine::whereRaw( SmLine::pgStrLower('alias', false, false) .' = '. SmLine::pgStrLower($alias, true,false) )->get();
        if ( !empty($sm_lineRow[0]->id) ) {
            return $sm_lineRow[0]->id;
        }
        return '';
    }



    public static function getSmLinesDir($short_path= false) : string
    {
        $public_path= with (new SmLine)->getPublicPath();
        $UPLOADS_CMSITEM_IMAGES_DIR= env('UPLOADS_CMSITEM_IMAGES_DIR');
        return ( !$short_path ? $public_path : '' ) . '/' . $UPLOADS_CMSITEM_IMAGES_DIR;
    }

    public static function getSmLineDir($sm_line_id= '', $short_path= false) : string
    {
        $public_path= with (new SmLine)->getPublicPath();
        $UPLOADS_CMSITEM_IMAGES_DIR= env('UPLOADS_CMSITEM_IMAGES_DIR');
        return $public_path . '/' . $UPLOADS_CMSITEM_IMAGES_DIR . '-sm_line-' . $sm_line_id.'/';
    }

    public static function getSmLineUrl($sm_line_id, $img) : string
    {
        if ( empty($img) ) return '';
        $base_url= with(new Settings)->getBaseUrl();
        $UPLOADS_CMSITEM_IMAGES_URL= env('UPLOADS_CMSITEM_IMAGES_URL');
        return $base_url .'/'. $UPLOADS_CMSITEM_IMAGES_URL.'-sm_line-' . $sm_line_id.'/' . $img;
    }

    public static function getSmLinePath($sm_line_id, $img, $short_path= false, $check_existing= false) : string
    {
        if ( empty($img) ) return '';
        $public_path= with (new SmLine)->getPublicPath();
        $UPLOADS_CMSITEM_IMAGES_DIR= env('UPLOADS_CMSITEM_IMAGES_DIR');
        return ( !$short_path ? $public_path . '/'  : '' ) . $UPLOADS_CMSITEM_IMAGES_DIR . '-sm_line-' . $sm_line_id . '/' . $img;
    }

    public static function getAllSmLinePaths($sm_line_id,  $short_path= false) : array
    {
        $public_path= with (new SmLine)->getPublicPath();
        $UPLOADS_IMAGES_DIR= env('UPLOADS_IMAGES_DIR');
        $UPLOADS_CMSITEM_IMAGES_DIR= env('UPLOADS_CMSITEM_IMAGES_DIR');
        $ret= [];
        $ret[]= $public_path . '/' . $UPLOADS_IMAGES_DIR;
        $ret[]= $public_path . '/' . $UPLOADS_CMSITEM_IMAGES_DIR;
        $ret[]= SmLine::getSmLineDir( $sm_line_id, $short_path );
        return $ret;
    }

    /* get additional properties of sm_line image : path, url, size etc... */
    public function getSmLineImagePropsAttribute() : array
    {
        return $this->sm_lineImagePropsArray;
    }

    /* set additional properties of sm_line image : path, url, size etc... */
    public function setSmLineImagePropsAttribute(array $sm_lineImagePropsArray)
    {
        $this->sm_lineImagePropsArray = $sm_lineImagePropsArray;
    }

    public function getContentHintsAttribute($data)
    {
        return !empty($data) ? $data : '';
    }


    public function getContentAttribute($data)
    {
        return !empty($data) ? $data : '';
    }

    /* output format of created_at field by ENV options */
    public function getCreatedAtAttribute($date)
    {
        if ( empty($date) ) return '';
        return $this->getFormattedDateTime($date);
    }

    /* output format of updated_at field by ENV options */
    public function getUpdatedAtAttribute($date)
    {
        if ( empty($date) ) return '';
        return $this->getFormattedDateTime($date);
    }

}