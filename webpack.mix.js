let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   // .js('resources/assets/js/my_components.js', 'public/js') // http://local-tasks.com/js/my_components.js?dt=1512720395
     // resources/assets/js/my_app.js
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.copy('node_modules/font-awesome/fonts', 'public/fonts');