-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 20, 2017 at 12:25 PM
-- Server version: 5.7.20-0ubuntu0.17.10.1
-- PHP Version: 7.1.8-1ubuntu1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tasks`
--

-- --------------------------------------------------------

--
-- Table structure for table `tsk_categories`
--

DROP TABLE IF EXISTS `tsk_categories`;
CREATE TABLE `tsk_categories` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` smallint(5) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_categories`
--

INSERT INTO `tsk_categories` (`id`, `name`, `description`, `image`, `parent_id`, `created_at`) VALUES
(1, 'PHP Development', 'PHP Development description...', NULL, NULL, '2017-11-23 12:10:35'),
(2, 'Wordpress Development', 'Wordpress Development description...', NULL, 1, '2017-11-23 12:10:35'),
(3, 'Joomla Development', 'Joomla Development description...', NULL, 1, '2017-11-23 12:10:35'),
(4, 'Laravel Development', 'Laravel Development description...', NULL, 1, '2017-11-23 12:10:35'),
(5, 'Laravel/vue.js', 'Laravel/vue.js Development description...', NULL, 4, '2017-11-23 12:10:35'),
(6, 'Laravel/angular.js', 'Laravel/angular.js Development description...', NULL, 4, '2017-11-23 12:10:35'),
(7, 'Laravel/react.js', 'Laravel/react.js Development description...', NULL, 4, '2017-11-23 12:10:35');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_document_categories`
--

DROP TABLE IF EXISTS `tsk_document_categories`;
CREATE TABLE `tsk_document_categories` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('D','C','P','M','T') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT ' D=>Task Document, C=>Chat Document, P=>Profile Document, M=>Profile Main Image, T=>Profile Thumbnail Image',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_document_categories`
--

INSERT INTO `tsk_document_categories` (`id`, `name`, `type`, `description`, `created_at`) VALUES
(1, 'Profile main image', 'M', 'Profile main image description...', '2017-12-15 12:36:58'),
(2, 'Profile thumbnail image', 'T', 'Profile thumbnail image description...', '2017-12-15 12:36:58'),
(3, 'User\'s documents (diploma, certificate, license)', 'P', 'User\'s documents (diploma, certificate, license) description...', '2017-12-15 12:36:58'),
(4, 'Task\'s specification documents', 'D', 'Task\'s specification documents description...', '2017-12-15 12:36:58'),
(5, 'Task\'s attached images', 'D', 'Task\'s attached images description...', '2017-12-15 12:36:58'),
(6, 'Task\'s attached schemes', 'D', 'Task\'s attached schemes description...', '2017-12-15 12:36:58'),
(7, 'Chat\'s attached files', 'C', 'Chat\'s attached files description...', '2017-12-15 12:36:58'),
(8, 'a', 'C', 'aaaa', '2017-12-19 13:10:45');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_groups`
--

DROP TABLE IF EXISTS `tsk_groups`;
CREATE TABLE `tsk_groups` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_groups`
--

INSERT INTO `tsk_groups` (`id`, `name`, `description`, `created_at`) VALUES
(1, 'Admin', 'Administrator', '2017-11-23 08:07:40'),
(2, 'Main Manager', 'Main Manager description...', '2017-11-23 08:07:40'),
(3, 'Manager', 'Manager description...', '2017-11-23 08:07:40'),
(4, 'Employee', 'Employee description...', '2017-11-23 08:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_migrations`
--

DROP TABLE IF EXISTS `tsk_migrations`;
CREATE TABLE `tsk_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_migrations`
--

INSERT INTO `tsk_migrations` (`id`, `migration`, `batch`) VALUES
(7, '2014_10_12_000000_create_users_table', 1),
(8, '2014_10_12_100000_create_password_resets_table', 1),
(9, '2017_11_22_155945_modify_users_table_status', 1),
(13, '2017_11_23_055108_create_users_groups_table', 2),
(16, '2017_11_23_080830_fill_users_groups_table_with_sapmle_data', 3),
(18, '2017_11_22_071254_create_tasks_table', 4),
(24, '2017_11_23_082235_create_categories_table', 5),
(25, '2017_11_23_084116_modify_tasks_table_status', 5),
(31, '2017_11_23_121432_modify_users_table_timestamp-', 6),
(34, '2017_11_23_145312_create_settings_table', 7),
(41, '2017_11_24_090056_modify_tasks_table_many_fields-', 8),
(45, '2017_11_24_124214_create_user_task_types_table', 9),
(49, '2017_11_24_131358_create_task_assigned_to_users_table', 10),
(51, '2017_11_27_084731_create_user_chats_table', 11),
(56, '2017_11_27_090448_create_user_chat_participants_table', 12),
(57, '2017_12_01_051343_modify_user_chats_table_task_id', 12),
(60, '2017_12_02_090642_modify_user_chat_participants_table', 13),
(61, '2017_12_02_114658_modify_user_chat_participants_table_status', 13),
(63, '2017_12_02_120953_modify_user_chats_table_manager_id', 14),
(68, '2017_12_04_112349_create_user_chat_messages_table', 15),
(69, '2017_12_05_051408_modify_user_chat_messages_table_updated_at', 15),
(74, '2017_12_08_065215_create_user_profile_table', 16),
(76, '2017_12_15_083529_create_document_categories_table', 17),
(80, '2017_12_17_081933_create_user_profile_documents_table', 18);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_password_resets`
--

DROP TABLE IF EXISTS `tsk_password_resets`;
CREATE TABLE `tsk_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tsk_settings`
--

DROP TABLE IF EXISTS `tsk_settings`;
CREATE TABLE `tsk_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_settings`
--

INSERT INTO `tsk_settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'items_per_page', '2', '2017-11-24 11:32:14', NULL),
(2, 'site_name', 'Loan import', '2017-11-24 11:32:14', NULL),
(3, 'max_str_length_in_listing', '50', '2017-12-14 05:48:20', NULL),
(4, 'default_background_color', '#ffffff', '2017-12-19 07:00:33', NULL),
(5, 'default_color', '#000000', '2017-12-19 07:00:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_tasks`
--

DROP TABLE IF EXISTS `tsk_tasks`;
CREATE TABLE `tsk_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `category_id` smallint(5) UNSIGNED NOT NULL,
  `priority` enum('0','1','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '  0-No, 1-Low, 2-Normal, 3-High, 4-Urgent, 5-Immediate  ',
  `status` enum('D','A','C','P','K','O') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'D => Draft, A=>Assigning, C => Cancelled, P => Processing, K=> Checking, O=> Completed',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `needs_reports` enum('0','1','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=>No, 1=>Hourly, 2=>Twice a day,3=>Daily,4=>Twice a week,5=>Weekly',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_tasks`
--

INSERT INTO `tsk_tasks` (`id`, `name`, `creator_id`, `category_id`, `priority`, `status`, `date_start`, `date_end`, `needs_reports`, `created_at`, `updated_at`) VALUES
(1, 'Mastering Laravel/vue.js', 5, 4, '3', 'P', '2017-11-24', '2018-01-24', '4', '2017-11-24 12:33:54', NULL),
(2, 'Develop Tasks management site using Laravel/vue.js', 5, 5, '4', 'A', '2017-11-24', '2018-03-24', '5', '2017-11-24 12:33:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_task_assigned_to_users`
--

DROP TABLE IF EXISTS `tsk_task_assigned_to_users`;
CREATE TABLE `tsk_task_assigned_to_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_task_type_id` smallint(5) UNSIGNED NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_task_assigned_to_users`
--

INSERT INTO `tsk_task_assigned_to_users` (`id`, `task_id`, `user_id`, `user_task_type_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, 'You need to learn/control Mastering Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-11-24 13:39:28', NULL),
(2, 1, 4, 2, 'You need to learn Mastering Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-11-24 13:39:28', NULL),
(3, 2, 3, 1, 'You need to learn/control Develop Tasks management site using Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-11-24 13:39:28', NULL),
(4, 2, 4, 2, 'You need to learn Develop Tasks management site using Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-11-24 13:39:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_users`
--

DROP TABLE IF EXISTS `tsk_users`;
CREATE TABLE `tsk_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('N','A','I') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' N => New(Waiting activation), A=>Active, I=>Inactive',
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_start` date DEFAULT NULL,
  `contract_end` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_users`
--

INSERT INTO `tsk_users` (`id`, `name`, `email`, `password`, `remember_token`, `status`, `first_name`, `last_name`, `phone`, `website`, `contract_start`, `contract_end`, `created_at`, `updated_at`) VALUES
(1, 'Jon Glads', 'admin@site.com', '$2y$10$OhIQ04.lGfITlPYkN.VChOKq4XtGhIbJFBw64t3h3G60KLKPHd8O2', NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-23 12:42:56', NULL),
(2, 'Rod Bodrick', 'rod_bodrick@site.com', '$2y$10$DfZsdd6/oEqXj3ESiFUlVOXYEpNCz.qvD.55y/OBdTWYK92w02Txu', NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-23 12:42:56', NULL),
(3, 'Tony Black', 'tony_black@site.com', '$2y$10$SMxNmvytxsML3lbHo6vU9.pruOV/emcu.XcUX5bBMCcl74dNQaYyy', NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-23 12:42:56', NULL),
(4, 'Adam Lang', 'adam_lang@site.com', '$2y$10$/AbYCz2CUyFYj.2bWNbsGuSrZarUt.iJ/Y5mAmftV.49NOsYHCK5q', NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-23 12:42:56', NULL),
(5, 'admin', 'admin@mail.com', '$2y$10$WX6s.b7tjQMTp3p/7YMWDe/fRIc/kwGqtNODDk9moOnyFhFEaj.Am', 'b6nw3jXNOuFjwR8eaZe3NSb8D1HY7Tww1s8pyT0tLmbGcyYe6yE5JBeJd6Q8', 'A', 'Madase', 'Pafase', 'vfsdf', 'dfgds', '2017-12-09', '2017-12-31', '2017-11-23 10:42:58', '2017-11-23 10:42:58'),
(6, 'user1', 'user1@mail.c0m', '$2y$10$Z0mH6mNKxJ2a1CLls18whe8fRAKzxnn1TOwPn0T2k9kdTdvv/Hvp2', 'NKYiOTUdUa6r2vZhzADSBbBEzyIbghTdrH3YulYUKQlkhlQIvTTMNVbDQOz2', 'N', 'Saper', 'Mader', 'grftg', 'retet', NULL, NULL, '2017-12-06 07:30:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_users_groups`
--

DROP TABLE IF EXISTS `tsk_users_groups`;
CREATE TABLE `tsk_users_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` smallint(5) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_users_groups`
--

INSERT INTO `tsk_users_groups` (`id`, `user_id`, `group_id`, `created_at`) VALUES
(5, 1, 1, '2017-11-23 08:13:04'),
(6, 2, 2, '2017-11-23 08:13:04'),
(7, 3, 3, '2017-11-23 08:13:04'),
(8, 4, 4, '2017-11-23 08:13:04');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chats`
--

DROP TABLE IF EXISTS `tsk_user_chats`;
CREATE TABLE `tsk_user_chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('A','C') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT ' A=>Active, C=>Closed',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `task_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `manager_id` int(10) UNSIGNED
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chats`
--

INSERT INTO `tsk_user_chats` (`id`, `name`, `description`, `creator_id`, `status`, `created_at`, `task_id`, `updated_at`, `manager_id`) VALUES
(1, 'Greeting all employees!', 'Greeting all employees! and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.++++', 5, 'A', '2017-11-27 09:03:32', NULL, '2017-12-01 14:04:45', 1),
(2, 'People, get the first task description', 'First task description and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 5, 'A', '2017-11-27 09:03:32', 2, '2017-12-02 03:24:49', 1),
(7, 'Let\'s discuss task Mastering Laravel/vue.js in this chat...', 'Let\'s discuss task Mastering Laravel/vue.js in this chat... and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.--------', 5, 'A', '2017-12-01 05:53:22', NULL, '2017-12-01 14:05:06', 1),
(8, 'People, this is chat for Develop Tasks management site using Laravel/vue.js task  discussion', 'People, this is chat for Develop Tasks management site using Laravel/vue.js task  discussion and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 5, 'A', '2017-12-01 05:53:22', 2, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chat_messages`
--

DROP TABLE IF EXISTS `tsk_user_chat_messages`;
CREATE TABLE `tsk_user_chat_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_chat_id` int(10) UNSIGNED NOT NULL,
  `is_top` tinyint(1) NOT NULL DEFAULT '0',
  `text` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chat_messages`
--

INSERT INTO `tsk_user_chat_messages` (`id`, `user_id`, `user_chat_id`, `is_top`, `text`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 1, ' That is first/top message on \"Greeting all employees!\" chan and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-19 08:39:13', NULL),
(2, 6, 1, 0, ' That is next message on \"Greeting all employees!\" chan and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-06 13:29:13', NULL),
(3, 5, 1, 1, ' That is first/top message on \"People, get the first task description\" chan and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-16 08:39:13', NULL),
(4, 6, 1, 0, ' That is next message on \"People, get the first task description\" chan and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2018-12-20 08:39:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chat_participants`
--

DROP TABLE IF EXISTS `tsk_user_chat_participants`;
CREATE TABLE `tsk_user_chat_participants` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_chat_id` int(10) UNSIGNED NOT NULL,
  `status` enum('M','W','R') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'R' COMMENT ' ''M''=>''Manage this chat'', ''W'' => ''Can write messages'', ''R'' => ''Can only read'' '
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chat_participants`
--

INSERT INTO `tsk_user_chat_participants` (`id`, `user_id`, `created_at`, `user_chat_id`, `status`) VALUES
(1, 1, '2017-12-02 12:06:30', 1, 'M'),
(2, 2, '2017-12-02 12:06:30', 1, 'W');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_profiles`
--

DROP TABLE IF EXISTS `tsk_user_profiles`;
CREATE TABLE `tsk_user_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_profiles`
--

INSERT INTO `tsk_user_profiles` (`id`, `user_id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(23, 5, 'lang', 'en', '2017-12-16 16:14:01', NULL),
(24, 5, 'submit_message_by_enter', 'Y', '2017-12-16 16:14:01', NULL),
(25, 5, 'color', '#004300', '2017-12-16 16:14:01', NULL),
(26, 5, 'background_color', '#ffff7f', '2017-12-16 16:14:01', NULL),
(27, 5, 'subscription_to_newsletters', 'Y', '2017-12-16 16:14:01', NULL),
(28, 5, 'show_online_status', 'Y', '2017-12-16 16:14:01', NULL),
(29, 6, 'lang', 'en', '2017-12-16 16:16:46', NULL),
(30, 6, 'submit_message_by_enter', 'N', '2017-12-16 16:16:46', NULL),
(31, 6, 'color', '#4a08bd', '2017-12-16 16:16:46', NULL),
(32, 6, 'background_color', '#00fc00', '2017-12-16 16:16:46', NULL),
(33, 6, 'subscription_to_newsletters', 'Y', '2017-12-16 16:16:46', NULL),
(34, 6, 'show_online_status', 'Y', '2017-12-16 16:16:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_profile_documents`
--

DROP TABLE IF EXISTS `tsk_user_profile_documents`;
CREATE TABLE `tsk_user_profile_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_category_id` smallint(5) UNSIGNED NOT NULL,
  `public_access` enum('P','E') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'E' COMMENT '  P-Public, E-Personal',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_profile_documents`
--

INSERT INTO `tsk_user_profile_documents` (`id`, `user_id`, `filename`, `extension`, `document_category_id`, `public_access`, `info`, `created_at`) VALUES
(30, 5, 'trindetch.jpg', 'jpg', 3, 'E', 'Info text...', '2017-12-20 08:05:06'),
(31, 5, 'wizdom.jpg', 'jpg', 3, 'E', 'Info text...', '2017-12-20 08:05:06');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_task_types`
--

DROP TABLE IF EXISTS `tsk_user_task_types`;
CREATE TABLE `tsk_user_task_types` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_task_types`
--

INSERT INTO `tsk_user_task_types` (`id`, `name`, `description`, `created_at`) VALUES
(1, 'Team Leader', 'Team Leader ... Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-11-24 13:12:58'),
(2, 'PHP developer', 'PHP developer Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-11-24 13:12:58'),
(3, 'HTML developer', 'HTML developer Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-11-24 13:12:58'),
(4, 'Javascript/Vue developer', 'Javascript/Vue developer Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-11-24 13:12:58'),
(5, 'Painter', 'Painter Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-11-24 13:12:58'),
(61, 'sss', 'ss', '2017-11-29 07:38:05'),
(65, 'bb65', 'aaa', '2017-11-29 07:43:03'),
(67, 'ss', 'ss', '2017-11-29 07:57:37'),
(69, 'bb', 'bb', '2017-11-29 08:30:36'),
(73, 'auuuuuuu--QQQQWWWWWWWWWWWW 73', 'a---', '2017-11-29 11:48:31'),
(76, 'ssddddd', 'a', '2017-12-05 16:56:44'),
(77, 'ssdddd', 'ddddd', '2017-12-05 16:57:02'),
(82, 'ddd', 'ddd', '2017-12-06 12:35:43'),
(87, 'assssss', 'a', '2017-12-13 13:33:20'),
(88, 'aaaaa', 'a', '2017-12-14 14:48:01'),
(90, 'adddd', 'a', '2017-12-19 13:19:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsk_categories`
--
ALTER TABLE `tsk_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`),
  ADD KEY `categories_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_document_categories`
--
ALTER TABLE `tsk_document_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `document_categories_name_unique` (`name`),
  ADD KEY `document_categories_created_at_index` (`created_at`),
  ADD KEY `document_type_name_at_index` (`type`,`name`);

--
-- Indexes for table `tsk_groups`
--
ALTER TABLE `tsk_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groups_name_unique` (`name`),
  ADD UNIQUE KEY `groups_description_unique` (`description`),
  ADD KEY `groups_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_migrations`
--
ALTER TABLE `tsk_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tsk_password_resets`
--
ALTER TABLE `tsk_password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tsk_settings`
--
ALTER TABLE `tsk_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_name_unique` (`name`),
  ADD KEY `settings_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_tasks`
--
ALTER TABLE `tsk_tasks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tasks_name_unique` (`name`),
  ADD KEY `tasks_creator_id_foreign` (`creator_id`),
  ADD KEY `tasks_category_id_foreign` (`category_id`),
  ADD KEY `tasks_status_date_start_date_end_index` (`status`,`date_start`,`date_end`),
  ADD KEY `tasks_priority_category_id_date_end_index` (`priority`,`category_id`,`date_end`),
  ADD KEY `tasks_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_task_assigned_to_users`
--
ALTER TABLE `tsk_task_assigned_to_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_assigned_to_users_user_id_task_id_unique` (`user_id`,`task_id`),
  ADD KEY `task_assigned_to_users_task_id_foreign` (`task_id`),
  ADD KEY `task_assigned_to_users_user_task_type_id_foreign` (`user_task_type_id`),
  ADD KEY `task_assigned_to_users_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_users`
--
ALTER TABLE `tsk_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_status_login_index` (`status`),
  ADD KEY `users_created_at_index` (`created_at`),
  ADD KEY `users_status_contract_start_index` (`status`,`contract_start`);

--
-- Indexes for table `tsk_users_groups`
--
ALTER TABLE `tsk_users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_groups_user_id_group_id_unique` (`user_id`,`group_id`),
  ADD KEY `users_groups_group_id_foreign` (`group_id`),
  ADD KEY `users_groups_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_chats`
--
ALTER TABLE `tsk_user_chats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_chats_created_at_index` (`created_at`),
  ADD KEY `user_chats_creator_id_status_name_index` (`creator_id`,`status`,`name`),
  ADD KEY `user_chats_task_id_foreign` (`task_id`),
  ADD KEY `tasks_status_task_id_creator_id_index` (`status`,`task_id`,`creator_id`),
  ADD KEY `user_chats_manager_id_status_index` (`manager_id`,`status`);

--
-- Indexes for table `tsk_user_chat_messages`
--
ALTER TABLE `tsk_user_chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_chat_messages_user_id_user_chat_id_index` (`user_id`,`user_chat_id`),
  ADD KEY `user_chat_messages_user_chat_id_is_top_index` (`user_chat_id`,`is_top`),
  ADD KEY `user_chat_messages_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_chat_participants`
--
ALTER TABLE `tsk_user_chat_participants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_chat_participants_created_at_index` (`created_at`),
  ADD KEY `user_chat_participants_status_user_index` (`user_id`),
  ADD KEY `user_chat_participants_user_chat_id_status_user_id_index` (`user_chat_id`,`status`,`user_id`);

--
-- Indexes for table `tsk_user_profiles`
--
ALTER TABLE `tsk_user_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_profiles_user_id_name_unique` (`user_id`,`name`),
  ADD KEY `user_profiles_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_profile_documents`
--
ALTER TABLE `tsk_user_profile_documents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_profile_documents_user_id_filename_unique` (`user_id`,`filename`),
  ADD KEY `user_profile_documents_document_category_id_foreign` (`document_category_id`),
  ADD KEY `user_profile_documents_user_id_public_access_unique` (`user_id`,`public_access`),
  ADD KEY `user_profile_documents_user_id_extension_unique` (`user_id`,`extension`),
  ADD KEY `user_profile_documents_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_task_types`
--
ALTER TABLE `tsk_user_task_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_task_types_name_unique` (`name`),
  ADD KEY `user_task_types_created_at_index` (`created_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsk_categories`
--
ALTER TABLE `tsk_categories`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tsk_document_categories`
--
ALTER TABLE `tsk_document_categories`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tsk_groups`
--
ALTER TABLE `tsk_groups`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tsk_migrations`
--
ALTER TABLE `tsk_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `tsk_settings`
--
ALTER TABLE `tsk_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tsk_tasks`
--
ALTER TABLE `tsk_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tsk_task_assigned_to_users`
--
ALTER TABLE `tsk_task_assigned_to_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tsk_users`
--
ALTER TABLE `tsk_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tsk_users_groups`
--
ALTER TABLE `tsk_users_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tsk_user_chats`
--
ALTER TABLE `tsk_user_chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tsk_user_chat_messages`
--
ALTER TABLE `tsk_user_chat_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tsk_user_chat_participants`
--
ALTER TABLE `tsk_user_chat_participants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tsk_user_profiles`
--
ALTER TABLE `tsk_user_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tsk_user_profile_documents`
--
ALTER TABLE `tsk_user_profile_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tsk_user_task_types`
--
ALTER TABLE `tsk_user_task_types`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tsk_categories`
--
ALTER TABLE `tsk_categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `tsk_categories` (`id`);

--
-- Constraints for table `tsk_tasks`
--
ALTER TABLE `tsk_tasks`
  ADD CONSTRAINT `tasks_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `tsk_categories` (`id`),
  ADD CONSTRAINT `tasks_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_task_assigned_to_users`
--
ALTER TABLE `tsk_task_assigned_to_users`
  ADD CONSTRAINT `task_assigned_to_users_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tsk_tasks` (`id`),
  ADD CONSTRAINT `task_assigned_to_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `task_assigned_to_users_user_task_type_id_foreign` FOREIGN KEY (`user_task_type_id`) REFERENCES `tsk_user_task_types` (`id`);

--
-- Constraints for table `tsk_users_groups`
--
ALTER TABLE `tsk_users_groups`
  ADD CONSTRAINT `users_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `tsk_groups` (`id`),
  ADD CONSTRAINT `users_groups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

  
  
--
-- Constraints for table `tsk_user_chats`
--
ALTER TABLE `tsk_user_chats`
  ADD CONSTRAINT `user_chats_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `user_chats_manager_id_foreign` FOREIGN KEY (`manager_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `user_chats_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tsk_tasks` (`id`);

--
-- Constraints for table `tsk_user_chat_messages`
--
ALTER TABLE `tsk_user_chat_messages`
  ADD CONSTRAINT `user_chat_messages_user_chat_id_foreign` FOREIGN KEY (`user_chat_id`) REFERENCES `tsk_user_chats` (`id`),
  ADD CONSTRAINT `user_chat_messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_chat_participants`
--
ALTER TABLE `tsk_user_chat_participants`
  ADD CONSTRAINT `user_chat_participants_user_chat_id_foreign` FOREIGN KEY (`user_chat_id`) REFERENCES `tsk_user_chats` (`id`),
  ADD CONSTRAINT `user_chat_participants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_profiles`
--
ALTER TABLE `tsk_user_profiles`
  ADD CONSTRAINT `user_profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_profile_documents`
--
ALTER TABLE `tsk_user_profile_documents`
  ADD CONSTRAINT `user_profile_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `tsk_document_categories` (`id`),
  ADD CONSTRAINT `user_profile_documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
