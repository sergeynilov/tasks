<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('user_profiles', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->string('name', 255);
                $table->string('value', 255);
                $table->timestamp('created_at')->useCurrent();

                $table->unique(['user_id', 'name'], 'user_profiles_user_id_name_unique');
                $table->index(['created_at'], 'user_profiles_created_at_index');
                $table->timestamp('updated_at')->nullable();
                
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();
            Schema::dropIfExists('user_profiles');

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }
}
