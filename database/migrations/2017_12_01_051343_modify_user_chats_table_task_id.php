<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserChatsTableTaskId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chats', function (Blueprint $table) {

                $table->integer('task_id')->unsigned()->nullable();
                $table->foreign('task_id')->references('id')->on('tasks')->onDelete('RESTRICT');
                $table->index(['status', 'task_id', 'creator_id'], 'tasks_status_task_id_creator_id_index');
                $table->timestamp('updated_at')->nullable();


            });

            Artisan::call('db:seed', array('--class' => 'UserChatsWithMoreData'));
            
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chats', function (Blueprint $table) {
                DB::statement('DELETE FROM `'.DB::getTablePrefix().'user_chats` WHERE task_id IS NOT NULL');
                
                $table->dropForeign('user_chats_task_id_foreign');
                $table->dropIndex('tasks_status_task_id_creator_id_index');
                $table->dropColumn('task_id');
                $table->dropColumn('updated_at');

            });
            
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }
}
