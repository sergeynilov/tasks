<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();


            Schema::create('events', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 255)->unique();
                $table->enum('access', ['P', 'U'])->comment('P=>Private, U=>Public');
                $table->timestamp('at_time');
                $table->integer('duration');

                $table->integer('task_id')->unsigned()->nullable();
                $table->foreign('task_id')->references('id')->on('tasks')->onDelete('RESTRICT');

                $table->mediumText('description');
                $table->timestamp('created_at')->useCurrent();

                $table->index(['created_at'], 'events_created_at_index');
                $table->index(['access','name'], 'events_access_name_at_index');
            });

            Schema::create('events_users', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->integer('event_id')->unsigned();
                $table->foreign('event_id')->references('id')->on('events')->onDelete('RESTRICT');

                $table->unique(['user_id', 'event_id'], 'users_events_user_id_event_id_unique');
                $table->timestamp('created_at')->useCurrent();
                $table->index(['created_at'], 'events_created_at_index');
            });
            Artisan::call('db:seed', array('--class' => 'eventsWithInitData'));

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        try {
            DB::beginTransaction();

            Schema::dropIfExists('events_users');
            Schema::dropIfExists('events');
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
        DB::commit();
    }
}
