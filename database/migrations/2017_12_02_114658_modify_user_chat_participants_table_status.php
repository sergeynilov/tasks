<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserChatParticipantsTableStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_participants', function (Blueprint $table) {
                $table->enum('status', ['M', 'W', 'R'])->default('R')->comment(" 'M'=>'Manage this chat', 'W' => 'Can write messages', 'R' => 'Can only read' ");
                $table->index(['user_chat_id', 'status', 'user_id'], 'user_chat_participants_user_chat_id_status_user_id_index');

            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_participants', function (Blueprint $table) {
                $table->dropIndex('user_chat_participants_user_chat_id_status_user_id_index');
                $table->dropColumn('status');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }
}
