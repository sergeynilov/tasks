<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTaskAssignedToUsersIsLeaderStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('task_assigned_to_users', function (Blueprint $table) {
                $table->enum('status', ['A', 'C', 'N'])->comment(' A=>Accepted, C=>Canceled, N-New(Waiting for acception)')->after('user_id')->default('N');
                $table->boolean('is_leader')->default(false)->after('status');
                $table->index(['task_id', 'status', 'is_leader'], 'task_assigned_to_users_task_id_status_is_leader_index');
            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        DB::commit();
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::table('task_assigned_to_users', function (Blueprint $table) {
                $table->dropIndex('task_assigned_to_users_task_id_status_is_leader_index');
                $table->dropColumn('status');
                $table->dropColumn('is_leader');
            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        DB::commit();

    }
}
