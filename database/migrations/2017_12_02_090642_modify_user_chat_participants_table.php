<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserChatParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_participants', function (Blueprint $table) {
                $table->integer('user_chat_id')->unsigned();
                $table->foreign('user_chat_id')->references('id')->on('user_chats')->onDelete('RESTRICT');
                $table->dropColumn('status');
            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_participants', function (Blueprint $table) {
                $table->dropForeign('user_chat_participants_user_chat_id_foreign');
                $table->dropColumn('user_chat_id');
                $table->enum('status', ['A', 'C'])->comment(' A=>Active, C=>Closed');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }
}
