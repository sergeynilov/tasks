<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('user_todos', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned()->default(1);
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');
                $table->string('text', 255);

                $table->enum('priority', ['0', '1', '3', '4', '5'])->default("0")->comment('  0-No, 1-Low, 2-Normal, 3-High, 4-Urgent, 5-Immediate  ');

                $table->integer('task_id')->unsigned()->nullable();
                $table->foreign('task_id')->references('id')->on('tasks')->onDelete('RESTRICT');
                $table->boolean('completed')->default(false);

                $table->timestamp('created_at')->useCurrent();

                $table->unique(['user_id', 'text'], 'user_todos_user_id_text_unique');
                $table->index(['user_id', 'completed'], 'user_todos_user_id_completed');
                $table->index(['created_at'], 'user_todos_created_at_index');
            });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        Artisan::call('db:seed', array('--class' => 'user_todosWithInitData'));
        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();
            Schema::dropIfExists('user_todos');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        DB::commit();
    }
}
