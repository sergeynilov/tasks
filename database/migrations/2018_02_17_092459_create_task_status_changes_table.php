<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskStatusChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('task_status_changes', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('task_id')->unsigned();
                $table->foreign('task_id')->references('id')->on('tasks')->onDelete('RESTRICT');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->enum('prior_status',
                    ['D', 'A', 'C', 'P', 'K', 'O'])->comment('D=>Draft, A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed');
                $table->enum('status',
                    ['D', 'A', 'C', 'P', 'K', 'O'])->comment('D=>Draft, A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed');

                $table->mediumText('info')->nullable();
                $table->timestamp('created_at')->useCurrent();

                $table->index(['created_at'], 'task_status_changes_created_at_index');
                $table->index(['task_id', 'status'], 'task_status_changes_document_task_id_status_at_index');
                $table->index(['user_id', 'status'], 'task_status_changes_document_user_id_status_at_index');

            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        DB::commit();
        /*             Schema::create('task_operations', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('task_assigned_to_user_id')->unsigned()->nullable();
                $table->foreign('task_assigned_to_user_id')->references('id')->on('task_assigned_to_users')->onDelete('RESTRICT');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->enum('prior_status', ['A','C','P','K','O'])->comment('A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed');
                $table->enum('status', ['A','C','P','K','O'])->comment('A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed');

                $table->mediumText('info')->nullable();
                $table->timestamp('created_at')->useCurrent();

                $table->index(['created_at'], 'task_operations_created_at_index');
                $table->index(['task_id', 'status'], 'task_operations_document_task_id_status_at_index');
                $table->index(['user_id', 'status'], 'task_operations_document_user_id_status_at_index');
            });

 */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::dropIfExists('task_status_changes');

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
        DB::commit();

    }
}
