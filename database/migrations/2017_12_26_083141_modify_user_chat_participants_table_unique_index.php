<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserChatParticipantsTableUniqueIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_participants', function (Blueprint $table) {
                $table->unique(['user_id', 'user_chat_id'], 'user_chat_participants_user_id_user_chat_id_unique');

            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_participants', function (Blueprint $table) {
                $table->dropUnique('user_chat_participants_user_id_user_chat_id_unique');
            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

    }
}
