<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserChatMessagesAddMessageType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_messages', function (Blueprint $table) {
                $table->enum('message_type', ['T', 'U'])->comment(' N=>Text added , U=>Files uploaded')->after('text')->default('T');
                $table->index(['message_type', 'user_id'], 'user_chat_messages_user_chat_id_message_type_index');
            });
            
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_messages', function (Blueprint $table) {
                $table->dropIndex('user_chat_messages_user_chat_id_message_type_index');
                $table->dropColumn('message_type');
            });

        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
