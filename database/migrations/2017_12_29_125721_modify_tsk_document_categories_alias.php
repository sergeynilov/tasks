<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTskDocumentCategoriesAlias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();
            Schema::table('document_categories', function (Blueprint $table) {
                $table->string('alias', 255)->after('name')->nullable()->unique();
            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::table('document_categories', function (Blueprint $table) {
                $table->dropColumn('alias');
            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }
}
