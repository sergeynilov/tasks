<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersTableTimestamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name',50)->nullable();
            $table->string('last_name',50)->nullable();
            $table->string('phone',50)->nullable();
            $table->string('website',50)->nullable();
            $table->dropColumn('login');

            $table->date('contract_start')->nullable();
            $table->date('contract_end')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();

            $table->index(['created_at'], 'users_created_at_index');
            $table->index(['status','contract_start'], 'users_status_contract_start_index');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('login',50)->nullable();
            $table->dropIndex('users_created_at_index');
            $table->dropIndex('users_status_contract_start_index');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('phone');
            $table->dropColumn('website');
            $table->dropColumn('contract_start');
            $table->dropColumn('contract_end');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
