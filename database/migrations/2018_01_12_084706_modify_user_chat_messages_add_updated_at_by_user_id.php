<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserChatMessagesAddUpdatedAtByUserId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_messages', function (Blueprint $table) {
                $table->integer('updated_at_by_user_id')->unsigned()->nullable();
                $table->foreign('updated_at_by_user_id')->references('id')->on('users')->onDelete('RESTRICT');
            });

        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_messages', function (Blueprint $table) {
                $table->dropForeign('user_chat_messages_updated_at_by_user_id_foreign');
                $table->dropColumn('updated_at_by_user_id');
            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
    }
}
