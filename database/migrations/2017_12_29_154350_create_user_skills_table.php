<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('user_skills', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->string('skill', 100);
                $table->tinyInteger('rating')->default(0);
                $table->timestamp('created_at')->useCurrent();

                $table->index(['created_at'], 'user_skills_created_at_index');
                $table->index(['user_id', 'skill'], 'user_skills_user_id_skill_index');
            });
            Artisan::call('db:seed', array('--class' => 'UserSkillsWithInitData'));

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::dropIfExists('user_skills');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

    }
}
