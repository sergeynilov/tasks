<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChatParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::create('user_chat_participants', function (Blueprint $table) {
            $table->increments('id');

                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

                $table->enum('status', ['N', 'O'])->comment(' N=>Online, O=>Offline');

                $table->timestamp('created_at')->useCurrent();

                $table->index(['created_at'], 'user_chat_participants_created_at_index');
                $table->index(['status', 'user_id'], 'user_chat_participants_status_user_index');

        });
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::dropIfExists('user_chat_participants');
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }
    }
}
