<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserChatMessagesTableUpdatedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_messages', function (Blueprint $table) {
                $table->timestamp('updated_at')->nullable();
            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            DB::beginTransaction();

            Schema::table('user_chat_messages', function (Blueprint $table) {
                $table->dropColumn('updated_at');
            });

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

    }
}
