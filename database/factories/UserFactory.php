<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

//    echo '<pre>11::'.print_r($faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = null),true).'</pre>';
    $contract_start= $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = null);
    $contract_end= $faker->dateTimeBetween($startDate = 'now', $endDate = ' 2 years', $timezone = null);

    $first_name = $faker->firstName;
    $last_name = $faker->lastName;
    $username= $first_name.' '.$last_name;
    return [
        'name' => $username,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'status' => 'A',
        'first_name' => $first_name,
        'last_name' => $last_name,
//        'first_name' => 'Firstname'.str_random(5),
//        'last_name' => 'Lastname'.str_random(5),
        'phone' => $faker->tollFreePhoneNumber,
//        'phone' => 'phone'.str_random(5),
        // $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
        'website' => str_slug($username).'.com',
        'contract_start' => $contract_start->format('Y-m-d H:i:s'),
        'contract_end' => $contract_end->format('Y-m-d H:i:s'),
    ];
});


/*
                DB::update( " UPDATE ".DB::getTablePrefix()."users SET first_name='Firstname4', last_name='Lastname4', phone='phone # 4', website='firstname_website_4',  ".
                " contract_start='2017-02-23', contract_end= '2018-09-23',	updated_at= '2018-02-28 08:49:32' where id= 4");


 As you can see, the define method being called on the $factory object takes in two parameters. The first one is an identifier
(model FQN), used to later reference the factory. The second parameter is a closure which
takes in Faker\Generator class and returns an array of users.

 *  $factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
         'username' => $faker->userName,
        'email' => $faker->email,
        'name' => $faker->name
    ];
}); */