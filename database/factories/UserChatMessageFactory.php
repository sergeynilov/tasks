<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\UserChatMessage::class, function ($faker, $parentUser) {
    $r= rand(1,10);
    $is_top= $r<= 2;
//    echo '<pre>$parentUser::'.print_r($parentUser,true).'</pre>';
    return [
//        'user_id' => $parentUser->id,
//        'user_id' => function () {
//            return factory(App\User::class)->create()->id;
//        },
        'user_chat_id' => rand(1,4),


        'is_top' =>   $is_top,
        'text' =>     $faker->text,
        'message_type' =>     'T',

//        'user_type' => function (array $post) {
//            return App\User::find($post['user_id'])->type;
//        }
    ];
});
/* CREATE TABLE `tsk_user_chat_messages` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	    `user_id` INT(10) UNSIGNED NOT NULL,
	    `user_chat_id` INT(10) UNSIGNED NOT NULL,
	  `is_top` TINYINT(1) NOT NULL DEFAULT '0',
	    `text` MEDIUMTEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`message_type` ENUM('T','U') NOT NULL DEFAULT 'T' COMMENT ' N=>Text added , U=>Files uploaded' COLLATE 'utf8mb4_unicode_ci',
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at_by_user_id` INT(10) UNSIGNED NULL DEFAULT NULL, */

/*$factory->define(App\UserChatMessage::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name->unique(),
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});*/


/*  As you can see, the define method being called on the $factory object takes in two parameters. The first one is an identifier
(model FQN), used to later reference the factory. The second parameter is a closure which
takes in Faker\Generator class and returns an array of users.

 *  $factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
         'username' => $faker->userName,
        'email' => $faker->email,
        'name' => $faker->name
    ];
}); */


