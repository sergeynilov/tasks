<?php

use Illuminate\Database\Seeder;

class user_weather_citiesWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('user_weather_locations')->insert([
            'user_id'  => 5,
            'location' => 'London,uk',
            'location_type' => 'C',
        ]);
        DB::table('user_weather_locations')->insert([
            'user_id'  => 5,
            'location' => 'Seattle,us',
            'location_type' => 'C',
        ]);
        DB::table('user_weather_locations')->insert([
            'user_id'  => 5,
            'location' => 'Ivano-Frankivsk,ua',
            'location_type' => 'C',
        ]);
        DB::table('user_weather_locations')->insert([
            'user_id'  => 5,
            'location' => 'Kiev,ua',
            'location_type' => 'C',
        ]);


    }
}
