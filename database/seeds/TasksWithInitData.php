<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class TasksWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            'name' => 'Mastering Laravel/vue.js',
            'creator_id' => 1,
            'category_id' => 4,
            'priority' => '3',
            'status' => 'P',
            'date_start' => now(),
            'date_end' => Carbon::now()->addMonths(2),
            'needs_reports' => '4',
        ]);
        DB::table('tasks')->insert([
            'name' => 'Develop Tasks management site using Laravel/vue.js',
            'creator_id' => 2,
            'category_id' => 5,
            'priority' => '4',
            'status' => 'A',
            'date_start' => now(),
            'date_end' => Carbon::now()->addMonths(4),
            'needs_reports' => '5',
        ]);

    }
}
