<?php

use Illuminate\Database\Seeder;

class CategoriesWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'id'=> 1,
            'name' => 'PHP Development',
            'description' => 'PHP Development description...',
            'parent_id' => null,
        ]);

        DB::table('categories')->insert([
            'id'=> 2,
            'name' => 'Wordpress Development',
            'description' => 'Wordpress Development description...',
            'parent_id' => 1,
        ]);

        DB::table('categories')->insert([
            'id'=> 3,
            'name' => 'Joomla Development',
            'description' => 'Joomla Development description...',
            'parent_id' => 1,
        ]);

        DB::table('categories')->insert([
            'id'=> 4,
            'name' => 'Laravel Development',
            'description' => 'Laravel Development description...',
            'parent_id' => 1,
        ]);


        DB::table('categories')->insert([
            'id'=> 5,
            'name' => 'Laravel/vue.js',
            'description' => 'Laravel/vue.js Development description...',
            'parent_id' => 4,
        ]);
        DB::table('categories')->insert([
            'id'=> 6,
            'name' => 'Laravel/angular.js',
            'description' => 'Laravel/angular.js Development description...',
            'parent_id' => 4,
        ]);
        DB::table('categories')->insert([
            'id'=> 7,
            'name' => 'Laravel/react.js',
            'description' => 'Laravel/react.js Development description...',
            'parent_id' => 4,
        ]);

    }
}
