<?php

namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\Task;
use App\UserTaskType;
use App\library\ListingReturnData;

class TaskAssignedToUser extends MyAppModel
{

    protected $fillable = ['user_id', 'status', 'is_leader', 'task_id', 'user_task_type_id', 'description'];


    protected $table = 'task_assigned_to_users';
    protected $primaryKey = 'id';
    public $timestamps = false;         // 'A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed' COLLATE 'utf8mb4_unicode_ci',
    private static $taskAssignedToUserStatusLabelValueArray = Array(
        'D' => 'Draft',
        'A' => 'Assigning',
        'C' => 'Cancelled',
        'P' => 'Processing',
        'K' => 'Checking',
        'O' => 'Completed'
    );


    public function task()
    {
        return $this->belongsTo('App\Task', 'task_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public static function getTaskAssignedToUserStatusValueArray($key_return = true, $statusLimitArray = []): array
    {
        $resArray = [];
        foreach (self::$taskAssignedToUserStatusLabelValueArray as $key => $value) {
            if ( ! empty($statusLimitArray)) {
                if ( ! in_array($key, $statusLimitArray)) {
                    continue;
                }
            }
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }

        return $resArray;
    }

    public static function getTaskAssignedToUserStatusLabel(string $status): string
    {
        if ( ! empty(self::$taskAssignedToUserStatusLabelValueArray[$status])) {
            return self::$taskAssignedToUserStatusLabelValueArray[$status];
        }

        return '';
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getTaskAssignedToUsersList(int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param = 0)
    {
        if (empty($order_by)) {
            $order_by = 'tau.created_at';
        } // set default ordering
        if (empty($order_direction)) {
            $order_direction = 'asc';
        }
        $limit = ! empty($filtersArray['limit']) ? $filtersArray['limit'] : '';

        $task_assigned_to_user_table_name = with(new TaskAssignedToUser)->getTableName();
        $quoteModel                       = TaskAssignedToUser::from(\DB::raw(DB::getTablePrefix() . $task_assigned_to_user_table_name . ' as tau'));
        if ($listingReturnData != ListingReturnData::ROWS_COUNT) { // getting rows numbers do not need $order_by/$order_direction parameters

            if ($order_by == 'task_priority_task_date_end') {
                $quoteModel->orderBy('task_priority', 'desc');
                $quoteModel->orderBy('task_date_end', 'desc');
            } else {
                $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
            }
        }

        $additive_fields_for_select = "";
        $fields_for_select          = 'tau.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if ( ! empty($filtersArray['user_id'])) {
            $quoteModel->where(\DB::raw('tau.user_id'), '=', $filtersArray['user_id']);
        }

        if ( ! empty($filtersArray['task_id'])) {
            $quoteModel->where(\DB::raw('tau.task_id'), '=', $filtersArray['task_id']);
        }

        if (isset($filtersArray['is_leader']) and strlen($filtersArray['is_leader']) > 0) {
            $quoteModel->where(\DB::raw('tau.is_leader'), '=', $filtersArray['is_leader']);
        }

        if ( ! empty($filtersArray['status'])) {
            $quoteModel->where(\DB::raw('tau.status'), '=', $filtersArray['status']);
        }

        if ( ! empty($filtersArray['user_task_type_id'])) {
            $quoteModel->where(\DB::raw('tau.user_task_type_id'), '=', $filtersArray['user_task_type_id']);
        }

        if ( ! empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw(\DB::raw("tau.created_at >='") . $filtersArray['created_at_from'] . "'");
        }
        if ( ! empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw(\DB::raw("tau.created_at <='") . $filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty($limit) and (int)$limit > 0) {
            $quoteModel = $quoteModel->take($limit);
        }
        if ($listingReturnData == ListingReturnData::ROWS_COUNT) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        if ( ! empty($filtersArray['show_username'])) { // need to join in select sql username and user_status field of author of t item
            $users_table_name           = DB::getTablePrefix() . (with(new User)->getTableName());
            $additive_fields_for_select .= ', u.name as username, u.first_name as user_first_name, u.last_name as user_last_name, u.status as  user_status';
            $quoteModel->join(\DB::raw($users_table_name . ' as u '), \DB::raw('u.id'), '=', \DB::raw('tau.user_id'));
        } // if ( !empty($filtersArray['show_username'])  ) { // need to join in select sql username and user_status field of author of t item


        if ( ! empty($filtersArray['show_tasks_info'])) { // need to join in select sql task name
            $tasks_table_name           = DB::getTablePrefix() . (with(new Task)->getTableName());
            $additive_fields_for_select .= ', t.name as task_name, t.status as task_status, t.date_start as task_date_start, t.date_end as task_date_end, t.priority as task_priority';
            $quoteModel->join(\DB::raw($tasks_table_name . ' as t '), \DB::raw('t.id'), '=', \DB::raw('tau.task_id'));

        } // if ( !empty($filtersArray[''show_tasks_info''])  ) { // need to join in select sql username and user_status field of author of t item

        if ( ! empty($filtersArray['show_user_task_type_name'])) { // need to join in select sql user_task_type_name
            $user_task_types_table_name = DB::getTablePrefix() . (with(new UserTaskType)->getTableName());
            $additive_fields_for_select .= ', utt.name as user_task_type_name';
            $quoteModel->join(\DB::raw($user_task_types_table_name . ' as utt '), \DB::raw('utt.id'), '=', \DB::raw('tau.user_task_type_id'));

        } // if ( !empty($filtersArray[''show_user_task_type_name''])  ) { // need to join in select sql username and user_status field of author of t item


        $fields_for_select .= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page    = with(new TaskAssignedToUser)->getItemsPerPage();
        $quoteModel->select(\DB::raw($fields_for_select));
        $data_retrieved = false;
        if ($listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and ( ! empty($page_param) and with(new TaskAssignedToUser)->isPositiveNumeric($page_param))) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $taskAssignedToUsersList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved          = true;
        }

        if ($listingReturnData == ListingReturnData::PAGINATION_BY_URL and ! $data_retrieved) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $taskAssignedToUsersList = $quoteModel->paginate($items_per_page);
            $data_retrieved          = true;
        }

        if ( ! $data_retrieved) {
            $taskAssignedToUsersList = $quoteModel->get();
            $data_retrieved          = true;
        }
        foreach ($taskAssignedToUsersList as $next_key => $nextTaskAssignedToUser) { /* map all retrieved data when need to set human readable labels for some fields */
            if ( ! empty($filtersArray['fill_labels'])) {
                $nextTaskAssignedToUser['status_label']          = with(new TaskAssignedToUser)->getTaskAssignedToUserStatusLabel($nextTaskAssignedToUser->status);
                $nextTaskAssignedToUser['is_leader_label']       = $nextTaskAssignedToUser->is_leader ? 'Task Leader' : '';
                $nextTaskAssignedToUser['created_at_label']      = with(new TaskAssignedToUser)->getFormattedDateTime($nextTaskAssignedToUser->created_at);
                $nextTaskAssignedToUser['task_priority_label']   = with(new Task)->getTaskPriorityLabel($nextTaskAssignedToUser->task_priority);
                $nextTaskAssignedToUser['task_date_start_label'] = with(new TaskAssignedToUser)->getFormattedDate($nextTaskAssignedToUser->task_date_start);
                $nextTaskAssignedToUser['task_date_end_label']   = with(new TaskAssignedToUser)->getFormattedDate($nextTaskAssignedToUser->task_date_end);
                if ( ! empty($nextTaskAssignedToUser['user_status'])) {
                    $taskAssignedToUsersList[$next_key]['user_status_label'] = User::getUserStatusLabel($nextTaskAssignedToUser->user_status);
                }
                if ( ! empty($nextTaskAssignedToUser['task_status'])) {
                    $taskAssignedToUsersList[$next_key]['task_status_label'] = Task::getTaskStatusLabel($nextTaskAssignedToUser->task_status);
                }
            }
        }

        return $taskAssignedToUsersList;

    } // public static function getTaskAssignedToUsersList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getTaskAssignedToUsersAsTeamLeaderList(int $user_id)
    {
        /* select tau.* , t.name as task_name, t.status as task_status, t.date_start as task_date_start, t.date_end as task_date_end, t.priority as task_priority
  from tsk_task_assigned_to_users as tau
  inner join tsk_tasks as t  on t.id = tau.task_id where
  tau.task_id in (  select tau2.task_id from tsk_task_assigned_to_users as tau2 where tau2.user_id = '5'  and  tau2.is_leader = '1'  )
  order by `task_priority` desc, `task_date_end` desc
 */
        $dataList = DB::select(" select tau.* , t.name as task_name, t.status as task_status, t.date_start as task_date_start, t.date_end as task_date_end, t.priority as task_priority
  from " . DB::getTablePrefix() . "task_assigned_to_users as tau
  inner join " . DB::getTablePrefix() . "tasks as t  on t.id = tau.task_id where
  tau.task_id in (  select tau2.task_id from " . DB::getTablePrefix() . "task_assigned_to_users as tau2 where tau2.user_id = ?  and  tau2.is_leader = '1'  )
  order by `task_priority` desc, `task_date_end` desc ", [$user_id]);
        $taskAssignedToUsersList= [];
        foreach ($dataList as $next_key => $nextTaskAssignedToUser) { /* map all retrieved data when need to set human readable labels for some fields */
            $nextTaskAssignedToUser= (array)$nextTaskAssignedToUser;
//            dump($nextTaskAssignedToUser);
//            die("-1 XXZ");
            $nextTaskAssignedToUser['status_label']          = with(new TaskAssignedToUser)->getTaskAssignedToUserStatusLabel($nextTaskAssignedToUser['status']);
            $nextTaskAssignedToUser['is_leader_label']       = $nextTaskAssignedToUser['is_leader'] ? 'Task Leader' : '';
            $nextTaskAssignedToUser['created_at_label']      = with(new TaskAssignedToUser)->getFormattedDateTime($nextTaskAssignedToUser['created_at']);
            $nextTaskAssignedToUser['task_priority_label']   = with(new Task)->getTaskPriorityLabel($nextTaskAssignedToUser['task_priority']);
            $nextTaskAssignedToUser['task_date_start_label'] = with(new TaskAssignedToUser)->getFormattedDate($nextTaskAssignedToUser['task_date_start']);
            $nextTaskAssignedToUser['task_date_end_label']   = with(new TaskAssignedToUser)->getFormattedDate($nextTaskAssignedToUser['task_date_end']);
            if ( ! empty($nextTaskAssignedToUser['user_status'])) {
                $nextTaskAssignedToUser['user_status_label'] = User::getUserStatusLabel($nextTaskAssignedToUser['user_status']);
            }
            if ( ! empty($nextTaskAssignedToUser['task_status'])) {
                $nextTaskAssignedToUser['task_status_label'] = Task::getTaskStatusLabel($nextTaskAssignedToUser['task_status']);
            }
            $taskAssignedToUsersList[]= $nextTaskAssignedToUser;
        }
        return $taskAssignedToUsersList;
    } // public static function getTaskAssignedToUsersAsTeamLeaderList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById(int $id, array $additiveParams = [])
    {
        if (empty($id)) {
            return false;
        }
        $is_row_retrieved = false;

        if ( ! empty($additiveParams['show_user_name'])) { // need to join in select sql username and user_status field of author of user Skill
            $additive_fields_for_select       = "";
            $fields_for_select                = 'tau.*';
            $task_assigned_to_user_table_name = with(new TaskAssignedToUser)->getTableName();
            $quoteModel                       = TaskAssignedToUser::from(\DB::raw(DB::getTablePrefix() . $task_assigned_to_user_table_name . ' as tau'));
            $quoteModel->where(\DB::raw('tau.id'), '=', $id);
            $users_table_name           = DB::getTablePrefix() . (with(new User)->getTableName());
            $additive_fields_for_select .= ', u.name as user_name,u.last_name as user_last_name,u.first_name as user_first_name, u.status as  user_status';
            $quoteModel->join(\DB::raw($users_table_name . ' as u '), \DB::raw('u.id'), '=', \DB::raw('tau.user_id'));
            $fields_for_select .= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select(\DB::raw($fields_for_select));
            $taskAssignedToUserRows = $quoteModel->get();
            if ( ! empty($taskAssignedToUserRows[0]) and get_class($taskAssignedToUserRows[0]) == 'App\TaskAssignedToUser') {
                $is_row_retrieved   = true;
                $taskAssignedToUser = $taskAssignedToUserRows[0];
            }
        } // if (!empty($additiveParams['show_user_name']) ) { // need to join in select sql username and user_status field of author of user Skill

        if ( ! $is_row_retrieved) {
            $taskAssignedToUser = TaskAssignedToUser::find($id);
        }

        if (empty($taskAssignedToUser)) {
            return false;
        }
        if ( ! empty($additiveParams['fill_labels'])) {
            $taskAssignedToUser['created_at_label'] = with(new TaskAssignedToUser)->getFormattedDateTime($taskAssignedToUser->created_at);
            $taskAssignedToUser['status_label']     = with(new TaskAssignedToUser)->getTaskAssignedToUserStatusLabel($taskAssignedToUser->status);
        }

        return $taskAssignedToUser;
    } // public function getRowById( int $id, array $additiveParams= [] )

    /*    public static function getRowByTaskIdAndUserId( int $task_id= null, int $user_id= null, $show_labels= false )
        {
            if (empty($task_id) or empty($user_id)) return false;
            $task_assigned_to_user_table_name= with(new TaskAssignedToUser)->getTableName();
            $quoteModel= TaskAssignedToUser::from(  \DB::raw(DB::getTablePrefix().$task_assigned_to_user_table_name.' as tau' ));
            $quoteModel->where( \DB::raw('tau.user_id'), '=', $user_id );
            $quoteModel->where( \DB::raw('tau.task_id'), '=', $task_id );
            $taskAssignedToUsersList = $quoteModel->get();
            if (empty($taskAssignedToUsersList[0])) return false;
            $taskAssignedToUser= $taskAssignedToUsersList[0];
    //        if (!empty($additiveParams['fill_labels'])) {
    //            $taskAssignedToUser['created_at_label']= with(new TaskAssignedToUser)->getFormattedDateTime($taskAssignedToUser->created_at);
    //            $taskAssignedToUser['status_label']= with(new TaskAssignedToUser)->getTaskAssignedToUserStatusLabel($taskAssignedToUser->status);
    //        }
            $additiveParams= $show_labels ? ['fill_labels'=>1, 'show_user_name'=>1  ] : [];
            return TaskAssignedToUser::getRowById($taskAssignedToUser->id, $additiveParams);
        } // public static function getRowByTaskIdAndUserId( int $task_id= null, int $user_id= null )*/

    public static function getTeamLeaderOfTask(int $task_id = null)
    {
        $assigningTasksListing = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, ['task_id' => $task_id, 'is_leader' => 1, 'show_username' => 1]);

        return ! empty($assigningTasksListing[0]) ? $assigningTasksListing[0] : null;
    } // public static function getRowByTaskIdAndUserId( int $task_id= null, int $user_id= null )

    public static function getValidationRulesArray($task_assigned_to_user_id): array
    {
        $validationRulesArray = [
            'user_id' => 'required|exists:' . (with(new User)->getTableName()) . ',id',
            'task_id' => 'required|exists:' . (with(new UserChat)->getTableName()) . ',id',
            'status'  => 'required|in:' . with(new TaskAssignedToUser)->getValueLabelKeys(TaskAssignedToUser::getTaskAssignedToUserStatusValueArray(false)),
        ];

        return $validationRulesArray;
    }

}

