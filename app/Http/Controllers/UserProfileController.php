<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Settings;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;

use App\User;
use App\UserProfiles;
use App\DocumentCategory;
use App\UserProfileDocument;
use App\UserSkill;
use App\UserChatMessage;
use App\TaskAssignedToUser;
use App\UsersGroups;
use App\Http\Requests\UserProfilesRequest;
use App\Http\Requests\UserProfileDocumentRequest;


class UserProfileController extends MyAppController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


//        $ret= preg_match('~^#(([0-9a-fA-F]{2}){3}|([0-9a-fA-F]){3})$~', preg_quote ('#004300'), $matches, PREG_OFFSET_CAPTURE);
//        echo '<pre>$ret::'.print_r($ret,true).'</pre>';
//        echo '<pre>$matches::'.print_r($matches,true).'</pre>';
//        $ret= preg_match('/(foo)(bar)(baz)/', 'foobarbaz', $matches, PREG_OFFSET_CAPTURE);
//        echo '<pre>$ret::'.print_r($ret,true).'</pre>';
//        echo '<pre>$matches::'.print_r($matches,true).'</pre>';
//        die("-1 XXZ");
//        <pre>$additional_check_color_validation_rule::regex:~^#(([0-9a-fA-F]{2}){3}|([0-9a-fA-F]){3})$~</pre><pre>$validationRulesArray::Array
//(
//[first_name] => nullable|max:50|regex:~^[a-zA-Z]*$~
//    [last_name] => nullable|max:50|regex:~^[a-zA-Z]*$~
//    [phone] => required|max:50
//    [website] => required|max:50
//    [color] => required|regex:~^#(([0-9a-fA-F]{2}){3}|([0-9a-fA-F]){3})$~
//    [background_color] => required|regex:~^#(([0-9a-fA-F]{2}){3}|([0-9a-fA-F]){3})$~
//    [lang] => required|
//              [submit_message_by_enter] => required|
//)
//
//#004300

        
        $this->addVariablesToJS();
        return view('admin.user_profiles.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserProfile $userProfile
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $loggedUser = Auth::user();
        try {


            $user = User::getRowById($loggedUser->id, ['fill_labels' => 1, 'show_thumbnail_image'=> 1]);
            if ($user == null) {
                return response()->json(['error_code' => 11, 'message' => 'User # "' . $loggedUser->id . '" not found!', 'user' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $langsArray                 = [
                ['key' => 'en', 'label' => 'English', 'locale' => 'en_US.utf8'],
                ['key' => 'ru', 'label' => 'Русский', 'locale' => 'ru_RU.utf8']
            ];
            $submitMessageByEntersArray = $this->getYesNoValueArray('mysql',true); // submit_message_by_enter
            $documentPublicAccessesArray = UserProfileDocument::getUserProfileDocumentPublicAccessValueArray();
            $uploadedDocumentsExtensionsArray= \Config::get('app.uploaded_documents_extensions');
            $userSkillOptionsValueArray= UserProfiles::getUserSkillOptionsValueArray();
            $users_groups_text= UsersGroups::getUsersGroupsTextLabel($loggedUser->id,true);


            $profileThumbnailImageDocumentCategory= DocumentCategory::getSimilarDocumentCategoryByAlias('profile_thumbnail_image');
            $user_profile_thumbnail_image_url= '';
//            echo '<pre>$profileThumbnailImageDocumentCategory::'.print_r($profileThumbnailImageDocumentCategory,true).'</pre>';
            if ( $profileThumbnailImageDocumentCategory!= null ) {
                $userProfileDocument= UserProfileDocument::getUserProfileDocumentsList( ListingReturnData::LISTING,
                    ['document_category_id'=>$profileThumbnailImageDocumentCategory->id, 'user_id'=>$loggedUser->id] );
//                echo '<pre>$userProfileDocument[0]::'.print_r($userProfileDocument[0],true).'</pre>';
                if ( !empty($userProfileDocument[0]) ) {
                    $base_url= self::getBaseUrl();
                    $next_user_profile_document_filename= UserProfileDocument::getUserProfileDocumentPath($userProfileDocument[0]->user_id,$userProfileDocument[0]->filename,  false);
                    $next_user_profile_document_url= $base_url.Storage::disk('local')->url($next_user_profile_document_filename );
//                    echo '<pre>$::'.print_r($next_user_profile_document_filename,true).'</pre>';
                    $file_exists = Storage::disk('local')->exists('public/'.$next_user_profile_document_filename);
//                    echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
                    if ( $file_exists ) {
                        $user_profile_thumbnail_image_url= $next_user_profile_document_url;
                    }
                }
            }
//            echo '<pre>$user_profile_thumbnail_image_url::'.print_r($user_profile_thumbnail_image_url,true).'</pre>';
//            die("-1 XXZ----");


            $documentCategoriesList= DocumentCategory::getDocumentCategoriesSelectionList( true, ['type'=>['P', 'M', 'T']] );

            $userProfileList            = UserProfiles::getUserProfilesList(['user_id' => $loggedUser->id], true);
            foreach ($userProfileList as $next_key => $next_value) {
                if ( in_array($next_key, ['subscription_to_newsletters','show_online_status']) ) {
                    $user[$next_key]= (strtoupper($next_value) == 'Y'?"Y":"");
                } else {
                    $user[$next_key] = $next_value;
                }
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code'                      => 1,
                'message'                         => $e->getMessage(),
                'user'                            => null,
                'langsArray'                      => $langsArray,
                'submitMessageByEntersArray'      => $submitMessageByEntersArray,
                'documentPublicAccessesArray'     => $documentPublicAccessesArray,
                'uploadedDocumentsExtensionsArray'=> $uploadedDocumentsExtensionsArray,
                'userSkillOptionsValueArray'      => $userSkillOptionsValueArray,
                'users_groups_text'               => $users_groups_text,
                'documentCategoriesList'          => $documentCategoriesList,
                'user_profile_thumbnail_image_url'=> $user_profile_thumbnail_image_url,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'user'                                => $user,
            'langsArray'                          => $langsArray,
            'submitMessageByEntersArray'          => $submitMessageByEntersArray,
            'documentPublicAccessesArray'         => $documentPublicAccessesArray,
            'uploadedDocumentsExtensionsArray'    => $uploadedDocumentsExtensionsArray,
            'userSkillOptionsValueArray'          => $userSkillOptionsValueArray,
            'users_groups_text'                   => $users_groups_text,
            'documentCategoriesList'              => $documentCategoriesList,
            'user_profile_thumbnail_image_url'    => $user_profile_thumbnail_image_url,
        ], HTTP_RESPONSE_OK);

    } // public function show()


    /////// USER PROFILE DOCUMENTS BLOCK START ////////

    public function load_user_profile_documents() {
        $public_access        = $this->getParameter('public_access', '');
        $loggedUser = Auth::user();
        try {
            $order_by = 'upd.id';
            $order_direction = 'desc';
            $userProfileDocumentsList= UserProfileDocument::getUserProfileDocumentsList( ListingReturnData::LISTING, [ 'user_id'=>$loggedUser->id,
                'document_category_type'=>['P','M','T'], 'public_access'=> $public_access, 'show_image'=> 1, 'show_image_info'=> 1, 'fill_labels'=> 1, 'show_document_category_name'=>1 ],
                $order_by,
                $order_direction );
        } catch (Exception $e) {
            return response()->json([
                'error_code'                      => 1,
                'message'                         => $e->getMessage(),
                'userProfileDocumentsList'        => null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );

        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'userProfileDocumentsList'            => $userProfileDocumentsList,
        ], HTTP_RESPONSE_OK);


    }

    /////// USER PROFILE DOCUMENTS BLOCK END ////////


    public function load_user_skills() {
        $loggedUser = Auth::user();

        $userSkillsArray= [];
        $get_all_skills        = $this->getParameter('get_all_skills', '');
        try {
            $order_by = 'us.rating';
            $order_direction = 'desc';
            $userSkillsList= UserSkill::getUserSkillsList( ListingReturnData::LISTING, [ 'skill'=>'', 'user_id'=>$loggedUser->id ], $order_by, $order_direction );
            if ( $get_all_skills ) {
                $distinctUserSkillsList= UserSkill::getDistinctUserSkillsList('');
                foreach( $distinctUserSkillsList as $nextDistinctUserSkill ) {
                    $rating= 0;
                    foreach( $userSkillsList as $next_key=>$nextUserSkill ) {
                        if ( $nextDistinctUserSkill->skill == $nextUserSkill->skill ) {
                            $rating= $nextUserSkill->rating;
                        }
                    }
                    $userSkillsArray[]= [ 'skill'=> $nextDistinctUserSkill->skill, 'rating'=> $rating ];
                }
                $userSkillsList= $userSkillsArray;
            }
        } catch (Exception $e) {
            return response()->json([
                'error_code'                      => 1,
                'message'                         => $e->getMessage(),
                'userSkillsList'        => null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );

        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'userSkillsList'                      => $userSkillsList,
        ], HTTP_RESPONSE_OK);

    }

    public function load_task_assigned_to_users_lists() {
        $loggedUser = Auth::user();
        try {
            $order_by = 'task_priority_task_date_end';
            $order_direction = 'desc';
            $taskAssignedToUsersList= TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::LISTING, [ 'user_id'=>$loggedUser->id, 'show_tasks_info'=> 1, 'fill_labels'=>1,
                'show_user_task_type_name'=> 1 ],  $order_by,    $order_direction );
        } catch (Exception $e) {
            return response()->json([
                'error_code'                      => 1,
                'message'                         => $e->getMessage(),
                'taskAssignedToUsersList'        => null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'taskAssignedToUsersList'            => $taskAssignedToUsersList,
        ], HTTP_RESPONSE_OK);


    } // public function load_task_assigned_to_users_lists() {


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserProfile $userProfile
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(UserProfiles $userProfile)
    {
        //
    }

    public function user_skills_update( )
    {
        $loggedUser = Auth::user();
        if ($loggedUser == null) {
            return response()->json( [
                'error_code' => 11,
                'message'    => 'User not found!',
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR );
        }
        try {
            DB::beginTransaction();

            $userSkillsList= UserSkill::getUserSkillsList( ListingReturnData::LISTING, [ 'user_id'=>$loggedUser->id ] );
            foreach( $userSkillsList as $next_key=>$nextUserSkill ) { // delete existing user skills
                $nextUserSkill->delete();
            }
            $userSkillsUpdateArray    = request()->all();
            foreach( $userSkillsUpdateArray as $nextUserSkill ) {
                $a= $this->pregSplit('~user_skill_~',$nextUserSkill['skill']);
                if ( count($a) == 1 and (int)$nextUserSkill['rating'] > 0 ) {
                    $newUserSkill           = new UserSkill();
                    $newUserSkill->user_id  = $loggedUser->id;
                    $newUserSkill->skill    = $a[0];
                    $newUserSkill->rating   = $nextUserSkill['rating'];
                    $newUserSkill->save();
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'user' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $this->setFlashMessage("User '" . $loggedUser->name . " was updated!", 'success');  // there was NO error : flash success message
        return ['error_code' => 0, 'message' => '', 'user' => $loggedUser];
    } //public function user_skills_update($request)

    public function update(UserProfilesRequest $request)
    {
        $loggedUser = Auth::user();
        $id         = $request->id;
        $user       = User::find($id);
        $site_name  = Settings::getValue('site_name', '');
        if ($user == null) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'User\'s profile # "' . $id . '" not found!',
                'user'       => (object)[
                    'name'        => 'User # "' . $id . '" not
            # found!',
                    'description' => ''
                ]
            ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $userDataArray    = $request->all();
//            echo '<pre>$userDataArray::'.print_r($userDataArray,true).'</pre>';
            $user->first_name = $userDataArray['first_name'];
            $user->last_name  = $userDataArray['last_name'];
            $user->phone      = $userDataArray['phone'];
            $user->website    = $userDataArray['website'];
            $user->save();

            $lang                           = $userDataArray['lang'];
            $submit_message_by_enter        = $userDataArray['submit_message_by_enter'];

            $subscription_to_newsletters    = (boolean)( isset($userDataArray['subscription_to_newsletters']) ? $userDataArray['subscription_to_newsletters'] : '' );
            $show_online_status             = (boolean)( isset($userDataArray['show_online_status'])?$userDataArray['show_online_status']:'' );


            $color = $userDataArray['color'];
            $background_color = $userDataArray['background_color'];

            $userProfileLang = UserProfiles::getUserProfilesList(['user_id' => $loggedUser->id, 'name' => 'lang'], false);
            if (empty($userProfileLang[0]) or $userProfileLang[0] == null) {
                UserProfiles::create(['user_id' => $loggedUser->id, 'name' => 'lang', 'value' => $lang]);
            } else {
                $userProfileLang[0]->update(['value' => $lang]);
            }

            $userProfileSubmitMessageByEnter = UserProfiles::getUserProfilesList(['user_id' => $loggedUser->id, 'name' => 'submit_message_by_enter'], false);
            if (empty($userProfileSubmitMessageByEnter[0]) or $userProfileSubmitMessageByEnter[0] == null) {
                UserProfiles::create(['user_id' => $loggedUser->id, 'name' => 'submit_message_by_enter', 'value' => $submit_message_by_enter]);
            } else {
                $userProfileSubmitMessageByEnter[0]->update(['value' => $submit_message_by_enter]);
            }

            $userProfileColor = UserProfiles::getUserProfilesList(['user_id' => $loggedUser->id, 'name' => 'color'], false);
            if (empty($userProfileColor[0]) or $userProfileColor[0] == null) {
                UserProfiles::create(['user_id' => $loggedUser->id, 'name' => 'color', 'value' => $color]);
            } else {
                $userProfileColor[0]->update(['value' => $color]);
            }

            $userProfileBackgroundColor = UserProfiles::getUserProfilesList(['user_id' => $loggedUser->id, 'name' => 'background_color'], false);
            if (empty($userProfileBackgroundColor[0]) or $userProfileBackgroundColor[0] == null) {
                UserProfiles::create(['user_id' => $loggedUser->id, 'name' => 'background_color', 'value' => $background_color]);
            } else {
                $userProfileBackgroundColor[0]->update(['value' => $background_color]);
            }


            $userProfileSubscriptionToNewsLetters = UserProfiles::getUserProfilesList(['user_id' => $loggedUser->id, 'name' => 'subscription_to_newsletters'], false);
            if (empty($userProfileSubscriptionToNewsLetters[0]) or $userProfileSubscriptionToNewsLetters[0] == null) {
                UserProfiles::create([ 'user_id' => $loggedUser->id, 'name' => 'subscription_to_newsletters', 'value' => ($subscription_to_newsletters?"Y":"N") ] );
            } else {
                $userProfileSubscriptionToNewsLetters[0]->update(['value' => ($subscription_to_newsletters?"Y":"N")]);
            }

            $userProfileShowOnlineStatus = UserProfiles::getUserProfilesList(['user_id' => $loggedUser->id, 'name' => 'show_online_status'], false);
            if (empty($userProfileShowOnlineStatus[0]) or $userProfileShowOnlineStatus[0] == null) {
                UserProfiles::create(['user_id' => $loggedUser->id, 'name' => 'show_online_status', 'value' => ($show_online_status?"Y":"N")]);
            } else {
                $userProfileShowOnlineStatus[0]->update(['value' => ($show_online_status?"Y":"N")]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'user' => null, 'site_name' => $site_name], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $this->setFlashMessage("User '" . $user->name . " was updated!", 'success');  // there was NO error : flash success message

        return ['error_code' => 0, 'message' => '', 'user' => $user, 'site_name' => $site_name];
//        return response()->json( ['error_code'=> 0, 'message'=> '', 'user'=>$user, 'site_name'=> $site_name], HTTP_RESPONSE_OK_RESOURCE_UPDATED );

    } //public function update(UserProfilesRequest $request)


    public function user_profiles_documents_upload(Request $request) {
        $loggedUser = Auth::user();
        /* <pre>  user_profiles_documents_upload  $itemDocumentPropertiesArray:::<pre>
<pre>  user_profiles_documents_upload  $itemDocumentArray:::<pre> */
        $itemDocumentPropertiesArray = $request->input('uploadedItemDocuments');
        $itemDocumentArray = $request->file('uploadedItemDocuments');
        $this->debToFile($itemDocumentPropertiesArray,'  user_profiles_documents_upload  $itemDocumentPropertiesArray:');
        $this->debToFile($itemDocumentArray,'  user_profiles_documents_upload  $itemDocumentArray:');
        
        try {
            DB::beginTransaction();

            $is_debug= false;
            $index= 0;
            $errors_message_text= '';
            foreach( $itemDocumentArray as $next_key=>$nextAttachmentFile ) { // check all attached files
                $user_profile_documents_filename= $nextAttachmentFile['attached_file']->getClientOriginalName();
                if ($is_debug) echo '<pre>$user_profile_documents_filename::'.print_r($user_profile_documents_filename,true).'</pre>';


                if ($is_debug) echo '<pre>$itemDocumentPropertiesArray[$index*4]::'.print_r($itemDocumentPropertiesArray[$index*4],true).'</pre>';


                
                $document_category_id =  !empty($itemDocumentPropertiesArray[$index*4]['document_category_id']) ?  $itemDocumentPropertiesArray[$index*4]['document_category_id'] : null;
                if ($is_debug) echo '<pre>$document_category_id::'.print_r($document_category_id,true).'</pre>';
                $info                 = !empty($itemDocumentPropertiesArray[$index*4+1]['info']) ? $itemDocumentPropertiesArray[$index*4+1]['info'] : null;
                if ($is_debug) echo '<pre>$info::'.print_r($info,true).'</pre>';

                $public_access        = !empty($itemDocumentPropertiesArray[$index*4+2]['public_access']) ? $itemDocumentPropertiesArray[$index*4+2]['public_access'] : null;
                if ($is_debug) echo '<pre>$public_access::'.print_r($public_access,true).'</pre>';

                $validationRulesArray= UserProfileDocument::getValidationRulesArray();
//                if ($is_debug) echo '<pre>$validationRulesArray::'.print_r($validationRulesArray,true).'</pre>';
                $insertDataArray= [
                    'filename'  => $user_profile_documents_filename,
                    'extension' => $this->getFilenameExtension($user_profile_documents_filename),
                    'document_category_id' => $document_category_id,
                    'public_access'  => $public_access,
                    'info'=> $info ];
//                echo '<pre>$insertDataArray::'.print_r($insertDataArray,true).'</pre>';
                $validator = Validator::make( $insertDataArray, $validationRulesArray);
                if ($validator->fails()) { // if error on validation return errors list
                    $errorsList = UserProfileDocument::getErrorsList($validator);
                    if ($is_debug) echo '<pre>$errorsList::'.print_r($errorsList,true).'</pre>';
                    foreach( $errorsList as $next_fieldname=>$next_error_message ) {
                        $errors_message_text.= (!empty($errors_message_text)?", ":"") . $user_profile_documents_filename . ' : ' . $next_error_message;
                    }
                }
                $index++;
            } // foreach( $itemDocumentArray as $next_key=>$nextAttachmentFile ) { // check all attached files
            
            if (!empty($errors_message_text)) { // there were errors
                return response()->json(['error_code' => 1, 'message' => $errors_message_text], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            reset($itemDocumentPropertiesArray);
            reset($itemDocumentArray);
            $index= 0;
            foreach( $itemDocumentArray as $next_key=>$nextAttachmentFile ) {
//                echo '<pre>$nextAttachmentFile::'.print_r($nextAttachmentFile,true).'</pre>';
                $user_profile_documents_filename= $nextAttachmentFile['attached_file']->getClientOriginalName();
//                echo '<pre>$user_profile_documents_filename::'.print_r($user_profile_documents_filename,true).'</pre>';
                $user_profile_documents_file_path = $nextAttachmentFile['attached_file']->getPathName();
                $dest_filename= UserProfileDocument::getUserProfileDocumentPath($loggedUser->id,$user_profile_documents_filename, true);
//                echo '<pre>$dest_filename::'.print_r($dest_filename,true).'</pre>';
                Storage::disk('local')->put($dest_filename, File::get( $user_profile_documents_file_path ) );
                $userProfileDocument= new UserProfileDocument();
                $userProfileDocument->user_id= $loggedUser->id;
                $userProfileDocument->filename= $user_profile_documents_filename;
                $userProfileDocument->extension= $this->getFilenameExtension($user_profile_documents_filename);


                if ($is_debug) echo '<pre>++$index::'.print_r($index,true).'</pre>';
//                $user_profile_documents_filename= $nextAttachmentFile['attached_file']->getClientOriginalName();
                if ($is_debug) echo '<pre>++$user_profile_documents_filename::'.print_r($user_profile_documents_filename,true).'</pre>';

                $document_category_id =  !empty($itemDocumentPropertiesArray[$index*4]['document_category_id']) ?  $itemDocumentPropertiesArray[$index*4]['document_category_id'] : null;
                if ($is_debug) echo '<pre>++$document_category_id::'.print_r($document_category_id,true).'</pre>';
                $info                 = !empty($itemDocumentPropertiesArray[$index*4+1]['info']) ? $itemDocumentPropertiesArray[$index*4+1]['info'] : null;
                if ($is_debug) echo '<pre>++$info::'.print_r($info,true).'</pre>';

                $public_access        = !empty($itemDocumentPropertiesArray[$index*4+2]['public_access']) ? $itemDocumentPropertiesArray[$index*4+2]['public_access'] : null;
                if ($is_debug) echo '<pre>++$public_access::'.print_r($public_access,true).'</pre>';


                if ($is_debug) echo '<pre>$itemDocumentPropertiesArray::'.print_r($itemDocumentPropertiesArray,true).'</pre>';
                $userProfileDocument->document_category_id=  $document_category_id;
                $userProfileDocument->info=  $info;
                $userProfileDocument->public_access=  $public_access;
                $user_profile_document_id= $userProfileDocument->save();
                $index++;
            }
            DB::commit();

        } catch(\Exception $e) {
//            echo '<pre>$e::'.print_r($e->getMessage(),true).'</pre>';
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'itemDocumentArray'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return [ 'error_code' => 0, 'message' => '', 'itemDocumentArray'=>$itemDocumentArray ];
//        return response()->json(['error_code'=> 0, 'message'=> '', 'attachments_files'=>$itemDocumentArray],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } //public function user_profiles_documents_upload(Request $request) {



    public function user_profile_document_destroy($id)
    {
        $loggedUser = Auth::user();
        try {
            $userProfileDocument = UserProfileDocument::find($id);
            if ( $userProfileDocument == null ) {
                return response()->json(['error_code'=> 1, 'message'=> 'User\'s profile # "'.$id.'" not found!', 'userProfileDocument'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();

            $userProfileDocument->delete();
            $user_profile_document_filename= UserProfileDocument::getUserProfileDocumentPath($loggedUser->id,$userProfileDocument->filename, true);
            UserProfileDocument::deleteFileByPath($user_profile_document_filename, true);
//            Storage::delete($user_profile_document_filename);
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userProfileDocument'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

    
}