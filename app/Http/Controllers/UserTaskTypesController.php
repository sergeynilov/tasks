<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\UserTaskType;
use App\TaskAssignedToUser;
use App\User;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\Http\Requests\UserTaskTypeRequest;

class UserTaskTypesController extends MyAppController
{

    public function index()
    {
        $filtersArray= ['fill_labels'=>1, 'short_description'=>'', 'name'=>'', 'show_task_assigned_to_users_count'=> 1];
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'name' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $rows_count= UserTaskType::getUserTaskTypesList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $userTaskTypesList= UserTaskType::getUserTaskTypesList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, $order_by, $order_direction, $page );
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> null, 'userTaskTypesList'=>null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page= with(new UserTaskType)->getItemsPerPage();
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'userTaskTypesList'=>$userTaskTypesList, 'per_page'=> $per_page ],
            HTTP_RESPONSE_OK);
    }

    public function show($id)  // OK
    {
        try {
            $userTaskType = UserTaskType::getRowById($id, ['fill_labels' => 1]);
            if ( $userTaskType == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'User\'s task type # "'.$id.'" not found!', 'userTaskType'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userTaskType'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json(['error_code'=> 0, 'message'=> '', 'userTaskType'=>$userTaskType],HTTP_RESPONSE_OK);
    }


    public function update(UserTaskTypeRequest $request)  // OK
    {
        $id= $request->id;
        $userTaskType = UserTaskType::find($id);
        if ( $userTaskType == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'User\'s task type # "'.$id.'" not found!', 'userTaskType'=>(object)['name'=> 'User\'s task type # "'.$id.'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $userTaskType->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userTaskType'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $this->setFlashMessage( "UserTaskType '".$userTaskType->name." was updated!", 'success' );  // there was NO error : flash success message
        return response()->json(['error_code'=> 0, 'message'=> '', 'userTaskType'=>$userTaskType],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function store(UserTaskTypeRequest $request) // ok
    {
        try {
            DB::beginTransaction();

            $userTaskType = UserTaskType::create($request->all());
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userTaskType'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'userTaskType'=>$userTaskType],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }


    public function destroy($id)  // ok
    {
        try {
            $userTaskType = UserTaskType::find($id);
            if ( $userTaskType == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'User\'s task type # "'.$id.'" not found!', 'userTaskType'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();

            $task_assigned_to_users_count= TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::ROWS_COUNT, [ 'user_task_type_id'=>$id ] );
            if ( $task_assigned_to_users_count > 0 ) {
                return response()->json(['error_code'=> 1, 'message'=> 'User\'s task type # "'.$userTaskType->name.'" can not be deleted, as it is used in 
                '.$task_assigned_to_users_count . ' user\'s assignment(s). ', 'userTaskType'=>null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $userTaskType->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userTaskType'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

}
