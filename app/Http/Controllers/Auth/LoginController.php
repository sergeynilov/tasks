<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Config;
use JavaScript;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard#/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        $API_VERSION= Config::get('app.API_VERSION',1);
//        $API_VERSION_LINK= '/api/v' . $API_VERSION;
        $API_VERSION_LINK= '/admin';
        $javaScriptVarsList= [ 'API_VERSION' => $API_VERSION, 'API_VERSION_LINK' => $API_VERSION_LINK ];
//        echo '<pre>$javaScriptVarsList::'.print_r($javaScriptVarsList,true).'</pre>';
//        die("-1 XXZ");
        JavaScript::put($javaScriptVarsList);

    }
}
