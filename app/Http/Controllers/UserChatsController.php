<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Config;

use App\UserChat;
use App\User;
use App\Group;
use App\Task;
use App\Http\Requests\UserChatRequest;
use App\UserChatParticipant;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\UserChatMessageDocument;
use App\UserChatMessage;
use App\UserChatLastVisited;
use App\UserProfiles;
use App\UserChatNewMessage;
use App\Settings;
use App\Events\ChatEvent;
use App\Http\Traits\funcsTrait;
use App\Exceptions\AppTaskCustomException;

class UserChatsController extends MyAppController
{
    use funcsTrait;

    public function index()
    {
        $loggedUser      = Auth::user();
        $order_by        = $this->getParameter('order_by', 'id');
        $page            = (int)$this->getParameter( 'page', 1 );
        $order_direction = $this->getParameter('order_direction', 'desc');
//        $list_filter= 'all';
        $list_filter = $this->getParameter('list_filter', 'all');
//        $order_by        = $this->getParameter('order_by', 'name');
//        $order_direction = $this->getParameter('order_direction', 'asc');

        $filtersArray    = ['participant_user_id' => $loggedUser->id, 'fill_labels' => 1, 'short_description' => 1, 'name' => '', 'show_creator_name' => '', 'show_user_chat_messages_count'=> 1, 'show_user_chat_participants_count'=> 1];
        if ($list_filter == 'my') {
//            $filtersArray['creator_id'] = $loggedUser->id;
            unset($filtersArray['participant_user_id']);
        }
        if ($list_filter == 'active') {
//            $filtersArray['status'] = 'A';
        }
            /* 				<p><input name="list_filter" type="radio" value="all"  v-bind:value="list_filter" v-on:click="filterApplied('all')" > All</p>
				<p><input name="list_filter" type="radio" value="my"  v-bind:value="list_filter" v-on:click="filterApplied('my')" > Created by me</p>
				<p><input name="list_filter" type="radio" value="active"  v-bind:value="list_filter" v-on:click="filterApplied('active')" > Only active</p>  */
        try {
//            $userChatsList = UserChat::getUserChatsList(ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction);
            $rows_count= UserChat::getUserChatsList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $userChatsList= UserChat::getUserChatsList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, $order_by, $order_direction, $page );
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'rows_count'=> $rows_count, 'userChatsList' => null, 'list_filter'=> $list_filter], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
//        echo '<pre>$userChatsList::'.print_r($userChatsList,true).'</pre>';
//        die("-1 XXZ");
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json(['error_code' => 0, 'message' => '', 'rows_count'=> $rows_count, 'userChatsList' => $userChatsList, 'list_filter'=> $list_filter], HTTP_RESPONSE_OK);
    }
//
//    public function user_chats_dictionaries($user_chat_id)
//    {
//        /* activeUsersSelectionList::[]
//userChatStatusSelectionList::[]
//taskSelectionList::[] */
//    } // public function user_chats_dictionaries($user_chat_id)


    public function user_chats_dictionaries($user_chat_id)
    {
        try {
            $order_by        = 'name';
            $order_direction = 'asc';
            $inGroups   = [];
            $adminGroup = Group::getSimilarGroupByName('Admin');
            if ($adminGroup != null) {
                $inGroups[] = $adminGroup->id;
            }

            $managerGroup = Group::getSimilarGroupByName('Manager');
            if ($managerGroup != null) {
                $inGroups[] = $managerGroup->id;
            }

            $employeeGroup = Group::getSimilarGroupByName('Employee');
            if ($employeeGroup != null) {
                $inGroups[] = $employeeGroup->id;
            }
//        echo '<pre>$inGroups::'.print_r($inGroups,true).'</pre>';
            //     public static function getTasksSelectionList( bool $key_return= true, array $filtersArray = array(), string $order_by = 'name', string $order_direction = 'asc') : array

            $activeUsersSelectionList = User::getUsersSelectionList(true, ['status' => 'A']);

            $taskSelectionList                      = Task::getTasksSelectionList(true, [ /*'creator_id' => $loggedUser->id*/ ] );
            $userChatParticipantStatusSelectionList = UserChatParticipant::getUserChatParticipantStatusValueArray(true, ['user_chat_id' => $user_chat_id]);
            $userChatStatusSelectionList            = UserChat::getUserChatStatusValueArray(true, ['status' => 'A', 'in_groups' => $inGroups], $order_by, $order_direction);

        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'documentCategory'=>null,
                                     'userChatStatusSelectionList'            => null,
                                     'taskSelectionList'                      => null,
                                     'userChatParticipantStatusSelectionList' => null            ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json( ['error_code'=> 0, 'message'=> '',
                                  'userChatStatusSelectionList'            => $userChatStatusSelectionList,
                                  'taskSelectionList'                      => $taskSelectionList,
                                  'activeUsersSelectionList'               => $activeUsersSelectionList,
                                  'userChatParticipantStatusSelectionList' => $userChatParticipantStatusSelectionList
        ], HTTP_RESPONSE_OK );
    } // public function user_chats_dictionaries($task_id)



    public function load_user_chat_messages_list($id)
    {
        $id = (int)$id;
        $loggedUser      = Auth::user();

        try {

            $order_by        = 'is_top_created_at';
            $order_direction = 'desc';
            $page_param= 1;
            $userChatMessagesList= UserChatMessage::getUserChatMessagesList( ListingReturnData::LISTING, ['user_chat_id'=>$id, 'show_author_name'=> 1, 'show_updated_at_by_user_name'=> 1, 'show_thumbnail_image'=> 1, 'show_user_profile_props'=> ['color', 'background_color'], 'fill_labels'=>1, 'time_ago_labels'=>'', 'show_user_user_chat_new_messages'=> $loggedUser->id ], $order_by,
                $order_direction,
                $page_param );
//            echo '<pre>$userChatMessagesList::'.print_r($userChatMessagesList,true).'</pre>';
//            die("-1 XXZ");
            foreach( $userChatMessagesList as $next_key=>$nextUserChatMessage ) {
                $userChatMessagesList[$next_key]['text']= nl2br($userChatMessagesList[$next_key]['text']);
            }

            UserChatNewMessage::clear( $loggedUser->id, $id );

        } catch (Exception $e) {
            return response()->json([
                'error_code'                             => 1,
                'message'                                => $e->getMessage(),
                'logged_username'                        => $loggedUser->first_name.' '.$loggedUser->last_name,
                'logged_user_id'                         => $loggedUser->id,
                'userChatMessagesList'                   => null
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'logged_username'                        => $loggedUser->first_name.' '.$loggedUser->last_name,
            'logged_user_id'                         => $loggedUser->id,
            'userChatMessagesList'                   => $userChatMessagesList,
        ],HTTP_RESPONSE_OK);

    } //public function load_user_chat_messages_list($id)



    public function run($id)
    {
        $id = (int)$id;
        $loggedUser      = Auth::user();
        $is_admin= $this->checkUserGroupAccess("Admin",[], $loggedUser);

        $error_code= 0;
        $error_message= 0;
        try {

            $userChat = UserChat::getRowById($id, ['fill_labels' => 1, 'show_creator_name' => 1, 'show_manager_name' => 1, 'set_null_fields_space'=> ["first_name", "last_name", "phone", "website"] ]);
            if ($userChat == null) {
                return response()->json(['error_code' => 11, 'message' => 'User chat # "' . $id . '" not found!', 'userChat' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
//            if ($userChat->task_id == null) {
//                $userChat->task_id = '';
//            }

            $isLoggedUserParticipantOfChat= UserChatParticipant::getUserChatParticipantsList( ListingReturnData::LISTING,[ 'user_chat_id'=> $id, 'user_id'=> $loggedUser->id, 'set_null_fields_space'=> ["first_name", "last_name", "phone", "website"] ] );
            if ( $isLoggedUserParticipantOfChat == null or count($isLoggedUserParticipantOfChat) == 0) {

                if ( $is_admin ) {
                    $error_code= 6;
                    $error_message= 'As admin, you have access to this chat only in view mode. ';
                } else {
                    return response()->json(['error_code' => 9, 'message' => 'You have no access to this chat. ', 'userChat' => null], HTTP_RESPONSE_OK);
                }
            }
            if ( /*!$is_admin and */count($isLoggedUserParticipantOfChat) == 1 ) { // that is not admin - check if user has readonly access
                if ($isLoggedUserParticipantOfChat[0]->status == 'R' ) {
                    $error_code= 4;
                    $error_message= 'You have access to this chat only in view mode. ';
                }

            } // if ( !$is_admin ) { // that is not admin


            if ( $userChat->status == 'C' ) { // chat is closed
                $error_code= 3;
                $error_message= 'As this chat is closed, you have access to this chat only in view mode. ';
            }

            $default_background_color = Settings::getSettingsList(['name'=>'default_background_color'], true);
            if ( $default_background_color == null ) {
//                $default_background_color= '#000000';
                $default_background_color= '#ddd';
            }
            $default_color = Settings::getSettingsList(['name'=>'default_color'], true);
            if ( $default_color == null ) {
//                $default_color= '#ffffff';
                $default_color= '#222';
            }

//            echo '<pre>$default_background_color::'.print_r($default_background_color,true).'</pre>';
//            die("-1 XXZ");
            $userProfileColorsArray = UserProfiles::getUserProfilesList(['user_id' => $loggedUser->id, 'name' => ['color', 'background_color'] ], true);
            $uploadedDocumentsExtensionsArray= \Config::get('app.uploaded_documents_extensions');
            $logged_user_background_color= !empty($userProfileColorsArray['background_color']) ? $userProfileColorsArray['background_color'] :  $default_background_color;
            $logged_user_color= !empty($userProfileColorsArray['color']) ? $userProfileColorsArray['color'] :  $default_color;

            $participantUsersList= UserChatParticipant::getUserChatParticipantsList(ListingReturnData::LISTING,['user_chat_id'=>$id, 'show_username'=>1, 'fill_labels'=> 1, 'show_user_chats_last_visited'=> 1],
                'status','asc');

        } catch (Exception $e) {
            return response()->json([
                'error_code'                             => 1,
                'message'                                => $e->getMessage(),
                'logged_username'                        => $loggedUser->first_name.' '.$loggedUser->last_name,
                'logged_user_id'                         => $loggedUser->id,
                'userChat'                               => null,
                'logged_user_background_color'           => null,
                'logged_user_color'                      => null,
                'participantUsersList'                   => null,
                'uploadedDocumentsExtensionsArray'       => $uploadedDocumentsExtensionsArray,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                             => $error_code,
            'message'                                => $error_message,
            'logged_username'                        => $loggedUser->first_name.' '.$loggedUser->last_name,
            'logged_user_id'                         => $loggedUser->id,
            'userChat'                               => $userChat,
            'logged_user_background_color'           => $logged_user_background_color,
            'logged_user_color'                      => $logged_user_color,
            'participantUsersList'                   => $participantUsersList,
            'uploadedDocumentsExtensionsArray'       => $uploadedDocumentsExtensionsArray,
        ],HTTP_RESPONSE_OK);

    } //public function run($id)


    public function show($id)
    {
        $id = (int)$id;
        $loggedUser      = Auth::user();
        try {

            $userChat = UserChat::getRowById($id, ['fill_labels' => 1, 'show_creator_name' => 1, 'set_null_fields_space'=>['task_id']]);
            if ($userChat == null) {
                return response()->json(['error_code' => 11, 'message' => 'User chat # "' . $id . '" not found!', 'userChat' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
//            foreach ($activeUsersSelectionList as $key => $nextUser) {
//                if ($loggedUser->id == (int)$nextUser['key']) {
//                    unset($activeUsersSelectionList[$key]);
//                }
//            }

        } catch (Exception $e) {
            return response()->json([
                'error_code'                             => 1,
                'message'                                => $e->getMessage(),
                'userChat'                               => null,
                    null
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'userChat'                               => $userChat,
        ],
            HTTP_RESPONSE_OK);

    } //public function show($id)





    public function update(UserChatRequest $request)
    {
        $id       = $request->id;
        $userChat = UserChat::find($id);
        if ($userChat == null) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'User chat # "' . $id . '" not found!',
                'userChat'   => (object)['name' => 'User chat # "' . $id . '" not found!', 'description' => '']
            ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            $dataArray = $request->all();
            if (empty($dataArray['task_id'])) {
                $dataArray['task_id'] = null;
            }
            $userChat->update($dataArray);
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'userChat' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $this->setFlashMessage("UserChat '" . $userChat->name . " was updated!", 'success');  // there was NO error : flash success message

        return response()->json(['error_code' => 0, 'message' => '', 'userChat' => $userChat], HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }

    public function store(UserChatRequest $request)
    {
        $loggedUser      = Auth::user();
        try {
            DB::beginTransaction();

            $userChatParticipantsArray = [];
            $insertDataArray           = $request->all();
            $new_user_chat_manager_id  = null;
            if (isset($insertDataArray['userChatParticipantsArray']) and is_array($insertDataArray['userChatParticipantsArray'])) {
                foreach ($insertDataArray['userChatParticipantsArray'] as $next_key => $nextUserChatParticipant) {
                    $a = $this->pregSplit('~user_chat_participant_status_~', $nextUserChatParticipant['user_id']);
                    if (count($a) == 1 and ! empty($nextUserChatParticipant['status'])) {
                        $userChatParticipantsArray[] = ['user_id' => $a[0], 'status' => $nextUserChatParticipant['status']];
                        if ($nextUserChatParticipant['status'] == 'M') {
                            $new_user_chat_manager_id = $a[0];
                        }
                    }
                }
                unset($insertDataArray['userChatParticipantsArray']);
            }
            $insertDataArray['creator_id'] = $loggedUser->id;
            if ($new_user_chat_manager_id == null) {    // manager of the new user chat was not selected : so set $loggedUser->id to it
                $new_user_chat_manager_id = $loggedUser->id;
            }
            $insertDataArray['manager_id'] = $new_user_chat_manager_id;
            $newUserChat                   = UserChat::create($insertDataArray);
            $new_user_chat_id              = $newUserChat->id;
            foreach ($userChatParticipantsArray as $next_key => $nextUserChatParticipant) {
                $newUserChatParticipant       = UserChatParticipant::create([
                    'user_id'      => $nextUserChatParticipant['user_id'],
                    'status'       => $nextUserChatParticipant['status'],
                    'user_chat_id' => $new_user_chat_id
                ]);
                $new_user_chat_participant_id = $newUserChatParticipant->id;
            }

            DB::commit();
        } catch (Exception $e) {

            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'userChat' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'userChat' => $newUserChat], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }

    public function destroy($id)
    {
        try {

            DB::beginTransaction();
            $userChat = UserChat::find($id);
            if ($userChat == null) {
                return response()->json(['error_code' => 11, 'message' => 'User chat # "' . $id . '" not found!', 'userChat' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            $userChat->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'userChat' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK);
    }


    public function send_user_chat_new_message(request $request)
    {
        $loggedUser      = Auth::user();
        $userChatNewMessageArray = $request->all();
//        echo '<pre>$userChatNewMessageArray::'.print_r($userChatNewMessageArray,true).'</pre>';
        $isLoggedUserParticipantOfChat= UserChatParticipant::getUserChatParticipantsList( ListingReturnData::LISTING,[ 'user_chat_id'=> $userChatNewMessageArray['user_chat_id'], 'user_id'=> $loggedUser->id ] );
        if ( $isLoggedUserParticipantOfChat == null or count($isLoggedUserParticipantOfChat) == 0) {
            return response()->json(['error_code' => 9, 'message' => 'You have no access to this chat. ', 'userChat' => null], HTTP_RESPONSE_OK);
        }
        if ( $isLoggedUserParticipantOfChat[0]->status == 'R') { // Can only read in this chat
            return response()->json(['error_code' => 9, 'message' => 'You have no access to write messages into this chat. ', 'userChat' => null], HTTP_RESPONSE_OK);
        }
        
        try {
            DB::beginTransaction();
            $newUserChatMessage= new UserChatMessage();
            $newUserChatMessage->user_id = $loggedUser->id;
            $newUserChatMessage->user_chat_id  = $userChatNewMessageArray['user_chat_id'];
            $newUserChatMessage->is_top      = !empty($userChatNewMessageArray['user_chat_new_message_is_top'])?1:0;
            $newUserChatMessage->text      = $userChatNewMessageArray['user_chat_new_message'];
            $newUserChatMessage->message_type      = 'T';
            $newUserChatMessage->save();

            UserChatLastVisited::updateVisitedAt( $loggedUser->id, $userChatNewMessageArray['user_chat_id'] );

            $userChatParticipantsList= UserChatParticipant::getUserChatParticipantsList( ListingReturnData::LISTING, ['user_chat_id'=> $newUserChatMessage->user_chat_id ] );
            foreach( $userChatParticipantsList as $next_key=>$nextUserChatParticipant ) {
                if ( $loggedUser->id != $newUserChatMessage->id ) { // not to send notification to myself
                    $newUserChatNewMessage = UserChatNewMessage::create([
                        'user_id'              => $nextUserChatParticipant['user_id'],
                        'user_chat_message_id' => $newUserChatMessage->id
                    ]);
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'userChat' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        event(new ChatEvent( $newUserChatMessage->id, $newUserChatMessage->user_id, $newUserChatMessage->user_chat_id, $newUserChatMessage->is_top, $newUserChatMessage->text,
            $loggedUser )  );
        return response()->json(['error_code' => 0, 'message' => '', 'id' => $newUserChatMessage->id], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }



    public function user_chat_message_destroy($id) 
    {
        try {

            DB::beginTransaction();
            $userChatMessage = UserChatMessage::find($id);
            if ($userChatMessage == null) {
                return response()->json(['error_code' => 11, 'message' => 'User chat # "' . $id . '" not found!', 'userChatMessage' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            UserChatLastVisited::updateVisitedAt( $userChatMessage->user_id, $userChatMessage->user_chat_id );
            $userChatMessage->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'userChatMessage' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK);
    }


    public function update_user_chat_message($user_chat_message_id) {
        $loggedUser = Auth::user();
        $dataArray = Request()->all();
        $this->debToFile($dataArray,'  update_user_chat_message  $dataArray:');
        try {

            $userChatMessage = UserChatMessage::getRowById($user_chat_message_id, []);
            if ($userChatMessage == null) {
                return response()->json(['error_code' => 11, 'message' => 'User chat message # "' . $user_chat_message_id . '" not found!', 'userChatMessage' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $userChatMessage->text= $dataArray['text'];
            $userChatMessage->is_top= $dataArray['is_top'];
            $userChatMessage->updated_at_by_user_id= $loggedUser->id;
            $userChatMessage->save();
            DB::commit();

        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userChatMessage'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return [ 'error_code' => 0, 'message' => '', 'userChatMessage'=>$userChatMessage, 'user_chat_message_id'=> $user_chat_message_id ];
    } //public function update_user_chat_message($user_chat_message_id) {

    
    /////// USER CHAT MESSAGE DOCUMENTS BLOCK START ////////

    public function load_user_chat_message_documents() {
        $user_chat_id            = (int)$this->getParameter( 'user_chat_id' );
        try {
            $order_by = 'ucmd.id';
            $order_direction = 'desc';
            $userChatMessageDocumentsList= UserChatMessageDocument::getUserChatMessageDocumentsList( ListingReturnData::LISTING, [ 'user_chat_id'=>$user_chat_id,
 'show_image'=> 1, 'show_image_info'=> 1 ], $order_by, $order_direction );
//            echo '<pre>$userChatMessageDocumentsList::'.print_r($userChatMessageDocumentsList,true).'</pre>';
        } catch (Exception $e) {
            return response()->json([
                'error_code'                      => 1,
                'message'                         => $e->getMessage(),
                'userChatMessageDocumentsList'    => null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );

        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'userChatMessageDocumentsList'        => $userChatMessageDocumentsList,
        ], HTTP_RESPONSE_OK);


    } // public function load_user_chat_message_documents() {


    public function user_chat_message_documents_upload(Request $request) {
        $chat_attached_files_document_category_id = Settings::getValue('chat_attached_files_document_category_id');
        throw_if($chat_attached_files_document_category_id == null, new AppTaskCustomException('Document category for files attaching to chats is not provided!'));

        $loggedUser = Auth::user();

        $userChatMessageDocumentPropertiesArray = $request->input('uploadedItemDocuments');
        $userChatMessageDocumentArray = $request->file('uploadedItemDocuments');
        $this->debToFile($userChatMessageDocumentPropertiesArray,'  user_chat_message_documents_upload  $userChatMessageDocumentPropertiesArray:');
        $this->debToFile($userChatMessageDocumentArray,'  user_chat_message_documents_upload  $userChatMessageDocumentArray:');

        try {
            DB::beginTransaction();
            $parent_item_id= null;
            $is_debug= false;
            $index= 0;
            $errors_message_text= '';
            $files_to_upload_count= 0;
            $files_to_upload_list= '';
            foreach( $userChatMessageDocumentArray as $next_key=>$nextAttachmentFile ) { // check all attached files
                $user_chat_message_documents_filename= $nextAttachmentFile['attached_file']->getClientOriginalName();
                if ($is_debug) echo '<pre>$user_chat_message_documents_filename::'.print_r($user_chat_message_documents_filename,true).'</pre>';
                if ($is_debug) echo '<pre>$userChatMessageDocumentPropertiesArray[$index*4]::'.print_r($userChatMessageDocumentPropertiesArray[$index*4],true).'</pre>';
                
                $next_extension= $this->getFilenameExtension( $user_chat_message_documents_filename );

                $parent_item_id = ! empty($userChatMessageDocumentPropertiesArray[$index * 4 + 3]['parent_item_id']) ?
                        $userChatMessageDocumentPropertiesArray[$index * 4 + 3]['parent_item_id'] : null;
                $files_to_upload_count++;
                $files_to_upload_list.= $user_chat_message_documents_filename.', ';


                $info                 = !empty($userChatMessageDocumentPropertiesArray[$index*4+1]['info']) ? $userChatMessageDocumentPropertiesArray[$index*4+1]['info'] : null;
                if ($is_debug) echo '<pre>$parent_item_id::'.print_r($parent_item_id,true).'</pre>';
                if ($is_debug) echo '<pre>$info::'.print_r($info,true).'</pre>';


                $validationRulesArray= UserChatMessageDocument::getValidationRulesArray($parent_item_id);
//                if ($is_debug) echo '<pre>$validationRulesArray::'.print_r($validationRulesArray,true).'</pre>';
                $document_category_id= $chat_attached_files_document_category_id;
                $insertDataArray= [
                    'user_chat_id'  => $parent_item_id,
                    'user_id'  => $loggedUser->id,
                    'filename'  => $user_chat_message_documents_filename,
                    'extension' => $this->getFilenameExtension($user_chat_message_documents_filename),
                    'document_category_id' => $document_category_id,
                    'info'=> $info ];
                $validator = Validator::make( $insertDataArray, $validationRulesArray);
                if ($validator->fails()) { // if error on validation return errors list
                    $errorsList = UserChatMessageDocument::getErrorsList($validator);
                    if ($is_debug) echo '<pre>$errorsList::'.print_r($errorsList,true).'</pre>';
                    foreach( $errorsList as $next_fieldname=>$next_error_message ) {
                        $errors_message_text.= (!empty($errors_message_text)?", ":"") . $user_chat_message_documents_filename . ' : ' . $next_error_message;
                    }
                }
                $index++;
                /// ///////


            } // foreach( $userChatMessageDocumentArray as $next_key=>$nextAttachmentFile ) { // check all attached files

            if (!empty($errors_message_text)) { // there were errors
                return response()->json(['error_code' => 1, 'message' => $errors_message_text], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            reset($userChatMessageDocumentPropertiesArray);
            reset($userChatMessageDocumentArray);

            $files_to_upload_list=  with(new UserChatMessageDocument)->trimRightSubString( $files_to_upload_list, ', ' );
            throw_if($parent_item_id == null, new AppTaskCustomException('Parent user chat is not provided!'));

            $newUserChatMessage= new UserChatMessage();
            $newUserChatMessage->user_id         = $loggedUser->id;
            $newUserChatMessage->user_chat_id    = $parent_item_id;
            $newUserChatMessage->is_top          = false;
            $upload_text_result= $files_to_upload_count . ' file'.($files_to_upload_count > 1? 's' : '' ). ' '.($files_to_upload_count > 1?'were':'was').' uploaded : '.$files_to_upload_list . ' by '.$loggedUser->first_name.' '.$loggedUser->last_name;
            $newUserChatMessage->text            = $upload_text_result;
            $newUserChatMessage->message_type      = 'U';
            $newUserChatMessage->save();
            $new_user_chat_message_id= $newUserChatMessage->id;

            $index= 0;
            foreach( $userChatMessageDocumentArray as $next_key=>$nextAttachmentFile ) {
                $user_chat_message_documents_filename= $nextAttachmentFile['attached_file']->getClientOriginalName();
                $user_chat_message_documents_file_path = $nextAttachmentFile['attached_file']->getPathName();
//                echo '<pre>$user_chat_message_documents_file_path::'.print_r($user_chat_message_documents_file_path,true).'</pre>';

                $next_info = ! empty($userChatMessageDocumentPropertiesArray[$index * 4 + 1]['info']) ?
                    $userChatMessageDocumentPropertiesArray[$index * 4 + 1]['info'] : null;

                $userChatMessageDocument= new UserChatMessageDocument();
                $userChatMessageDocument->user_id= $loggedUser->id;
                $userChatMessageDocument->user_chat_id= $parent_item_id;
                $userChatMessageDocument->filename= $user_chat_message_documents_filename;
                $userChatMessageDocument->extension= $this->getFilenameExtension($user_chat_message_documents_filename);
                $userChatMessageDocument->info= $next_info;

                $userChatMessageDocument->document_category_id= $chat_attached_files_document_category_id;
                $userChatMessageDocument->save();

                $dest_filename= UserChatMessageDocument::getUserChatMessageDocumentPath($userChatMessageDocument->id,$user_chat_message_documents_filename, true);
                Storage::disk('local')->put($dest_filename, File::get( $user_chat_message_documents_file_path ) );

                $index++;
            }
            DB::commit();

        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userChatMessageDocumentArray'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return [ 'error_code' => 0, 'message' => '', 'userChatMessageDocumentArray'=>$userChatMessageDocumentArray, 'user_chat_message_id'=> $new_user_chat_message_id,  'upload_text_result'=>
            $upload_text_result ];
    } //public function user_chat_message_documents_upload(Request $request) {

    public function update_user_chat_message_document($user_chat_message_document_id) {
        $loggedUser = Auth::user();
        $dataArray = Request()->all();
        $this->debToFile($dataArray,'  update_user_chat_message_document  $dataArray:');
        try {

            $userChatMessageDocument = UserChatMessageDocument::getRowById($user_chat_message_document_id, []);
            if ($userChatMessageDocument == null) {
                return response()->json(['error_code' => 11, 'message' => 'User chat message document # "' . $user_chat_message_document_id . '" not found!', 'userChatMessageDocument' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $userChatMessageDocument->info= $dataArray['info'];
            $userChatMessageDocument->save();
            DB::commit();

        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userChatMessageDocument'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return [ 'error_code' => 0, 'message' => '', 'userChatMessageDocument'=>$userChatMessageDocument, 'user_chat_message_document_id'=> $user_chat_message_document_id ];
    } //public function update_user_chat_message_document($user_chat_message_document_id) {

    public function user_chat_message_document_destroy($id)
    {
        try {
            DB::beginTransaction();
            $userChatMessageDocument = UserChatMessageDocument::find($id);
            if ($userChatMessageDocument == null) {
                return response()->json(['error_code' => 11, 'message' => 'User chat # "' . $id . '" not found!', 'userChatMessage' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            $userChatMessageDocument->delete();
            $user_chat_message_document_filename= UserChatMessageDocument::getUserChatMessageDocumentPath($userChatMessageDocument->id,$userChatMessageDocument->filename, true);
//            echo '<pre>$user_chat_message_document_filename::'.print_r($user_chat_message_document_filename,true).'</pre>';
            UserChatMessageDocument::deleteFileByPath($user_chat_message_document_filename, true);
//            Storage::delete($user_chat_message_document_filename);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'userChatMessage' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK);
    }


    /////// USER CHAT MESSAGE DOCUMENTS BLOCK END ////////


//Route::post('load_related_user_chats_list/{request_type}/{filter_id}','UserChatsController@load_related_user_chats_list');
    public function load_related_user_chats_list(string $request_type, int $filter_id)
    {
//        echo '<pre>load_related_user_chats_list $request_type::'.print_r($request_type,true).'</pre>';
//        echo '<pre>load_related_user_chats_list $filter_id::'.print_r($filter_id,true).'</pre>';
        $filtersArray= [ 'fill_labels' => 1, 'show_creator_name'=> 1, 'show_user_chat_messages_count'=> 1 ];
        if ( $request_type == 'by_task_id' ) {
            $filtersArray['task_id']= $filter_id;
        }
//        if ( $request_type == 'by_task_assigned_to_user_id' ) {
//            $filtersArray['task_assigned_to_user_id']= $filter_id;
//        }
//        $taskOperationHistoryList = TaskOperation::getTaskOperationsList(ListingReturnData::LISTING, $filterArray, 'too.created_at', 'desc' );
        $relatedUserChatsList= UserChat::getUserChatsList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, 'user_chat_messages_count', 'asc' );

        return response()->json( ['error_code' => 0, 'message' => '', 'relatedUserChatsList' => $relatedUserChatsList], HTTP_RESPONSE_OK );
    } // public function public function load_related_user_chats_list(int $task_assigned_to_user_id)

}
