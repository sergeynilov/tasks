<?php

namespace App\Http\Controllers;

use App\Settings;
use Auth;
use DB;
//use File;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Config;

use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;

use App\User;
use App\UserProfiles;
use App\UsersGroups;
use App\Task;
use App\DocumentCategory;
use App\UserProfileDocument;
use App\UserChat;
use App\Event;
use App\UserWeatherLocation;
use App\UserTodo;
use App\EventUser;
use App\TaskAssignedToUser;
use App\Http\Requests\UserWeatherLocationRequest;
use App\Http\Requests\UserTodoRequest;

class DashboardController extends MyAppController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->addVariablesToJS();
        $site_name  = Settings::getValue('site_name', '');
        return view('admin.dashboard.index',['site_name'=> $site_name]);

    }

    public function test()
    {
        $this->addVariablesToJS();
        return view('admin.test.index');

    }


//$filtersArray= ['fill_labels'=>1, 'short_description'=>'', 'name'=>''];
//$page= (int)$this->getParameter( 'page', 1 );
//$order_by= $this->getParameter( 'order_by', 'name' );
//$order_direction= $this->getParameter( 'order_direction', 'asc' );
//
//try {
//$rows_count= Task::getTasksList( ListingReturnData::ROWS_COUNT, $filtersArray );


    public function dashboard_profile_data()
    {
        $loggedUser      = Auth::user();
        $is_admin= $this->checkUserGroupAccess("Admin",[], $loggedUser);
//        echo '<pre>$is_admin::'.print_r($is_admin,true).'</pre>';
//        die("-1 XXZ++++");
//        echo '<pre>$loggedUserInfoArray::'.print_r($loggedUserInfoArray,true).'</pre>';
        $order_by= 'task_priority_task_date_end';
        $forAdminAssigningTasksAssignedToUserList= [];
        $forAdminProcessingTasksAssignedToUserList= [];
        $forAdminCheckingTasksAssignedToUserList= [];

//
        $forAdminAssigningTasksAssignedToUserAsTeamLeaderList= [];
        $forAdminProcessingTasksAssignedToUserAsTeamLeaderList= [];
        $forAdminCheckingTasksAssignedToUserAsTeamLeaderList= [];


        $activeUserChatsList= [];
        $yourActiveUserChatsList= [];
        $yourClosedUserChatsList= [];
        $yourFutureEventsList= [];
        try {
            $assigningTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::LISTING, ['user_id' => $loggedUser->id, 'status' => 'A', 'fill_labels' => 1, 'show_tasks_info'=> 1], $order_by );
            $processingTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, ['user_id' => $loggedUser->id, 'status' => 'P', 'fill_labels' => 1, 'show_tasks_info'=> 1], $order_by );
            $checkingTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, ['user_id' => $loggedUser->id, 'status' => 'K', 'fill_labels' => 1, 'show_tasks_info'=> 1], $order_by );


//            $tasksAssignedToUserAsTeamLeaderList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, ['user_id' => $loggedUser->id, /*'status' => 'K', */'is_leader'=> 1, 'fill_labels' => 1, 'show_tasks_info'=> 1], $order_by );   // Get llisting of tasks where logged user is admin
            // public static function getTaskAssignedToUsersAsTeamLeaderList( int $user_id ) {
            $tasksAssignedToUserAsTeamLeaderList = TaskAssignedToUser::getTaskAssignedToUsersAsTeamLeaderList( $loggedUser->id );   // Get llisting of tasks where logged user is admin
//            echo '<pre>$tasksAssignedToUserAsTeamLeaderList::'.print_r($tasksAssignedToUserAsTeamLeaderList,true).'</pre>';
//            die("-1 XXZ");
            foreach( $tasksAssignedToUserAsTeamLeaderList as $next_key=>$nextTasksAssignedToUserAsTeamLeader ) {
                $asTeamLeaderAssigningList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, [ 'task_id'=> $nextTasksAssignedToUserAsTeamLeader['task_id'], 'status' => 'A', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );
                $tasksAssignedToUserAsTeamLeaderList[$next_key]['asTeamLeaderAssigningList']= $asTeamLeaderAssigningList;


                $asTeamLeaderProcessingList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, [ 'task_id'=> $nextTasksAssignedToUserAsTeamLeader['task_id'], 'status' => 'P', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );
                $tasksAssignedToUserAsTeamLeaderList[$next_key]['asTeamLeaderProcessingList']= $asTeamLeaderProcessingList;


                $asTeamLeaderCheckingList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, [ 'task_id'=> $nextTasksAssignedToUserAsTeamLeader['task_id'], 'status' => 'K', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );
                $tasksAssignedToUserAsTeamLeaderList[$next_key]['asTeamLeaderCheckingList']= $asTeamLeaderCheckingList;
            }
//            die("-1 XXZ");

            if ( $is_admin ) { // admin is logged
                $forAdminAssigningTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, ['status' => 'A', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );
                $forAdminProcessingTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, ['status' => 'P', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );
                $forAdminCheckingTasksAssignedToUserList = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::LISTING, [ 'status' => 'K', 'fill_labels' => 1, 'show_tasks_info'=> 1, 'show_username'=> 1], $order_by );


                $activeUserChatsList = UserChat::getUserChatsList(ListingReturnData::LISTING, [ 'status' => 'A', 'fill_labels' => 1, 'show_user_chat_messages_count'=> 1, 'show_user_chat_participants_count'=> 1, 'show_manager_name'=> 1], 'uc.created_at', 'asc' );
                $yourActiveUserChatsList = UserChat::getUserChatsList(ListingReturnData::LISTING, [ 'status' => 'A', 'fill_labels' => 1, 'show_user_chat_messages_count_by_user_id'=> $loggedUser->id, 'show_user_chat_messages_count'=> 1, 'show_user_chat_participants_count'=> 1, 'show_manager_name'=> 1], 'uc.created_at', 'asc' );
                $yourClosedUserChatsList = UserChat::getUserChatsList(ListingReturnData::LISTING, [ 'status' => 'C', 'fill_labels' => 1, 'show_user_chat_messages_count_by_user_id'=> $loggedUser->id, 'show_user_chat_messages_count'=> 1, 'show_user_chat_participants_count'=> 1, 'show_manager_name'=> 1], 'uc.created_at', 'asc' );

                $yourFutureEventsList = Event::getEventsList(ListingReturnData::LISTING, [ 'only_future' => 1, 'user_id' => $loggedUser->id, 'show_task_name'=> 1], 'e.created_at', 'asc' );

//                echo '<pre>$yourFutureEventsList::'.print_r($yourFutureEventsList,true).'</pre>';
//                die("-1 XXZ=====");
//                $rows_count= UserChat::getUserChatsList( ListingReturnData::ROWS_COUNT, $filtersArray );

            }
        } catch (Exception $e) {
            return response()->json([
                'error_code'                         => 1,
                'message'                            => $e->getMessage(),
                'loggedUser'                         => null,
                'assigningTasksAssignedToUserList'   => null,
                'processingTasksAssignedToUserList'  => null,
                'checkingTasksAssignedToUserList'    => null,

//                'assigningTasksAssignedToUserAsTeamLeaderList'   => null,
//                'processingTasksAssignedToUserAsTeamLeaderList'  => null,
//                'checkingTasksAssignedToUserAsTeamLeaderList'    => null,
                'tasksAssignedToUserAsTeamLeaderList'=> null,

                
                'forAdminAssigningTasksAssignedToUserList' => null,
                'forAdminProcessingTasksAssignedToUserList' => null,
                'forAdminCheckingTasksAssignedToUserList'   => null,
                'activeUserChatsList'                => null,
                'yourActiveUserChatsList'            => null,
                'yourClosedUserChatsList'            => null,
                'yourFutureEventsList'            => null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        sleep(  config('app.sleep_in_seconds',0) );

        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'loggedUser'                             => $loggedUser,
            'assigningTasksAssignedToUserList'       => $assigningTasksAssignedToUserList,
            'processingTasksAssignedToUserList'      => $processingTasksAssignedToUserList,
            'checkingTasksAssignedToUserList'        => $checkingTasksAssignedToUserList,


            'tasksAssignedToUserAsTeamLeaderList'        => $tasksAssignedToUserAsTeamLeaderList,


            'forAdminAssigningTasksAssignedToUserList'    => $forAdminAssigningTasksAssignedToUserList,
            'forAdminProcessingTasksAssignedToUserList'   => $forAdminProcessingTasksAssignedToUserList,
            'forAdminCheckingTasksAssignedToUserList'     => $forAdminCheckingTasksAssignedToUserList,
            'activeUserChatsList'                         => $activeUserChatsList,
            'yourActiveUserChatsList'                     => $yourActiveUserChatsList,
            'yourClosedUserChatsList'                     => $yourClosedUserChatsList,
            'yourFutureEventsList'                        => $yourFutureEventsList,

        ], HTTP_RESPONSE_OK );

    } // public function dashboard_profile_data()


    public function refresh_user_profile_quick_info()
    {
        $loggedUser      = Auth::user();
//        echo '<pre>$is_admin::'.print_r($is_admin,true).'</pre>';
//        die("-1 XXZ++++");
//        echo '<pre>$loggedUserInfoArray::'.print_r($loggedUserInfoArray,true).'</pre>';
        try {
            $assigning_tasks_count = TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::ROWS_COUNT, ['user_id' => $loggedUser->id, 'status' => 'A'] );


            $user_weather_locations_count = UserWeatherLocation::getUserWeatherLocationsList( ListingReturnData::ROWS_COUNT, ['user_id' => $loggedUser->id] );
            $user_todos_count = UserTodo::getUserTodosList( ListingReturnData::ROWS_COUNT, ['user_id' => $loggedUser->id] );

//            $future_events_count = Event::getTaskAssignedToUsersList( ListingReturnData::ROWS_COUNT, ['user_id' => $loggedUser->id, 'status' => 'A'] );
            $future_events_count = Event::getEventsList(ListingReturnData::ROWS_COUNT, [ 'only_future' => 1, 'user_id' => $loggedUser->id] );


        } catch (Exception $e) {
            return response()->json([
                'error_code'                         => 1,
                'message'                            => $e->getMessage(),
                'loggedUser'                         => null,
                'assigning_tasks_count'              => null,
                'future_events_count'                => null,
                'user_weather_locations_count'       => null,
                'user_todos_count'       => null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'loggedUser'                             => $loggedUser,
            'assigning_tasks_count'                  => $assigning_tasks_count,
            'future_events_count'                    => $future_events_count,
            'user_weather_locations_count'           => $user_weather_locations_count,
            'user_todos_count'                       => $user_todos_count,
        ], HTTP_RESPONSE_OK );

    } // public function refresh_user_profile_quick_info()


    public function get_assigning_tasks_details()
    {
        $loggedUser      = Auth::user();
        try {
            $assigningTasksListing = TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::LISTING, [ 'fill_labels'=> 1, 'user_id' => $loggedUser->id, 'status' => 'A',
                'show_username'=> 1, 'show_tasks_info'=> 1, 'show_user_task_type_name'=> 1 ] );
        } catch (Exception $e) {
            return response()->json([
                'error_code'                         => 1,
                'message'                            => $e->getMessage(),
                'loggedUser'                         => null,
                'assigningTasksListing'              => null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'loggedUser'                             => $loggedUser,
            'assigningTasksListing'                  => $assigningTasksListing,
        ], HTTP_RESPONSE_OK );

    } // public function get_assigning_tasks_details()



    
    public function get_user_view_data($id)
    {
        try {
            $previewUser = User::find($id);
            $users_groups_text= UsersGroups::getUsersGroupsTextLabel($previewUser->id,true);

            $profileThumbnailImageDocumentCategory= DocumentCategory::getSimilarDocumentCategoryByAlias('profile_thumbnail_image');
            $user_profile_thumbnail_image_url= '';
//            echo '<pre>$profileThumbnailImageDocumentCategory::'.print_r($profileThumbnailImageDocumentCategory,true).'</pre>';
            if ( $profileThumbnailImageDocumentCategory!= null ) {
                $userProfileDocument= UserProfileDocument::getUserProfileDocumentsList( ListingReturnData::LISTING,
                    ['document_category_id'=>$profileThumbnailImageDocumentCategory->id, 'user_id'=>$id] );
//                echo '<pre>$userProfileDocument[0]::'.print_r($userProfileDocument[0],true).'</pre>';
                if ( !empty($userProfileDocument[0]) ) {
                    $base_url= self::getBaseUrl();
                    $next_user_profile_document_filename= UserProfileDocument::getUserProfileDocumentPath($userProfileDocument[0]->user_id,$userProfileDocument[0]->filename,  false);
                    $next_user_profile_document_url= $base_url.Storage::disk('local')->url($next_user_profile_document_filename );
//                    echo '<pre>$::'.print_r($next_user_profile_document_filename,true).'</pre>';
                    $file_exists = Storage::disk('local')->exists('public/'.$next_user_profile_document_filename);
//                    echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
                    if ( $file_exists ) {
                        $user_profile_thumbnail_image_url= $next_user_profile_document_url;
                    }
                }
            }
//            echo '<pre>$user_profile_thumbnail_image_url::'.print_r($user_profile_thumbnail_image_url,true).'</pre>';
//            die("-1 XXZ----");


        } catch (Exception $e) {
            return response()->json( [ 'error_code' => 1, 'message' => $e->getMessage(), 'user' => null, 'users_groups_text'  => null, 'user_profile_thumbnail_image_url'=> null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( [
            'error_code'=> 0,
            'message'=> '',
            'user'  => $previewUser,
            'users_groups_text'  => $users_groups_text,
            'user_profile_thumbnail_image_url'=> $user_profile_thumbnail_image_url

        ], HTTP_RESPONSE_OK );
    } //public function get_user_view_data()

    
    public function get_settings_value()
    {
        try {
            $name        = $this->getParameter('name');
            $default_value        = $this->getParameter('default_value');
            $settings_value  = Settings::getValue($name, $default_value);
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'settings_value' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( ['error_code'=> 0, 'message'=> '', 'settings_value' => $settings_value ], HTTP_RESPONSE_OK );
    } //public function get_logged_user_info()



    public function get_logged_user_info()
    {
        try {
            $loggedUser= '';
            if ( Auth::check() ) {
                $loggedUser = Auth::user();
            }
            $site_name  = Settings::getValue('site_name', '');

            $usersGroupsList= UsersGroups::getUsersGroupsList( ListingReturnData::LISTING, ['user_id'=> $loggedUser->id, 'show_group_name'=> 1 ] );
            $loggedUserInGroups= [];
            foreach( $usersGroupsList as $next_key=>$nextUsersGroup ) {
//                if ( $nextUsersGroup->status!= 'A' ) continue; // get only active groups
                $loggedUserInGroups[]= [ 'group_id'=> $nextUsersGroup->group_id, 'status'=> $nextUsersGroup->status, 'group_name'=> $nextUsersGroup->group_name ];
            }

        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'loggedUser' => '', 'loggedUserInGroups'=> null, 'site_name' => null],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( ['error_code'=> 0, 'message'=> '', 'loggedUser' => $loggedUser, 'loggedUserInGroups'=> $loggedUserInGroups, 'site_name'=> $site_name],
            HTTP_RESPONSE_OK );
    } //public function get_logged_user_info()


    /////////////////  USER WEATHER  BLOCK BEGIN //////////////////////
    public function user_weather_locations_dictionaries($user_weather_location_id)
    {
        try {
            $userWeatherLocationValueArraySelectionList   = UserWeatherLocation::getUserWeatherLocationValueArray();
        } catch (Exception $e) {
            return response()->json( ['error_code'=> 1, 'message'=> $e->getMessage(), 'userWeatherLocationValueArraySelectionList'=> null ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR );
        }
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'userWeatherLocationValueArraySelectionList' => $userWeatherLocationValueArraySelectionList ], HTTP_RESPONSE_OK );
    } // public function user_weather_locations_dictionaries($user_weather_location_id)



    public function user_weather_location_load()
    {
        $loggedUser      = Auth::user();
        $filtersArray    = [ 'user_id'=> $loggedUser->id, 'fill_labels'=>1 ];
        //tsk_user_chat_message_documents
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'ordering' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $rows_count= UserWeatherLocation::getUserWeatherLocationsList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $userWeatherLocationsList= UserWeatherLocation::getUserWeatherLocationsList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, $order_by, $order_direction,
                $page );
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> null, 'userWeatherLocationsList'=>null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page= with(new UserWeatherLocation)->getItemsPerPage();
        sleep(  config('app.sleep_in_seconds',0) );

        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'userWeatherLocationsList'=>$userWeatherLocationsList, 'per_page'=> $per_page ],
            HTTP_RESPONSE_OK);
    }

    public function user_weather_location_update(UserWeatherLocationRequest $request)  // OK
    {
        $loggedUser      = Auth::user();
        $id= $request->user_weather_location_id;
        $userWeatherLocation = UserWeatherLocation::find($id);
        if ( $userWeatherLocation == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'User\'s weather location # "'.$id.'" not found!', 'userWeatherLocation'=>(object)['name'=> 'User\'s weather location # "'.$id.'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            $updateData= $request->all();
            $user_weather_location_count = UserWeatherLocation::getSimilarUserWeatherLocationByLocation( $updateData['location'], $loggedUser->id, $userWeatherLocation->id, true );
            if ($user_weather_location_count > 0) {
                return response()->json( ['error_code'=> 1, 'message'=> 'For you "'.$updateData['location'].'" weather location is already taken!', 'userWeatherLocation'=>null ],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            echo '<pre>$updateData::'.print_r($updateData,true).'</pre>';
            $userWeatherLocation->update($updateData);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userWeatherLocation'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $this->setFlashMessage( "User weather location '".$userWeatherLocation->location." was updated!", 'success' );  // there was NO error : flash success message
        return response()->json(['error_code'=> 0, 'message'=> '', 'userWeatherLocation'=>$userWeatherLocation],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }

    public function user_weather_location_store(UserWeatherLocationRequest $request)
    {
        $loggedUser      = Auth::user();
        try {
            $insertData= $request->all();
            $user_weather_location_count = UserWeatherLocation::getSimilarUserWeatherLocationByLocation( $insertData['location'], $loggedUser->id, null, true );
            if ($user_weather_location_count > 0) {
                return response()->json( ['error_code'=> 1, 'message'=> 'For you "'.$insertData['location'].'" weather location is already taken!', 'userWeatherLocation'=>null ],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $insertData['user_id']= $loggedUser->id;
            $insertData['location_type']= 'C';
            if ( empty($insertData['ordering']) ) {
                $insertData['ordering']= UserWeatherLocation::getMaxOrdering();
            }
            $userWeatherLocation = UserWeatherLocation::create($insertData);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userWeatherLocation'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'userWeatherLocation'=>$userWeatherLocation],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }



    public function user_weather_location_destroy($id)  // ok
    {
        try {
            $userWeatherLocation = UserWeatherLocation::find($id);
            if ( $userWeatherLocation == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'User\'s weather location # "'.$id.'" not found!', 'userWeatherLocation'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $userWeatherLocation->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userWeatherLocation'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

    public function get_weather_info_by_location($location)
    {
        $openweathermap_key= Config::get('app.OPENWEATHERMAP_KEY');
        if ( empty($openweathermap_key) ) {
            return response()->json( ['error_code'=> 1, 'message'=> 'Open weather map key is not set in application\'s environment', 'results'=> null ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $icon_base_url = '//openweathermap.org/img/w/$$.png';
        $query_url = "api.openweathermap.org/data/2.5/forecast?q=".$location."&appid=".$openweathermap_key.'&units=metric';
        $session = curl_init($query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($session);

        $weatherData = json_decode($json);
        if ( empty($weatherData->list) ) {
            return response()->json( ['error_code'=> 1, 'message'=> 'Invalid request for \'' . $location .'\' location', 'results'=> null ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $retWeatherDataArray= [];
        $index= 0;
        $prior_day_number= -1;
        $today_date_label= '';
        $dayNumbersArray= [];
        foreach( $weatherData->list as $nextWeatherItem ) { // all weather rows
            $next_time_short_label= '';
            if ( !empty($weatherData->list[$index+1]) ) {
                $next_time_short_label= '-'.with(new UserWeatherLocation)->getFormattedDateTime( trim($weatherData->list[$index+1]->dt), '%H:%M%p' );
            }
            $day_number= with(new UserWeatherLocation)->getDayNumberByTimestamp( trim( $nextWeatherItem->dt ) );

            if ($day_number == $prior_day_number+1) {
                $time_label = with(new UserWeatherLocation)->getFormattedDateTime(trim($nextWeatherItem->dt), '%d %B, %H:%M') . $next_time_short_label;
                $date_label = with(new UserWeatherLocation)->getFormattedDateTime(trim($nextWeatherItem->dt), '%d %B, %A');
                $dayNumbersArray[]= [ 'key'=> $day_number, 'label' => $date_label ];
                if ($day_number == 0) {
                    $today_date_label = $date_label;
                }
            } else {
                $time_label = with(new UserWeatherLocation)->getFormattedDateTime(trim($nextWeatherItem->dt), '%H:%M') . $next_time_short_label;
            }

            $weatherItem= [
                'day_number'          => $day_number,
                'time'                => $nextWeatherItem->dt,
                'time_label'          => $time_label,
                'time_short_label'    => with(new UserWeatherLocation)->getFormattedDateTime( trim($nextWeatherItem->dt), '%H:%M%p' ),
                'temp'                => $nextWeatherItem->main->temp,
                'pressure'            => $nextWeatherItem->main->pressure, // Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
                'humidity'            => $nextWeatherItem->main->humidity, // Humidity, %
                'sea_level'           => $nextWeatherItem->main->sea_level, // Atmospheric pressure on the sea level, hPa
            ];

            $weatherItem['temp']= $nextWeatherItem->main->temp; // Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.

            if ( !empty($nextWeatherItem->weather[0]) ) {
                $weatherItem['main_text']        = $nextWeatherItem->weather[0]->main;
                $weatherItem['main_description'] = $nextWeatherItem->weather[0]->description;
                $weatherItem['icon_url']         = 'http:'.str_replace( '$$', $nextWeatherItem->weather[0]->icon, $icon_base_url );
            }
            if ( !empty($nextWeatherItem->clouds->all) ) {
                $weatherItem['clouds']           = $nextWeatherItem->clouds->all;
            }

            if ( !empty($nextWeatherItem->snow->all) ) {
                $weatherItem['snow']            = $nextWeatherItem->snow->all;
            }

            if ( !empty($nextWeatherItem->snow->all) ) {
                $weatherItem['snow']            = $nextWeatherItem->snow->all;
            }
            $retWeatherDataArray[]= $weatherItem;
            $index++;
            $prior_day_number= $day_number;
        } // foreach( $weatherData->list as $nextWeatherItem ) { // all weather rows

        $cityInfoArray= [];
        if ( !empty($weatherData->city) ) {
            $cityInfoArray= [
                'city_name'=> $weatherData->city->name,
                'lat'=> $weatherData->city->coord->lat,
                'lon'=> $weatherData->city->coord->lon,
                'country'=> $weatherData->city->country,
                'country_label'=> $this->getCountryName( strtolower($weatherData->city->country) ),
            ];
        }

        return response()->json( ['error_code'=> 0, 'message'=> '', 'weatherDataArray'=> $retWeatherDataArray, 'cityInfoArray'=> $cityInfoArray, 'dayNumbersArray'=>
            $dayNumbersArray, 'today_date_label'=> $today_date_label ], HTTP_RESPONSE_OK);
    }

    ///////////////// USER WEATHER BLOCK END //////////////////////





    /////////////////  USER TODOS BLOCK BEGIN //////////////////////
    public function user_todos_dictionaries()
    {
        try {
            $taskPrioritiesSelectionList   = Task::getTaskPrioritiesValueArray(true);
            $tasksSelectionList            = Task::getTasksSelectionList();
            $userTodoCompletedValueArray   = UserTodo::getUserTodoCompletedValueArray();
        } catch (Exception $e) {
            return response()->json( ['error_code'=> 1, 'message'=> $e->getMessage(), 'taskPrioritiesSelectionList'=> null, 'tasksSelectionList'=> null, 'userTodoCompletedValueArray'=> null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR );
        }
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'taskPrioritiesSelectionList' => $taskPrioritiesSelectionList, 'tasksSelectionList'=> $tasksSelectionList, 'userTodoCompletedValueArray'=> $userTodoCompletedValueArray ], HTTP_RESPONSE_OK );
    } // public function user_todos_dictionaries($user_todo_id)



    public function user_todos_load()
    {
        $loggedUser      = Auth::user();
        $filtersArray    = [ 'user_id'=> $loggedUser->id, 'fill_labels'=>1, 'show_task_name'=> 1 ];
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'completed_priority' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $rows_count= UserTodo::getUserTodosList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $userTodosList= UserTodo::getUserTodosList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, $order_by, $order_direction,
                $page );
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> null, 'userTodosList'=>null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page= with(new UserTodo)->getItemsPerPage();
        sleep(  config('app.sleep_in_seconds',0) );

        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'userTodosList'=>$userTodosList, 'per_page'=> $per_page ],
            HTTP_RESPONSE_OK);
    }

    public function user_todo_update(UserTodoRequest $request)
    {
        $loggedUser      = Auth::user();
        $id= $request->user_todo_id;
        $userTodo = UserTodo::find($id);

        if ( $userTodo == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'User\'s todo # "'.$id.'" not found!', 'userTodo'=>(object)['name'=> 'User\'s todo # "'.$id.'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            $updateData= $request->all();
            DB::beginTransaction();
            $userTodo->update($updateData);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userTodo'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $this->setFlashMessage( "User todo '".$userTodo->text." was updated!", 'success' );  // there was NO error : flash success message
        return response()->json(['error_code'=> 0, 'message'=> '', 'userTodo'=>$userTodo],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }

    public function user_todo_store(UserTodoRequest $request)
    {
        $loggedUser      = Auth::user();
        try {
            $insertData= $request->all();
            $user_todo_count = UserTodo::getSimilarUserTodoByText( $insertData['text'], $loggedUser->id, null, true );
            if ($user_todo_count > 0) {
                return response()->json( ['error_code'=> 1, 'message'=> 'For you "'.$insertData['text'].'" todo is already taken!', 'userTodo'=>null ],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $insertData['user_id']= $loggedUser->id;
            $insertData['priority']= 1;
            $insertData['completed']= false;
            $userTodo = UserTodo::create($insertData);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userTodo'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> '', 'userTodo'=>$userTodo],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }

    public function user_todo_destroy($id)
    {
        try {
            $userTodo = UserTodo::find($id);
            if ( $userTodo == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'User\'s todo # "'.$id.'" not found!', 'userTodo'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $userTodo->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'userTodo'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }
    ///////////////// USER TODOS BLOCK END //////////////////////


    ///////////////// USER EVENTS BLOCK START //////////////////////
    public function get_user_events($user_id)
    {
        $filtersArray    = [ 'user_id'=> $user_id, 'fill_labels'=>1, 'show_task_name'=> 1, 'show_event_name'=> 1, 'set_former_at_time'=> 1 ];
        try { // https://github.com/Wanderxx/vue-fullcalendar
            $eventUsersArray= [];
            $eventUsersList= EventUser::getEventUsersList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray );
            foreach( $eventUsersList as $next_key=>$nextEventUser ) { // "event_at_time": "2018-02-08 17:30:00",
                $end_time= $this->addMinutesToFormattedDateTime($nextEventUser->event_at_time, $nextEventUser->event_duration);
                $eventUsersArray[]= [
                    'title'=> $nextEventUser->event_name,
                    'start'=> $nextEventUser->event_at_time,
                    'start_time_label'=> $this->getFormattedDateTime($nextEventUser->event_at_time, 'H:i A' ),
                    'end'=> $end_time,
                    'end_time_label'=> $this->getFormattedDateTime($end_time, 'H:i A' ),
                    'event_duration'=> $nextEventUser->event_duration,
                    'event_access'=> $nextEventUser->event_access,
                    'at_time_is_past'=> $nextEventUser->event_at_time_is_past,
                'cssClass' => ['family', $nextEventUser->event_at_time_is_past] , 'YOUR_DATA' => ['val'=>'Let it be.'] ];
            }
            
        } catch (Exception $e) {
            return response()->json( ['error_code'=> 1, 'message'=> $e->getMessage(), 'eventUsersList'=> null, 'eventUsersArray'=> null ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR );
        }
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'eventUsersList' => $eventUsersList, 'eventUsersArray'=> $eventUsersArray ], HTTP_RESPONSE_OK );
    } // public function get_user_events($user_id)




    ///////////////// USER EVENTS BLOCK END //////////////////////

    public function get_source_file()
    {
        try {
            $src_file          =   $this->getParameter('src_file');
            $file_type         =   $this->getParameter('file_type');
            $src_file_path     =   base_path($src_file);
//            $src_file_path      =   ($src_file);
//            echo '<pre>$src_file_path::'.print_r($src_file_path,true).'</pre>';

            if (!File::exists($src_file_path))
            {
//                echo "No. No exists.";
                return response()->json(['error_code' => 1, 'message' => "File '".$src_file_path."' not found!", 'src_file' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            $file_content      =   File::get(base_path($src_file));
//            echo '<pre>EXISTS $file_content::'.print_r($file_content,true).'</pre>';
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'src_file' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( ['error_code'=> 0, 'message'=> '', 'src_file' => $src_file, 'file_type'=> $file_type, 'file_content'=> $file_content ],
            HTTP_RESPONSE_OK );
    } //public function get_source_file()

    public function get_system_info()
    {
        try {
            $system_info             =   $this->getSystemInfo();
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'src_file' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json( ['error_code'=> 0, 'message'=> '', 'system_info' => $system_info ], HTTP_RESPONSE_OK );
    } //public function get_system_info()



    public function app_description()
    {
        $this->addVariablesToJS();
        return view('admin.app_description.index');

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/admin/dashboard');
    }


}
