<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\User;
use App\Category;
use App\Http\Requests\TaskRequest;
use App\Http\Requests\CategoryRequest;
use App\Task;
use App\Settings;
use App\TaskAssignedToUser;
use App\Http\Traits\funcsTrait;
use App\UserTaskType;
use App\TaskOperation;
use App\TaskStatusChange;

class TasksController extends MyAppController
{
    use funcsTrait;

    public function tasksFilter($filter)
    {
//        echo '<pre>tasksFilter $filter::'.print_r($filter,true).'</pre>';
        $taskStatusSelectionList  = Task::getTaskStatusValueArray(false);
        foreach( $taskStatusSelectionList as $next_key=>$next_value ) {
            if ( strtolower($filter)== strtolower($next_value) ) {
                $filter= $next_key;
                break;
            }
        }
        return $this->index($filter);
    }

    public function index($filter='')
    {
//        echo '<pre>index $filter::'.print_r($filter,true).'</pre>';
        $filtersArray= ['fill_labels'=>1];
        if ( !empty($filter)and strtolower($filter) != 'all' ) {
            $filtersArray['status']= $filter;
        }
        $categories_count= 0;
        $tasks_count= 0;
        try {
            $rows_count= Task::getTasksList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $tasksList= Task::getTasksTreeDataArray( $filter,  $filtersArray, true );
            $categories_count = Category::getCategoriesList(ListingReturnData::ROWS_COUNT);
            $tasks_count = Task::getTasksList(ListingReturnData::ROWS_COUNT, $filtersArray);
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> 0, 'tasksList'=>null, 'categories_count'=> $categories_count,
                'tasks_count'=> $tasks_count ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page= with(new Task)->getItemsPerPage();
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'tasksList'=>$tasksList, 'per_page'=> $per_page, 'categories_count'=> $categories_count,
            'tasks_count'=> $tasks_count ], HTTP_RESPONSE_OK);
    }



    public function show($id)  // OK
    {
        $id = (int)$id;
        try {

            $task = Task::getRowById($id, ['fill_labels' => 1, 'show_creator_name'=>1]);
            if ($task == null) {
                return response()->json([
                    'error_code'              => 11,
                    'message'                 => 'Task # "' . $id . '" not found!',
                    'task'                    => null,
                    'categoriesSelectionList' => null
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

        } catch (Exception $e) {
            return response()->json( ['error_code'=> 1, 'message'=> $e->getMessage(), 'task'=>null,
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );

        return response()->json(['error_code'=> 0, 'message'=> '', 'task'=>$task,
        ],HTTP_RESPONSE_OK);
    }

    public function dictionaries($task_id)
    {
        try {
            $taskStatusSelectionList                = Task::getTaskStatusValueArray(true,['D','A']);
            $taskPrioritiesSelectionList            = Task::getTaskPrioritiesValueArray(true);
            $taskNeedsReportsSelectionList          = Task::getTaskNeedsReportsValueArray(true);
            $taskAssignedToUsersStatusSelectionList = Task::getTaskAssignedToUsersStatusValueArray(true);
            $categoriesSelectionList                = Category::getCategoriesSelectionList();
            $userTaskTypesSelectionList             = UserTaskType::getUserTaskTypesSelectionList();
            $activeUsersSelectionList               = User::getUsersList(ListingReturnData::LISTING, ['status' => 'A'/*, 'task_assigned_to_users_by_task_id'=>$task_id */]);

            $is_debug= 0;
//            echo '<pre>$task_id::'.print_r($task_id,true).'</pre>';
//            echo '<pre>$activeUsersSelectionList::'.print_r($activeUsersSelectionList,true).'</pre>';
//            die("-1 XXZ");
            $taskAssignedToUsersArray= [];
            $taskAssignedToUsersList= TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::LISTING, [ 'task_id'=>$task_id  ] );
            foreach( $taskAssignedToUsersList as $next_key=>$nextTaskAssignedToUser ) {
                $taskAssignedToUsersArray[]= $nextTaskAssignedToUser->toArray();
            }
            if ($is_debug) echo '<pre>$taskAssignedToUsersArray::'.print_r($taskAssignedToUsersArray,true).'</pre>';

            $activeUsersSelectionArray= [];
            foreach( $activeUsersSelectionList as $next_key=>$nextActiveUsersSelection ) {
                $is_found= false;
                $nextActiveUsersSelection= $nextActiveUsersSelection->toArray();
                if ($is_debug) echo '<pre>$activeUsersSelectionArray::'.print_r($activeUsersSelectionArray,true).'</pre>';
//                die("-1 XXZ");
                reset($taskAssignedToUsersArray);
//                echo '<pre>$nextActiveUsersSelection[\'id\']::'.print_r($nextActiveUsersSelection['id'],true).'</pre>';
                foreach ($taskAssignedToUsersArray as $next_assigned_to_user_key => $nextTaskAssignedToUser) {
//                    var_dump($nextActiveUsersSelection['id']);
//                    echo '<pre>$nextTaskAssignedToUser[\'user_id\']::'.print_r($nextTaskAssignedToUser['user_id'],true).'</pre>';
//                    var_dump($nextTaskAssignedToUser->user_id);
                    /* taskAssignedToUsersStatusSelectionList:: [ { "key": "L", "label": "Leader" }, { "key": "M", "label": "Member" }, { "key": "N", "label": "Not member" } ] */
                    if ( ( (int)$nextActiveUsersSelection['id'] ) == ( (int)$nextTaskAssignedToUser['user_id'] ) ) { // found assigned user
//                        $nextActiveUsersSelection['user_id']= $nextTaskAssignedToUser->user_id;

                        $nextActiveUsersSelection['task_assigned_status'] = 'N';
                        if ($nextTaskAssignedToUser['status'] == 'A' ) {
                            $nextActiveUsersSelection['task_assigned_status'] = 'M';
                            if ( $nextTaskAssignedToUser['is_leader'] ) {
                                $nextActiveUsersSelection['task_assigned_status'] = 'L';
                            }
                        }

//                        $nextActiveUsersSelection['status']= $nextTaskAssignedToUser->status;
                        $nextActiveUsersSelection['task_assigned_leader']= $nextTaskAssignedToUser['is_leader'];
                        $nextActiveUsersSelection['task_assigned_user_task_type_id']= $nextTaskAssignedToUser['user_task_type_id'];
                        $nextActiveUsersSelection['task_assigned_description']= $nextTaskAssignedToUser['description'];
                        $nextActiveUsersSelection['task_assigned_created_at']= $nextTaskAssignedToUser['created_at'];
                        $is_found= true;
//                        die("-1 XXZ FOUND!!!");
                        break;
                    }
//                    die("-1 XXZ  ???");
                }
                if ( !$is_found ) {
                    $nextActiveUsersSelection['task_assigned_status']= '';
                    $nextActiveUsersSelection['task_assigned_leader']= '';
                    $nextActiveUsersSelection['task_assigned_user_task_type_id']= '';
                    $nextActiveUsersSelection['task_assigned_description']= '';
                    $nextActiveUsersSelection['task_assigned_created_at']= '';
                }
                $activeUsersSelectionArray[]= $nextActiveUsersSelection;
            }
//
//            TaskAssignedToUser
            if ($is_debug) echo '<pre>+++  $activeUsersSelectionArray::'.print_r($activeUsersSelectionArray,true).'</pre>';
            if ($is_debug) echo die("-1 XXZ==");
            $categoriesSelectionList= $this->setArrayHeader( [ '' => ' -Select Category- ' ], $categoriesSelectionList );
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'documentCategory'=>null,
                'categoriesSelectionList'=> null,
                'userTaskTypesSelectionList'=> null,
                'taskStatusSelectionList'=> null,
                'taskPrioritiesSelectionList'=> null,
                'taskNeedsReportsSelectionList'=> null,
                'taskAssignedToUsersStatusSelectionList'=> null,
                'activeUsersSelectionArray'      => null
            ],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );
        usort($activeUsersSelectionArray, array($this,"activeUsersSelectionArraySort") );
        return response()->json( ['error_code'=> 0, 'message'=> '',
            'categoriesSelectionList'       => $categoriesSelectionList,
            'userTaskTypesSelectionList'    => $userTaskTypesSelectionList,
            'taskStatusSelectionList'       => $taskStatusSelectionList,
            'taskPrioritiesSelectionList'   => $taskPrioritiesSelectionList,
            'taskNeedsReportsSelectionList' => $taskNeedsReportsSelectionList,
            'taskAssignedToUsersStatusSelectionList' => $taskAssignedToUsersStatusSelectionList,
            'activeUsersSelectionArray'      => $activeUsersSelectionArray
        ], HTTP_RESPONSE_OK );
    } // public function dictionaries($task_id)






    public function activeUsersSelectionArraySort($a, $b) {

            if ($a['task_assigned_leader'] == $b['task_assigned_leader']) {


                if ($a['task_assigned_status'] == $b['task_assigned_status']) {


                    return 0;
                }
                return ($a['task_assigned_status'] < $b['task_assigned_status']) ? 1 : -1;


//                return 0;
            }
            return ($a['task_assigned_leader'] < $b['task_assigned_leader']) ? 1 : -1;
/* "id": 5,
"name": "admin",
"email": "admin@mail.com",
"status": "A",
"first_name": "Mode",
"last_name": "Pdode",
"phone": "123-567-87",
"website": "fdasdf.com",
"contract_start": "2017-01-21",
"contract_end": "2018-11-22",
"created_at": "2017-12-31 06:48:06",
"updated_at": null,
"task_assigned_status": "L",
"task_assigned_leader": 1,
"task_assigned_user_task_type_id": 1,
"task_assigned_description": "111 Team leader",
"task_assigned_created_at": "2018-01-23 10:55:51" */
    }
    
    public function update(TaskRequest $request)  // OK
    {
        $loggedUser      = Auth::user();
        $id= $request->id;
        $task = Task::find($id);

//        echo '<pre>update   $task ::'.print_r($task,true).'</pre>';
        $this->debToFile($task,'UPDATE $task::');
        if ( $task == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'Task # "'.$id.'" not found!', 'task'=>(object)['name'=> 'Task # "'.$id.'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();

            $taskAssignedToUsersList= $task->taskAssignedToUsers()->get();
            foreach( $taskAssignedToUsersList as $next_key=>$nextTaskAssignedToUser ) {
                $this->debToFile($nextTaskAssignedToUser->id,'UPDATE-4  $nextTaskAssignedToUser::');
//                echo '<pre>$nextTaskAssignedToUser->id::'.print_r($nextTaskAssignedToUser->id,true).'</pre>';
                $nextTaskAssignedToUser->delete();
            }

            $taskAssignedToUsersArray = [];
            $updateDataArray           = $request->all();

            $this->debToFile($updateDataArray,'UPDATE-2  $updateDataArray::');
//            echo '<pre>++ $updateDataArray::'.print_r($updateDataArray,true).'</pre>';
            if (isset($updateDataArray['taskAssignedToUsersArray']) and is_array($updateDataArray['taskAssignedToUsersArray'])) {
                foreach ($updateDataArray['taskAssignedToUsersArray'] as $next_key => $nextTaskAssignedToUser) {
                    $a = $this->pregSplit('~task_assigned_to_user_status_~', $nextTaskAssignedToUser['user_id']);
                    if (count($a) == 1 and ! empty($nextTaskAssignedToUser['status'])) {
                        $taskAssignedToUsersArray[] = ['user_id' => $a[0], 'status' => $nextTaskAssignedToUser['status'], 'user_task_type_id' => $nextTaskAssignedToUser['user_task_type_id'], 'description' => $nextTaskAssignedToUser['description']];
                    }
                }
                unset($updateDataArray['taskAssignedToUsersArray']);
            }


            $this->debToFile($updateDataArray,'UPDATE-3  $updateDataArray::');
            echo '<pre>$updateDataArray::'.print_r($updateDataArray,true).'</pre>';
            $task->update($updateDataArray);


            foreach ($taskAssignedToUsersArray as $next_key => $nextTaskAssignedToUser) {
                $this->debToFile($nextTaskAssignedToUser,'++ $nextTaskAssignedToUser::');
//                echo '<pre>$nextTaskAssignedToUser::'.print_r($nextTaskAssignedToUser,true).'</pre>';
                if ( !empty($nextTaskAssignedToUser['status']) and $nextTaskAssignedToUser['status'] == 'N') continue; // that is Not member - skip storing

                $newTaskAssignedToUser   =  TaskAssignedToUser::create([
                    'user_id'            => $nextTaskAssignedToUser['user_id'],
                    'status'             => 'A', //Assigning(Waiting for acception)      //                 $table->enum('status', ['A','C','P','K','O'])->comment('A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed')->default('A')->change();

                    'task_id'            => $id,
                    'is_leader'          => $nextTaskAssignedToUser['status']== 'L', // check is it task leader
                    'user_task_type_id'  => $nextTaskAssignedToUser['user_task_type_id'],
                    'description'        => $nextTaskAssignedToUser['description'],
                ]);
                $new_task_assigned_to_user_id = $newTaskAssignedToUser->id;
            }




            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'task'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $this->setFlashMessage( "Task '".$task->name." was updated!", 'success' );  // there was NO error : flash success message
        return response()->json(['error_code'=> 0, 'message'=> '', 'task'=>$task],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function store(TaskRequest $request) // ok
    {
        $loggedUser      = Auth::user();
        try {
            DB::beginTransaction();
            $insertDataArray= $request->all();
//            $this->debToFile($insertDataArray,'$insertDataArray::');


            $taskAssignedToUsersArray = [];
//            $insertDataArray           = $request->all();
            if (isset($insertDataArray['taskAssignedToUsersArray']) and is_array($insertDataArray['taskAssignedToUsersArray'])) {
                foreach ($insertDataArray['taskAssignedToUsersArray'] as $next_key => $nextTaskAssignedToUser) {
                    $a = $this->pregSplit('~task_assigned_to_user_status_~', $nextTaskAssignedToUser['user_id']);
                    if (count($a) == 1 and ! empty($nextTaskAssignedToUser['status'])) {
                        $taskAssignedToUsersArray[] = ['user_id' => $a[0], 'status' => $nextTaskAssignedToUser['status'], 'user_task_type_id' => $nextTaskAssignedToUser['user_task_type_id'], 'description' => $nextTaskAssignedToUser['description']];
                    }
                }
                unset($insertDataArray['taskAssignedToUsersArray']);
            }

            $insertDataArray['creator_id'] = $loggedUser->id;
//            echo '<pre>$insertDataArray::'.print_r($insertDataArray,true).'</pre>';
            $newTask          = Task::create($insertDataArray);

            $new_task_id = $newTask->id;
            foreach ($taskAssignedToUsersArray as $next_key => $nextTaskAssignedToUser) {
//                $this->debToFile($nextTaskAssignedToUser,'$nextTaskAssignedToUser::');
//                echo '<pre>$nextTaskAssignedToUser::'.print_r($nextTaskAssignedToUser,true).'</pre>';
                if ( !empty($nextTaskAssignedToUser['status']) and $nextTaskAssignedToUser['status'] == 'N') continue; // that is Not member - skip storing

                $newTaskAssignedToUser   =  TaskAssignedToUser::create([
                    'user_id'            => $nextTaskAssignedToUser['user_id'],
                    'status'             => 'A', //Assigning(Waiting for acception)      //                 $table->enum('status', ['A','C','P','K','O'])->comment('A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed')->default('A')->change();

                    'task_id'            => $new_task_id,              
                    'is_leader'          => $nextTaskAssignedToUser['status']== 'L', // check is it task leader
                    'user_task_type_id'  => $nextTaskAssignedToUser['user_task_type_id'],
                    'description'        => $nextTaskAssignedToUser['description'],
                ]);
                $new_task_assigned_to_user_id = $newTaskAssignedToUser->id;
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'task'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'task'=>$newTask],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }


    public function destroy($id)  // ok
    {
        try {
            $task = Task::find($id);
            if ( $task == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'Task # "'.$id.'" not found!', 'task'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $task->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'task'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }


    ////////////// TASK ASSIGNED TO USER BLOCK END ////////////////
    public function get_task_assigned_to_user_info(int $task_assigned_to_user_id)
    {
        $loggedUser      = Auth::user();
        $taskAssignedToUser = TaskAssignedToUser::getRowById( $task_assigned_to_user_id, ['fill_labels'=> 1, 'show_user_name'=> 1] );
        if ( $taskAssignedToUser == null ) {
            return response()->json(['error_code' => 8, 'message' => 'Sub Task # '.$task_assigned_to_user_id.' not found '], HTTP_RESPONSE_OK);
        }

        $task = Task::getRowById($taskAssignedToUser->task_id, ['fill_labels'=> 1, 'show_creator_name'=> 1]);
        if ( $task == null ) {
            return response()->json(['error_code' => 8, 'message' => 'Task # '.$task_assigned_to_user_id.' not found '], HTTP_RESPONSE_OK);
        }

        if ( $taskAssignedToUser == null) {
            return response()->json(['error_code' => 9, 'message' => 'Current user is not assigned to sub task # '.$task_assigned_to_user_id, 'taskAssignedToUser'=> null, 'userTeamLeaderOfTask'=> null
            ], HTTP_RESPONSE_OK);
        }

        $userTeamLeaderOfTask= TaskAssignedToUser::getTeamLeaderOfTask( $taskAssignedToUser->task_id );

        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json(['error_code' => 0, 'message' => '', 'taskAssignedToUser' => $taskAssignedToUser, 'task'=> $task, 'userTeamLeaderOfTask'=> $userTeamLeaderOfTask],
            HTTP_RESPONSE_OK);
    } // public function public function get_task_assigned_to_user_info(int $task_assigned_to_user_id)


    public function load_task_operations_history(string $request_type, int $filter_id)
    {
//        echo '<pre>load_task_operations_history $request_type::'.print_r($request_type,true).'</pre>';
//        echo '<pre>load_task_operations_history $filter_id::'.print_r($filter_id,true).'</pre>';
        $filterArray= [ 'fill_labels' => 1, 'show_tasks_info'=> '', 'show_username'=> 1, 'user_operation_id'=> 1 ];
        if ( $request_type == 'by_task_id' ) {
            $filterArray['task_id']= $filter_id;
        }
        if ( $request_type == 'by_task_assigned_to_user_id' ) {
            $filterArray['task_assigned_to_user_id']= $filter_id;
        }
        $taskOperationHistoryList = TaskOperation::getTaskOperationsList(ListingReturnData::LISTING, $filterArray, 'too.created_at', 'desc' );
       return response()->json(['error_code' => 0, 'message' => '', 'taskOperationHistoryList' => $taskOperationHistoryList],
            HTTP_RESPONSE_OK);
    } // public function public function load_task_operations_history(int $task_assigned_to_user_id)

    public function accept_task_to_processing(int $task_assigned_to_user_id)
    {
        $loggedUser      = Auth::user();
        $taskAssignedToUser = TaskAssignedToUser::getRowById( $task_assigned_to_user_id );
        if ( $taskAssignedToUser == null) {
            return response()->json(['error_code' => 9, 'message' => 'Current user is not assigned to task # '.$task_assigned_to_user_id ], HTTP_RESPONSE_OK);
        }

        $task = Task::getRowById($taskAssignedToUser->task_id);
        if ( $task == null ) {
            return response()->json(['error_code' => 9, 'message' => 'Task # '.$taskAssignedToUser->task_id.' not found '], HTTP_RESPONSE_OK);
        }
        $info= $this->getParameter( 'info' );
        try {
            DB::beginTransaction();
            $newTaskOperation= new TaskOperation();
            $newTaskOperation->task_id           = $taskAssignedToUser->task_id;
            $newTaskOperation->task_assigned_to_user_id = $taskAssignedToUser->id;
            $newTaskOperation->user_id           = $loggedUser->id;

            $newTaskOperation->prior_status      = $taskAssignedToUser->status;
            $newTaskOperation->status            = 'P';
            $newTaskOperation->user_operation_id = $loggedUser->id;
            $newTaskOperation->info              = $info;
            $newTaskOperation->save();

            $taskAssignedToUser->status= 'P';
            $taskAssignedToUser->save();
//            $taskAssignedToUser= TaskAssignedToUser::getRowByTaskIdAndUserId( $task_id, $loggedUser->id, true );
            $taskAssignedToUser = TaskAssignedToUser::getRowById( $task_assigned_to_user_id, ['fill_labels'=> 1, 'show_user_name'=> 1] );
            DB::commit();

            $site_name  = Settings::getValue('site_name', '');

            $to= $loggedUser->email;
            $email_title= $loggedUser->first_name . $loggedUser->last_name . ', you accepted the \''.$task->name.'\' task and can start working with it.';
            $email_content= $loggedUser->first_name . $loggedUser->last_name . ', you accepted task at ' . $site_name;
            $ok= $this->sendEmail($to, $email_title, $email_content);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'new_task_operation_id' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'new_task_operation_id' => $newTaskOperation->id], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } // public function accept_task_to_processing(in $task_assigned_to_user_id)

//////////////////////////////



    public function accept_task_to_cancelled(int $task_assigned_to_user_id)
    {
        $loggedUser      = Auth::user();
        $taskAssignedToUser = TaskAssignedToUser::getRowById( $task_assigned_to_user_id );
        if ( $taskAssignedToUser == null) {
            return response()->json(['error_code' => 9, 'message' => 'Current user is not assigned to task # '.$task_assigned_to_user_id ], HTTP_RESPONSE_OK);
        }

        $task = Task::getRowById($taskAssignedToUser->task_id);
        if ( $task == null ) {
            return response()->json(['error_code' => 9, 'message' => 'Task # '.$taskAssignedToUser->task_id.' not found '], HTTP_RESPONSE_OK);
        }
        $info= $this->getParameter( 'info' );
        try {
            DB::beginTransaction();
            $newTaskOperation= new TaskOperation();
            $newTaskOperation->task_id       = $taskAssignedToUser->task_id;
            $newTaskOperation->task_assigned_to_user_id = $taskAssignedToUser->id;
            $newTaskOperation->user_id       = $loggedUser->id;

            $newTaskOperation->prior_status  = $taskAssignedToUser->status;
            $newTaskOperation->status        = 'C';
            $newTaskOperation->user_operation_id = $loggedUser->id;
            $newTaskOperation->info          = $info;
            $newTaskOperation->save();

            $taskAssignedToUser->status= 'C';
            $taskAssignedToUser->save();
            $taskAssignedToUser = TaskAssignedToUser::getRowById( $task_assigned_to_user_id, ['fill_labels'=> 1, 'show_user_name'=> 1] );
            DB::commit();

            $site_name  = Settings::getValue('site_name', '');

            $to= $loggedUser->email;
            $email_title= $loggedUser->first_name. ', you cancelled the \''.$task->name.'\' task and can start working with it.';
            $email_content= $loggedUser->first_name . $loggedUser->last_name . ', you cancelled task at ' . $site_name;
            $ok= $this->sendEmail($to, $email_title, $email_content);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'new_task_operation_id' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'new_task_operation_id' => $newTaskOperation->id], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } // public function accept_task_to_cancelled(int $task_assigned_to_user_id)


    public function accept_task_to_checking(int $task_assigned_to_user_id)
    {
        $loggedUser      = Auth::user();
        $taskAssignedToUser = TaskAssignedToUser::getRowById( $task_assigned_to_user_id );
        if ( $taskAssignedToUser == null) {
            return response()->json(['error_code' => 9, 'message' => 'Current user is not assigned to task # '.$task_assigned_to_user_id ], HTTP_RESPONSE_OK);
        }

        $task = Task::getRowById($taskAssignedToUser->task_id);
        if ( $task == null ) {
            return response()->json(['error_code' => 9, 'message' => 'Task # '.$taskAssignedToUser->task_id.' not found '], HTTP_RESPONSE_OK);
        }

        $info= $this->getParameter( 'info' );
        try {
            DB::beginTransaction();
            $newTaskOperation= new TaskOperation();
            $newTaskOperation->task_id       = $taskAssignedToUser->task_id;
            $newTaskOperation->task_assigned_to_user_id = $taskAssignedToUser->id;
            $newTaskOperation->user_id       = $loggedUser->id;

            $newTaskOperation->prior_status  = $taskAssignedToUser->status;
            $newTaskOperation->status        = 'K'; // Checking
            $newTaskOperation->user_operation_id = $loggedUser->id;
            $newTaskOperation->info          = $info;
            $newTaskOperation->save();

            $taskAssignedToUser->status= 'K';  // Checking
            $taskAssignedToUser->save();
//            $taskAssignedToUser= TaskAssignedToUser::getRowByTaskIdAndUserId( $task_id, $loggedUser->id, true );
            $taskAssignedToUser = TaskAssignedToUser::getRowById( $task_assigned_to_user_id, ['fill_labels'=> 1, 'show_user_name'=> 1] );
            DB::commit();

            $site_name  = Settings::getValue('site_name', '');

            $to= $loggedUser->email;
            $email_title= $loggedUser->first_name. ', you set the \''.$task->name.'\' task to checking status and the manager of this task would check it.';
            $email_content= $loggedUser->first_name . $loggedUser->last_name . ', you set task to checking status at ' . $site_name;
            $ok= $this->sendEmail($to, $email_title, $email_content);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'new_task_operation_id' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'new_task_operation_id' => $newTaskOperation->id], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } // public function accept_task_to_checking(int $task_assigned_to_user_id)

    public function accept_task_to_complete(int $task_assigned_to_user_id)
    {
        $loggedUser      = Auth::user();
        $taskAssignedToUser = TaskAssignedToUser::getRowById( $task_assigned_to_user_id );
        if ( $taskAssignedToUser == null) {
            return response()->json(['error_code' => 9, 'message' => 'Current user is not assigned to task # '.$task_assigned_to_user_id ], HTTP_RESPONSE_OK);
        }

        $task = Task::getRowById($taskAssignedToUser->task_id);
        if ( $task == null ) {
            return response()->json(['error_code' => 9, 'message' => 'Task # '.$taskAssignedToUser->task_id.' not found '], HTTP_RESPONSE_OK);
        }

        $info= $this->getParameter( 'info' );
        try {
            DB::beginTransaction();
            $newTaskOperation= new TaskOperation();
            $newTaskOperation->task_id       = $taskAssignedToUser->task_id;
            $newTaskOperation->task_assigned_to_user_id = $taskAssignedToUser->id;
            $newTaskOperation->user_id       = $loggedUser->id;

            $newTaskOperation->prior_status  = $taskAssignedToUser->status;
            $newTaskOperation->status        = 'O'; // Completed
            $newTaskOperation->user_operation_id = $loggedUser->id;
            $newTaskOperation->info          = $info;
            $newTaskOperation->save();

            $taskAssignedToUser->status= 'O';  // Complete
            $taskAssignedToUser->save();
//            $taskAssignedToUser= TaskAssignedToUser::getRowByTaskIdAndUserId( $task_id, $loggedUser->id, true );
            $taskAssignedToUser = TaskAssignedToUser::getRowById( $task_assigned_to_user_id, ['fill_labels'=> 1, 'show_user_name'=> 1] );
            DB::commit();

            $site_name  = Settings::getValue('site_name', '');

            $to= $loggedUser->email;
            $email_title= $loggedUser->first_name. ', you set the \''.$task->name.'\' task to complete status and the manager of this task would check it.';
            $email_content= $loggedUser->first_name . $loggedUser->last_name . ', you set task to complete status at ' . $site_name;
            $ok= $this->sendEmail($to, $email_title, $email_content);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'new_task_operation_id' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'new_task_operation_id' => $newTaskOperation->id], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } // public function accept_task_to_complete(int $task_assigned_to_user_id)


    public function change_task_status(int $task_id)
    {
        $loggedUser      = Auth::user();

        $task = Task::getRowById($task_id);
        if ( $task == null ) {
            return response()->json(['error_code' => 9, 'message' => 'Task # '.$task_id.' not found '], HTTP_RESPONSE_OK);
        }
        $to_status= $this->getParameter( 'to_status' );
        $info= $this->getParameter( 'info' );
        $to_status_label= TaskStatusChange::getTaskStatusChangeStatusLabel($to_status);

        try {
            DB::beginTransaction();
            $newTaskStatusChange= new TaskStatusChange();
            $newTaskStatusChange->task_id           = $task_id;
            $newTaskStatusChange->user_id           = $loggedUser->id;
            $newTaskStatusChange->prior_status      = $task->status;
            $newTaskStatusChange->status            = $to_status;
            $newTaskStatusChange->info              = $info;
            $newTaskStatusChange->save();

            $task->status= $to_status;
            $task->updated_at= now();
            $task->save();
            $task = Task::getRowById( $task_id, ['fill_labels'=> 1, 'show_user_name'=> 1] );

            DB::commit();

            $site_name  = Settings::getValue('site_name', '');
            $to= $loggedUser->email;
            $email_title= $loggedUser->first_name . $loggedUser->last_name . ', you changed status of the \''.$task->name.'\' task to '.$to_status_label.'.';
            $email_content= $loggedUser->first_name . $loggedUser->last_name . ', you changed status of the \''.$task->name.'\' task to '.$to_status_label.' task at ' . $site_name;
            $ok= $this->sendEmail($to, $email_title, $email_content);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'new_task_status_change_id' => null, 'task'=> null, 'to_status_label'=> null],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'new_task_status_change_id' => $newTaskStatusChange->id, 'task'=> $task, 'to_status_label'=> $to_status_label],
            HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } // public function change_task_status(in $task_id)

    public function load_task_status_changes_history(string $request_type, int $filter_id)
    {
//        echo '<pre>load_task_status_changes_history $request_type::'.print_r($request_type,true).'</pre>';
//        echo '<pre>load_task_status_changes_history $filter_id::'.print_r($filter_id,true).'</pre>';
        $filterArray= [ 'fill_labels' => 1, 'show_tasks_info'=> '', 'show_username'=> 1/*, 'user_operation_id'=> 1*/ ];
        if ( $request_type == 'by_task_id' ) {
            $filterArray['task_id']= $filter_id;
        }
//        if ( $request_type == 'by_task_assigned_to_user_id' ) {
//            $filterArray['task_assigned_to_user_id']= $filter_id;
//        }
        $taskStatusChangesHistoryList = TaskStatusChange::getTaskStatusChangesList(ListingReturnData::LISTING, $filterArray, 'tsc.created_at', 'desc' );
        return response()->json(['error_code' => 0, 'message' => '', 'taskStatusChangesHistoryList' => $taskStatusChangesHistoryList],
            HTTP_RESPONSE_OK);
    } // public function public function load_task_status_changes_history(int $task_assigned_to_user_id)

    ////////////// TASK ASSIGNED TO USER BLOCK END ////////////////



    /////////////////  CATEGORY  BLOCK BEGIN //////////////////////
    ///
    ///
    ///
    public function category_show($category_id)  // OK
    {
        $category_id = (int)$category_id;
        try {

            $category = Category::getRowById($category_id, ['fill_labels' => 1, 'show_creator_name'=>1]);
            if ($category == null) {
                return response()->json([
                    'error_code'              => 11,
                    'message'                 => 'Category # "' . $category_id . '" not found!',
                    'category'                    => null,
                    'categoriesSelectionList' => null
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

        } catch (Exception $e) {
            return response()->json( ['error_code'=> 1, 'message'=> $e->getMessage(), 'category'=>null,
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );

        return response()->json(['error_code'=> 0, 'message'=> '', 'category'=>$category,
        ],HTTP_RESPONSE_OK);
    }

    public function categories_dictionaries($category_id)
    {
        try {
            $categoriesSelectionList   = Category::getCategoriesSelectionList();
            $categoriesSelectionList= $this->setArrayHeader( [ '' => ' -Select Category- ' ], $categoriesSelectionList );
        } catch (Exception $e) {
            return response()->json( ['error_code'=> 1, 'message'=> $e->getMessage(), 'categoriesSelectionList'=> null ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR );
        }
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'categoriesSelectionList' => $categoriesSelectionList ], HTTP_RESPONSE_OK );
    } // public function categories_dictionaries($category_id)

    public function category_update(CategoryRequest $request)  // OK
    {
        $id= $request->id;
        $category = Category::find($id);
        if ( $category == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'Category # "'.$id.'" not found!', 'category'=>(object)['name'=> 'Category # "'.$id.'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $category->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'category'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $this->setFlashMessage( "Category '".$category->name." was updated!", 'success' );  // there was NO error : flash success message
        return response()->json(['error_code'=> 0, 'message'=> '', 'category'=>$category],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function category_store(CategoryRequest $request) // ok
    {
        try {
            DB::beginTransaction();

            $category = Category::create($request->all());
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'category'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'category'=>$category],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }


    public function category_destroy($id) 
    {
        try {
            $category = Category::find($id);
            DB::beginTransaction();

            foreach ( $category->tasks()->get() as $task ) {
                $task->delete();
            }

            $category->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'category'=>null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

    ///////////////// CATEGORY BLOCK END //////////////////////

}
