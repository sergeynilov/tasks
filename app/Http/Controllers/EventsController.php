<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Event;
use App\User;
use App\Task;
use App\EventUser;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\Http\Requests\EventRequest;

class EventsController extends MyAppController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $filtersArray= [ 'fill_labels'=>1, 'short_description'=>'', 'show_event_users_count'=>1, 'set_former_at_time'=> 1, 'show_task_name'=> 1 ];
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'at_time' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $rows_count= Event::getEventsList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $eventsList= Event::getEventsList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, $order_by, $order_direction, $page );
//            echo '<pre>$eventsList::'.print_r($eventsList,true).'</pre>';
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> null, 'eventsList'=>null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page= with(new Event)->getItemsPerPage();
        sleep(  config('app.sleep_in_seconds',0) );

        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'eventsList'=>$eventsList, 'per_page'=> $per_page ],
            HTTP_RESPONSE_OK);
    }


    public function show($id)  // OK
    {
        $id = (int)$id;
        try {
            $event = Event::getRowById($id, ['fill_labels' => 1, 'set_former_at_time'=>1 ]);
            if ($event == null) {
                return response()->json([
                    'error_code'                        => 11,
                    'message'                           => 'Event # "' . $id . '" not found!',
                    'event'                  => null,
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'event'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json(['error_code'=> 0, 'message'=> '', 'event'=>$event],HTTP_RESPONSE_OK);
    }


    public function events_dictionaries($event_id)
    {

        try {
//            $eventTypeSelectionList   = Event::getEventTypeValueArray();
            $tasksSelectionList   = Task::getTasksSelectionList();
            $eventAccessesSelectionList= Event::getEventAccessValueArray();
            $eventAssignedForUsersArray   = [];
            $activeUsersSelectionList     = User::getUsersList(ListingReturnData::LISTING, ['status' => 'A'/*, 'task_assigned_to_users_by_task_id'=>$event_id*/  ]);

//            echo '<pre>$activeUsersSelectionList::'.print_r($activeUsersSelectionList,true).'</pre>';

//            die("-1 XXZ===");
            $is_debug= 0;
//            echo '<pre>$$event_id::'.print_r($event_id,true).'</pre>';
//            echo '<pre>$activeUsersSelectionList::'.print_r($activeUsersSelectionList,true).'</pre>';
//            die("-1 XXZ");
            $eventAssignedForUsersArray= [];
            $eventAssignedForUsersList= EventUser::getEventUsersList( ListingReturnData::LISTING, [ 'event_id'=>$event_id  ] );
            foreach( $eventAssignedForUsersList as $next_key=>$nextEventAssignedForUser ) {
                $eventAssignedForUsersArray[]= $nextEventAssignedForUser->toArray();
            }
//            if ($is_debug) echo '<pre>$eventAssignedForUsersArray::'.print_r($eventAssignedForUsersArray,true).'</pre>';

//            die("-1 XXZ====");

            $activeUsersSelectionArray= [];
            foreach( $activeUsersSelectionList as $next_key=>$nextActiveUsersSelection ) {
                $is_found= false;
                $nextActiveUsersSelection= $nextActiveUsersSelection->toArray();
                if ($is_debug) echo '<pre>$activeUsersSelectionArray::'.print_r($activeUsersSelectionArray,true).'</pre>';
//                die("-1 XXZ");
                reset($eventAssignedForUsersArray);
//                echo '<pre>$nextActiveUsersSelection[\'id\']::'.print_r($nextActiveUsersSelection['id'],true).'</pre>';
                foreach ($eventAssignedForUsersArray as $next_assigned_to_user_key => $nextEventAssignedForUser) {
//                    var_dump($nextActiveUsersSelection['id']);
//                    echo '<pre>$nextEventAssignedForUser[\'user_id\']::'.print_r($nextEventAssignedForUser['user_id'],true).'</pre>';
//                    var_dump($nextEventAssignedForUser->user_id);
                    /* taskAssignedToUsersStatusSelectionList:: [ { "key": "L", "label": "Leader" }, { "key": "M", "label": "Member" }, { "key": "N", "label": "Not member" } ] */
                    if ( ( (int)$nextActiveUsersSelection['id'] ) == ( (int)$nextEventAssignedForUser['user_id'] ) ) { // found assigned user
//                        $nextActiveUsersSelection['user_id']= $nextEventAssignedForUser->user_id;

                        // eventAssignedForUsersArray
                        $nextActiveUsersSelection['event_assigned_user_id']= $nextEventAssignedForUser['user_id'];
                        $is_found= true;
//                        die("-1 XXZ FOUND!!!");
                        break;
                    }
//                    die("-1 XXZ  ???");
                }
                if ( !$is_found ) {
                    $nextActiveUsersSelection['event_assigned_user_id']= '';
                }
                $activeUsersSelectionArray[]= $nextActiveUsersSelection;
            }
//
//            TaskAssignedToUser
            if ($is_debug) echo '<pre>+++  $activeUsersSelectionArray::'.print_r($activeUsersSelectionArray,true).'</pre>';
            if ($is_debug) echo die("-1 XXZ==");


        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'event'=>null, 'eventTypeSelectionList'=> null, 'tasksSelectionList' => null],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json( ['error_code'=> 0, 'message'=> '', 'eventAccessesSelectionList'=> $eventAccessesSelectionList, 'eventAssignedForUsersArray'=>
            $eventAssignedForUsersArray, 'activeUsersSelectionArray'=> $activeUsersSelectionArray, 'tasksSelectionList' => $tasksSelectionList], HTTP_RESPONSE_OK );
    } // public function events_dictionaries($event_id)


    public function update(EventRequest $request)  // OK
    {
        $id= $request->id;
        $event = Event::find($id);
        if ( $event == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'Event # "'.$id.'" not found!', 'event'=>(object)['name'=> 'Event # "'.$id
                                                                                                                                                      .'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $event->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'event'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $this->setFlashMessage( "Event '".$event->name." was updated!", 'success' );  // there was NO error : flash success message
        return response()->json(['error_code'=> 0, 'message'=> '', 'event'=>$event],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function store(EventRequest $request) // ok
    {
        try {
            DB::beginTransaction();
            $event = Event::create($request->all());
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'event'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'event'=>$event],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }


    public function destroy($id)  // ok
    {
        try {
            $event = Event::find($id);
            if ( $event == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'Event # "'.$id.'" not found!', 'event'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();

            $event->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'event'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

    public function load_related_events_list(string $request_type, int $filter_id)
    {
        $filterArray= [ 'fill_labels' => 1, 'set_former_at_time'=> 1 ];
        if ( $request_type == 'by_task_id' ) {
            $filterArray['task_id']= $filter_id;
        }
        $relatedEventsList = Event::getEventsList(ListingReturnData::LISTING, $filterArray, 'e.at_time', 'desc' );
        return response()->json( ['error_code' => 0, 'message' => '', 'relatedEventsList' => $relatedEventsList], HTTP_RESPONSE_OK );
    } // public function public function load_related_events_list(string $request_type, int $filter_id)


}
