<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\DocumentCategory;
use App\UserProfileDocument;
use App\UserChatMessageDocument;
use App\User;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\Http\Requests\DocumentCategoryRequest;

class DocumentCategoriesController extends MyAppController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $filtersArray= ['fill_labels'=>1, 'short_description'=>'', 'show_user_profile_documents_count'=>1, 'show_user_chat_message_documents_count'=>1, 'name'=>'']; //tsk_user_chat_message_documents
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'name' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $rows_count= DocumentCategory::getDocumentCategoriesList( ListingReturnData::ROWS_COUNT, $filtersArray );
            $documentCategoriesList= DocumentCategory::getDocumentCategoriesList( ListingReturnData::PAGINATION_BY_PARAM, $filtersArray, $order_by, $order_direction, $page );
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> null, 'documentCategoriesList'=>null ],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page= with(new DocumentCategory)->getItemsPerPage();
        sleep(  config('app.sleep_in_seconds',0) );

        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'documentCategoriesList'=>$documentCategoriesList, 'per_page'=> $per_page ],
            HTTP_RESPONSE_OK);
    }


    public function show($id)  // OK
    {
        $id = (int)$id;
        try {
            $documentCategory = DocumentCategory::getRowById($id, ['fill_labels' => 1]);
            if ($documentCategory == null) {
                return response()->json([
                    'error_code'                        => 11,
                    'message'                           => 'Document category # "' . $id . '" not found!',
                    'documentCategory'                  => null,
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'documentCategory'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json(['error_code'=> 0, 'message'=> '', 'documentCategory'=>$documentCategory],HTTP_RESPONSE_OK);
    }


    public function dictionaries()  // OK
    {
        try {
            $documentCategoryTypeSelectionList   = DocumentCategory::getDocumentCategoryTypeValueArray();
        } catch (Exception $e) {
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'documentCategory'=>null, 'documentCategoryTypeSelectionList'=> null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        sleep(  config('app.sleep_in_seconds',0) );
        return response()->json( ['error_code'=> 0, 'message'=> '', 'documentCategoryTypeSelectionList'=> $documentCategoryTypeSelectionList], HTTP_RESPONSE_OK );
    }


    public function update(DocumentCategoryRequest $request)  // OK
    {
        $id= $request->id;
        $documentCategory = DocumentCategory::find($id);
        if ( $documentCategory == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'Document category # "'.$id.'" not found!', 'documentCategory'=>(object)['name'=> 'Document category # "'.$id
                                                                                                                                                      .'" not
            # found!', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $documentCategory->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'documentCategory'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $this->setFlashMessage( "DocumentCategory '".$documentCategory->name." was updated!", 'success' );  // there was NO error : flash success message
        return response()->json(['error_code'=> 0, 'message'=> '', 'documentCategory'=>$documentCategory],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function store(DocumentCategoryRequest $request) // ok
    {
        try {
            DB::beginTransaction();
            $documentCategory = DocumentCategory::create($request->all());
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'documentCategory'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'documentCategory'=>$documentCategory],HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }


    public function destroy($id)  // ok
    {
        try {
            $documentCategory = DocumentCategory::find($id);
            if ( $documentCategory == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'Document category # "'.$id.'" not found!', 'documentCategory'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $user_profile_documents_count= UserProfileDocument::getUserProfileDocumentsList( ListingReturnData::ROWS_COUNT, [ 'document_category_id'=>$id ] );
            if ( $user_profile_documents_count > 0 ) {
                return response()->json(['error_code'=> 1, 'message'=> 'Document category # "'.$documentCategory->name.'" can not be deleted, as it is used in 
                '.$user_profile_documents_count . ' profile document(s). ',
                'documentCategory'=>null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $user_chat_message_documents_count= UserChatMessageDocument::getUserChatMessageDocumentsList( ListingReturnData::ROWS_COUNT, [ 'document_category_id'=>$id ] );
            if ( $user_chat_message_documents_count > 0 ) {
                return response()->json(['error_code'=> 1, 'message'=> 'Document category # "'.$documentCategory->name.'" can not be deleted, as it is used in 
                '.$user_chat_message_documents_count . ' chat message document(s). ',
                'documentCategory'=>null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            // user_chat_message_documents_count

            DB::beginTransaction();

            $documentCategory->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'documentCategory'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

}
