<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Traits\funcsTrait;

class TaskAssignedToUsersSelected implements Rule
{

    use funcsTrait;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function passes($attribute, $taskAssignedToUsersArray)
    {
        if (empty($taskAssignedToUsersArray) or !is_array($taskAssignedToUsersArray)) return false;

        $selected_users_count= 0;
        $selected_leaders_count= 0;
        foreach( $taskAssignedToUsersArray as $next_key=>$nextTaskAssignedToUser ) {
            if ( !empty($nextTaskAssignedToUser['status']) and $nextTaskAssignedToUser['status'] == 'N') continue; // that is Not member - skip checking
            if ( empty($nextTaskAssignedToUser['user_task_type_id']) ) return false; // user task type is not selected
            if ( !empty($nextTaskAssignedToUser['status']) ) {
                $selected_users_count++;
                if ( $nextTaskAssignedToUser['status'] == 'L' ) {
                    $selected_leaders_count++;
                }
            }
        }
        if ( $selected_users_count< 1 or $selected_leaders_count != 1 ) return false;
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Select at least 1 user(s) and only 1 of them can be the team leader with user task type selected ! ';
    }
}
