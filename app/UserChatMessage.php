<?php
namespace App;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\MyAppModel;
use App\User;
use App\Task;
use App\UserProfileDocument;
use App\DocumentCategory;
use App\UserChatNewMessage;
use App\library\ListingReturnData;
use App\Events\UserChatMessageUpdatingEvent;

class UserChatMessage extends MyAppModel
{

    protected $fillable = [ 'user_id', 'user_chat_id', 'is_top', 'text'];

    protected $table = 'user_chat_messages';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $userChatMessageIsTopValueArray = Array('M'=>'Manage this chat', 'W' => 'Can write messages', 'R' => 'Can only read');

    protected $dispatchesEvents = [
        'updating' => UserChatMessageUpdatingEvent::class,
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }

    public function userChat(){
        return $this->belongsTo('App\UserChat', 'user_chat_id','id');
    }

    public function userChatNewMessages()
    {
//        echo '<pre>userChatNewMessages -13::'.print_r(-13,true).'</pre>';
        return $this->hasMany('App\UserChatNewMessage');
    }

    protected static function boot() {
        parent::boot();
        
        // static::deleting(function($userChat) {
//        echo '<pre>boot -1::'.print_r(-1,true).'</pre>';
        static::deleting(function(UserChatMessage $userChatMessage) {
            echo '<pre>boot $userChatMessage->id::'.print_r($userChatMessage->id,true).'</pre>';
            $userChatMessage->userChatNewMessages()->delete();
        });
    }



    /* return array of key value/label pairs based on self::$userChatStatusValueArray - db enum key values/labels implementation */
    public static function getUserChatMessageIsTopValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$userChatMessageIsTopValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$userChatStatusValueArray - db enum key values/labels implementation */
    public static function getUserChatMessageIsTopLabel(string $is_top) : string
    {
        if (!empty(self::$userChatMessageIsTopValueArray[$is_top])) {
            return self::$userChatMessageIsTopValueArray[$is_top];
        }
        return '';
    }

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserChatMessagesList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'ucm.created_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $user_chat_message_table_name= with(new UserChatMessage)->getTableName();
        $quoteModel= UserChatMessage::from(  \DB::raw(DB::getTablePrefix().$user_chat_message_table_name.' as ucm' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            if ( $order_by == 'is_top_created_at') {
                $quoteModel->orderBy('is_top', 'desc');
                $quoteModel->orderBy('created_at', 'asc');
            } else {
                $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
            }
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'ucm.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( 'user_id', '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['user_chat_id'])) {
            $quoteModel->where( 'user_chat_id', '=', $filtersArray['user_chat_id'] );
        }

        if ( !empty($filtersArray['is_top']) ) {
            $quoteModel->where( 'is_top', '=', $filtersArray['is_top'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "ucm.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "ucm.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }


        if ( !empty($filtersArray['show_author_name'])  ) { // need to join in select sql username and user_status field of author of ucm
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u.name as author_name, u.first_name as author_first_name, u.last_name as author_last_name, u.status as  user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u '), \DB::raw('u.id'), '=', \DB::raw('ucm.user_id') );
        } // if ( !empty($filtersArray[show_author_name])  ) { // need to join in select sql username and user_status field of author of ucm


        if ( !empty($filtersArray['show_updated_at_by_user_name'])  ) { // need to join in select sql username and user_status field of author of ucm
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_2.name as updated_at_by_user_name, u_2.status as user_status' ;
            $quoteModel->leftJoin( \DB::raw($users_table_name . ' as u_2 '), \DB::raw('u_2.id'), '=', \DB::raw('ucm.updated_at_by_user_id') );
        } // if ( !empty($filtersArray[show_updated_at_by_user_name])  ) { // need to join in select sql username and user_status field of author of ucm


//        $l= 'users_documents';
        if ( !empty($filtersArray['show_thumbnail_image']) ) {
            $user_profile_documents_table_name= DB::getTablePrefix() . ( with(new UserProfileDocument)->getTableName() );
            $profileThumbnailImageDocumentCategory= DocumentCategory::getSimilarDocumentCategoryByAlias('profile_thumbnail_image');
//            echo '<pre>$profileThumbnailImageDocumentCategory::'.print_r($profileThumbnailImageDocumentCategory,true).'</pre>';
//            die("-1 XXZ----");
            if ( $profileThumbnailImageDocumentCategory!= null ) {
                $profile_thumbnail_document_category_id = $profileThumbnailImageDocumentCategory->id;
                $additive_fields_for_select .= ', ( select filename from ' . $user_profile_documents_table_name . ' as upd where upd.user_id = ' . \DB::raw('ucm.user_id') . ' AND upd.document_category_id = '.$profile_thumbnail_document_category_id.' AND public_access= \'P\' ) as user_thumbnail';
            }
        }


        if ( !empty($filtersArray['show_user_user_chat_new_messages']) ) {
            $user_chat_new_message_table_name= DB::getTablePrefix() . ( with(new UserChatNewMessage)->getTableName() );
//            $profile_thumbnail_document_category_id = $profileThumbnailImageDocumentCategory->id;
            $additive_fields_for_select .= ', ( select count(*) from ' . $user_chat_new_message_table_name . ' as ucnm where ucnm.user_id = ' . $filtersArray['show_user_user_chat_new_messages'] . ' AND ucnm.user_chat_message_id = ucm.id ) as user_user_chat_new_messages_count';
        }


        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserChatMessage)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserChatMessage)->isPositiveNumeric($page_param) ) ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $userChatMessagesList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $userChatMessagesList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $userChatMessagesList = $quoteModel->get();
            $data_retrieved= true;
        }
        $base_url= with(new UserProfileDocument)->getBaseUrl();
        foreach( $userChatMessagesList as $next_key=>$nextUserChatMessage ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['time_ago_labels'])) {
                $nextUserChatMessage['created_at_label']= with(new UserChatMessage)->time2str($nextUserChatMessage->created_at);
                $nextUserChatMessage['updated_at_label']= with(new UserChatMessage)->time2str($nextUserChatMessage->updated_at);
            }
            if (!empty($filtersArray['fill_labels'])) {
                $nextUserChatMessage['is_top_label']= UserChatMessage::getUserChatMessageIsTopLabel($nextUserChatMessage->is_top);
            }

            if ( !empty($filtersArray['show_user_profile_props']) and is_array($filtersArray['show_user_profile_props']) ) { // need to join in
                $userProfileValuesArray= UserProfiles::getUserProfilesList(['user_id'=> $nextUserChatMessage->user_id, 'name'=>['background_color', 'color'] ], true );
                $nextUserChatMessage['userProfileValuesArray'] = $userProfileValuesArray;
            } // if ( !empty($filtersArray[show_user_profile_props])  ) { // need to join in select sql username and user_status field of author of ucm

            if ( !empty($filtersArray['show_thumbnail_image']) and !empty($nextUserChatMessage['user_thumbnail']) ) {
                $next_user_thumbnail_image_filename= UserProfileDocument::getUserProfileDocumentPath($nextUserChatMessage->user_id, $nextUserChatMessage['user_thumbnail'], false);
                $next_user_thumbnail_image_filename_url= $base_url.Storage::disk('local')->url($next_user_thumbnail_image_filename );
                $file_exists = Storage::disk('local')->exists('public/'.$next_user_thumbnail_image_filename);
                if ( $file_exists ) {
                    $nextUserChatMessage['user_thumbnail_image_filename_url'] = $next_user_thumbnail_image_filename_url;
                }
            } // if ( !empty($filtersArray['show_thumbnail_image']) ) {
        }
        return $userChatMessagesList;

    } // public static function getUserChatMessagesList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

        if ( !$is_row_retrieved ) {
            $userChatMessage = UserChatMessage::find($id);
        }

        if (empty($userChatMessage)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $userChatMessage['created_at_label']= with(new UserChatMessage)->getFormattedDateTime($userChatMessage->created_at);
            $userChatMessage['updated_at_label']= with(new UserChatMessage)->getFormattedDateTime($userChatMessage->updated_at);
            $userChatMessage['is_top_label']= UserChatMessage::getUserChatMessageIsTopLabel($userChatMessage->is_top);
        }
        return $userChatMessage;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray($user_chat_id) : array
    {
        $validationRulesArray = [
            'user_id'        => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'user_chat_id'   => 'required|exists:'.( with(new UserChat)->getTableName() ).',id',
            'is_top'         => 'required|in:'.with( new UserChatMessage)->getValueLabelKeys( UserChatMessage::getUserChatMessageIsTopValueArray(false) ),
            'text'           => 'required',
        ];
        return $validationRulesArray;
    }



}

