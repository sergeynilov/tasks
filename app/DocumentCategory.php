<?php
namespace App;

use DB;
use App\MyAppModel;
use App\UserProfileDocument;
use App\UserChatMessageDocument;
use App\library\ListingReturnData;

class DocumentCategory extends MyAppModel
{

    protected $table = 'document_categories';
    protected $primaryKey = 'id';
    public $timestamps = false;

    private static $documentCategoryTypeValueArray = Array('D' => 'Task Document', 'C' => 'Chat Document', 'P' => 'Profile Document', 'M' => 'Profile Main Image', 'T' => 'Profile Thumbnail Image');


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'type', 'description'];

    /* return array of key value/label pairs based on self::$documentCategoryTypeValueArray - db enum key values/labels implementation */
    public static function getDocumentCategoryTypeValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$documentCategoryTypeValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$documentCategoryTypeValueArray - db enum key values/labels implementation */
    public static function getDocumentCategoryTypeLabel(string $type) : string
    {
        if (!empty(self::$documentCategoryTypeValueArray[$type])) {
            return self::$documentCategoryTypeValueArray[$type];
        }
        return '';
    }

    /* return data array by keys id/name based on filters/ordering... */
    public static function getDocumentCategoriesSelectionList( bool $key_return= true, array $filtersArray = array(), string $order_by = 'name', string $order_direction = 'asc') : array
    {
        $documentCategoriesList = DocumentCategory::getDocumentCategoriesList(ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction);
        $resArray = [];
        foreach ($documentCategoriesList as $nextTask) {
            if ($key_return) {
                $resArray[] = array('key' => $nextTask->id, 'label' => $nextTask->name);
            } else{
                $resArray[ $nextTask->id ]= $nextTask->name . ' (' . $nextTask->email . ')';
            }
        }
        return $resArray;
    }

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getDocumentCategoriesList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'dc.name'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $document_category_table_name= with(new DocumentCategory)->getTableName();
        $quoteModel= DocumentCategory::from(  \DB::raw(DB::getTablePrefix().$document_category_table_name.' as dc' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'dc.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( DocumentCategory::myStrLower('name', false, false) . ' like ' . DocumentCategory::myStrLower( $filtersArray['name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.DocumentCategory::myStrLower('name', false, false) . ' like ' . DocumentCategory::myStrLower( $filtersArray['name'], true,true ) . ' OR ' . DocumentCategory::myStrLower('description', false, false) . ' like ' . DocumentCategory::myStrLower( $filtersArray['name'], true,true ) .
                                       ' OR ' . DocumentCategory::myStrLower('description', false, false) . ' like ' . DocumentCategory::myStrLower( $filtersArray['name'], true,true ) . ' ) ');
            }
        }

        if (!empty($filtersArray['type'])) {
            if ( is_array($filtersArray['type']) ) {
                $quoteModel->whereIn(DB::raw('dc.type'), $filtersArray['type']);
            } else {
                $quoteModel->where(DB::raw('dc.type'), '=', $filtersArray['type']);
            }
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "dc.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "dc.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( !empty($filtersArray['show_user_profile_documents_count']) ) {
            $user_profile_document_table_name=  with(new UserProfileDocument)->getTableName();
            $additive_fields_for_select .= ', ( select count(*) from ' . \DB::raw(DB::getTablePrefix() . $user_profile_document_table_name ) . ' as upd where upd.document_category_id = ' . \DB::raw('dc.id').' ) as user_profile_documents_count';
        }

        if ( !empty($filtersArray['show_user_chat_message_documents_count']) ) {
            $user_chat_message_document_table_name=  with(new UserChatMessageDocument)->getTableName();
            $additive_fields_for_select .= ', ( select count(*) from ' . \DB::raw(DB::getTablePrefix() . $user_chat_message_document_table_name ) . ' as ucmd where ucmd.document_category_id = ' . \DB::raw('dc.id').' ) as user_chat_message_documents_count';
        }


        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new DocumentCategory)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new DocumentCategory)->isPositiveNumeric($page_param) ) ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $documentCategoriesList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $documentCategoriesList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $documentCategoriesList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $documentCategoriesList as $next_key=>$nextDocumentCategory ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextDocumentCategory['created_at_label']= with(new DocumentCategory)->getFormattedDateTime($nextDocumentCategory->created_at);
                $nextDocumentCategory['type_label']= DocumentCategory::getDocumentCategoryTypeLabel($nextDocumentCategory->type);
            }
            if (!empty($filtersArray['short_description'])) {
                $nextDocumentCategory['description']= with(new DocumentCategory)->concatStr($nextDocumentCategory->description,50);
            }
        }
        return $documentCategoriesList;

    } // public static function getDocumentCategoriesList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

        if ( !$is_row_retrieved ) {
            $documentCategory = DocumentCategory::find($id);
            if ( $documentCategory == null ) return false;
            if (!empty($additiveParams['fill_labels'])) {
                $documentCategory['created_at_label']= with(new DocumentCategory)->getFormattedDateTime($documentCategory->created_at);
                $documentCategory['type_label']= DocumentCategory::getDocumentCategoryTypeLabel($documentCategory->type);
            }
        }

        return $documentCategory;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray($document_category_id) : array
    {
//        return[];
        $additional_name_validation_rule= 'check_document_category_unique_by_name:'.( !empty($document_category_id)?$document_category_id:'');
        $validationRulesArray = [
            'name'=> 'required|max:255|'.$additional_name_validation_rule,
            'type'       => 'required|in:'.with( new DocumentCategory)->getValueLabelKeys( DocumentCategory::getDocumentCategoryTypeValueArray(false) ),
            'description'=> 'required',

        ];
        return $validationRulesArray;
    }

    /* check if provided name is unique for document_categories.name field */
    public static function getSimilarDocumentCategoryByName( string $name, int $id= null, bool $return_count = false )
    {        
        $quoteModel = DocumentCategory::whereRaw( DocumentCategory::myStrLower('name', false, false) .' = '. DocumentCategory::myStrLower( DocumentCategory::mysqlEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    /* check if provided alias is unique for document_categories.alias field */
    public static function getSimilarDocumentCategoryByAlias( string $alias, int $id= null, bool $return_count = false )
    {
        $quoteModel = DocumentCategory::whereRaw( DocumentCategory::myStrLower('alias', false, false) .' = '. DocumentCategory::myStrLower( DocumentCategory::mysqlEscape($alias), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }



}

