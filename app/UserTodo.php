<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\Task;
use App\library\ListingReturnData;

class UserTodo extends MyAppModel
{

    protected $table = 'user_todos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $userTodoCompletedValueArray = Array('0'=>'Not Completed', '1' => 'Completed');
    private static $userTodoPriorityLabelValueArray = Array(1 => 'No', 2 => 'Low', 3 => 'Normal', 4 => 'High', 5 => 'Urgent', 6 => 'Immediate');
    protected $fillable = [ 'user_id', 'text', 'priority', 'task_id' , 'completed' ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }

    public function task(){
        return $this->belongsTo('App\Task', 'task_id','id');
    }


    public static function getUserTodoPrioritiesValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$userTodoPriorityLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getUserTodoPriorityLabel(string $priority):string
    {
        if (!empty(self::$userTodoPriorityLabelValueArray[$priority])) {
            return self::$userTodoPriorityLabelValueArray[$priority];
        }
        return '';
    }


    /* return array of key value/label pairs based on self::$userChatStatusValueArray - db enum key values/labels implementation */
    public static function getUserTodoCompletedValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$userTodoCompletedValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$userTodoCompletedValueArray - db enum key values/labels implementation */
    public static function getUserTodoCompletedValueArrayLabel(string $location_type) : string
    {
        if (!empty(self::$userTodoCompletedValueArray[$location_type])) {
            return self::$userTodoCompletedValueArray[$location_type];
        }
        return '';
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserTodosList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'ut.priority';
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $user_todo_table_name= with(new UserTodo)->getTableName();
        $quoteModel= UserTodo::from(  \DB::raw(DB::getTablePrefix().$user_todo_table_name.' as ut' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            if ( $order_by == 'completed_priority') {
                $quoteModel->orderBy('completed', 'asc');
                $quoteModel->orderBy('priority', 'desc');
            } else {
                $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
            }
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'ut.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['text'])) {
            $quoteModel->whereRaw( UserTodo::myStrLower('text', false, false) . ' like ' . UserTodo::myStrLower( $filtersArray['text'], true,true ));
        }

        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( DB::raw('ut.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['task_id'])) {
            $quoteModel->where( DB::raw('ut.task_id'), '=', $filtersArray['task_id'] );
        }

        if (!empty($filtersArray['priority'])) {
            $quoteModel->where( DB::raw('ut.priority'), '=', $filtersArray['priority'] );
        }
        if (!empty($filtersArray['completed'])) {
            $quoteModel->where( DB::raw('ut.completed'), '=', $filtersArray['completed'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("ut.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("ut.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        if ( !empty($filtersArray['show_task_name'])  ) { // need to join in select sql task name and task_status field
            $tasks_table_name= DB::getTablePrefix() . ( with(new Task)->getTableName() );
            $additive_fields_for_select .= ', t.name as task_name, t.status as task_status' ;
            $quoteModel->leftJoin( \DB::raw($tasks_table_name . ' as t '), \DB::raw('t.id'), '=', \DB::raw('ut.task_id') );
        } // if ( !empty($filtersArray['show_task_name'])  ) { // need to join in select sql task name and task_status field


        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserTodo)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserTodo)->isPositiveNumeric($page_param) ) ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $userTodosList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $userTodosList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $userTodosList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $userTodosList as $next_key=>$nextUserTodo ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextUserTodo['created_at_label']= with(new UserTodo)->getFormattedDateTime($nextUserTodo->created_at);
                $nextUserTodo['priority_label']= UserTodo::getUserTodoPriorityLabel($nextUserTodo->priority);
                $nextUserTodo['completed_label']= UserTodo::getUserTodoCompletedValueArrayLabel($nextUserTodo->completed);
            }
        }
        return $userTodosList;

    } // public static function getUserTodosList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if (!empty($additiveParams['show_user_name']) ) { // need to join in select sql username and user_status field of author of user Todo
            $additive_fields_for_select= "";
            $fields_for_select= 'ut.*';
            $user_todo_table_name= with(new UserTodo)->getTableName();
            $quoteModel= UserTodo::from(  \DB::raw(DB::getTablePrefix().$user_todo_table_name.' as uc' ));
            $quoteModel->where( \DB::raw('ut.id'), '=', $id );
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_c.name as user_name, u_c.status as  user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_c '), \DB::raw('u_c.id'), '=', \DB::raw('ut.user_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $userTodoRows = $quoteModel->get();
            if (!empty($userTodoRows[0]) and get_class($userTodoRows[0]) == 'App\UserTodo' ) {
                $userTodo= $userTodoRows[0];
                $is_row_retrieved= true;
            }
        } // if (!empty($additiveParams['show_user_name']) ) { // need to join in select sql username and user_status field of author of user Todo


        if ( !$is_row_retrieved ) {
            $userTodo = UserTodo::find($id);
        }

        if (empty($userTodo)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $userTodo['created_at_label']= with(new UserTodo)->getFormattedDateTime($userTodo->created_at);
            $userTodo['priority_label']= UserTodo::getUserTodoPriorityLabel($userTodo->priority);
            $userTodo['completed_label']= UserTodo::getUserTodoCompletedValueArrayLabel($userTodo->completed);
        }
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $userTodo= with(new UserTodo)->substituteNullValues($userTodo, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $userTodo;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $user_todo_id ) : array
    {
        $validationRulesArray = [
            'text'            => 'required|max:255',
            'user_id'         => 'nullable|exists:'.( with(new User)->getTableName() ).',id',
            'priority'        => 'required|in:'.with( new UserTodo)->getValueLabelKeys( UserTodo::getUserTodoPrioritiesValueArray(false) ),
            'task_id'         => 'nullable|exists:'.( with(new Task)->getTableName() ).',id',
            'completed'       => 'required|in:'.with( new UserTodo)->getValueLabelKeys( UserTodo::getUserTodoCompletedValueArray(false) ),
/* 	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(10) UNSIGNED NOT NULL DEFAULT '1',
	`text` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`priority` ENUM('0','1','3','4','5') NOT NULL DEFAULT '0' COMMENT '  0-No, 1-Low, 2-Normal, 3-High, 4-Urgent, 5-Immediate  ' COLLATE 'utf8mb4_unicode_ci',
	`task_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`completed` TINYINT(1) NOT NULL DEFAULT '0',
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  */
        ];

        return $validationRulesArray;
    }

    /* check if provided name is unique for user_todo.text field */
    public static function getSimilarUserTodoByText( string $text, int $user_id, int $id= null, bool $return_count = false )
    {
        $quoteModel = UserTodo::whereRaw( UserTodo::myStrLower('text', false, false) .' = '. UserTodo::myStrLower( UserTodo::mysqlEscape($text), true,false) );
        $quoteModel = $quoteModel->where( 'user_id', '=' , $user_id );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


}