<?php
namespace App;

use DB;
use App\MyAppModel;
use App\TaskAssignedToUser;
use App\User;
use App\library\ListingReturnData;

class UserTaskType extends MyAppModel
{

    protected $table = 'user_task_types';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /* return data array by keys id/name based on filters/ordering... */
    public static function getUserTaskTypesSelectionList( bool $key_return= true, array $filtersArray = array(), string $order_by = 'name', string $order_direction = 'asc') : array
    {
        $userTaskTypesList = UserTaskType::getUserTaskTypesList(ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction);
        $resArray = [];
        foreach ($userTaskTypesList as $nextUserTaskType) {
            if ($key_return) {
                $resArray[] = array('key' => $nextUserTaskType->id, 'label' => $nextUserTaskType->name);
            } else{
                $resArray[ $nextUserTaskType->id ]= $nextUserTaskType->name;
            }
        }
        return $resArray;
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserTaskTypesList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'utt.name'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $user_task_type_table_name= with(new UserTaskType)->getTableName();
        $quoteModel= UserTaskType::from(  \DB::raw(DB::getTablePrefix().$user_task_type_table_name.' as utt' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'utt.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( UserTaskType::myStrLower('name', false, false) . ' like ' . UserTaskType::myStrLower( $filtersArray['name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.UserTaskType::myStrLower('name', false, false) . ' like ' . UserTaskType::myStrLower( $filtersArray['name'], true,true ) . ' OR ' . UserTaskType::myStrLower('description', false, false) . ' like ' . UserTaskType::myStrLower( $filtersArray['name'], true,true ) .
                                       ' OR ' . UserTaskType::myStrLower('description', false, false) . ' like ' . UserTaskType::myStrLower( $filtersArray['name'], true,true ) . ' ) ');
            }
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "utt.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "utt.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( !empty($filtersArray['show_task_assigned_to_users_count']) ) {
            $task_assigned_to_user_table_name=  with(new TaskAssignedToUser)->getTableName();
            $additive_fields_for_select .= ', ( select count(*) from ' . \DB::raw(DB::getTablePrefix() . $task_assigned_to_user_table_name ) . ' as tatu where tatu.user_task_type_id = ' . \DB::raw('utt.id').' ) as task_assigned_to_users_count';
        }


        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserTaskType)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserTaskType)->isPositiveNumeric($page_param) ) ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $userTaskTypesList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $userTaskTypesList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $userTaskTypesList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $userTaskTypesList as $next_key=>$nextUserTaskType ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextUserTaskType['created_at_label']= with(new UserTaskType)->getFormattedDateTime($nextUserTaskType->created_at);
            }
            if (!empty($filtersArray['short_description'])) {
                $nextUserTaskType['description']= with(new UserTaskType)->concatStr($nextUserTaskType->description,50);
            }
        }
        return $userTaskTypesList;

    } // public static function getUserTaskTypesList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

        if ( !$is_row_retrieved ) {
            $userTaskType = UserTaskType::find($id);
            if ( $userTaskType == null ) return false;
            if (!empty($additiveParams['fill_labels'])) {
                $userTaskType['created_at_label']= with(new UserTaskType)->getFormattedDateTime($userTaskType->created_at);
            }
        }
        return $userTaskType;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray($user_task_type_id) : array
    {
        $additional_name_validation_rule= 'check_user_task_type_unique_by_name:'.( !empty($user_task_type_id)?$user_task_type_id:'');
        $validationRulesArray = [
            'name'=> 'required|max:50|'.$additional_name_validation_rule,
            'description'=> 'required',

        ];
        return $validationRulesArray;
    }

    /* check if provided name is unique for user_task_types.name field */
    public static function getSimilarUserTaskTypeByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = UserTaskType::whereRaw( UserTaskType::myStrLower('name', false, false) .' = '. UserTaskType::myStrLower( UserTaskType::mysqlEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }



}

