<?php namespace App\library {

    use WBoyz\LaravelEnum\BaseEnum;

    class ListingReturnData extends BaseEnum
    {
        const ROWS_COUNT = 1;
        const LISTING = 2;
        const PAGINATION_BY_URL = 3;
        const PAGINATION_BY_PARAM = 4;
    }

}