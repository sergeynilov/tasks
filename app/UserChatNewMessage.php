<?php
namespace App;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\Facades\Image as Image;

use App\MyAppModel;
use App\User;
use App\Config;
use App\UserChatMessage;
use App\library\ListingReturnData;

class UserChatNewMessage extends MyAppModel
{

    protected $table = 'user_user_chat_new_messages';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $userChatNewMessageImagePropsArray = [];
    protected $fillable = [ 'user_chat_message_id', 'user_id' ];

    public function userChatMessage(){
        return $this->belongsTo('App\UserChatMessage', 'user_chat_message_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }

/* CREATE TABLE `tsk_user_user_chat_new_messages` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(10) UNSIGNED NOT NULL,
	`user_chat_message_id` INT(10) UNSIGNED NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, */

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserChatNewMessagesList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0
    ) {
        if (empty($order_by)) $order_by = 'ucnm.user_id'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $user_chat_new_message_table_name= with(new UserChatNewMessage)->getTableName();
        $user_chat_table_message_name= DB::getTablePrefix().with(new UserChatMessage())->getTableName();
        $quoteModel= UserChatNewMessage::from(  \DB::raw(DB::getTablePrefix().$user_chat_new_message_table_name.' as ucnm' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'ucnm.*';

        if (!empty($filtersArray['user_chat_message_id'])) {
            $quoteModel->where( DB::raw('ucnm.user_chat_message_id'), '=', $filtersArray['user_chat_message_id'] );
        }

        if (!empty($filtersArray['user_chat_id'])) {
            $quoteModel->join(\DB::raw($user_chat_table_message_name . \DB::raw(' as ucm ')), \DB::raw('ucm.id'), '=', \DB::raw('ucnm.user_chat_message_id'));
            $quoteModel->where( DB::raw('ucm.user_chat_id'), '=', $filtersArray['user_chat_id'] );
        }

        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( DB::raw('ucnm.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "ucnm.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "ucnm.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserChatNewMessage)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserChatNewMessage)->isPositiveNumeric($page_param) ) ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $userChatNewMessagesList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $userChatNewMessagesList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $userChatNewMessagesList = $quoteModel->get();
            $data_retrieved= true;
        }
        return $userChatNewMessagesList;
    } // public static function getUserChatNewMessagesList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param=
    // 0 ) {


    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

        if ( !$is_row_retrieved ) {
            $userChatNewMessage = UserChatNewMessage::find($id);
        }

        if (empty($userChatNewMessage)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $userChatNewMessage['created_at_label']= with(new UserChatNewMessage)->getFormattedDateTime($userChatNewMessage->created_at);
        }
        return $userChatNewMessage;
    } // public function getRowById( int $id, array $additiveParams= [] )


    public static function getValidationRulesArray($user_id= null) : array
    {
        $validationRulesArray = [
            'user_id'        => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'user_chat_id'   => 'required|exists:'.( with(new UserChat)->getTableName() ).',id',

        ];
        return $validationRulesArray;
    }

    public static function clear( int $user_id,  int $user_chat_id )
    {
        $userChatNewMessagesList= UserChatNewMessage::getUserChatNewMessagesList( ListingReturnData::LISTING, ['user_id'=> $user_id, 'user_chat_id'=> $user_chat_id] );
        foreach( $userChatNewMessagesList as $nextUserChatNewMessage ) {
            $nextUserChatNewMessage->delete();
        }
    }

}
