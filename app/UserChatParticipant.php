<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\UserChatLastVisited;
use App\library\ListingReturnData;

class UserChatParticipant extends MyAppModel
{

    protected $fillable = [ 'user_id', 'user_chat_id', 'status'];
    
    protected $table = 'user_chat_participants';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $userChatParticipantStatusValueArray = Array('M'=>'Manage this chat', 'W' => 'Can write messages', 'R' => 'Can only read');


    public function userChat(){
        return $this->belongsTo('App\UserChat', 'user_chat_id','id');
    }

    /* return array of key value/label pairs based on self::$userChatParticipantStatusValueArray - db enum key values/labels implementation */
    public static function getUserChatParticipantStatusValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$userChatParticipantStatusValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$userChatParticipantStatusValueArray - db enum key values/labels implementation */
    public static function getUserChatParticipantStatusLabel(string $status) : string
    {
        if (!empty(self::$userChatParticipantStatusValueArray[$status])) {
            return self::$userChatParticipantStatusValueArray[$status];
        }
        return '';
    }

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserChatParticipantsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if (empty($order_by)) $order_by = 'ucp.status'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $user_chat_participant_table_name= with(new UserChatParticipant)->getTableName();
        $quoteModel= UserChatParticipant::from(  \DB::raw(DB::getTablePrefix().$user_chat_participant_table_name.' as ucp' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'ucp.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( 'user_id', '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['user_chat_id'])) {
            $quoteModel->where( 'user_chat_id', '=', $filtersArray['user_chat_id'] );
        }

        if (!empty($filtersArray['status'])) {
            $quoteModel->where( 'status', '=', $filtersArray['status'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "ucp.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "ucp.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        if ( !empty($filtersArray['show_username'])  ) { // need to join in select sql username and user_status field of author of t item
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u.name as username, u.first_name, u.last_name, u.status as  user_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u '), \DB::raw('u.id'), '=', \DB::raw('ucp.user_id') );
        } // if ( !empty($filtersArray['show_username'])  ) { // need to join in select sql username and user_status field of author of t item


        if ( !empty($filtersArray['show_user_chats_last_visited'])  ) { // need to join user chats last visited
            $user_chat_last_visited_table_name= DB::getTablePrefix().( with(new UserChatLastVisited)->getTableName() );
            $additive_fields_for_select .= ', ( select visited_at from ' . $user_chat_last_visited_table_name . ' as uclv where uclv.user_chat_id = ' . 'ucp.user_chat_id and uclv.user_id = ' . 'ucp.user_id ) as visited_at';
        } // if ( !empty($filtersArray['show_user_chats_last_visited'])  ) { // need to join user chats last visited


//        if ( !empty($filtersArray['show_creator_name'])  ) { // need to join in select sql username and user_status field of author of ucp
//            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
//            $additive_fields_for_select .= ', u.name as username, u.status as  user_status' ;
//            $quoteModel->join( \DB::raw($users_table_name . ' as u '), \DB::raw('u.id'), '=', \DB::raw('ucp.creator_id') );
//        } // if ( !empty($filtersArray[show_creator_name])  ) { // need to join in select sql username and user_status field of author of ucp

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserChatParticipant)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserChatParticipant)->isPositiveNumeric($page_param) ) ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $userChatParticipantsList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $userChatParticipantsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $userChatParticipantsList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $userChatParticipantsList as $next_key=>$nextUserChatParticipant ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextUserChatParticipant['created_at_label']= with(new UserChatParticipant)->getFormattedDateTime($nextUserChatParticipant->created_at);
                $nextUserChatParticipant['status_label']= UserChatParticipant::getUserChatParticipantStatusLabel($nextUserChatParticipant->status);
                if ( !empty($nextUserChatParticipant['visited_at']) ) {
                    $nextUserChatParticipant['visited_at_label']= with(new UserChatParticipant)->getFormattedDateTime($nextUserChatParticipant->visited_at);
                }
                if ( !empty($nextUserChatParticipant['user_status']) ) {
                    $nextUserChatParticipant['user_status_label'] = User::getUserStatusLabel($nextUserChatParticipant->user_status);
                }

            }
            if (!empty($filtersArray['set_null_fields_space']) and is_array($filtersArray['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
                $userChatParticipantsList[$next_key]= with(new User)->substituteNullValues($nextUserChatParticipant, $filtersArray['set_null_fields_space']);
            } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        }
        return $userChatParticipantsList;

    } // public static function getUserChatParticipantsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if ( !$is_row_retrieved ) {
            $userChatParticipant = UserChatParticipant::find($id);
        }

        if (empty($userChatParticipant)) return false;
        if (!empty($additiveParams['fill_labels'])) {
            $userChatParticipant['created_at_label']= with(new UserChatParticipant)->getFormattedDateTime($userChatParticipant->created_at);
            $userChatParticipant['status_label']= UserChatParticipant::getUserChatParticipantStatusLabel($userChatParticipant->status);
        }
        return $userChatParticipant;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray($user_chat_id) : array
    {
        $validationRulesArray = [
            'user_id'        => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'user_chat_id'   => 'required|exists:'.( with(new UserChat)->getTableName() ).',id',
            'status'         => 'required|in:'.with( new UserChatParticipant)->getValueLabelKeys( UserChatParticipant::getUserChatParticipantStatusValueArray(false) ),
        ];
        return $validationRulesArray;
    }

    /* check if provided name is unique for user_chats.name field */
    public static function getSimilarUserChatParticipantByUserIdAndUserChatId( int $user_id, int $user_chat_id, bool $return_count = false )
    {
        $quoteModel = UserChatParticipant::where( 'user_id', $user_id );
        $quoteModel = $quoteModel->where( 'user_chat_id', $user_chat_id );
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


}

