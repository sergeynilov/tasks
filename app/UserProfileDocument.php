<?php
namespace App;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\Facades\Image as Image;

use App\MyAppModel;
use App\User;
use App\Config;
use App\DocumentCategory;
use App\library\ListingReturnData;


class UserProfileDocument extends MyAppModel
{

    protected $table = 'user_profile_documents';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $userProfileDocumentPublicAccessValueArray = Array('P'=>'Public', 'E' => 'Personal');

    protected $userProfileDocumentImagePropsArray = [];
    protected static $img_filename_max_length= 50;
    protected static $img_preview_width= 128;
    protected static $img_preview_height= 96;

    protected $fillable = [ 'filename', 'user_id', 'document_category_id', 'public_access', 'info' ];

    /* return array of key value/label pairs based on self::$userProfileDocumentPublicAccessValueArray - db enum key values/labels implementation */
    public static function getUserProfileDocumentPublicAccessValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$userProfileDocumentPublicAccessValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$userProfileDocumentPublicAccessValueArray - db enum key values/labels implementation */
    public static function getUserProfileDocumentPublicAccessLabel(string $public_access) : string
    {
        if (!empty(self::$userProfileDocumentPublicAccessValueArray[$public_access])) {
            return self::$userProfileDocumentPublicAccessValueArray[$public_access];
        }
        return '';
    }

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getUserProfileDocumentsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0
    ) {
        if (empty($order_by)) $order_by = 'upd.filename'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $document_categories_table_name= with(new DocumentCategory)->getTableName();
        $user_profile_document_table_name= with(new UserProfileDocument)->getTableName();
        $quoteModel= UserProfileDocument::from(  \DB::raw(DB::getTablePrefix().$user_profile_document_table_name.' as upd' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'upd.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['filename'])) {
            if ( empty($filtersArray['in_info']) ) {
                $quoteModel->whereRaw( UserProfileDocument::myStrLower('filename', false, false) . ' like ' . UserProfileDocument::myStrLower( $filtersArray['filename'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.UserProfileDocument::myStrLower('filename', false, false) . ' like ' . UserProfileDocument::myStrLower( $filtersArray['filename'], true,true ) . ' OR ' . UserProfileDocument::myStrLower('info', false, false) . ' like ' . UserProfileDocument::myStrLower( $filtersArray['filename'], true,true ) .
                                       ' OR ' . UserProfileDocument::myStrLower('info', false, false) . ' like ' . UserProfileDocument::myStrLower( $filtersArray['filename'], true,true ) . ' ) ');
            }
        }
        
        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( DB::raw('upd.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['document_category_id'])) {
            $quoteModel->where( DB::raw('upd.document_category_id'), '=', $filtersArray['document_category_id'] );
        }

        if (!empty($filtersArray['document_category_type'])) {
            if (is_array($filtersArray['document_category_type'])) {
                $quoteModel->join(DB::raw(DB::getTablePrefix() . $document_categories_table_name . ' as dct '), \DB::raw('dct.id'), '=', \DB::raw('upd.document_category_id'));
                $quoteModel->whereIn(DB::raw('dct.type'), $filtersArray['document_category_type']);
            } else {
                $quoteModel->join(DB::raw(DB::getTablePrefix() . $document_categories_table_name . ' as dct '), \DB::raw('dct.id'), '=', \DB::raw('upd.document_category_id'));
                $quoteModel->where(DB::raw('dct.type'), '=', $filtersArray['document_category_type']);
            }
        }

        if (!empty($filtersArray['public_access'])) {
            $quoteModel->where(DB::raw('upd.public_access'), '=', $filtersArray['public_access']);
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "upd.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "upd.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( !empty($filtersArray['show_document_category_name'])  ) { // need to join in select sql show_document_category name
            $document_categories_table_name= DB::getTablePrefix() . ( with(new DocumentCategory)->getTableName() );
            $additive_fields_for_select .= ', dc.name as document_category_name' ;
            $quoteModel->join( \DB::raw($document_categories_table_name . ' as dc '), \DB::raw('dc.id'), '=', \DB::raw('upd.document_category_id') );
        } // if ( !empty($filtersArray[show_document_category_name])  ) { //  need to join in select sql show_document_category name

        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new UserProfileDocument)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UserProfileDocument)->isPositiveNumeric($page_param) ) ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $documentCategoriesList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $documentCategoriesList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $documentCategoriesList = $quoteModel->get();
            $data_retrieved= true;
        }
        $base_url= with(new UserProfileDocument)->getBaseUrl();
        $imagesExtensionsArray= \Config::get('app.images_extensions');
        foreach( $documentCategoriesList as $next_key=>$nextUserProfileDocument ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextUserProfileDocument['created_at_label']= with(new UserProfileDocument)->getFormattedDateTime($nextUserProfileDocument->created_at);
                $nextUserProfileDocument['public_access_label']= with(new UserProfileDocument)->getUserProfileDocumentPublicAccessLabel($nextUserProfileDocument->public_access);
            }
            if (!empty($filtersArray['short_info'])) {
                $nextUserProfileDocument['info']= with(new UserProfileDocument)->concatStr($nextUserProfileDocument->info,50);
            }


            if (!empty($filtersArray['show_image']) or !empty($filtersArray['show_image_info']) ) {
                    if ( ! empty($filtersArray['show_image_info'])) {
                        $next_user_profile_document_filename= UserProfileDocument::getUserProfileDocumentPath($filtersArray['user_id'],$nextUserProfileDocument->filename, false);
                        $next_user_profile_document_url= $base_url.Storage::disk('local')->url($next_user_profile_document_filename );
                        $file_exists = Storage::disk('local')->exists('public/'.$next_user_profile_document_filename);
                        if ( $file_exists and in_array($nextUserProfileDocument->extension, $imagesExtensionsArray) ) { // that is image - get all props
                            $image             = $nextUserProfileDocument->filename;
                            $image_path        = public_path('storage/'.$next_user_profile_document_filename);
                            $image_url         = $next_user_profile_document_url;
                            $imagePropsArray   = [ 'image'=> $image, 'image_path'=> $image_path, 'image_url'=> $image_url ];
                            $previewSizeArray= with(new UserProfileDocument)->getImageShowSize($image_path, self::$img_preview_width, self::$img_preview_height );
                            if ( !empty($previewSizeArray['width']) ) {
                                $imagePropsArray['preview_width']= $previewSizeArray['width'];
                                $imagePropsArray['preview_height']= $previewSizeArray['height'];
                            }
                            $nextUserProfileDocumentImgPropsArray= UserProfileDocument::getImageProps( $image_path, $imagePropsArray );
                            if ( !empty($nextUserProfileDocumentImgPropsArray) and is_array($nextUserProfileDocumentImgPropsArray) ) {
                                foreach( $nextUserProfileDocumentImgPropsArray as $next_document_img_key=>$next_document_img_value ) {
                                    $documentCategoriesList[$next_key][$next_document_img_key]= $next_document_img_value;
                                }
                            }
                        } // if ( $file_exists and in_array($nextUserProfileDocument->extension, $imagesExtensionsArray) ) { // that is image - get all props
                        if ( $file_exists and !in_array($nextUserProfileDocument->extension, $imagesExtensionsArray) ) { // that is NOT image - get only some props
                            $image             = $nextUserProfileDocument->filename;
                            $image_path        = public_path('storage/'.$next_user_profile_document_filename);
                            $image_url         = $next_user_profile_document_url;
                            $imagePropsArray   = [ 'image'=> $image, 'image_path'=> $image_path, 'image_url'=> $image_url ];
                            $nextUserProfileDocumentImgPropsArray= UserProfileDocument::getImageProps( $image_path, $imagePropsArray );
                            if ( !empty($nextUserProfileDocumentImgPropsArray) and is_array($nextUserProfileDocumentImgPropsArray) ) {
                                foreach( $nextUserProfileDocumentImgPropsArray as $next_document_img_key=>$next_document_img_value ) {
                                    $documentCategoriesList[$next_key][$next_document_img_key]= $next_document_img_value;
                                }
                            }
                        }
                    } // if ( ! empty($filtersArray['show_image_info'])) {
                }

        }
        return $documentCategoriesList;

    } // public static function getUserProfileDocumentsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param=
    // 0 ) {

    public static function getValidationRulesArray($user_profile_document_id= null) : array
    {
        $additional_filename_validation_rule= 'check_user_profile_document_unique_by_filename:'.( !empty($user_profile_document_id)?$user_profile_document_id:'');
        $additional_filename_valid_extension_validation_rule= 'check_filename_valid_extension:'.( !empty($user_profile_document_id)?$user_profile_document_id:'');
        $validationRulesArray = [
            'filename'               => 'required|max:255|' . $additional_filename_validation_rule,
            'extension'              => 'required|max:10|' . $additional_filename_valid_extension_validation_rule,
            'document_category_id'   => 'required|exists:'.( with(new DocumentCategory)->getTableName() ).',id',
            'public_access'          => 'required|in:'.with( new UserProfileDocument)->getValueLabelKeys( UserProfileDocument::getUserProfileDocumentPublicAccessValueArray(false) ),
            'info'                   => 'nullable',
        ];
        return $validationRulesArray;
    }

    public static function getSimilarUserProfileDocumentByFilename( string $filename, int $user_id, int $id= null, $return_count= false )
    {
        $quoteModel = UserProfileDocument::whereRaw( UserProfileDocument::myStrLower('filename', false, false) .' = '. UserProfileDocument::myStrLower(UserProfileDocument::mysqlEscape($filename), true,false) );
        $quoteModel = $quoteModel->where( 'user_id', '=' , $user_id );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }

        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
//        echo '<pre>$retRow::'.print_r($retRow,true).'</pre>';
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getUserProfileDocumentDir(int $user_profile_document_id, bool $short_path= false) : string
    {
        $public_path= '';  //with (new UserProfileDocument)->getPublicPath();
        $UPLOADS_USER_PROFILE_DOCUMENTS_DIR= \Config::get('app.UPLOADS_USER_PROFILE_DOCUMENTS_DIR');
        return $public_path . '/' . $UPLOADS_USER_PROFILE_DOCUMENTS_DIR . '-user_profile_document-' . $user_profile_document_id.'/';
    }

    public static function getUserProfileDocumentPath(int $user_profile_document_id, $filename, bool $set_public_subdirectory= false, bool $short_path= false) : string
    {
        $public_path= '';//public_path().'/';
        if ( empty($filename) ) return '';
        $UPLOADS_USER_PROFILE_DOCUMENTS_DIR= ($set_public_subdirectory ? "public/" : "") . \Config::get('app.UPLOADS_USER_PROFILE_DOCUMENTS_DIR');
        return ( !$short_path ? $public_path /*.  '/' */  : '' ) . $UPLOADS_USER_PROFILE_DOCUMENTS_DIR . '-user_profile_document-' . $user_profile_document_id . '/' . $filename;
    }

    public function getUserProfileDocumentImagePropsAttribute($only_existing_files= false) : array
    {
        return $this->userProfileDocumentImagePropsArray;
    }

    public function setUserProfileDocumentImagePropsAttribute(array $userProfileDocumentImagePropsArray)
    {
        $this->userProfileDocumentImagePropsArray = $userProfileDocumentImagePropsArray;
    }



}
