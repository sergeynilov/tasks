<?php
namespace App;

use DB;
use App\MyAppModel;
use App\Rules\CheckValidColor;
use App\User;
use App\library\ListingReturnData;


class UserProfiles extends MyAppModel
{

    protected $table = 'user_profiles';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [ 'name', 'user_id', 'value' ];

    private static $userSkillOptionsLabelValueArray = [
        0=> '0 : No skills',
        1=> '1 : Very few skills',
        2=> '2 : Rather few skills',
        3=> '3 : Rather medium skills',
        4=> '4 : Medium skills',
        5=> '5 : Better medium skills',
        6=> '6 : Rather good skills',
        7=> '7 : Good skills',
        8=> '8 : Very good skills',
        9=> '9 : Excellent skills',
        10=> '10 : Guru',
    ];

    public static function getUserSkillOptionsValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$userSkillOptionsLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getUserSkillOptionsLabel(string $status):string
    {
        if (!empty(self::$userSkillOptionsLabelValueArray[$status])) {
            return self::$userSkillOptionsLabelValueArray[$status];
        }
        return '';
    }
    
    public static function getUserProfilesList( array $filtersArray = [], bool $return_keys_array= false )
    {

        $user_profiles_table_name= with(new UserProfiles)->getTableName();
        $quoteModel= UserProfiles::from(  \DB::raw(DB::getTablePrefix().$user_profiles_table_name.' as up' ));

        $additive_fields_for_select= "";
        $fields_for_select= 'up.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( DB::raw('up.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['name'])) {
            if ( is_array($filtersArray['name'])) {
                $quoteModel->whereIn(DB::raw('up.name'), $filtersArray['name']);
            } else {
                $quoteModel->where(DB::raw('up.name'), '=', $filtersArray['name']);
            }
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of s table */
        $quoteModel->select( \DB::raw($fields_for_select) );
        $userProfilesList = $quoteModel->get();
        if ( $return_keys_array ) {
            $retArray= [];
            foreach( $userProfilesList as $next_key=>$nextUserProfile ) {
                $retArray[$nextUserProfile->name]= $nextUserProfile->value;
            }
            return $retArray;
        }
        return $userProfilesList;

    }


    public static function getValidationRulesArray($user_id) : array
    {
        $valid_nice_name_format= config('app.valid_nice_name_format');
        $additional_check_nice_name_validation_rule= 'regex:'.$valid_nice_name_format;
        $valid_phone_format= config('app.valid_phone_format');
//        $additional_phone_validation_rule= 'regex:'.$valid_phone_format;
        $validationRulesArray = [
            'first_name'      => 'nullable|max:50|'.$additional_check_nice_name_validation_rule,
            'last_name'       => 'nullable|max:50|'.$additional_check_nice_name_validation_rule,
            'phone'           => 'required|max:50',
            'website'         => 'required|max:50',
            'color'           => [ 'required', new CheckValidColor() ],
            'background_color'=> [ 'required', new CheckValidColor() ],
            'lang'            => 'required|',
            'submit_message_by_enter'=> 'required|',
        ];

        return $validationRulesArray;
    } // public static function getValidationRulesArray($user_id) : array
    
}
