<?php

namespace App\Providers;
use Auth;
use Validator;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Database\Events\TransactionCommitted;
use Illuminate\Database\Events\TransactionRolledBack;
use Illuminate\Support\Facades\Gate;
//use App\Http\Traits\funcsTrait;
use App\Settings;
use App\UserTaskType;
use App\UserChat;
use App\Category;
use App\DocumentCategory;
use App\Task;
use App\Event;
use App\UserProfileDocument;
use App\UserChatMessageDocument;

use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    { //file:///_wwwroot/lar/lprods/app/Providers/AppServiceProvider.php




        if ($this->app->environment('local')) {


            \Event::listen(
                [
                    TransactionBeginning::class,
                ],
                function ($event) {
                    //return; // to comment

                    $str= "  BEGIN; ";
                    $dbLog = new \Monolog\Logger('Query');
                    $dbLog->pushHandler(new \Monolog\Handler\RotatingFileHandler(storage_path('logs/Query.txt'), 5, \Monolog\Logger::DEBUG));
                    $dbLog->info($str);
                    $dbLog->info('');
                    $dbLog->info('');
                    if ( !empty($_SESSION['sqlmonitor_tracing_on']) ) {
                        $sqlmonitor_tracing_lines_array= ( !empty($_SESSION['sqlmonitor_tracing_lines_array']) ? $_SESSION['sqlmonitor_tracing_lines_array'] : [] );
                        if ( !is_array($sqlmonitor_tracing_lines_array) ) {
                            $sqlmonitor_tracing_lines_array= [];
                        }
                        $sqlmonitor_tracing_lines_array[]= "  <br>" . $str."<br><br>";
                        $_SESSION['sqlmonitor_tracing_lines_array']= $sqlmonitor_tracing_lines_array;
                    }
                    writeSqlToLog($str);
                    writeSqlToLog("");
                    writeSqlToLog("");
                });


            \Event::listen(
                [
                    TransactionCommitted::class,
                ],
                function ($event) {
                    //return; // to comment

                    $str= "  COMMIT; ";
                    $dbLog = new \Monolog\Logger('Query');
                    $dbLog->pushHandler(new \Monolog\Handler\RotatingFileHandler(storage_path('logs/Query.txt'), 5, \Monolog\Logger::DEBUG));
                    $dbLog->info($str);
                    $dbLog->info('');
                    $dbLog->info('');
                    if ( !empty($_SESSION['sqlmonitor_tracing_on']) ) {
                        $sqlmonitor_tracing_lines_array= ( !empty($_SESSION['sqlmonitor_tracing_lines_array']) ? $_SESSION['sqlmonitor_tracing_lines_array'] : [] );
                        if ( !is_array($sqlmonitor_tracing_lines_array) ) {
                            $sqlmonitor_tracing_lines_array= [];
                        }
                        $sqlmonitor_tracing_lines_array[]= "  <br>" . $str."<br><br>";
                        $_SESSION['sqlmonitor_tracing_lines_array']= $sqlmonitor_tracing_lines_array;
                    }
                    writeSqlToLog($str);
                    writeSqlToLog("");
                    writeSqlToLog("");
                });


            \Event::listen(
                [
                    TransactionRolledBack::class,
                ],
                function ($event) {
                    //return; // to comment
                    //
                    $str= "  ROLLBACK; ";
                    $dbLog = new \Monolog\Logger('Query');
                    $dbLog->pushHandler(new \Monolog\Handler\RotatingFileHandler(storage_path('logs/Query.txt'), 5, \Monolog\Logger::DEBUG));
                    $dbLog->info($str);
                    if ( !empty($_SESSION['sqlmonitor_tracing_on']) ) {
                        $sqlmonitor_tracing_lines_array= ( !empty($_SESSION['sqlmonitor_tracing_lines_array']) ? $_SESSION['sqlmonitor_tracing_lines_array'] : [] );
                        if ( !is_array($sqlmonitor_tracing_lines_array) ) {
                            $sqlmonitor_tracing_lines_array= [];
                        }
                        $sqlmonitor_tracing_lines_array[]= "  <br>" . $str."<br><br>";
                        $_SESSION['sqlmonitor_tracing_lines_array']= $sqlmonitor_tracing_lines_array;
                    }
                    $dbLog->info('');
                    $dbLog->info('');
                    writeSqlToLog($str);
                    writeSqlToLog("");
                    writeSqlToLog("");
                });



            \DB::listen(function($query) {
                // return; // to comment

                $dbLog = new \Monolog\Logger('Query');
                $dbLog->pushHandler(new \Monolog\Handler\RotatingFileHandler(storage_path('logs/Query.txt'), 5, \Monolog\Logger::DEBUG));
                $dbLog->info($query->sql, ['Bindings' => $query->bindings, 'Time' => $query->time]);

                $str= $query->sql;
                $str= str_replace('%','QWERTYQWERTY',$str);
                $str= str_replace('?',"%s",$str);
                if ( count($query->bindings) == 1 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'");
                }
                if ( count($query->bindings) == 2 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'" );
                }
                if ( count($query->bindings) == 3 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'" );
                }
                if ( count($query->bindings) == 4 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'" );
                }
                if ( count($query->bindings) == 5 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'" );
                }
                if ( count($query->bindings) == 6 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" );
                }


                if ( count($query->bindings) == 7 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" );
                }

                if ( count($query->bindings) == 8 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" );
                }

                if ( count($query->bindings) == 9 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'" );
                }

                if ( count($query->bindings) == 10 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'" , "'".$query->bindings[9]."'" );
                }

                if ( count($query->bindings) == 11 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'", "'".$query->bindings[9]."'", "'".$query->bindings[10]."'" );
                }

                if ( count($query->bindings) == 12 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'", "'".$query->bindings[9]."'", "'".$query->bindings[10]."'" , "'".$query->bindings[11]."'" );
                }


                if ( count($query->bindings) == 13 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'", "'".$query->bindings[9]."'", "'".$query->bindings[10]."'" , "'".$query->bindings[11]."'", "'".$query->bindings[12]."'" );
                }


                if ( count($query->bindings) == 14 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'", "'".$query->bindings[9]."'", "'".$query->bindings[10]."'" , "'".$query->bindings[11]."'", "'".$query->bindings[12]."'", "'".$query->bindings[13]."'" );
                }


                if ( count($query->bindings) == 15 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'", "'".$query->bindings[9]."'", "'".$query->bindings[10]."'" , "'".$query->bindings[11]."'" , "'".$query->bindings[12]."'" , "'".$query->bindings[13]."'" , "'".$query->bindings[14]."'" , "'".$query->bindings[15]."'" );
                }


                if ( count($query->bindings) == 16 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'", "'".$query->bindings[9]."'", "'".$query->bindings[10]."'" , "'".$query->bindings[11]."'" , "'".$query->bindings[12]."'" , "'".$query->bindings[13]."'" , "'".$query->bindings[14]."'" , "'".$query->bindings[15]."'", "'".$query->bindings[16]."'" );
                }


                if ( count($query->bindings) == 17 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'", "'".$query->bindings[9]."'", "'".$query->bindings[10]."'" , "'".$query->bindings[11]."'" , "'".$query->bindings[12]."'" , "'".$query->bindings[13]."'" , "'".$query->bindings[14]."'" , "'".$query->bindings[15]."'", "'".$query->bindings[16]."'" , "'".$query->bindings[17]."'" );
                }

                if ( count($query->bindings) == 18 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'", "'".$query->bindings[9]."'", "'".$query->bindings[10]."'" , "'".$query->bindings[11]."'" , "'".$query->bindings[12]."'" , "'".$query->bindings[13]."'" , "'".$query->bindings[14]."'" , "'".$query->bindings[15]."'", "'".$query->bindings[16]."'" , "'".$query->bindings[17]."'"  , "'".$query->bindings[18]."'" );
                }

                if ( count($query->bindings) == 19 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'", "'".$query->bindings[9]."'", "'".$query->bindings[10]."'" , "'".$query->bindings[11]."'" , "'".$query->bindings[12]."'" , "'".$query->bindings[13]."'" , "'".$query->bindings[14]."'" , "'".$query->bindings[15]."'", "'".$query->bindings[16]."'" , "'".$query->bindings[17]."'"  , "'".$query->bindings[18]."'", "'".$query->bindings[19]."'" );
                }


                if ( count($query->bindings) == 20 ) {
                    $str = sprintf($str, "'".$query->bindings[0]."'", "'".$query->bindings[1]."'", "'".$query->bindings[2]."'", "'".$query->bindings[3]."'", "'".$query->bindings[4]."'", "'".$query->bindings[5]."'" , "'".$query->bindings[6]."'" , "'".$query->bindings[7]."'" , "'".$query->bindings[8]."'", "'".$query->bindings[9]."'", "'".$query->bindings[10]."'" , "'".$query->bindings[11]."'" , "'".$query->bindings[12]."'" , "'".$query->bindings[13]."'" , "'".$query->bindings[14]."'" , "'".$query->bindings[15]."'", "'".$query->bindings[16]."'" , "'".$query->bindings[17]."'"  , "'".$query->bindings[18]."'", "'".$query->bindings[19]."'", "'".$query->bindings[20]."'" );
                }



                $str= str_replace('QWERTYQWERTY', '%',$str);

                writeSqlToLog($str, 'Time ' . $query->time.' : '.PHP_EOL);
                writeSqlToLog('');
                writeSqlToLog('');
                if ( !empty($_SESSION['sqlmonitor_tracing_on']) ) {
                    $sqlmonitor_tracing_lines_array= ( !empty($_SESSION['sqlmonitor_tracing_lines_array']) ? $_SESSION['sqlmonitor_tracing_lines_array'] : [] );
                    if ( !is_array($sqlmonitor_tracing_lines_array) ) {
                        $sqlmonitor_tracing_lines_array= [];
                    }
                    $sqlmonitor_tracing_lines_array[]= '<u>Time ' . $query->time." :</u> <br>" . $str."<br><br>";
                    $_SESSION['sqlmonitor_tracing_lines_array']= $sqlmonitor_tracing_lines_array;
                }
                $dbLog->info($str, [ 'Time' => $query->time]);
                $dbLog->info('');
                $dbLog->info('');

            }); // \DB::listen(function($query) {


        } // if ($this->app->environment('local')) {


        /* check is entered name is valid */
        Validator::extend('check_nice_name', function($user, $value, $parameters, $validator) {
            $valid_nice_name_format= env('VALID_NICE_NAME_FORMAT');
            $ret = preg_match($valid_nice_name_format.'si', $value, $a);
            return $ret;
        });



        Validator::extend('check_filename_valid_extension', function($user, $value, $parameters, $validator) {
            $uploadedDocumentsExtensionsArray= \Config::get('app.uploaded_documents_extensions');
//            echo '<pre>check_filename_valid_extension  $uploadedDocumentsExtensionsArray::'.print_r($uploadedDocumentsExtensionsArray,true).'</pre>';
//            echo '<pre>check_filename_valid_extension  $value::'.print_r($value,true).'</pre>';
            return in_array( $value, $uploadedDocumentsExtensionsArray );
        });



        //                 check_user_chat_message_document_unique_by_filename
        Validator::extend('check_user_chat_message_unique_by_filename', function($userChatMessageDocument, $value, $parameters, $validator) {
            $user_chat_id= $parameters[0] ?? null;
            $loggedUser = Auth::user();
            $user_chat_message_count = UserChatMessageDocument::getSimilarUserChatMessageDocumentByFilename( $value, $user_chat_id,0, true );
            return $user_chat_message_count == 0;
        });

        Validator::extend('check_user_profile_document_unique_by_filename', function($userProfileDocument, $value, $parameters, $validator) {
            $user_profile_document_id= $parameters[0] ?? null;
            $loggedUser = Auth::user();
            $user_profile_document_count = UserProfileDocument::getSimilarUserProfileDocumentByFilename( $value, $loggedUser->id,(int)$user_profile_document_id, true );
            return $user_profile_document_count == 0;
        });

        Validator::extend('check_event_unique_by_name', function($event, $value, $parameters, $validator) {
            $event_id= $parameters[0] ?? null;
            $event_count = Event::getSimilarEventByName( $value, (int)$event_id, true );
            return $event_count == 0;
        });

        Validator::extend('check_document_category_unique_by_name', function($documentCategory, $value, $parameters, $validator) {
            $document_category_id= $parameters[0] ?? null;
            $document_category_count = DocumentCategory::getSimilarDocumentCategoryByName( $value, (int)$document_category_id, true );
            return $document_category_count == 0;
        });

        Validator::extend('check_task_unique_by_name', function($task, $value, $parameters, $validator) {
            $task_id= $parameters[0] ?? null;
            $task_count = Task::getSimilarTaskByName( $value, (int)$task_id, true );
            return $task_count == 0;
        });

        Validator::extend('check_category_unique_by_name', function($category, $value, $parameters, $validator) {
            $category_id= $parameters[0] ?? null;
            $category_count = Category::getSimilarCategoryByName( $value, (int)$category_id, true );
            return $category_count == 0;
        });

        Validator::extend('check_user_chat_unique_by_name', function($userChat, $value, $parameters, $validator) {
            $user_chat_id= $parameters[0] ?? null;
            $user_chat_count = UserChat::getSimilarUserChatByName( $value, (int)$user_chat_id, true );
            return $user_chat_count == 0;
        });

        Validator::extend('check_user_task_type_unique_by_name', function($userTaskType, $value, $parameters, $validator) {
            $user_task_type_id= $parameters[0] ?? null;
            $user_task_type_count = UserTaskType::getSimilarUserTaskTypeByName( $value, (int)$user_task_type_id, true );
            return $user_task_type_count == 0;
        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}


function formatSql($sql, $is_break_line = true, $is_include_html = true)
{
//    return $sql;
    $break_line = '';
    $space_char = '  ';
    if ($is_break_line) {
        if ( $is_include_html ) {
            $break_line = '<br>';
        }  else {
            $break_line = PHP_EOL;
        }
    }
    $bold_start= '';
    $bold_end= '';
    if ( $is_include_html ) {
        $bold_start= '<B>';
        $bold_end= '</B>';
    }
    $sql = ' ' . $sql . ' ';
    $left_cond= '~\b(?<![%\'])';
    $right_cond= '(?![%\'])\b~i';
    $sql = preg_replace( $left_cond . "insert[\s]+into" . $right_cond, $space_char . $space_char .$bold_start . "INSERT INTO" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "insert" . $right_cond, $space_char . $bold_start . "INSERT" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "delete" . $right_cond, $space_char . $bold_start . "DELETE" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "values" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "VALUES" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "update" . $right_cond, $space_char . $bold_start . "UPDATE" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "inner[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "INNER JOIN" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "straight[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "STRAIGHT_JOIN" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "left[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "LEFT JOIN" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "select" . $right_cond, $space_char . $bold_start . "SELECT" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "from" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "FROM" . $bold_end, $sql );
    $sql = preg_replace( $left_cond . "where" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "WHERE" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "group by" . $right_cond, $break_line . $space_char . $space_char . "GROUP BY" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "having" . $right_cond, $break_line . $space_char . $bold_start . "HAVING" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "order[\s]+by" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "ORDER BY" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "and" . $right_cond,  $space_char . $space_char . $bold_start . "AND" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "or" . $right_cond,  $space_char . $space_char . $bold_start . "OR" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "as" . $right_cond,  $space_char . $space_char . $bold_start . "AS" . $bold_end, $sql);
    $sql = preg_replace( $left_cond . "exists" . $right_cond,  $break_line . $space_char . $space_char . $bold_start . "EXISTS" . $bold_end, $sql);
    return $sql;
}

function writeSqlToLog($contents, string  $descr_text = '', bool $is_sql = false, string $file_name= '')
{
    $APP_DEBUG= env('APP_DEBUG');
    if (!$APP_DEBUG) return;
    try {
        if (empty($file_name))
            $file_name = storage_path().'/logs/sql_tracing.txt';
//                $fd = fopen($file_name, ($is_clear_text ? "w+" : "a+"));
        $fd = fopen($file_name, "a+");
        if (  is_array($contents)== 'array' ) {
            $contents= print_r($contents,true);
        }
        if ( $is_sql ) {
            fwrite($fd, $descr_text . $this->formatSql($contents, false) . chr(13), false, false);
        } else {
            fwrite($fd, $descr_text . $contents . chr(13));
        }
        //                     echo '<b><I>' . gettype($Var) . "</I>" . '&nbsp;$Var:=<font color= red>&nbsp;' . AppUtil::showFormattedSql($Var) . "</font></b></h5>";

        fclose($fd);

//        die("-1 XXZ writeSqlToLog");
        return true;
    } catch (Exception $lException) {
        return false;
    }
}
