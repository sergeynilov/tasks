<?php

namespace App;

use App\MyAppModel;
use App\Http\Traits\funcsTrait;
use DB;

class Settings extends MyAppModel {
    protected $table = 'settings';
    protected $primaryKey = 'id';
    public $timestamps = false;

    use funcsTrait;
    public static function scopeName($query, $name)
    {
        return $query->where('name', '=', $name);
    }

    public static function getValue( $name, $default_value = '' ) {
        $value= Settings::name($name)->get()->first();
        if ( empty($value->value) ) return $default_value;
        return $value->value;
    }


    public static function getSettingsList( array $filtersArray = [], bool $return_keys_array= false )
    {

        $settings_table_name= with(new Settings)->getTableName();
        $quoteModel= Settings::from(  \DB::raw(DB::getTablePrefix().$settings_table_name.' as s' ));

        $additive_fields_for_select= "";
        $fields_for_select= 's.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            $quoteModel->where( DB::raw('s.name'), '=', $filtersArray['name'] );
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of s table */
        $quoteModel->select( \DB::raw($fields_for_select) );
        $userProfilesList = $quoteModel->get();
        if ( $return_keys_array ) {
            $retArray= [];
            foreach( $userProfilesList as $next_key=>$nextUserProfile ) {
//                $retArray[$nextUserProfile->name]= addslashes(strip_tags($nextUserProfile->value));
                $retArray[$nextUserProfile->name]= $nextUserProfile->value;
            }
            return $retArray;
        }
        return $userProfilesList;

    }

}