<?php

namespace App;

use DB;
use Barryvdh\Debugbar\Facade as Debugbar;
use App\MyAppModel;
use App\Category;
use App\TaskAssignedToUser;
use App\TaskOperation;
use App\UserChat;
use App\UserTodo;
use App\library\ListingReturnData;
use App\Events\TaskUpdatingEvent;
use App\Rules\TaskAssignedToUsersSelected;

class Task extends MyAppModel
{

    protected $table = 'tasks';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $taskStatusLabelValueArray = Array('D' => 'Draft', 'A' => 'Assigning', 'C' => 'Cancelled', 'P' => 'Processing', 'K' => 'Checking', 'O' => 'Completed');
    private static $taskPriorityLabelValueArray = Array(1 => 'No', 2 => 'Low', 3 => 'Normal', 4 => 'High', 5 => 'Urgent', 6 => 'Immediate');
    private static $taskNeedsReportsLabelValueArray = Array(0 => 'No', 1 => 'Hourly', 2 => 'Twice a day', 3 => 'Daily', 4 => 'Twice a week', 5 => 'Weekly');
    private static $taskAssignedToUsersStatusLabelValueArray = Array('L' => 'Leader', 'M' => 'Member', 'N' => 'Not member');

    protected $dispatchesEvents = [
        'updating' => TaskUpdatingEvent::class,
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'creator_id', 'leader_id', 'category_id', 'priority', 'date_start', 'date_end', 'needs_reports', 'description', 'updated_at'
    ];


    public function category(){
        return $this->belongsTo('App\Category', 'category_id','id');
    }

    public function userChats()
    {
        return $this->hasMany('App\UserChat');
    }

    public function userTodos()
    {
        return $this->hasMany('App\UserTodo');
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function taskAssignedToUsers()
    {
        return $this->hasMany('App\TaskAssignedToUser');
    }

    public function taskStatusChange()
    {
        return $this->hasMany('App\TaskStatusChange');
    }

    public function taskOperations()
    {
        return $this->hasMany('App\TaskOperation');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($task) {
            foreach ( $task->userChats()->get() as $nextUserChat ) {
                $nextUserChat->task_id= null;
                $nextUserChat->save();
            }

            foreach ( $task->UserTodos()->get() as $nextUserTodo ) {
                $nextUserTodo->task_id= null;
                $nextUserTodo->save();
            }

            foreach ( $task->events()->get() as $nextEvent ) {
                $nextEvent->task_id= null;
                $nextEvent->save();
            }
        });

        static::deleting(function($task) {
            $task->taskOperations()->delete();
        });

        static::deleting(function($task) {
            $task->taskAssignedToUsers()->delete();
        });

        static::deleting(function($task) {
            $task->taskStatusChange()->delete();
        });

    }
    

    public static function getTaskStatusValueArray($key_return= true, $statusLimitArray=[]) : array
    {
        $resArray = [];
        foreach (self::$taskStatusLabelValueArray as $key => $value) {
            if ( !empty($statusLimitArray) ) {
                if ( !in_array($key, $statusLimitArray) ) continue;
            }
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getTaskStatusLabel(string $status):string
    {
        if (!empty(self::$taskStatusLabelValueArray[$status])) {
            return self::$taskStatusLabelValueArray[$status];
        }
        return '';
    }



    public static function getTaskPrioritiesValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$taskPriorityLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getTaskPriorityLabel(string $priority):string
    {
        if (!empty(self::$taskPriorityLabelValueArray[$priority])) {
            return self::$taskPriorityLabelValueArray[$priority];
        }
        return '';
    }



    public static function getTaskNeedsReportsValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$taskNeedsReportsLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getTaskNeedsReportsLabel(string $priority):string
    {
        if (!empty(self::$taskNeedsReportsLabelValueArray[$priority])) {
            return self::$taskNeedsReportsLabelValueArray[$priority];
        }
        return '';
    }


    public static function getTaskAssignedToUsersStatusValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$taskAssignedToUsersStatusLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getTaskAssignedToUsersStatusLabel(string $priority):string
    {
        if (!empty(self::$taskAssignedToUsersStatusLabelValueArray[$priority])) {
            return self::$taskAssignedToUsersStatusLabelValueArray[$priority];
        }
        return '';
    }


    /* return data array by keys id/name based on filters/ordering... */
    public static function getTasksSelectionList( bool $key_return= true, array $filtersArray = array(), string $order_by = 'name', string $order_direction = 'asc') : array
    {
        $tasksList = Task::getTasksList(ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction);
        $resArray = [];
        foreach ($tasksList as $nextTask) {
            if ($key_return) {
                $resArray[] = array('key' => $nextTask->id, 'label' => $nextTask->name);
            } else{
                $resArray[ $nextTask->id ]= $nextTask->name;
            }
        }
        return $resArray;
    }

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getTasksList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 't.name'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $task_table_name= with(new Task)->getTableName();
        $quoteModel= Task::from(  \DB::raw(DB::getTablePrefix().$task_table_name.' as t' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 't.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            if ( empty($filtersArray['in_content']) ) {
                $quoteModel->whereRaw( Task::myStrLower('name', false, false) . ' like ' . Task::myStrLower( $filtersArray['name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.Task::myStrLower('name', false, false) . ' like ' . Task::myStrLower( $filtersArray['name'], true,true ) . ' OR ' . Task::myStrLower('content', false, false) . ' like ' . Task::myStrLower( $filtersArray['name'], true,true ) .
                                       ' OR ' . Task::myStrLower('description', false, false) . ' like ' . Task::myStrLower( $filtersArray['name'], true,true ) . ' ) ');
            }
        }

        if (!empty($filtersArray['category_id'])) {
            $quoteModel->where( 'category_id', '=', $filtersArray['category_id'] );
        }

        if (!empty($filtersArray['creator_id'])) {
            $quoteModel->where( 'creator_id', '=', $filtersArray['creator_id'] );
        }

        if (!empty($filtersArray['status'])) {
            $quoteModel->where( 'status', '=', $filtersArray['status'] );
        }

        if (!empty($filtersArray['priority'])) {
            $quoteModel->where( 'priority', '=', $filtersArray['priority'] );
        }

        if (!empty($filtersArray['needs_reports'])) {
            $quoteModel->where( 'needs_reports', '=', $filtersArray['needs_reports'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }
        // creator_id
        if ( !empty($filtersArray['show_creator_name'])  ) { // need to join in select sql username and user_status field of author of t item
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', uc.name as creator_name, uc.last_name as last_name, uc.first_name as first_name, uc.status as creator_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as uc '), \DB::raw('uc.id'), '=', \DB::raw('t.creator_id') );
        } // if ( !empty($filtersArray['show_creator_name'])  ) { // need to join in select sql username and user_status field of author of t item

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new Task)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new Task)->isPositiveNumeric($page_param) ) ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $tasksList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $tasksList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $tasksList = $quoteModel->get();
            $data_retrieved= true;
        }
        foreach( $tasksList as $next_key=>$nextTask ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['fill_labels'])) {
                $nextTask['created_at_label']= with(new UserWeatherLocation)->getFormattedDateTime($nextTask->created_at);
                $nextTask['updated_at_label']= with(new UserWeatherLocation)->getFormattedDateTime($nextTask->updated_at);
                $nextTask['date_start_label']= with(new UserWeatherLocation)->getFormattedDate($nextTask->date_start);
                $nextTask['date_end_label']= with(new UserWeatherLocation)->getFormattedDate($nextTask->date_end);
                $nextTask['status_label']= with(new Task)->getTaskStatusLabel($nextTask->status);
                $nextTask['priority']= $nextTask->priority;
                $nextTask['priority_label']= with(new Task)->getTaskPriorityLabel($nextTask->priority);
                $nextTask['needs_reports_label']= with(new Task)->getTaskNeedsReportsLabel($nextTask->needs_reports);
            }
        }
        return $tasksList;

    } // public static function getTasksList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {




    protected static function getCategoriesGroupedList( string $tasks_status= '', $dataArray = [], $parent_id )
    {
        $debug= '';
        if ( $debug == 'echo' )  {
            echo '<pre>$parent_id::'.print_r($parent_id,true).'</pre>';
        }
        if ( $debug == 'info' ) {
            Debugbar::info('<pre>$dataArray::' . print_r($dataArray, true) . '</pre>');
            Debugbar::info('<pre>$parent_id::' . print_r($parent_id, true) . '</pre>');
        }
//        die("-1 XXZ");
        $subGroupArray= [];
        foreach( $dataArray as $next_key=>$nextCategory ) {
            if ( $parent_id == $nextCategory['parent_id'] ) {
                $subGroupArray[]= $nextCategory;
            }
        }
//        Debugbar::info('<pre>$subGroupArray::'.print_r($subGroupArray,true).'</pre>');
        if ( $debug == 'echo' ) {
            echo '<pre>$subGroupArray::'.print_r($subGroupArray,true).'</pre>';
        }
        if ( $debug == 'info' ) {
            Debugbar::info('<pre>$subGroupArray::' . print_r($subGroupArray, true) . '</pre>');
        }

        foreach( $subGroupArray as $next_sub_group_key=>$nextSubGroup ) {
            $a= with(new Task)->getCategoriesGroupedList($tasks_status, $dataArray, $nextSubGroup['id']);
            $subGroupArray[$next_sub_group_key]['children_count']= count($a);
            $subGroupArray[$next_sub_group_key]['children']= $a;
            $subGroupArray[$next_sub_group_key]['is_data']= 'category';

//            $tasksSubArray= [];
            $filtersArray= ['category_id' => $nextSubGroup['id'], 'fill_labels'=> 1 ];
            if ( !empty($tasks_status) and strtolower($tasks_status)!= 'all' ) {
                $filtersArray['status']= $tasks_status;
            }
            $tasksList = Task::getTasksList(ListingReturnData::LISTING, $filtersArray, 'name', 'asc' );
            foreach( $tasksList as $nextTask ) {
                $tasksSubArray= [ 'id'=>$nextTask['id'], 'name'=>  /* $nextTask['id'] . ' = Task : ' . */ $nextTask['name'], 'status_label'=>$nextTask['status_label'] ,
                'priority_label'=>$nextTask['priority_label'], 'priority'=>$nextTask['priority'], 'date_start_label'=>$nextTask['date_start_label'], 'date_end_label'=>$nextTask['date_end_label'], 'is_data'=>'task' ];
                $subGroupArray[$next_sub_group_key]['children'][]= $tasksSubArray;
            }
            if ( count($a) > 0 ) {
                $subGroupArray[$next_sub_group_key]['name']= $subGroupArray[$next_sub_group_key]['name'] . ( count($a) > 0 ? ' ('.count($a).' sub groups) ' : '' );
            }

        }
        if ( $debug == 'echo' ) {
            echo '<pre>++ $subGroupArray::'.print_r($subGroupArray,true).'</pre>';
        }
        if ( $debug == 'info' ) {
            Debugbar::info('++++<pre>$subGroupArray::' . print_r($subGroupArray, true) . '</pre>');
        }
        if ( $debug == 'echo' ) {
            echo '<pre>++$subGroupArray::' . print_r($subGroupArray, true) . '</pre>';
            die("-1 XXZ");
        }
        return $subGroupArray;
    }

    public static function getTasksTreeDataArray( string $tasks_status= '', array $filtersArray = [], bool $with_tasks= false ) {
        if (empty($order_by)) $order_by = 't.name'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $categoriesArray= [];
        $categoriesList = Category::getCategoriesList(ListingReturnData::LISTING, [], 'id', 'asc');

        foreach( $categoriesList as $next_key=>$nextCategory ) {

//            if ( $nextCategory->id > 6) continue;
//            if ( !in_array($nextCategory->id, [4,5] ) ) continue;

            if (empty($nextCategory->parent_id)) {
                $categoriesList[$next_key]->parent_id= 0;
            }
            $categoriesArray[]= $nextCategory->toArray();
        }

        $parent_id= 0;
        $groupedCategoriesList= with(new Task)->getCategoriesGroupedList($tasks_status, $categoriesArray, $parent_id);
//        if ( $with_tasks ) {
//            foreach( $groupedCategoriesList as $next_key=>$nextGroupedCategory ) {
//
//                $tasksSubArray= [];
//                $tasksList = Task::getTasksList(ListingReturnData::LISTING, ['category_id' => $nextGroupedCategory['id'], 'fill_labels'=> 1 ], 'name', 'asc' );
//                foreach( $tasksList as $nextTask ) {
//                    $tasksSubArray[]= [ 'id'=>$nextTask['id'], 'name'=>$nextTask['name'], 'status_label'=>$nextTask['status_label'] , 'priority_label'=>$nextTask['priority_label'] ];
//                }
//                $groupedCategoriesList[$next_key]['tasks']= $tasksSubArray;
//                $groupedCategoriesList[$next_key]['tasks_count']= count($tasksSubArray);
//
//            }
//        }
        return $groupedCategoriesList;

    } // public static function getTasksTreeDataArray( string $tasks_status= '', array $filtersArray = [], bool $with_tasks= false ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if (!empty($additiveParams['show_creator_name']) ) { // need to join in select sql username and user_status field of author of t item
            $additive_fields_for_select= "";
            $fields_for_select= 't.*';
            $task_table_name= with(new Task)->getTableName();
            $quoteModel= Task::from(  \DB::raw(DB::getTablePrefix().$task_table_name.' as t' ));
            $quoteModel->where( \DB::raw('t.id'), '=', $id );
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', uc.name as creator_name, uc.last_name as creator_last_name, uc.first_name as creator_first_name, uc.status as creator_status' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as uc '), \DB::raw('uc.id'), '=', \DB::raw('t.creator_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $taskRows = $quoteModel->get();
            if (!empty($taskRows[0]) and get_class($taskRows[0]) == 'App\Task' ) {
                $task= $taskRows[0];
                $is_row_retrieved= true;
            }
        } // if (!empty($additiveParams['show_creator_name']) ) { // need to join in select sql username and user_status field of author of t item

        if ( !$is_row_retrieved ) {
            $task = Task::find($id);
            if (empty($task)) return false;
        }
        if (!empty($additiveParams['fill_labels'])) {
                $task['created_at_label']= with(new Task)->getFormattedDateTime($task->created_at);
                $task['updated_at_label']= with(new Task)->getFormattedDateTime($task->updated_at);
                $task['date_start_label']= with(new Task)->getFormattedDate($task->date_start);
                $task['date_end_label']= with(new Task)->getFormattedDate($task->date_end);
                $task['status_label']= with(new Task)->getTaskStatusLabel($task->status);
                $task['priority_label']= with(new Task)->getTaskPriorityLabel($task->priority);
                $task['needs_reports_label']= with(new Task)->getTaskNeedsReportsLabel($task->needs_reports);
            }

        if (empty($task)) return false;

        return $task;
    } // public function getRowById( int $id, array $additiveParams= [] )



    /* check if provided name is unique for tasks.name field */
    public static function getSimilarTaskByName( string $name, int $id= null, bool $return_count = false )
    {         
        $quoteModel = Task::whereRaw( Task::myStrLower('name', false, false) .' = '. Task::myStrLower( Task::mysqlEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getValidationRulesArray( $task_id ) : array
    {
        $additional_name_validation_rule= 'check_task_unique_by_name:'.( !empty($task_id)?$task_id:'');
        $validationRulesArray = [
            'name'             => 'required|max:255|'.$additional_name_validation_rule,
            'description'      => 'required',
            'creator_id'       => 'nullable|exists:'.( with(new User)->getTableName() ).',id',
            'category_id'      => 'required|exists:'.( with(new Category)->getTableName() ).',id',
            'priority'         => 'required|in:'.with( new Task)->getValueLabelKeys( Task::getTaskPrioritiesValueArray(false) ),
            'status'           => 'required|in:'.with( new Task)->getValueLabelKeys( Task::getTaskStatusValueArray(false) ),
            'date_start'       => 'required',
            'date_end'         => 'required',
            'needs_reports'    => 'required|in:'.with( new Task)->getValueLabelKeys( Task::getTaskNeedsReportsValueArray(false) ),
            'taskAssignedToUsersArray'=> [ '',new TaskAssignedToUsersSelected() ]
        ];

        return $validationRulesArray;
    }

}
