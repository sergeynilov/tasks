<?php

Route::group(['middleware' => 'web', 'prefix' => 'sqlmonitor', 'namespace' => 'Modules\SqlMonitor\Http\Controllers'], function()
{
    Route::get('/', 'SqlMonitorController@index');
});
