<?php

use Illuminate\Http\Request;
use App\Http\Middleware\WorkTextString;


Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::post('register', 'AuthController@register');

Route::resource('recipes', 'RecipeController');


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
//
//Auth::routes();
//
//
///* Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
// */
////Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
//Route::group([ /*'middleware' => ['auth'],*/ 'prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
//// Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
//
//    Route::resource('tasks', 'TasksController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
//    Route::resource('user_task_types', 'UserTaskTypesController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
//    Route::resource('user_chats', 'UserChatsController', ['except' => ['create', 'edit', 'run']])->middleware('WorkTextString');
//
//    Route::get('user_chat_run', 'UserChatsController@run')->name('user_chats.run');
//
//    Route::get('send','UserChatsController@send');
//
//    // http://local-tasks.com/admin/user_chats#/admin/user_chats/run/35
//});
