<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});


Broadcast::channel('nsn_tasks',function($user){
    return ['user'=>$user];
});

Broadcast::channel('nsn_tasks_chat',function($user/*, $userChat*/){
    return ['user_id'=>$user->id, 'name'=>$user->name/*, 'userChat'=> $userChat*/];
});