<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if( !empty($site_name) ){{ $site_name }}@endif</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-confirm.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">

</head>
<body>
<div id="app">

    <app-header :header_title="''"></app-header>

    @yield('content')

    <status-line></status-line>
</div> <!-- id="app">-->

@include('layouts.footer')

<script src="{{ asset('js/app.js'    ) }}{{  "?dt=".time()  }}"></script>
<script src="{{ asset('js/debug.js'    ) }}{{  "?dt=".time()  }}"></script>

<script src="{{ asset('js/jquery/select2.js') }}{{  "?dt=".time()  }}"></script>
<script src="{{ asset('js/jquery/jquery-confirm.min.js') }}"></script>

</body>


</html>