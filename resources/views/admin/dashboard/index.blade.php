@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body table-responsive">
                    <router-view name="dashboardIndex"></router-view>
                    <router-view></router-view>
                </div>
            </div>
        </div>
    </div>
@endsection
