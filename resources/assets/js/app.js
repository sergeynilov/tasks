
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
require('jquery');
require('jquery-confirm');


window.Vue = require('vue');

import VueRouter from 'vue-router';

import VueTimeago from 'vue-timeago'  // https://github.com/egoist/vue-timeago
Vue.use(VueTimeago, {
    name: 'timeago', // component name, `timeago` by default
    locale: 'en-US',
    locales: {
        // you will need json-loader in webpack 1
        'en-US': require('vue-timeago/locales/en-US.json')
    }
})


import Vue from 'vue';
import Element from 'element-ui'

import VeeValidate from 'vee-validate'; // http://vee-validate.logaretm.com/  ,  http://vee-validate.logaretm.com/examples.html
// http://jsfiddle.net/xcha53ub/6/, https://blog.pusher.com/complete-guide-to-form-validation-in-vue/
import VeeValidateLaravel from 'vee-validate-laravel';
const veeValidateConfigArray = {
    errorBagName: 'vueErrorsList', // change if property conflicts
    fieldsBagName: 'fields',
    delay: 0,
    locale: 'en',
    dictionary: null,
    strict: true,
    classes: false,
    classNames: {
        touched: 'touched', // the control has been blurred
        untouched: 'untouched', // the control hasn't been blurred
        valid: 'valid', // model is valid
        invalid: 'invalid', // model is invalid
        pristine: 'pristine', // control has not been interacted with
        dirty: 'dirty' // control has been interacted with
    },
    events: 'input|blur',
    inject: true,
    validity: false,
    aria: true,
    i18n: null, // the vue-i18n plugin instance,
    i18nRootKey: 'validations' // the nested key under which the validation messsages will be located
};

Vue.use(VeeValidate, veeValidateConfigArray);
Vue.use(VeeValidateLaravel);


import moment from "moment";
Vue.use(require('vue-moment')); // https://www.npmjs.com/package/vue-moment


import Datepicker from 'vuejs-datepicker'; // https://github.com/charliekassel/vuejs-datepicker

// import Vue from 'vue';
import datePicker from 'vue-bootstrap-datetimepicker';  // https://github.com/ankurk91/vue-bootstrap-datetimepicker
// import 'bootstrap/dist/css/bootstrap.css';
import 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css';
Vue.use(datePicker);

import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)

import VueCollapse from 'vue2-collapse'
Vue.use(VueCollapse) // https://roszpun.github.io/vue-collapse/#/?id=examples


window.Vue.use(VueRouter);

Vue.component( 'app-header',  require( './components/lib/AppHeader.vue') );
Vue.component( 'status-line',  require( './components/lib/StatusLine.vue') );
// Vue.component( 'weather-viewer',  require( './components/lib/WeatherViewer.vue') ); //  //resources/assets/js/components/lib/WeatherViewer.vue


import AuthRegister from './components/Auth/Register.vue';
import AuthLogin from './components/Auth/Login.vue';


import TasksIndex from './components/tasks/TasksIndex.vue';                 /*      imported reference to .vue file */
import TasksFilter from './components/tasks/TasksIndex.vue';
import CategoryEdit from './components/tasks/CategoryEdit.vue';
import TasksEdit from './components/tasks/TasksEdit.vue';
import TaskOperation from './components/tasks/TaskOperation.vue';
import BlockContainer from './components/lib/BlockContainer.vue';

import UserTaskTypesIndex from './components/user_task_types/UserTaskTypesIndex.vue';
import UserTaskTypesEdit from './components/user_task_types/UserTaskTypesEdit.vue';


import EventsIndex from './components/events/EventsIndex.vue';
import EventsEdit from './components/events/EventsEdit.vue';



import DocumentCategoriesIndex from './components/document_categories/DocumentCategoriesIndex.vue';
import DocumentCategoryEdit from './components/document_categories/DocumentCategoryEdit.vue';

import DashboardIndex from './components/dashboard/DashboardIndex.vue';
import TestIndex from './components/test/Test.vue';


import AppDescription from './components/dashboard/AppDescription.vue';
// import TestPage from './components/test/Test.vue';
import TodoPage from './components/dashboard/Todo.vue';

// import SourceViewer from './components/lib/SourceViewer.vue';

// import SourceViewer from './components/lib/SourceViewer.vue';
import SourceViewerContainer from './components/lib/SourceViewerContainer.vue';

import UserChatsIndex from './components/user_chats/UserChatsIndex.vue';
import UserChatsEdit from './components/user_chats/UserChatsEdit.vue';

import UserChatsRun from './components/user_chats/UserChatsRun.vue';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en'
Vue.use(ElementUI, { locale })


// import {DatePicker} from 'uiv'
// import my_table from 'vue-simple-table';
// Vue.component('my-table', my_table)

Vue.directive('focus', { inserted(el) { el.focus() } })
Vue.directive('selected', { inserted(el) { el.select() } })



import appMixin from './appMixin';


// Messages notifications
import Toaster from 'v-toaster';
import 'v-toaster/dist/v-toaster.css'
Vue.use(Toaster, {timeout: 4000})

import Notify from 'vue-notify-me' ;// https://github.com/PygmySlowLoris/vue-notify-me
// resources/assets/js/components/lib/NotifyMe.vue
// import lib/NotifyMe from './components/lib/NotifyMe.vue';
// Vue.component( 'notify-me',  require( './components/lib/NotifyMe.vue') );


import vSelect from 'vue-select'
Vue.component('v-select', vSelect) // https://github.com/sagalbot/vue-select


import Popover  from 'vue-js-popover'
Vue.use(Popover)

import UserProfilesEdit from './components/user_profiles/UserProfilesEdit.vue';
import UserProfileView from './components/user_profiles/UserProfileView.vue';

const routes = [
    {
        path: '/',
        components: {
            // commonElements:CommonElements,
            auth_register: AuthRegister,                      /* router-view name in .vue file : imported reference to .vue file */
            tasksIndex: TasksIndex,
            tasksFilter: TasksFilter,
            userTaskTypesIndex: UserTaskTypesIndex,


            eventsIndex: EventsIndex,


            documentCategoriesIndex: DocumentCategoriesIndex,
            userChatsIndex: UserChatsIndex,
            dashboardIndex: DashboardIndex,
            testIndex: TestIndex,
            appDescriptionIndex: AppDescription,
            todoPage: TodoPage,
            // sourceViewer: SourceViewer,


            userProfilesEdit: UserProfilesEdit,
            userProfileView: UserProfileView,

            'notify-me': Notify
            // 'editor': Editor,
        }
    },
    {path: 'auth_register', component: AuthRegister, name: 'auth_register'},
    {path: '/auth/login', component: AuthLogin, name: 'AuthLogin'},


    // {path: '/admin/tasks/create', component: TasksCreate, name: 'createTask'},

    {path: '/admin/tasks/category/:id', component: CategoryEdit, name: 'editCategory'},
    {path: '/admin/tasks/edit/:id', component: TasksEdit, name: 'editTask'},

    {path: '/admin/task_operation/:id', component: TaskOperation, name: 'taskOperation'},
    {path: '/admin/block_container/:type', component: BlockContainer, name: 'blockContainer'},
    {path: '/admin/test_todo', component: TodoPage, name: 'todoPage'},
    {path: '/admin/app_description', component: AppDescription, name: 'appDescription'},
    {path: '/admin/dashboard', component: DashboardIndex, name: 'dashboardIndex'},
    {path: '/admin/test', component: TestIndex, name: 'testIndex'}, //            testIndex: TestIndex,
    // {path: '/admin/tasks/filter/:filter', component: TasksFilter, name: 'tasksFilter'},
    {path: '/admin/tasks/filter/:filter', component: TasksFilter, name: 'tasksFilter'},
    {path: '/admin/tasks', component: TasksIndex, name: 'tasksIndex'},
    {path: '/admin/events', component: EventsIndex, name: 'eventsIndex'},
    // {path: '/admin/source_viewer/:src_file/:file_type', component: SourceViewer, name: 'sourceViewer'},
    // {path: '/admin/source_viewer/:src_file', component: SourceViewer, name: 'sourceViewer'},
    // {path: '/admin/source_viewer/:filename/:ext', component: SourceViewer, name: 'sourceViewer'},
    {path: '/admin/source_viewer_container/:filename/:ext', component: SourceViewerContainer, name: 'sourceViewerContainer'},


    {path: '/admin/user_task_types', component: UserTaskTypesIndex, name: 'userTaskTypesIndex'},
    {path: '/admin/user_task_types/edit/:id', component: UserTaskTypesEdit, name: 'editUserTaskType'},
    {path: '/admin/events/edit/:id', component: EventsEdit, name: 'editEvent'},

    {path: '/admin/user_chats/:filter', component: UserChatsIndex, name: 'userChatsIndex'},
    {path: '/admin/user_chats/edit/:id', component: UserChatsEdit, name: 'editUserChat'},
    {path: '/admin/user_chat_run/:id', component: UserChatsRun, name: 'runUserChat'},


    // {path: '/admin/document_categories/create', component: DocumentCategoryCreate, name: 'createDocumentCategory'},
    // documentCategoriesIndex: DocumentCategoriesIndex,

    {path: '/admin/document_categories', component: DocumentCategoriesIndex, name: 'documentCategoriesIndex'},
    {path: '/admin/document_categories/edit/:id', component: DocumentCategoryEdit, name: 'editDocumentCategory'},

    {path: '/admin/user_profile', component: UserProfilesEdit, name: 'editUserProfiles'},
    {path: '/admin/user_profile_view/:id', component: UserProfileView, name: 'userProfileView'},
    //     {path: '/admin/task_operation/:id', component: TaskOperation, name: 'taskOperation'},

]

// alert("-02")   UserProfileEdit.vue
const router = new VueRouter( {
    // mode: 'hash', // default
    // mode: 'history',
    routes
})





export const bus = new Vue();

new Vue({ router,

    data:{
        app_title: '',
        loggedUserProfile: {},
        loggedUserInGroups: {},
    },

    mixins : [appMixin],

    methods: {
        initEchoServices() {

            Echo.channel('nsn_tasks')
                .listen('UserProfileModified', (e) => {
                    console.log('Inside of UserProfileModified::')
                    console.log(e)
                });


            Echo.private('nsn_tasks_chat')
                .listenForWhisper('typing', (e) => {
                    // alert( "listenForWhisper e::"+var_dump(e) )
                    console.log(" resources/assets/js/app.js : listenForWhisper::");
                    // console.log("e:::");
                    // console.log(e);
                    // console.log(e.logged_user_id);
                    // console.log(e.logged_user_background_color);
                    // console.log(e.logged_user_color);
                    //  console.log("e.user_chat_new_message:::");
                    //  console.log(e.user_chat_new_message);

                    // if (jQuery.trim(e.user_chat_new_message) != '') {
                    //     this.typing = 'typing...'
                    // }else{
                    //     this.typing = ''
                    // }

                    bus.$emit('userChatTypingEvent', {
                        'typing_username' : e.logged_username,
                        'typing_user_id' : e.logged_user_id,
                        'typing_user_background_color' : e.logged_user_background_color,
                        'typing_user_color' : e.logged_user_color,
                        'typing_text' : e.user_chat_new_message,
                        'user_chat_id' : e.user_chat_id,
                        'created_at': this.getNowDateTime()
                    });

                })

                .listen('ChatEvent', (e) => { // received triggered event from other user to refresh chat messages listing of current chart
                    // alert( "resources/assets/js/app.js ChatEvent  e::"+var_dump(e) )
                    console.log("??? resources/assets/js/app.js ChatEvent::");
                    console.log(e);

                    console.log("this.loggedUserProfile::");
                    console.log(this.loggedUserProfile);


                    // if ( e.user.id != this.loggedUserProfile['id'] ) { // fo not sent event to itself
                    bus.$emit('newUserChatAddedEvent', {
                        'user_chat_message_id': e.user_chat_message_id,
                        'user_chat_id': e.user_chat_id,
                        'author_user_id': e.user_id,
                        'author_name': e.author_name,
                        'message_type': e.message_type,
                        'text': e.text,
                        'is_top': e.is_top,
                        'created_at': this.getNowDateTime()
                    });
                    // } // if ( e.user.id != this.loggedUserProfile.id ) { // fo not sent event to itself

                })


            Echo.join(`nsn_tasks_chat`)
                .here((usersList) => {
                    var app_route_name= this.$route.name

                    // console.log("here  app_route_name:::");
                    // console.log(app_route_name)
                    // if ( app_route_name.toString() == 'runUserChat' ) { // event must be triggered only on char page
                    // console.log("here INSIDE of runUserChat ");
                    if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                        bus.$emit('setChartUsersListEvent', {'usersList': usersList});
                    } //                     if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                })
                .joining((user) => {
                    var app_route_name= this.$route.name

                    console.log("this.$route::")
                    console.log( this.$route )

                    console.log("app_route_name::")
                    console.log( app_route_name )

                    // console.log(user.name);
                    // if ( app_route_name.toString() == 'runUserChat' ) { // event must be triggered only on char page
                    // console.log("joining INSIDE ");
                    if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                        this.showPopupMessage('User "' + user.name + '" joined this chat !', 'success');
                        bus.$emit('setChartUserJoiningEvent', {'user': user});
                    } // if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                })
                .leaving((user) => {
                    var app_route_name= this.$route.name

                    // console.log("leaving  app_route_name:::");
                    // console.log(app_route_name)
                    // console.log(user.name);
                    // if ( app_route_name.toString() == 'runUserChat' ) { // event must be triggered only on char page
                    // console.log("joining INSIDE ");
                    if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                        this.showPopupMessage('User "' + user.name + '" leaving this chat !', 'warning');
                        bus.$emit('setChartUserLeavingEvent', {'user': user});
                    } // if ( app_route_name ==  'runUserChat' ) { // flash chat message only on chat page
                });

        }, // initEchoServices() {

    }, // methods: {


    created() {
        this.getSettingsValue('site_name','',window.API_VERSION_LINK, 'setSiteNameEvent', bus);

        bus.$on('UserProfileIsSetEvent', (username, site_name) => {
            this.app_title = site_name+" for "+username;
        })
        bus.$on('setSiteNameEvent', (site_name) => {
            // alert( "setSiteNameEvent site_name::"+site_name )
            var dataArray= Array(site_name);
            // alert( "this.loggedUserProfile::"+var_dump(this.loggedUserProfile) )
            if ( typeof this.loggedUserProfile.first_name != "undefined" && typeof this.loggedUserProfile.last_name != "undefined" ) {
                dataArray[dataArray.length-1] = this.loggedUserProfile.last_name +" "+ this.loggedUserProfile.first_name
            }
            var app_title= this.concatStrings (dataArray, '');
            // alert( "app_title::"+var_dump(app_title) )
            this.app_title = app_title;
        })
    }, // created() {

    mounted() {

        // console.log(" mounted!! this.loggedUserProfile::");
        // console.log( typeof this.loggedUserProfile);
        // console.log(this.loggedUserProfile);
        //
        //
        // console.log(" mounted!! this.loggedUserProfile.id::");
        // console.log( typeof this.loggedUserProfile.id);
        // console.log(this.loggedUserProfile.id);
        //
        // alert( "window.logged_user_id::"+(window.logged_user_id) +"  window.logged_user_name::"+window.logged_user_name )


        if ( typeof window.logged_user_id == "undefined" || typeof window.logged_user_name == "undefined" ) return;// check if user is logged
        axios.get(window.API_VERSION_LINK + '/get_logged_user_info/' ).then((response) => {
            if ( typeof response.data.loggedUser == "object" ) {
                this.loggedUserProfile = response.data.loggedUser;
                this.loggedUserInGroups = response.data.loggedUserInGroups;
                bus.$emit('UserProfileIsSetEvent', response.data.loggedUser.first_name + ' ' + response.data.loggedUser.last_name, response.data.site_name, this.loggedUserProfile, this.loggedUserInGroups );
                // alert( "LOGGED::"+var_dump() )
                this.initEchoServices()
            }
        }).catch((error) => {
            this.showRunTimeError(error, this);
        });

    }, // mounted(){


} ).$mount('#app')  // new Vue({ router,


Vue.filter('timeInAgoFormat', function (value) {
    if ( typeof value == "undefined" || typeof value == "object") return '';
    var formatted_val= moment(value).fromNow();
    // alert( "timeInAgoFormat value::"+(value) +"   formatted_val::"+formatted_val + "  :: " + ( typeof value ) )
    return formatted_val;
})

Vue.filter('Capitalize', function (value) {
    var ret= value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
    return ret;
})

Vue.filter('toFixed', function (price, limit) {
    return price.toFixed(limit);
});

Vue.filter('toUSD', function (price) {
    return `$${price}`;
});

Vue.filter('json', function (value) {
    return JSON.stringify(value);
});

Vue.filter('pluck', function (objects, key) {
    return objects.map(function(object) {
        return object[key];
    });
});

Vue.filter('at', function (value, index) {
    return value[index];
});

Vue.filter('first', function (values) {
    if(Array.isArray(values)) {
        return values[0];
    }
    return values;
});

Vue.filter('last', function (values) {
    if(Array.isArray(values)) {
        return values[values.length - 1];
    }
    return values;
});

Vue.filter('without', function (values, exclude) { //Return a copy of the array without the given elements:
    return values.filter(function(element) {
        return !exclude.includes(element);
    });
});

Vue.filter('unique', function (values, unique) {
    return values.filter(function(element, index, self) {
        return index == self.indexOf(element);
    });
});

function var_dump (oElem, from_line, till_line) {
    if (typeof oElem == 'undefined') return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if (typeof from_line == "number" && typeof till_line == "number") {
        return sStr.substr(from_line, till_line);
    }
    if (typeof from_line == "number") {
        return sStr.substr(from_line);
    }
    return sStr;
}