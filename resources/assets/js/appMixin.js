export default {

//
//     Vue.filter('myOwnTime', function (value) {
//     return moment(value).fromNow();
//     // return moment(value).calendar();
// })

    computed: {

        // timeInAgoFormat (value) {
        //     return moment(value).fromNow();
        // // return moment(value).calendar();
        // },
        //
        // fruitsFiltered() {
        //     return this.fruits.filter((fruit) => {
        //         return fruit.match(this.filteredFruit)
        //     })
        // }
    }, // computed: {

    methods: {

        convertMinsToHrsMins: function (minutes) {
            if ( minutes< 60) return (minutes+' minutes');
            var h = Math.floor(minutes / 60);
            var m = minutes % 60;
            h = h < 10 ? h : h;
            m = m < 10 ? m : m;
            return h + ':' + m + ' hour(s)';
        },

        pluralize: function (n, single_str, multi_str) {
            return n === 1 ? single_str : multi_str
        },

        showFullUserName: function (username, first_name, last_name) {
            var ret_str= first_name + " " + last_name + " ( " + username + " ) ";
            return ret_str;
        },

        getHeaderIcon (icon) {
            if ( typeof menuIconsArray[icon]) return '<i class="'+menuIconsArray[icon]+'"></i>' ;
        },

        getActiveUserGroupLabel(loggedUserInGroupsArray) {
            // alert( "checkUserGroupAccess   typeof loggedUserInGroupsArray ::"+ ( typeof loggedUserInGroupsArray) + "  group_name::"+group_name+"   loggedUserInGroupsArray ::"+(loggedUserInGroupsArray) )
            var ret= '';
            loggedUserInGroupsArray.map((nextLoggedUserInGroup,index)=> {
                // alert( "nextLoggedUserInGroup::" + var_dump(nextLoggedUserInGroup) )
                if ( nextLoggedUserInGroup.group_name != "undefined" && nextLoggedUserInGroup.status == 'A' ) {
                    ret= ret+nextLoggedUserInGroup.group_name+', ';
                }
            });
            return ret;
        },

        checkUserGroupAccess(loggedUserInGroupsArray, group_name) {
            // alert( "checkUserGroupAccess   typeof loggedUserInGroupsArray ::"+ ( typeof loggedUserInGroupsArray) + "  group_name::"+group_name+"   loggedUserInGroupsArray ::"+(loggedUserInGroupsArray) )
            var ret= false;
            loggedUserInGroupsArray.map((nextLoggedUserInGroup,index)=> {
                // alert( "nextLoggedUserInGroup::" + var_dump(nextLoggedUserInGroup) )
                if ( nextLoggedUserInGroup.group_name == group_name && nextLoggedUserInGroup.status == 'A' ) {
                    ret= true;
                }
            });
            return ret;
        },


        getIsPastColor(is_past) { //
            if ( typeof settings_isPastColorsArray[is_past] != 'undefined' ) {
                return 'backgroundColor:'+settings_isPastColorsArray[is_past]
            }
            return '#fff'
        },

        getPriorityColor(priority) {
            // return '' // TODO COMMENT
            if ( typeof settings_priorityColorsArray[priority] != 'undefined' ) {
                return 'backgroundColor:'+settings_priorityColorsArray[priority]
            }
            return ''
        },

        dateToMySqlFormat(dat) {
            if ( typeof dat != 'object' ) return dat;
            // alert( "dat::"+(dat) +  "   typeof  dat::"+(typeof dat) + "  dat::"+var_dump(dat) )

            var mm = dat.getMonth() + 1; // getMonth() is zero-based
            var dd = dat.getDate();

            return [dat.getFullYear(),
                (mm>9 ? '' : '0') + mm,
                (dd>9 ? '' : '0') + dd
            ].join('-');
        },
        say(row) {
            alert('Open console and looking for response object!')
            console.log(row);
        },
        edit(value) {
            this.action.edit_value = value;
        },
        submit(res) {
            if (res.row[res.k] != this.action.edit_value) {
                let after_update = JSON.parse(JSON.stringify(res.row))
                this.$refs.my_table.set_row_undo(after_update, res.c)
                res.row[res.k] = this.action.edit_value
            }
            this.reset_edit()
        },
        reset_edit() {
            this.$refs.my_table.close()
        },
        set_checkbox(item) {
            this.action.checkbox = item
        },
        edit_row(row) {
            for (let k in row) {
                this.action.edit_row[k] = row[k]
            }
        },
        submit_row(row) {
            let after_update = JSON.parse(JSON.stringify(row))
            for (let key in this.action.edit_row) {
                row[key] = this.action.edit_row[key]
            }
            this.$refs.my_table.set_row_undo(after_update)
            this.$refs.my_table.close(true)
        },

        // timeInAgoFormat : function(value) {
        //     alert( "-2 timeInAgoFormat  value::"+(value) )
        //     return moment(value).fromNow();
        //     // return moment(value).calendar();
        // },
        //

        getSettingsValue: function (name, default_value, api_version_link, trigger_event_name, bus) {
            axios.get(api_version_link+'/get_settings_value?name='+name )
                .then(function (response) {
                    // alert( "getSettingsValue response.data::"+(response.data) )
                    if ( typeof trigger_event_name != "undefined" && typeof bus != "undefined" ) {
                        // alert( "INSIDE -1 trigger_event_name"+trigger_event_name )
                        bus.$emit(trigger_event_name, response.data.settings_value );
                    }
                    //                     bus.$emit('UserProfileIsSetEvent', response.data.loggedUser.first_name + ' ' + response.data.loggedUser.last_name, response.data.site_name);

                    return response.data.settings_value;
                })
                .catch(function (error) {
                        // alert( "error::"+var_dump(error) )
//                     app.showRunTimeError(error, app);
                })
        },

        formatColor: function (rgb) {
            var isOk  = /^#[0-9A-F]{6}$/i.test(rgb)
            // var isOk  = /^#[0-9A-F]{6}$/i.test('#aabbcc')
            // alert( "isOk::"+isOk )
            if ( isOk ) return rgb;

            // alert( typeof rgb )
            if ( typeof rgb != "string" || this.trim(rgb) == "" ) return "";
            rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
            return (rgb && rgb.length === 4) ? "#" +
                ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';



            var parts = rgba.substring(rgba.indexOf("(")).split(","),
                r = parseInt(this.trim(parts[0].substring(1)), 10),
                g = parseInt(this.trim(parts[1]), 10),
                b = parseInt(this.trim(parts[2]), 10),
                a = parseFloat(this.trim(parts[3].substring(0, parts[3].length - 1))).toFixed(2);

            return ('#' + r.toString(16) + g.toString(16) + b.toString(16) + (a * 255).toString(16).substring(0,2));
            // // const rgba = "rgba(47, 154, 47, 1)"
            // const numbers = value.match(/[0-9]+/g)
            // numbers[3] = Math.round(numbers[3] * 255) //since the alpha is represented as 0-1 rather than 0-255
            // const hex = '#' + numbers.map(n => Number(n).toString(16)).join('')
            // return hex;
        },

        trim: function  (str) {
            return str.replace(/^\s+|\s+$/gm,'');
        },
        
        concatStrings: function (dataArray, splitter) {
            var ret= '';
            // alert( "dataArray::"+(typeof dataArray)+"   "+var_dump(dataArray) )
            const l = dataArray.length;
            dataArray.map((next_string, index) => {
                next_string= jQuery.trim(next_string);
                // alert( "next_string::"+(next_string) + (typeof next_string) +"  splitter::"+splitter )
                // if ( typeof next_string != "undefined" && typeof next_string != "string" ) {
                if ( typeof next_string == "string" ) {
                    if (next_string) {
                        if (l === index + 1) {
                            ret = ret + next_string;
                        } else {
                            ret = ret + next_string + splitter;
                        }
                    } // if ( next_string ) {
                }
            });
            return ret;
        },

        showRunTimeError: function (error, app) {

            app.message = '';
            app.errorsList = [];
            // alert("::00000")

            if (( typeof error.response != "undefined" && typeof error.response.data != "undefined" && typeof error.response.data.message != "undefined" ) && ( typeof error.response.data.errors == "undefined" || error.response.data.errors.length == 0)) {
                // alert("::-1   "+ error.response.data.message )
                app.message = this.commonError(error.response.data.message);
            }
            // alert( "::-02" )
            if (typeof error.response != "undefined" && typeof error.response.data != "undefined" && typeof error.response.data.errors != "undefined" /*&& error.response.data.errors.length > 0*/) {
                // alert( "::-3" )
                app.errorsList = error.response.data.errors;
            }

            return app;
        },

        commonError : function ( err_text ) {
            return err_text;
        },

        showPopupMessage: function (message, type) {
            if (type == 'success') {         // https://www.npmjs.com/package/v-toaster
                this.$toaster.success(message)
            }
            if (type == 'info') {
                this.$toaster.info(message)
            }
            if (type == 'error') {
                this.$toaster.error(message)
            }
            if (type == 'warning') {
                this.$toaster.warning(message)
            }
        }, // showPopupMessage: function (message, type) {


        concatStr: function (str) {
            if (str.length > settings_max_str_length_in_listing) {
                return str.slice(0, settings_max_str_length_in_listing) + '...';
            }
            return str;
        },

        // timeSinceLabel: function (time) {
        //     // jQuery.timeago.settings.allowFuture = true;
        //     return jQuery.timeago(time);
        // },

        getNowDateTime: function () {
            return Date.now();
        },

        getNowTimestamp: function () {
            return Date.now() / 1000 | 0;
        },


        getFileSizeAsString: function (file_size) {
            if (parseInt(file_size) < 1024) {
                return file_size + 'b';
            }
            if (parseInt(file_size) < 1024 * 1024) {
                return Math.floor(file_size / 1024) + 'kb';
            }
            return Math.floor(file_size / (1024 * 1024)) + 'mb';
        },


        Capitalize: function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },


        confirmMsg: function (question, confirm_function_code, title, icon) {
            if (typeof title == "undefined" || jQuery.trim(title) == "") {
                title = 'Confirm!'
            }
            if (typeof icon == "undefined" || jQuery.trim(icon) == "") {
                icon = 'glyphicon glyphicon-signal'
            }
            $.confirm({
                icon: icon,
                title: title,
                content: question,
                confirmButton: 'YES',
                cancelButton: 'Cancel',
                confirmButtonClass: 'btn-info',
                cancelButtonClass: 'btn-danger',
                keyboardEnabled: true,
                columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
                confirm: function () {
                    confirm_function_code()
                }
            });
        },

        func_setting_focus: function (focus_field) {
            $('#' + focus_field).focus();
        },

        alertMsg: function (content, title, confirm_button, icon, focus_field) {
            $.alert({
                title: title,
                content: content,
                icon: ( typeof icon != 'undefined' ? icon : 'fa fa-info-circle' ),
                confirmButton: ( typeof confirm_button != 'undefined' ? confirm_button : 'OK' ),
                keyboardEnabled: true,
                columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
                confirm: function () {
                    setTimeout("func_setting_focus('" + focus_field + "')", 500);
                }
            });
        },

        br2nl : function(str) {
            return str.replace(/<br\s*\/?>/mg,"\n");
        },

        // nl2br: function (str) { // Inserts HTML line breaks before all newlines in a string
        //     return str.replace(/([^>])\n/g, '$1<br/>');
        // },

        nl2br : function(str, is_xhtml) {
            var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        },

        replaceAll : function(str, search, replacement) {
            return str.replace(new RegExp(search, 'g'), replacement);
        },

    }, // methods: {

}

function var_dump (oElem, from_line, till_line) {
    if (typeof oElem == 'undefined') return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if (typeof from_line == "number" && typeof till_line == "number") {
        return sStr.substr(from_line, till_line);
    }
    if (typeof from_line == "number") {
        return sStr.substr(from_line);
    }
    return sStr;
}