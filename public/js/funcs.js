// alert( "funcs!::"+(1) )
// al( checked_states_selection, "make_load_hostel_treeview_data checked_states_selection::" )
/*    public static function tbUrlDecode($Url)
 {
 $Url = str_replace('ZZZZZ', '/', $Url);
 $Url = str_replace('XXXXX', '.', $Url);
 $Url = str_replace('YYYYY', '-', $Url);
 $Url = str_replace('WWWWW', '_', $Url);
 return $Url;
 }

 public static function tbUrlEncode($Url)
 {
 $Url = str_replace('/', 'ZZZZZ', $Url);
 $Url = str_replace('.', 'XXXXX', $Url);
 $Url = str_replace('-', 'YYYYY', $Url);
 $Url = str_replace('_', 'WWWWW', $Url);
 return $Url;
 }
 */

// function backendInit() {
//
//     // alert( "backendInit::"+var_dump(11) )
//     $(".chosen_select_box").chosen({
//         disable_search_threshold: 10,
//         no_results_text: "Nothing found!",
//         height: "300px",
//         width: "100%"
//
//     });
//     // $('.selectpicker').selectpicker();
//     $('[data-toggle="tooltip"]').tooltip()
//     $('.image-link').magnificPopup({type: 'image'});
//     // alert( "AFTER tooltip::" )
// }

function returnNewlineCharacters (str)
{
    str = str.replace(new RegExp( 'ZZZZZ' , 'g'), String.fromCharCode(13) );
    str = str.replace(new RegExp( 'XXXXX' , 'g'), String.fromCharCode(10) );
    return str;
}

function replaceNewlineCharacters (str)
{
    // alert( "replaceNewlineCharacters str::"+str )
    if ( typeof str != "string") {
        // alert( "typeof str ::"+( typeof str  ) )
        return "";
    }
    str = str.replace(new RegExp(String.fromCharCode(13), 'g'), 'ZZZZZ');
    str = str.replace(new RegExp(String.fromCharCode(10), 'g'), 'XXXXX');
    return str;
}

function  strip_tags(str, allowed_tags) {
    var key = '', allowed = false;
    var matches = [];
    var allowed_array = [];
    var allowed_tag = '';
    var i = 0;
    var k = '';
    var html = '';

    var replacer = function(search, replace, str) {
        return str.split(search).join(replace);
    };

    // Build allowes tags associative array
    if (allowed_tags) {
        allowed_array = allowed_tags.match(/([a-zA-Z]+)/gi);
    }

    str += '';

    // Match tags
    matches = str.match(/(<\/?[\S][^>]*>)/gi);

    // Go through all HTML tags
    for (key in matches) {
        if (isNaN(key)) {
            // IE7 Hack
            continue;
        }

        // Save HTML tag
        html = matches[key].toString();

        // Is tag not in allowed list? Remove from str!
        allowed = false;

        // Go through all allowed tags
        for (k in allowed_array) {
            // Init
            allowed_tag = allowed_array[k];
            i = -1;

            if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+'>');}
            if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+' ');}
            if (i != 0) { i = html.toLowerCase().indexOf('</'+allowed_tag)   ;}

            // Determine
            if (i == 0) {
                allowed = true;
                break;
            }
        }

        if (!allowed) {
            str = replacer(html, "", str); // Custom replace. No regexing
        }
    }

    return str;
}


function switchPostcodeLoader(show_postcode_loader) {
    if (!show_postcode_loader) {
        $("#span_postcode_block_loading_image").css( "display", "block" );
    } else {
        $("#span_postcode_block_loading_image").css( "display", "none" );
    }
}

function confirmMsg(question, confirm_function_code, title, icon ) {
    if ( typeof title == "undefined" || jQuery.trim(title) == "" ) {
        title= 'Confirm!'
    }
    if ( typeof icon == "undefined" || jQuery.trim(icon) == "" ) {
        icon= 'glyphicon glyphicon-signal'
    }
    $.confirm({
        icon: icon,
        title: title,
        content: question,
        confirmButton: 'YES',
        cancelButton: 'Cancel',
        confirmButtonClass: 'btn-info',
        cancelButtonClass: 'btn-danger',
        keyboardEnabled: true,
        columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
        confirm: function(){
            confirm_function_code()
        }
    });
}

function func_setting_focus(focus_field) {
    $('#'+focus_field).focus();
}

function alertMsg(content, title, confirm_button, icon, focus_field) {
    $.alert({
		title: title,
		content: content,
		icon: ( typeof icon != 'undefined' ? icon : 'fa fa-info-circle' ),
		confirmButton: ( typeof confirm_button != 'undefined' ? confirm_button : 'OK' ),
		keyboardEnabled: true,
        columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
		confirm: function(){
            setTimeout("func_setting_focus('"+focus_field+"')", 500);
		}
	});
}

function effectiveDeviceWidth(param) {
    var viewport = {
        width  : $(window).width(),
        height : $(window).height()
    };
    //alert( "viewport::"+var_dump(viewport) )
    if (typeof param != "undefined") {
        if (param.toLowerCase() == 'width') {
            return viewport.width;
        }
        if (param.toLowerCase() == 'height') {
            return viewport.height;
        }
    }
    return viewport;
    //var deviceWidth = window.orientation == 0 ? window.screen.width : window.screen.height;
    //// iOS returns available pixels, Android returns pixels / pixel ratio
    //// http://www.quirksmode.org/blog/archives/2012/07/more_about_devi.html
    //if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
    //    deviceWidth = deviceWidth / window.devicePixelRatio;
    //}
    //return deviceWidth;
}


function disabledEventPropagation(event)
{
    if (event.stopPropagation){
        event.stopPropagation();
    }
    else if(window.event){
        window.event.cancelBubble=true;
    }
}

/*function show_alert( content_text, title, icon_type ) {
    if ( $.trim(icon_type == "") || typeof icon_type == "undefined") {
        icon_type = "glyphicon glyphicon-exclamation-sign"
    }
    if ( $.trim(title) == "" || typeof title == "undefined") {
        title = "Alert!"
    }
    $.alert({
        title: title,
        content: content_text,
        icon: icon_type,
        keyboardEnabled: true,
        //rtl: true,
        //confirmKeys: [65],
        //cancelKeys: [66],
        //confirm: function() {
        //    alert( "confirm" )
        //    disabledEventPropagation(event)
        //},
        //cancel: function(){
        //    alert('Canceled')
        //},
        //
        //onClose: function(){
        //    alert('before the modal is closed');
        //    disabledEventPropagation(event)
        //},
        //onAction: function(action){
        //    // action is either 'confirm', 'cancel' or 'close'
        //    alert(action + ' was clicked');
        //},
    });
}*/


function getBootstrapPlugins() {
    var ret_str= '';
    var check_glyphicon_obj = document.getElementById("check_glyphicon");
     //alert( "check_glyphicon_obj::"+( typeof check_glyphicon_obj) + "   check_glyphicon_obj::"+var_dump(check_glyphicon_obj))
    if (typeof check_glyphicon_obj == "undefined" || typeof check_glyphicon_obj != 'Element' ) return ret_str;

    var check_glyphicon_object= window.getComputedStyle(check_glyphicon_obj )

    // alert( "check_glyphicon_object::"+(typeof check_glyphicon_object)+" : "+var_dump(check_glyphicon_object) )
    var computedStyle= jQuery.makeArray( check_glyphicon_object )

    var glyphicons_ret= false
    var array = $.map(computedStyle, function(value, index) {
        K = value.indexOf('font-family');
        if (K > -1) {
            var font_family_val= $("#check_glyphicon").css("font-family")
            K = font_family_val.indexOf('Glyphicons Halflings');
            if (K > -1) {
                glyphicons_ret= true
            }
        }
    });
    ret_str+= "Glyphicons:" + ( (glyphicons_ret) ? " <b>On</b>" :"Off" ) + ". ";

    ret_str+= "alert:" + ( (typeof($.fn.alert) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "button:" + ( (typeof($.fn.button) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "carousel:" + ( (typeof($.fn.carousel) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "dropdown:" + ( (typeof($.fn.dropdown) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "modal:" + ( (typeof($.fn.modal) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "tooltip:" + ( (typeof($.fn.tooltip) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "popover:" + ( (typeof($.fn.popover) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "tab:" + ( (typeof($.fn.tab) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "affix:" + ( (typeof($.fn.affix) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "collapse:" + ( (typeof($.fn.collapse) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "scrollspy:" + ( (typeof($.fn.scrollspy) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    ret_str+= "transition:" + ( (typeof($.fn.transition) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    //ret_str+= "alert:" + ( (typeof($.fn.alert) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
    //alert( "ret_str::"+var_dump(ret_str) )
    return ret_str;
}

function htmlspecialChars(html) {
    html = html.replace(/&/g, "&amp;");
    html = html.replace(/</g, "&lt;");
    html = html.replace(/>/g, "&gt;");
    html = html.replace(/"/g, "&quot;");
    return html;
}


function htmlspecialCharsDecode (string, quote_style) { // eslint-disable-line camelcase

    var opt_temp = 0
    var i = 0
    var noquotes = false

    if (typeof quote_style === 'undefined') {
        quote_style = 2
    }
    string = string.toString()
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
    var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
    }
    if (quote_style === 0) {
        noquotes = true
    }
    if (typeof quote_style !== 'number') {
        // Allow for a single string or an array of string flags
        quote_style = [].concat(quote_style)
        for (i = 0; i < quote_style.length; i++) {
            // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
            if (OPTS[quote_style[i]] === 0) {
                noquotes = true
            } else if (OPTS[quote_style[i]]) {
                opt_temp = opt_temp | OPTS[quote_style[i]]
            }
        }
        quote_style = opt_temp
    }
    if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
        // PHP doesn't currently escape if more than one 0, but it should:
        string = string.replace(/&#0*39;/g, "'")
        // This would also be useful here, but not a part of PHP:
        // string = string.replace(/&apos;|&#x0*27;/g, "'");
    }
    if (!noquotes) {
        string = string.replace(/&quot;/g, '"')
    }
    // Put this in last place to avoid escape being double-decoded
    string = string.replace(/&amp;/g, '&')

    return string
}

function roundNumber(num, dec) {
    return num;
    var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    return result;
}

function addDecimalsdigit(digit) {
    return formatMoney(digit, 2, '.', ',');


    if (digit == null || typeof digit == "undefined") return '0.00';

    digit = digit.toString();
    K = digit.indexOf('.');
    if (K == -1) return digit + '.00';
    var decimals_value = digit.substring(K + 1, digit.length);
    if (decimals_value.length == 0) return digit + '00';
    if (decimals_value.length == 1) return digit + '0';
    return digit;
}


function getIndexOf(List, text_word, is_case_sensitive) {
    var L = List.length
    for (var I = 0; I < L; I++) {
        if (is_case_sensitive) {
            if (List[I] == text_word) return I;
        } else {
            if (List[I].toUpperCase() == text_word.toUpperCase()) return I;
        }
    }
    return -1;
}


function formatMoney(digit, c, d, t) {
    var n = digit, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

// form location parameter get it's value by parameter name
function randomNumber (m,n) {
    m = parseInt(m);
    n = parseInt(n);
    return Math.floor( Math.random() * (n - m + 1) ) + m;
}

function getYearMonthOfDate(date) {
  var L= date.length
  if ( L< 6 ) return date;
  return date.substr(2,6);
}

function clearUnsafeCookieChars(str) {
  str= str.replace ( / /g, "_" );
  return str;
}

function hasUnsafeCookieChars(str) {
  K= str.indexOf('+');
  if ( K>= 0 ) return '+';
  K= str.indexOf('_');
  if ( K>= 0 ) return '_';
  K= str.indexOf(',');
  if ( K>= 0 ) return ',';
  K= str.indexOf(';');
  if ( K>= 0 ) return ';';
  K= str.indexOf('\t');
  if ( K>= 0 ) return '\t';
  return '';
}

function checkEmail(value) {
  regex=/^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
  return regex.test(value);
}

function checkInteger(value) {
  regex=/^[0-9]+$/;
  return regex.test(value);
}

function checkisNormalInteger(str, can_be_zero) {
    var n = Math.floor(Number(str));
    if ( can_be_zero ) {
        return String(n) === str && n >= 0;
    } else {
        return String(n) === str && n > 0;
    }
}
function checkFloat(value) {
  // value= "87.";  //"2034.33";
  // regex= /^\d+(\.\d\d)?$/;
  regex= /^\d+(\.\d{0,2})?$/;
  // regex= /^[1-9]\d{2,4}([\.,]\d\d)?$/;
  // alert( regex+"  value::"+value )
  return regex.test(value);
  //return isNaN(parseFloat(value)) 
}


function getParameterValue(parameter_name) {
  if ( location.search.length > 1 ) {
    var query_str = location.search.substring( 1, location.search.length );
    var parameterValuesArray = query_str.split( '&' );
    for ( var I = 0; I < parameterValuesArray.length; I++ ) {
      var Element = parameterValuesArray[I].split( '=' );
      if ( parameter_name== Element[0] ) {
        return Element[1];
      }
    }
  }
  return '';
}

function getSplitted(str, splitter, index) {
    var value_arr = str.split( splitter );
    if (typeof value_arr[index] != "undefined" ) {
        return value_arr[index];
    }
    return '';
}



function formatPhone( obj ) {
  var phone = obj.value;
  var digits = phone.replace ( /[^0-9]/ig, "" );
  if ( !digits ) {
    return phone;
  }
  if ( digits.length == 10 ) {
    phone = "(" + digits.substring ( 0, 3 ) + ") " + digits.substring ( 3, 6 ) + "-" + digits.substring ( 6, 10 );
  } else {
    phone = digits;
  }
  return phone;
}

function formatZip( obj ) {
  var zip = obj.value;
  var digits = zip.replace ( /[^0-9]/ig, "" );
  if ( !digits ) {
    return zip;
  }
  if ( digits.length == 9 ) {
    zip = digits.substring ( 0, 5 ) + "-" + digits.substring ( 5, 9 );
  } else {
    zip = digits;
  }
  return zip;
}

/*
function Mod( Digit, Dividor ) {
  if ( Dividor== 0 ) {
    return 0;
  }
  return Math.ceil(Digit/Dividor);
}

function isDate( Day, Month, Year ) {
  if (typeof Day== "undefined") return false;
  if (typeof Month== "undefined") return false;
  if (typeof Year== "undefined") return false;
  //  alert("isDate  Day:"+Day+"  Month:"+Month+"  Year:"+Year);
  if ( Month== 1 || Month== 3 || Month== 5 || Month== 7 || Month== 8 || Month== 10 || Month== 12 ) {
    if ( Day>31 || Day<= 0 ) { return false; }
  }
  if ( Month== 4 || Month== 6 || Month== 9 || Month== 11 ) {
    if ( Day> 30 || Day<= 0 ) { return false; }
  }
  if ( Month== 2 ) {
    if ( ( Year % 4 )== 0 ) { // Leap Year
      if ( Day> 29 ) {
        return false;
      }
    } else {
      if ( Day> 28 ) { return false; }
    }
  }  // if ( Month== 2 } {
  return true;
}

function Concat( S, S1, S2 ) {
  return S + S1 + S2;
}
*/


function getCenteredTop( obj_height ) {
  var h= screen.availHeight;
  return (h - obj_height)/2;
}
function getCenteredLeft( obj_width ) {
  var w= screen.availWidth;
  return (w - obj_width)/2;
}

//
// var MAX_DUMP_DEPTH = 20;
// function dumpObj(obj, name, indent, depth) {
//
//   if (depth > MAX_DUMP_DEPTH) {
//
//     return indent + name + ": <Maximum Depth Reached>\n";
//
//   }
//
//   if (typeof obj == "object") {
//
//     var child = null;
//
//     var output = indent + name + "\n";
//
//     indent += "\t";
//
//     for (var item in obj)
//
//     {
//
//       try {
//
//         child = obj[item];
//
//       } catch (e) {
//
//         child = "<Unable to Evaluate>";
//
//       }
//
//       if (typeof child == "object") {
//
//         output += dumpObj(child, item, indent, depth + 1);
//
//       } else {
//
//         output += indent + item + ": " + child + "\n";
//
//       }
//
//     }
//
//     return output;
//
//   } else {
//
//     return obj;
//
//   }
//
// }

function Capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function var_dump(oElem, from_line, till_line) {
    if ( typeof oElem == 'undefined' ) return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number')     {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if ( typeof from_line == "number" && typeof till_line == "number" ) {
        return sStr.substr( from_line, till_line );
    }
    if ( typeof from_line == "number" ) {
        return sStr.substr( from_line );
    }
    return sStr;
}

function clearDDLBItems(field_name, clear_first_element ) {
  var ddlbObj= document.getElementById(field_name);
  try {
  var L= ddlbObj.length;
  } 
  catch(e) {  
  	//alert( "ClearDDLBItems field_name::"+field_name ) 
  }
  for (var I= L-1; I>=1; I-- ) {
    ddlbObj.remove(I);
  }
  if ( clear_first_element ) ddlbObj.remove(0);
}

function addDDLBItem( field_name, id, text) {
  var ddlbObj= document.getElementById(field_name);
  var OptObj = document.createElement("OPTION");
  OptObj.value= id;
  OptObj.text= text;
  ddlbObj.options.add(OptObj);
  return OptObj;
}

function setDDLBActiveItem( field_name, value) {
  var ddlbObj= document.getElementById(field_name);
  if ( !ddlbObj ) alert("Error::"+field_name )
  for(var I=0;I<ddlbObj.options.length;I++) {
    if ( ddlbObj.options[I].value == value ) {
      ddlbObj.options[I].selected = true;
      return;
    }
  }
}

function fillDdlbValue( field_name, id, text, AddEmpty ) {
  var ddlbObj= document.getElementById(field_name);
  for(var I=0;I<ddlbObj.options.length;I++) {
    if ( ddlbObj.options[I].value == id ) {
      ddlbObj.options[I].selected = true;
      return;
    }
  }
  addDDLBItem( field_name, id, text)
  setDDLBActiveItem( field_name, id)
}

function clearDDLBActiveItem( field_name) {
  var ddlbObj= document.getElementById(field_name);
  //  alert("ddlbObj.options.length::"+ddlbObj.options.length);
  for(var I=0;I<ddlbObj.options.length;I++) {
    ddlbObj.options[I].selected = false;
  }
}

