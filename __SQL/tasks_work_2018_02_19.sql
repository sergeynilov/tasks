-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 19, 2018 at 08:46 AM
-- Server version: 5.7.21-0ubuntu0.17.10.1
-- PHP Version: 7.1.11-0ubuntu0.17.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tasks_work`
--

-- --------------------------------------------------------

--
-- Table structure for table `tsk_categories`
--

CREATE TABLE `tsk_categories` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` smallint(5) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_categories`
--

INSERT INTO `tsk_categories` (`id`, `name`, `description`, `parent_id`, `created_at`) VALUES
(1, 'PHP Development', 'PHP Development description...', NULL, '2017-12-30 16:48:18'),
(2, 'Wordpress Development', 'Wordpress Development description...', 1, '2017-12-30 16:48:18'),
(3, 'Joomla Development', 'Joomla Development description...', 1, '2017-12-30 16:48:18'),
(4, 'Laravel Development', 'Laravel Development description...', 1, '2017-12-30 16:48:18'),
(5, 'Laravel/vue.js', 'Laravel/vue.js Development description...', 4, '2017-12-30 16:48:18'),
(6, 'Laravel/angular.js', 'Laravel/angular.js Development description...', 4, '2017-12-30 16:48:18'),
(7, 'Laravel/react.js', 'Laravel/react.js Development description...', 4, '2017-12-30 16:48:18'),
(8, 'HTML-programming', 'HTML-programming description...', NULL, '2018-01-15 05:40:42'),
(9, 'Bootstrap Development', 'Bootstrap Development description...', 8, '2018-01-15 05:40:42'),
(10, 'vue-strap library testing', 'vue-strap library description ( https://wffranco.github.io/vue-strap/ )...', 9, '2018-01-15 05:40:42'),
(11, 'Learning Bootstrap 4 Cookbook book', 'Learning Bootstrap 4 Cookbook book description ...', 9, '2018-01-15 05:40:42'),
(12, 'Learning Bootstrap 4 Essential Training ', 'Learning Bootstrap 4 Essential Training description ...', 9, '2018-01-15 05:40:42'),
(13, 'Responsive design with Skeleton', 'Responsive design with Skeleton description...', 8, '2018-01-15 05:40:42'),
(14, 'Learn Skeleton at http://getskeleton.com/ site', 'Learn Skeleton at http://getskeleton.com/ site description...', 13, '2018-01-15 05:40:42'),
(15, 'Graphical tasks', 'Graphical tasks description...', NULL, '2018-01-15 05:40:42'),
(16, 'Learn Corel Draw', 'Learn Corel Draw description...', 15, '2018-01-15 05:40:42'),
(17, 'Learn \"Getting Started with Corel Painter Video\"', 'Learn Corel Draw by \"Getting Started with Corel Painter 2016 Training Video\" description...', 16, '2018-01-15 05:40:42'),
(18, 'Clipart for Corel Draw (12750)', 'Clipart for Corel Draw (12750) description...', 16, '2018-01-15 05:40:42'),
(21, 'zzzPHP Development', 'xx', NULL, '2018-01-28 07:45:12'),
(22, 'from 21', 'from 21zzzzzz', 21, '2018-01-28 07:45:37'),
(23, 'But I must e', 'But I must exp', 18, '2018-01-28 12:20:34'),
(24, 'from 21zzz', 'dxdxd', 9, '2018-02-06 15:57:32'),
(25, '21=>1', '21=>1', 21, '2018-02-18 07:58:20'),
(26, '25=>25', '25=>25', 25, '2018-02-18 07:58:46');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_document_categories`
--

CREATE TABLE `tsk_document_categories` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('D','C','P','M','T') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT ' D=>Task Document, C=>Chat Document, P=>Profile Document, M=>Profile Main Image, T=>Profile Thumbnail Image',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_document_categories`
--

INSERT INTO `tsk_document_categories` (`id`, `name`, `alias`, `type`, `description`, `created_at`) VALUES
(2, 'Profile thumbnail image', 'profile_thumbnail_image', 'T', 'Profile thumbnail image description...', '2017-12-30 16:55:33'),
(3, 'User\'s documents (diploma, certificate, license)', 'users_documents', 'P', 'User\'s documents (diploma, certificate, license) description...', '2017-12-30 16:55:33'),
(4, 'Task\'s specification documents', 'tasks_specification_documents', 'D', 'Task\'s specification documents description...', '2017-12-30 16:55:33'),
(5, 'Task\'s attached images', 'tasks_attached_images', 'D', 'Task\'s attached images description...', '2017-12-30 16:55:33'),
(6, 'Task\'s attached schemes', 'tasks_attached_schemes', 'D', 'Task\'s attached schemes description...', '2017-12-30 16:55:33'),
(7, 'Chat\'s attached files', 'chats_attached_files', 'C', 'Chat\'s attached files description...vvvvvvvv', '2017-12-30 16:55:33'),
(8, 'But I must e', NULL, 'D', 'ZZZZBut I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\nBut I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\nBut I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\nBut I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?', '2018-01-28 12:18:25'),
(10, 'sss', NULL, 'P', 'ssssss', '2018-02-18 08:37:12');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_events`
--

CREATE TABLE `tsk_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` enum('P','U') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'P=>Private, U=>Public',
  `at_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `duration` int(11) NOT NULL,
  `task_id` int(10) UNSIGNED DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_events`
--

INSERT INTO `tsk_events` (`id`, `name`, `access`, `at_time`, `duration`, `task_id`, `description`, `created_at`) VALUES
(1, 'Members of \"Mastering Laravel/vue.js\" task would discuss how to start the task', 'P', '2018-02-19 07:10:47', 80, 1, 'Members of \"Mastering Laravel/vue.js\" task would discuss how to start the task description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2018-02-27 05:55:48'),
(2, 'Members of \"Mastering Laravel/vue.js\" task would discuss the started project', 'P', '2018-02-19 07:11:04', 60, 1, 'Members of \"Mastering Laravel/vue.js\" task would discuss the started project description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2018-02-24 05:55:48'),
(3, 'Members of \"Develop Tasks management site using Laravel/vue.js\" task would discuss how to start the task', 'P', '2018-02-02 15:50:00', 30, 2, 'Members of \"Develop Tasks management site using Laravel/vue.js\" task would discuss how to start the task description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2018-01-30 05:55:48'),
(4, 'Members of \"Develop Tasks management site using Laravel/vue.js\" task would discuss the started project', 'P', '2018-02-28 07:11:11', 60, 2, 'Members of \"Develop Tasks management site using Laravel/vue.js\" task would discuss the started project description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2018-02-28 05:55:48'),
(5, 'Celebrate birthday of our boss', 'U', '2018-02-19 07:10:28', 240, NULL, 'Celebrate birthday of our boss description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2018-02-28 05:55:48'),
(6, 'vuejs6', 'P', '2018-02-28 07:10:55', 80, 1, 'eeeeeee vuejs6', '2018-02-26 14:51:49'),
(7, 'vuejs6666', 'P', '2018-02-22 08:48:00', 234, NULL, 'xzvxcv', '2018-02-04 08:49:23'),
(8, 'Very long event', 'U', '2018-02-20 08:29:06', 4500, 2, 'Very long event', '2018-02-19 08:27:45');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_events_users`
--

CREATE TABLE `tsk_events_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_events_users`
--

INSERT INTO `tsk_events_users` (`id`, `user_id`, `event_id`, `created_at`) VALUES
(1, 3, 1, '2018-01-30 05:55:48'),
(2, 10, 1, '2018-01-30 05:55:48'),
(3, 5, 1, '2018-01-30 05:55:48'),
(4, 3, 2, '2018-01-30 05:55:48'),
(5, 10, 2, '2018-01-30 05:55:48'),
(6, 5, 2, '2018-01-30 05:55:48'),
(7, 3, 3, '2018-01-30 05:55:48'),
(8, 10, 3, '2018-01-30 05:55:48'),
(9, 5, 3, '2018-01-30 05:55:48'),
(10, 3, 4, '2018-01-30 05:55:48'),
(11, 10, 4, '2018-01-30 05:55:48'),
(12, 5, 4, '2018-01-30 05:55:48'),
(13, 1, 5, '2018-01-30 05:55:48'),
(14, 2, 5, '2018-01-30 05:55:48'),
(15, 3, 5, '2018-01-30 05:55:48'),
(16, 4, 5, '2018-01-30 05:55:48'),
(18, 6, 5, '2018-01-30 05:55:48'),
(19, 7, 5, '2018-01-30 05:55:48'),
(20, 8, 5, '2018-01-30 05:55:48'),
(21, 9, 5, '2018-01-30 05:55:48'),
(22, 10, 5, '2018-01-30 05:55:48'),
(23, 11, 5, '2018-01-30 05:55:48'),
(24, 12, 5, '2018-01-30 05:55:48'),
(26, 5, 5, '2018-01-30 07:18:26'),
(27, 5, 8, '2018-02-19 08:28:14'),
(28, 13, 8, '2018-02-19 08:28:17'),
(29, 1, 8, '2018-02-19 08:28:26');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_groups`
--

CREATE TABLE `tsk_groups` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_groups`
--

INSERT INTO `tsk_groups` (`id`, `name`, `description`, `created_at`) VALUES
(1, 'Admin', 'Administrator', '2017-12-30 16:47:41'),
(2, 'Manager', 'Manager description...', '2017-12-30 16:47:41'),
(3, 'Developer', 'Developer description...', '2017-12-30 16:47:41'),
(4, 'Employee', 'Employee description...', '2017-12-30 16:47:41');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_migrations`
--

CREATE TABLE `tsk_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_migrations`
--

INSERT INTO `tsk_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_22_155945_modify_users_table_status', 2),
(4, '2017_11_23_055108_create_users_groups_table', 2),
(5, '2017_11_23_080830_fill_users_groups_table_with_sapmle_data', 3),
(6, '2017_11_22_071254_create_tasks_table', 4),
(7, '2017_11_23_082235_create_categories_table', 5),
(8, '2017_11_23_084116_modify_tasks_table_status', 6),
(9, '2017_11_23_121432_modify_users_table_timestamp-', 7),
(10, '2017_11_23_145312_create_settings_table', 8),
(11, '2017_11_24_090056_modify_tasks_table_many_fields-', 9),
(12, '2017_11_24_124214_create_user_task_types_table', 10),
(13, '2017_11_24_131358_create_task_assigned_to_users_table', 11),
(14, '2017_11_27_084731_create_user_chats_table', 12),
(15, '2017_11_27_090448_create_user_chat_participants_table', 13),
(16, '2017_12_01_051343_modify_user_chats_table_task_id', 14),
(17, '2017_12_02_090642_modify_user_chat_participants_table', 15),
(18, '2017_12_02_114658_modify_user_chat_participants_table_status', 16),
(19, '2017_12_02_120953_modify_user_chats_table_manager_id', 17),
(20, '2017_12_04_112349_create_user_chat_messages_table', 18),
(21, '2017_12_05_051408_modify_user_chat_messages_table_updated_at', 19),
(22, '2017_12_08_065215_create_user_profile_table', 20),
(23, '2017_12_15_083529_create_document_categories_table', 21),
(24, '2017_12_17_081933_create_user_profile_documents_table', 22),
(25, '2017_12_26_061829_create_user_chats_last_visited_table', 23),
(26, '2017_12_26_083141_modify_user_chat_participants_table_unique_index', 24),
(27, '2017_12_27_133926_modify_tasks_description', 25),
(29, '2017_12_29_125721_modify_tsk_document_categories_alias', 26),
(30, '2017_12_29_154350_create_user_skills_table', 27),
(34, '2018_01_01_114651_create_user_chat_message_documents_table', 28),
(35, '2018_01_10_121453_modify_user_chat_message_documents_drop_user_chat_message_id', 29),
(36, '2018_01_10_125823_create_user_chat_message_documents_table', 30),
(38, '2018_01_12_084706_modify_user_chat_messages_add_updated_at_by_user_id', 31),
(54, '2018_01_12_173218_modify_user_chat_messages_add_message_type', 32),
(58, '2018_01_13_135147_add_faker_data_user_chat_messages', 33),
(61, '2018_01_17_150116_modify_tasks_leader_id', 34),
(62, '2018_01_17_151540_modify_task_assigned_to_users_is_leader_status', 35),
(66, '2018_01_19_164802_create_task_operations_table', 36),
(67, '2018_01_26_093523_create_user_user_chat_new_messages_table', 36),
(70, '2018_01_30_064012_create_events_table', 37),
(72, '2018_02_03_141612_remove_tasks_leader_id', 38),
(74, '2018_02_03_142511_modify_task_operations_table_user_operation_id', 39),
(77, '2018_02_04_151511_create_user_weather_cities_table', 40),
(80, '2018_02_12_143654_create_user_todos_table', 41),
(83, '2018_02_17_092459_create_task_status_changes_table', 42);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_password_resets`
--

CREATE TABLE `tsk_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tsk_settings`
--

CREATE TABLE `tsk_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_settings`
--

INSERT INTO `tsk_settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'items_per_page', '20', '2017-12-30 16:49:42', NULL),
(2, 'site_name', 'Tasks/Chats/Events', '2017-12-30 16:49:42', NULL),
(3, 'chat_attached_files_document_category_id', '7', '2018-01-10 13:17:14', NULL),
(4, 'priority_colors', '1:\'#ffffaa\', 2:\'#c5c5c5\', 3:\'#a5a5a5\', 4:\'#e097f8\', 5:\'#f87063\', 6:\'#ff280c\'', '2018-01-16 12:35:32', NULL),
(5, 'show_alert_popup', 'Y', '2018-01-18 08:17:21', NULL),
(6, 'is_past_colors', 'is_past:\'#bfbda2\', is_today:\'#ff280c\', is_tomorrow:\'#fc8865\', is_future:\'#ffffff\'', '2018-01-30 12:27:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_tasks`
--

CREATE TABLE `tsk_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `category_id` smallint(5) UNSIGNED NOT NULL,
  `priority` enum('0','1','3','4','5','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '  0-No, 1-Low, 2-Normal, 3-High, 4-Urgent, 5-Immediate  ',
  `status` enum('D','A','C','P','K','O') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'D => Draft, A=>Assigning, C => Cancelled, P => Processing, K=> Checking, O=> Completed',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `needs_reports` enum('0','1','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=>No, 1=>Hourly, 2=>Twice a day,3=>Daily,4=>Twice a week,5=>Weekly',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_tasks`
--

INSERT INTO `tsk_tasks` (`id`, `name`, `description`, `creator_id`, `category_id`, `priority`, `status`, `date_start`, `date_end`, `needs_reports`, `created_at`, `updated_at`) VALUES
(1, 'Mastering Laravel/vue.js', '', 1, 4, '3', 'P', '2017-12-30', '2018-03-02', '4', '2017-12-30 16:50:26', NULL),
(2, 'Develop Tasks management site using Laravel/vue.js', '', 2, 5, '4', 'A', '2017-12-30', '2018-04-30', '5', '2017-12-30 16:50:26', NULL),
(50, 'Prepare design of a new task', 'Prepare design of a new task in 2 men', 5, 15, '3', 'K', '2018-03-21', '2019-06-30', '4', '2018-01-28 08:21:25', NULL),
(55, 'AAgggg', 'Descr...gggg', 5, 22, '1', 'D', '2018-03-21', '2019-05-25', '3', '2018-02-07 16:21:37', NULL),
(56, 'AA55', 'Descr...11', 5, 15, '1', 'D', '2018-03-21', '2019-05-25', '3', '2018-02-08 12:25:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_task_assigned_to_users`
--

CREATE TABLE `tsk_task_assigned_to_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` enum('A','C','P','K','O') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed',
  `is_leader` tinyint(1) NOT NULL DEFAULT '0',
  `user_task_type_id` smallint(5) UNSIGNED NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_task_assigned_to_users`
--

INSERT INTO `tsk_task_assigned_to_users` (`id`, `task_id`, `user_id`, `status`, `is_leader`, `user_task_type_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 'A', 0, 1, 'You need to learn/control Mastering Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:51:12', NULL),
(2, 1, 10, 'A', 0, 2, 'You need to learn Mastering Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:51:12', NULL),
(3, 2, 3, 'A', 0, 1, 'You need to learn/control Develop Tasks management site using Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:51:12', NULL),
(4, 2, 5, 'C', 0, 3, 'You need to learn Develop Tasks management site using Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:51:12', NULL),
(46, 1, 5, 'A', 1, 5, '2 painter', '2018-01-19 12:11:35', NULL),
(50, 50, 6, 'A', 0, 5, 'Adrain Batz : 1 painter', '2018-01-28 08:21:25', NULL),
(51, 50, 5, 'A', 1, 1, 'Admin  as leader 222', '2018-01-28 08:21:25', NULL),
(57, 55, 5, 'A', 1, 3, 'nnnnnnnnn', '2018-02-07 16:21:37', NULL),
(58, 56, 5, 'A', 1, 2, 'cxcxc', '2018-02-08 12:25:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_task_operations`
--

CREATE TABLE `tsk_task_operations` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `task_assigned_to_user_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `prior_status` enum('A','C','P','K','O') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed',
  `status` enum('A','C','P','K','O') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed',
  `user_operation_id` int(10) UNSIGNED NOT NULL DEFAULT '5',
  `info` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_task_operations`
--

INSERT INTO `tsk_task_operations` (`id`, `task_id`, `task_assigned_to_user_id`, `user_id`, `prior_status`, `status`, `user_operation_id`, `info`, `created_at`) VALUES
(15, 2, 4, 5, 'A', 'C', 5, 'Cancel 4', '2018-02-03 09:00:45');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_task_status_changes`
--

CREATE TABLE `tsk_task_status_changes` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `prior_status` enum('D','A','C','P','K','O') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'D=>Draft, A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed',
  `status` enum('D','A','C','P','K','O') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'D=>Draft, A=>Assigning(Waiting for acception), C => Cancelled, P => Processing(accepted), K=> Checking, O=> Completed',
  `info` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tsk_users`
--

CREATE TABLE `tsk_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('N','A','I') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' N => New(Waiting activation), A=>Active, I=>Inactive',
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_start` date DEFAULT NULL,
  `contract_end` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_users`
--

INSERT INTO `tsk_users` (`id`, `name`, `email`, `password`, `remember_token`, `status`, `first_name`, `last_name`, `phone`, `website`, `contract_start`, `contract_end`, `created_at`, `updated_at`) VALUES
(1, 'JonGlads', 'admin@site.com', '$2y$10$cH4J5WQBXdlzSV0hQB/MPOva8x.UFI3BmcmlYmCWjhKrPW13K554a', NULL, 'N', 'Jon', 'Glads', 'phone # 1', 'firstname_website_1', '2017-02-23', '2018-09-23', '2017-12-30 16:49:31', '2018-02-28 06:49:32'),
(2, 'RodBodrick', 'rod_bodrick@site.com', '$2y$10$Zz8705fDquwM7O5xvW5dregHygiVTjSUDYBhmdx4oHoHyk/loEXRy', NULL, 'N', 'Rod', 'Bodrick', 'phone # 2', 'firstname_website_2', '2017-02-23', '2018-09-23', '2017-12-30 16:49:31', '2018-02-28 06:49:32'),
(3, 'Tony Black', 'tony_black@site.com', '$2y$10$a04mlfvvPqt9qA1LsGjKtuSJkJjJTw6Urn2Ik8zN.fjwqpyS.WD62', NULL, 'I', 'Firstname3', 'Lastname3', 'phone # 3', 'firstname_website_3', '2017-02-23', '2018-09-23', '2017-12-30 16:49:31', '2018-02-28 06:49:32'),
(4, 'Adam Lang', 'adam_lang@site.com', '$2y$10$JuQOQyHaC9zGVuKUg3OcuOTQR3tvZI31Zck31ZxQrXpKn5Zhl7PBa', NULL, 'I', 'Firstname4', 'Lastname4', 'phone # 4', 'firstname_website_4', '2017-02-23', '2018-09-23', '2017-12-30 16:49:31', '2018-02-28 06:49:32'),
(5, 'admin', 'admin@mail.com', '$2y$10$qzgpltH4YRR2EwX1cHcL4uy47g9WaYc9XRJrBiCm1lyOlN1UzxMvW', 'ewwK8NXI2RvMc4RHJcroxGylphAqchDZnspY29JEFx2TkdgQVONCFd0icgKz', 'A', 'Mode', 'Pdode', '123-567-87', 'fdasdf.com', '2017-01-21', '2018-11-22', '2017-12-31 04:48:06', NULL),
(6, 'EmployeeUser', 'EmployeeUser@mail.com', '$2y$10$0jwh.8jtp/WaK3ZVKNITterBPXliN89bL6Qr44QixBbf6/tTLb1Nq', 'KsbyUbfgWBeyIBU5zysJ8ZkyFyxQsLnzL45gQC43hF8Cl6Xyoqq1IXguQ1Jv', 'A', 'Gred', 'Tred', '00', '00', NULL, NULL, '2017-12-31 04:52:23', NULL),
(7, 'admindd', 'admindd@mail.com', '$2y$10$yrzLT8EvxAmhsH0gEfVY8eUF3AiyH5qe6F5eSWOeeMfz4WlsvDp5G', 'gmHVZVc1SzRCKDJErdW1xJFgRP8ZKLmk7F4e90F9fiD8j7JrNVIok1R74JFV', 'I', '', '', 'klnl', 'kjk', NULL, NULL, '2018-01-02 13:12:28', NULL),
(8, 'Wilma Morissette', 'betsy.oconner@example.com', '$2y$10$4824TaEKcE35U5qiiU/7ge.vzA/FIL9hhXqCQBiWTDkDsp07VOUnm', 'zXe3tKULlv', 'A', 'Wilma', 'Morissette', '1-800-693-8132', 'wilma-morissette.com', '2016-04-03', '2019-12-14', '2018-01-14 12:33:36', NULL),
(9, 'Dave Heller', 'julian.kovacek@example.org', '$2y$10$4824TaEKcE35U5qiiU/7ge.vzA/FIL9hhXqCQBiWTDkDsp07VOUnm', '936Eppvwjm', 'I', 'Dave', 'Heller', '1-866-848-5524', 'dave-heller.com', '2017-05-11', '2018-05-11', '2018-01-14 12:33:36', NULL),
(10, 'Filomena White', 'crist.dawn@example.com', '$2y$10$tIPw4FZrRqi4HMF3iKcPNuVs8H4pw/oNgG6IVTVTDb12yKZxcwFUi', 'ff8sU4XwsI', 'I', 'Filomena', 'White', '877.827.4641', 'filomena-white.com', '2016-09-16', '2019-07-23', '2018-01-14 12:35:13', NULL),
(11, 'Shakira Bosco', 'maureen32@example.net', '$2y$10$tIPw4FZrRqi4HMF3iKcPNuVs8H4pw/oNgG6IVTVTDb12yKZxcwFUi', 'uq2MYVhW2T', 'N', 'Shakira', 'Bosco', '1-844-883-4084', 'shakira-bosco.com', '2017-01-26', '2019-04-18', '2018-01-14 12:35:13', NULL),
(12, 'Joanne Halvorson', 'jaclyn.corwin@example.com', '$2y$10$qwhO3VmDg5aXM3XKlN4DheVxvadRiMf0nMA/AZOxVhy17B9GtkCFu', 'oy4dBmFDq0', 'I', 'Joanne', 'Halvorson', '800.694.6187', 'joanne-halvorson.com', '2016-10-08', '2018-05-29', '2018-01-14 15:47:34', NULL),
(13, 'Adrain Batz', 'westley.graham@example.net', '$2y$10$qwhO3VmDg5aXM3XKlN4DheVxvadRiMf0nMA/AZOxVhy17B9GtkCFu', 'iN47oWwz2L', 'A', 'Adrain', 'Batz', '855.593.6647', 'adrain-batz.com', '2017-01-16', '2018-08-06', '2018-01-14 15:47:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_users_groups`
--

CREATE TABLE `tsk_users_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` smallint(5) UNSIGNED NOT NULL,
  `status` enum('N','A','I') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'N => New(Waiting for confirmation), A=>Active, I => Inactive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_users_groups`
--

INSERT INTO `tsk_users_groups` (`id`, `user_id`, `group_id`, `status`, `created_at`) VALUES
(1, 5, 1, 'A', '2018-01-14 15:47:35'),
(2, 1, 2, 'N', '2018-01-14 15:47:35'),
(3, 2, 2, 'A', '2018-01-14 15:47:35'),
(4, 3, 3, 'A', '2018-01-14 15:47:35'),
(5, 6, 3, 'A', '2018-01-14 15:47:35'),
(6, 4, 4, 'A', '2018-01-14 15:47:35'),
(7, 7, 4, 'I', '2018-01-14 15:47:35'),
(8, 5, 2, 'I', '2018-01-15 13:10:57'),
(9, 5, 3, 'I', '2018-01-15 13:11:02'),
(10, 5, 4, 'N', '2018-01-15 13:11:23'),
(11, 2, 3, 'I', '2018-01-25 08:02:47'),
(12, 6, 1, 'I', '2018-02-08 12:31:17');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chats`
--

CREATE TABLE `tsk_user_chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('A','C') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT ' A=>Active, C=>Closed',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `task_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `manager_id` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chats`
--

INSERT INTO `tsk_user_chats` (`id`, `name`, `description`, `creator_id`, `status`, `created_at`, `task_id`, `updated_at`, `manager_id`) VALUES
(1, 'Greeting all employees!', 'Greeting all employees! and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 1, 'A', '2017-12-30 16:51:22', NULL, NULL, 5),
(2, 'People, get the first task description', 'First task description and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 2, 'A', '2017-12-30 16:51:22', NULL, '2018-02-18 07:35:50', 5),
(3, 'Let\'s discuss task Mastering Laravel/vue.js in this chat...', 'Let\'s discuss task Mastering Laravel/vue.js in this chat... and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 1, 'A', '2017-12-30 16:51:51', 1, NULL, 1),
(4, 'People, this is chat for Develop Tasks management site using Laravel/vue.js task  discussion', 'People, this is chat for Develop Tasks management site using Laravel/vue.js task  discussion and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 2, 'A', '2017-12-30 16:51:51', 2, NULL, 1),
(5, 'a', 'a', 5, 'A', '2018-02-08 12:23:06', NULL, '2018-02-18 07:35:50', 5),
(6, 'azz6', '', 5, 'A', '2018-02-08 13:26:18', 2, '2018-02-08 13:39:10', 5),
(7, 'ad', 'adddd', 5, 'A', '2018-02-18 11:34:24', NULL, NULL, 5),
(8, 'ass', 'asss', 5, 'C', '2018-02-18 11:37:39', NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chats_last_visited`
--

CREATE TABLE `tsk_user_chats_last_visited` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_chat_id` int(10) UNSIGNED NOT NULL,
  `visited_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chats_last_visited`
--

INSERT INTO `tsk_user_chats_last_visited` (`id`, `user_id`, `user_chat_id`, `visited_at`) VALUES
(1, 5, 1, '2018-02-04 08:40:00'),
(2, 6, 1, '2018-02-04 08:40:18'),
(3, 1, 1, '2018-01-10 09:18:35'),
(4, 4, 1, '2018-01-10 10:03:32'),
(5, 6, 3, '2018-01-22 15:51:56'),
(6, 6, 2, '2018-01-25 05:51:25'),
(7, 2, 1, '2018-01-25 10:58:45'),
(8, 5, 2, '2018-02-14 08:01:28');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chat_messages`
--

CREATE TABLE `tsk_user_chat_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_chat_id` int(10) UNSIGNED NOT NULL,
  `is_top` tinyint(1) NOT NULL DEFAULT '0',
  `text` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_type` enum('T','U') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'T' COMMENT ' N=>Text added , U=>Files uploaded',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_at_by_user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chat_messages`
--

INSERT INTO `tsk_user_chat_messages` (`id`, `user_id`, `user_chat_id`, `is_top`, `text`, `message_type`, `created_at`, `updated_at`, `updated_at_by_user_id`) VALUES
(1, 1, 1, 1, ' That is first/top message on \"Greeting all employees!\" chan and Lorem ipsum ', 'T', '2017-12-30 16:55:16', NULL, NULL),
(3, 1, 2, 1, ' That is first/top message on \"People, get the first task description\" chan and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 'T', '2017-12-30 16:55:16', NULL, NULL),
(4, 4, 2, 0, ' That is next message on \"People, get the first task description\" chan and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 'T', '2017-12-30 16:55:16', NULL, NULL),
(69, 5, 1, 1, 'ZZZss sssss', 'T', '2018-01-17 08:54:01', '2018-01-24 15:25:00', 5),
(70, 5, 1, 1, 'line 1\nline 2\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'T', '2018-01-17 08:57:00', '2018-01-25 05:00:43', 5),
(90, 2, 1, 0, '3 files were uploaded : 1.png, 2.jpeg, 4.jpeg by Mode Pdode', 'U', '2018-01-24 07:30:41', NULL, NULL),
(91, 5, 1, 0, 'Yes,', 'T', '2018-01-24 14:49:08', '2018-01-25 05:02:22', 5),
(92, 2, 1, 0, 'Sala Bala 1111111\n222222\n333', 'T', '2018-01-25 05:01:14', NULL, NULL),
(93, 6, 1, 0, 'nana line 1\nnana line 2\nnana line 3\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'T', '2018-01-25 05:02:03', NULL, NULL),
(94, 5, 1, 0, 'kilo1\naaaa', 'T', '2018-01-25 05:02:53', NULL, NULL),
(95, 6, 1, 0, '3 files were uploaded : short_original.csv, lender_table_1.xls, borrower_doc_2.doc by gred Tred', 'U', '2018-01-25 05:50:51', NULL, NULL),
(96, 2, 1, 0, 'fee\n111111', 'T', '2018-01-25 05:51:25', NULL, NULL),
(102, 6, 1, 0, 'example line 1\nexample line 2\nexample line 3', 'T', '2018-01-25 07:10:54', '2018-01-25 07:46:31', 5),
(107, 5, 1, 0, 'AAAAAAAA', 'T', '2018-01-25 07:49:16', NULL, NULL),
(108, 2, 1, 0, 'DDDDD', 'T', '2018-01-25 10:58:45', NULL, NULL),
(109, 5, 1, 0, '4 files were uploaded : 2.jpeg, 1.png, 4.jpeg, 5.jpeg by Mode Pdode', 'U', '2018-01-25 12:29:26', NULL, NULL),
(110, 5, 1, 0, '5 files were uploaded : lender_table_1.xls, borrower_doc_2.doc, 1.png, 2.jpeg, 4.jpeg by Mode Pdode', 'U', '2018-01-25 12:45:48', NULL, NULL),
(111, 5, 1, 0, '6 files were uploaded : short_original.csv, borrower_doc_2.doc, lender_table_1.xls, 1.png, 4.jpeg, 5.jpeg by Mode Pdode', 'U', '2018-01-25 12:48:41', NULL, NULL),
(114, 5, 6, 0, 'zzzzzzzzz', 'T', '2018-01-26 08:25:31', NULL, NULL),
(115, 5, 1, 0, 'aaa', 'T', '2018-01-31 07:48:55', NULL, NULL),
(116, 6, 6, 0, 'ZZZZZZZZ', 'T', '2018-01-31 07:49:35', NULL, NULL),
(117, 5, 1, 0, 'DDDDDDDDDDDDD', 'T', '2018-01-31 07:50:36', NULL, NULL),
(118, 5, 1, 0, 'AAAAAAA', 'T', '2018-02-04 08:40:00', NULL, NULL),
(119, 6, 1, 0, 'sssssssssssss', 'T', '2018-02-04 08:40:18', NULL, NULL),
(123, 5, 1, 0, '1 file was uploaded : 2.jpeg by Mode Pdode', 'U', '2018-02-04 08:41:22', NULL, NULL),
(140, 5, 2, 0, '3 files were uploaded : 2.jpeg, 1.png, 4.jpeg by Mode Pdode', 'U', '2018-02-14 12:10:13', NULL, NULL),
(141, 5, 2, 0, '2 files were uploaded : 6.jpeg, borrower_doc_1.doc by Mode Pdode', 'U', '2018-02-14 12:11:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chat_message_documents`
--

CREATE TABLE `tsk_user_chat_message_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_chat_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_category_id` smallint(5) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chat_message_documents`
--

INSERT INTO `tsk_user_chat_message_documents` (`id`, `user_chat_id`, `user_id`, `filename`, `extension`, `info`, `document_category_id`, `created_at`) VALUES
(61, 1, 5, 'short_original.csv', 'csv', 'Info text...', 7, '2018-01-25 12:48:41'),
(62, 1, 5, 'borrower_doc_2.doc', 'doc', 'Info text...', 7, '2018-01-25 12:48:41'),
(63, 1, 5, 'lender_table_1.xls', 'xls', 'Info text...', 7, '2018-01-25 12:48:41'),
(64, 1, 5, '1.png', 'png', 'Info text...', 7, '2018-01-25 12:48:41'),
(65, 1, 5, '4.jpeg', 'jpeg', 'Info text...', 7, '2018-01-25 12:48:41'),
(66, 1, 5, '5.jpeg', 'jpeg', 'Info text...', 7, '2018-01-25 12:48:41'),
(70, 1, 5, '2.jpeg', 'jpeg', 'Info text...', 7, '2018-02-04 08:41:22'),
(87, 2, 5, '2.jpeg', 'jpeg', 'Info text...222222eeeeeeeeeeee', 7, '2018-02-14 12:10:13'),
(88, 2, 5, '1.png', 'png', 'Info text...111111zzzzzz', 7, '2018-02-14 12:10:13');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chat_participants`
--

CREATE TABLE `tsk_user_chat_participants` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_chat_id` int(10) UNSIGNED NOT NULL,
  `status` enum('M','W','R') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'R' COMMENT ' ''M''=>''Manage this chat'', ''W'' => ''Can write messages'', ''R'' => ''Can only read'' '
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chat_participants`
--

INSERT INTO `tsk_user_chat_participants` (`id`, `user_id`, `created_at`, `user_chat_id`, `status`) VALUES
(1, 5, '2018-01-01 06:20:49', 1, 'M'),
(2, 6, '2018-01-01 12:18:05', 1, 'W'),
(3, 2, '2018-01-05 05:31:47', 1, 'W'),
(4, 5, '2018-01-05 05:31:52', 2, 'M'),
(5, 6, '2018-01-22 15:10:55', 3, 'W'),
(6, 6, '2018-01-24 05:49:27', 2, 'W'),
(7, 5, '2018-02-08 12:23:06', 5, 'M'),
(8, 13, '2018-02-08 12:23:06', 5, 'W'),
(9, 6, '2018-02-08 12:23:06', 5, 'R'),
(10, 5, '2018-02-08 13:26:18', 6, 'M'),
(11, 13, '2018-02-08 13:26:18', 6, 'W'),
(12, 6, '2018-02-08 13:26:18', 6, 'R'),
(13, 5, '2018-02-18 11:34:24', 7, 'M'),
(14, 13, '2018-02-18 11:34:24', 7, 'W'),
(15, 5, '2018-02-18 11:37:39', 8, 'M'),
(16, 13, '2018-02-18 11:37:39', 8, 'W'),
(17, 6, '2018-02-18 11:37:39', 8, 'R');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_profiles`
--

CREATE TABLE `tsk_user_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_profiles`
--

INSERT INTO `tsk_user_profiles` (`id`, `user_id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 5, 'lang', 'en', '2018-01-02 12:27:22', NULL),
(2, 5, 'submit_message_by_enter', 'N', '2018-01-02 12:27:22', NULL),
(3, 5, 'color', '#efd303', '2018-01-02 12:27:22', NULL),
(4, 5, 'background_color', '#0a309d', '2018-01-02 12:27:22', NULL),
(5, 5, 'subscription_to_newsletters', 'Y', '2018-01-02 12:27:22', NULL),
(6, 5, 'show_online_status', 'Y', '2018-01-02 12:27:22', NULL),
(7, 7, 'lang', 'en', '2018-01-02 14:36:01', NULL),
(8, 7, 'submit_message_by_enter', 'N', '2018-01-02 14:36:01', NULL),
(9, 7, 'color', '#dd3a3a', '2018-01-02 14:36:01', NULL),
(10, 7, 'background_color', '#bbff00', '2018-01-02 14:36:01', NULL),
(11, 7, 'subscription_to_newsletters', 'N', '2018-01-02 14:36:01', NULL),
(12, 7, 'show_online_status', 'N', '2018-01-02 14:36:01', NULL),
(13, 6, 'lang', 'en', '2018-01-06 14:48:03', NULL),
(14, 6, 'submit_message_by_enter', 'N', '2018-01-06 14:48:03', NULL),
(15, 6, 'color', '#345c06', '2018-01-06 14:48:03', NULL),
(16, 6, 'background_color', '#fbdd71', '2018-01-06 14:48:03', NULL),
(17, 6, 'subscription_to_newsletters', 'Y', '2018-01-06 14:48:03', NULL),
(18, 6, 'show_online_status', 'Y', '2018-01-06 14:48:03', NULL),
(19, 2, 'lang', 'en', '2018-01-25 08:03:10', NULL),
(20, 2, 'submit_message_by_enter', 'N', '2018-01-25 08:03:10', NULL),
(21, 2, 'color', '#f4d9d9', '2018-01-25 08:03:10', NULL),
(22, 2, 'background_color', '#000306', '2018-01-25 08:03:10', NULL),
(23, 2, 'subscription_to_newsletters', 'Y', '2018-01-25 08:03:10', NULL),
(24, 2, 'show_online_status', 'Y', '2018-01-25 08:03:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_profile_documents`
--

CREATE TABLE `tsk_user_profile_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_category_id` smallint(5) UNSIGNED NOT NULL,
  `public_access` enum('P','E') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'E' COMMENT '  P-Public, E-Personal',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_profile_documents`
--

INSERT INTO `tsk_user_profile_documents` (`id`, `user_id`, `filename`, `extension`, `document_category_id`, `public_access`, `info`, `created_at`) VALUES
(37, 6, '2.jpeg', 'jpeg', 2, 'P', NULL, '2018-01-25 10:54:37'),
(39, 2, '4.jpeg', 'jpeg', 2, 'P', 'Info text...', '2018-01-25 10:59:51'),
(50, 5, '1.png', 'png', 2, 'P', 'Info text...', '2018-01-25 12:58:10'),
(51, 5, 'borrower_doc_2.doc', 'doc', 3, 'E', 'Info text...', '2018-01-25 12:59:47'),
(52, 5, 'lender_table_2.xls', 'xls', 3, 'E', 'Info text...', '2018-01-25 12:59:47'),
(54, 5, '4.jpeg', 'jpeg', 3, 'E', 'Info text...', '2018-02-04 08:33:45'),
(56, 5, '6.jpeg', 'jpeg', 3, 'E', 'Info text...', '2018-02-04 08:33:45');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_skills`
--

CREATE TABLE `tsk_user_skills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `skill` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_skills`
--

INSERT INTO `tsk_user_skills` (`id`, `user_id`, `skill`, `rating`, `created_at`) VALUES
(1, 1, 'PHP', 8, '2017-12-31 05:18:32'),
(2, 1, 'Laravel', 6, '2017-12-31 05:18:32'),
(3, 1, 'Vue.js', 0, '2017-12-31 05:18:32'),
(4, 1, 'HTML', 7, '2017-12-31 05:18:32'),
(5, 1, 'Team leader', 8, '2017-12-31 05:18:32'),
(6, 2, 'PHP', 6, '2017-12-31 05:18:32'),
(7, 2, 'Laravel', 5, '2017-12-31 05:18:32'),
(8, 2, 'Vue.js', 2, '2017-12-31 05:18:32'),
(9, 2, 'HTML', 9, '2017-12-31 05:18:32'),
(10, 3, 'PHP', 4, '2017-12-31 05:18:32'),
(11, 3, 'Laravel', 0, '2017-12-31 05:18:32'),
(12, 3, 'Vue.js', 7, '2017-12-31 05:18:32'),
(13, 3, 'Bootstrap', 9, '2017-12-31 05:18:32'),
(14, 3, 'HTML', 10, '2017-12-31 05:18:32'),
(15, 3, 'Remote administrator', 8, '2017-12-31 05:18:32'),
(16, 4, 'PHP', 6, '2017-12-31 05:18:32'),
(17, 4, 'Laravel', 5, '2017-12-31 05:18:32'),
(18, 4, 'Vue.js', 2, '2017-12-31 05:18:32'),
(19, 4, 'Bootstrap', 8, '2017-12-31 05:18:32'),
(20, 4, 'HTML', 9, '2017-12-31 05:18:32'),
(26, 6, 'PHP', 6, '2017-12-31 05:18:32'),
(27, 6, 'Laravel', 2, '2017-12-31 05:18:32'),
(28, 6, 'Vue.js', 2, '2017-12-31 05:18:32'),
(29, 6, 'Bootstrap', 5, '2017-12-31 05:18:32'),
(30, 6, 'HTML', 4, '2017-12-31 05:18:32'),
(31, 6, 'Tester', 7, '2017-12-31 05:18:32'),
(71, 5, 'Bootstrap', 5, '2018-01-20 16:09:04'),
(72, 5, 'HTML', 4, '2018-01-20 16:09:04'),
(73, 5, 'Laravel', 3, '2018-01-20 16:09:04'),
(74, 5, 'PHP', 9, '2018-01-20 16:09:04'),
(75, 5, 'Remote administrator', 2, '2018-01-20 16:09:04'),
(76, 5, 'Remote administrator and white rabits calculating', 1, '2018-01-20 16:09:04'),
(77, 5, 'Tester', 2, '2018-01-20 16:09:04');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_task_types`
--

CREATE TABLE `tsk_user_task_types` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_task_types`
--

INSERT INTO `tsk_user_task_types` (`id`, `name`, `description`, `created_at`) VALUES
(1, 'Team Leader', 'Team Leader ... Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:50:59'),
(2, 'PHP developer', 'PHP developer Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:50:59'),
(3, 'HTML developer', '1111111111HTML developer Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-12-30 16:50:59'),
(4, 'Javascript/Vue developer', 'Javascript/Vue developer Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-12-30 16:50:59'),
(5, 'Painter', 'Painter Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:50:59'),
(6, 'Tester', 'Tester Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:50:59'),
(7, 'with wwwwTas\' 00000 wwwwTas\' 2222', 'with wwwwTas\' 00000 wwwwTas\' 22222', '2018-01-20 11:55:36'),
(8, 'rrrr', 'eeeee', '2018-02-18 08:44:14');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_todos`
--

CREATE TABLE `tsk_user_todos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` enum('0','1','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '  0-No, 1-Low, 2-Normal, 3-High, 4-Urgent, 5-Immediate  ',
  `task_id` int(10) UNSIGNED DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_todos`
--

INSERT INTO `tsk_user_todos` (`id`, `user_id`, `text`, `priority`, `task_id`, `completed`, `created_at`) VALUES
(1, 5, 'To do line Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut 1...', '4', 1, 0, '2018-02-16 08:17:49'),
(2, 5, 'To do line 2...', '3', NULL, 1, '2018-02-16 08:17:49'),
(3, 5, 'To do line 3333...', '1', 2, 0, '2018-02-16 08:17:49'),
(4, 5, 'dd', '0', NULL, 0, '2018-02-16 16:30:49'),
(5, 5, 'ddd', '0', NULL, 0, '2018-02-16 16:31:00'),
(6, 5, 'ddddddd', '0', NULL, 0, '2018-02-16 16:31:11');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_user_chat_new_messages`
--

CREATE TABLE `tsk_user_user_chat_new_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_chat_message_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_user_chat_new_messages`
--

INSERT INTO `tsk_user_user_chat_new_messages` (`id`, `user_id`, `user_chat_message_id`, `created_at`) VALUES
(3, 2, 114, '2018-01-26 08:25:31'),
(6, 2, 115, '2018-01-31 07:48:55'),
(9, 2, 116, '2018-01-31 07:49:35'),
(12, 2, 117, '2018-01-31 07:50:36'),
(15, 2, 118, '2018-02-04 08:40:00'),
(18, 2, 119, '2018-02-04 08:40:18');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_weather_locations`
--

CREATE TABLE `tsk_user_weather_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(10) UNSIGNED NOT NULL,
  `location_type` enum('C','I','G','Z') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'C=>By city name, O=>By city id, G=By geographic coordinates, Z-By ZIP code',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_weather_locations`
--

INSERT INTO `tsk_user_weather_locations` (`id`, `user_id`, `location`, `ordering`, `location_type`, `created_at`) VALUES
(1, 5, 'London,uk', 1, 'C', '2018-02-09 08:41:18'),
(2, 5, 'Seattle,us', 2, 'C', '2018-02-09 08:41:18'),
(4, 5, 'Ivano-Frankivsk,ua', 3, 'C', '2018-02-15 08:22:12'),
(5, 5, 'Kiev,ua', 4, 'C', '2018-02-15 08:22:24'),
(6, 5, '111', 5, 'C', '2018-02-15 12:06:57'),
(8, 6, 'kiev', 6, 'C', '2018-02-15 14:43:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsk_categories`
--
ALTER TABLE `tsk_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`),
  ADD KEY `categories_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_document_categories`
--
ALTER TABLE `tsk_document_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `document_categories_name_unique` (`name`),
  ADD UNIQUE KEY `document_categories_alias_unique` (`alias`),
  ADD KEY `document_categories_created_at_index` (`created_at`),
  ADD KEY `document_categories_name_at_index` (`type`,`name`);

--
-- Indexes for table `tsk_events`
--
ALTER TABLE `tsk_events`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `events_name_unique` (`name`),
  ADD KEY `events_task_id_foreign` (`task_id`),
  ADD KEY `events_created_at_index` (`created_at`),
  ADD KEY `events_access_name_at_index` (`access`,`name`);

--
-- Indexes for table `tsk_events_users`
--
ALTER TABLE `tsk_events_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_events_user_id_event_id_unique` (`user_id`,`event_id`),
  ADD KEY `events_users_event_id_foreign` (`event_id`),
  ADD KEY `events_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_groups`
--
ALTER TABLE `tsk_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groups_name_unique` (`name`),
  ADD UNIQUE KEY `groups_description_unique` (`description`),
  ADD KEY `groups_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_migrations`
--
ALTER TABLE `tsk_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tsk_password_resets`
--
ALTER TABLE `tsk_password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tsk_settings`
--
ALTER TABLE `tsk_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_name_unique` (`name`),
  ADD KEY `settings_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_tasks`
--
ALTER TABLE `tsk_tasks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tasks_name_unique` (`name`),
  ADD KEY `tasks_creator_id_foreign` (`creator_id`),
  ADD KEY `tasks_category_id_foreign` (`category_id`),
  ADD KEY `tasks_status_date_start_date_end_index` (`status`,`date_start`,`date_end`),
  ADD KEY `tasks_priority_category_id_date_end_index` (`priority`,`category_id`,`date_end`),
  ADD KEY `tasks_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_task_assigned_to_users`
--
ALTER TABLE `tsk_task_assigned_to_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_assigned_to_users_user_id_task_id_unique` (`user_id`,`task_id`),
  ADD KEY `task_assigned_to_users_user_task_type_id_foreign` (`user_task_type_id`),
  ADD KEY `task_assigned_to_users_created_at_index` (`created_at`),
  ADD KEY `task_assigned_to_users_task_id_status_is_leader_index` (`task_id`,`status`,`is_leader`);

--
-- Indexes for table `tsk_task_operations`
--
ALTER TABLE `tsk_task_operations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_operations_task_assigned_to_user_id_foreign` (`task_assigned_to_user_id`),
  ADD KEY `task_operations_created_at_index` (`created_at`),
  ADD KEY `task_operations_document_task_id_status_at_index` (`task_id`,`status`),
  ADD KEY `task_operations_document_user_id_status_at_index` (`user_id`,`status`),
  ADD KEY `task_operations_user_operation_id_foreign` (`user_operation_id`);

--
-- Indexes for table `tsk_task_status_changes`
--
ALTER TABLE `tsk_task_status_changes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_status_changes_created_at_index` (`created_at`),
  ADD KEY `task_status_changes_document_task_id_status_at_index` (`task_id`,`status`),
  ADD KEY `task_status_changes_document_user_id_status_at_index` (`user_id`,`status`);

--
-- Indexes for table `tsk_users`
--
ALTER TABLE `tsk_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_status_login_index` (`status`),
  ADD KEY `users_created_at_index` (`created_at`),
  ADD KEY `users_status_contract_start_index` (`status`,`contract_start`);

--
-- Indexes for table `tsk_users_groups`
--
ALTER TABLE `tsk_users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_groups_user_id_group_id_unique` (`user_id`,`group_id`),
  ADD KEY `users_groups_group_id_foreign` (`group_id`),
  ADD KEY `users_groups_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_chats`
--
ALTER TABLE `tsk_user_chats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_chats_created_at_index` (`created_at`),
  ADD KEY `user_chats_creator_id_status_name_index` (`creator_id`,`status`,`name`),
  ADD KEY `user_chats_task_id_foreign` (`task_id`),
  ADD KEY `tasks_status_task_id_creator_id_index` (`status`,`task_id`,`creator_id`),
  ADD KEY `user_chats_manager_id_status_index` (`manager_id`,`status`);

--
-- Indexes for table `tsk_user_chats_last_visited`
--
ALTER TABLE `tsk_user_chats_last_visited`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_chats_last_visited_user_id_user_chat_id_unique` (`user_id`,`user_chat_id`),
  ADD KEY `user_chats_last_visited_user_chat_id_foreign` (`user_chat_id`),
  ADD KEY `user_chats_last_visited_visited_at_index` (`visited_at`);

--
-- Indexes for table `tsk_user_chat_messages`
--
ALTER TABLE `tsk_user_chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_chat_messages_user_id_user_chat_id_index` (`user_id`,`user_chat_id`),
  ADD KEY `user_chat_messages_user_chat_id_is_top_index` (`user_chat_id`,`is_top`),
  ADD KEY `user_chat_messages_created_at_index` (`created_at`),
  ADD KEY `user_chat_messages_updated_at_by_user_id_foreign` (`updated_at_by_user_id`),
  ADD KEY `user_chat_messages_user_chat_id_message_type_index` (`message_type`,`user_id`);

--
-- Indexes for table `tsk_user_chat_message_documents`
--
ALTER TABLE `tsk_user_chat_message_documents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_chat_message_documents_1_unique` (`user_chat_id`,`filename`),
  ADD KEY `user_chat_message_documents_user_id_foreign` (`user_id`),
  ADD KEY `user_chat_message_documents_document_category_id_foreign` (`document_category_id`),
  ADD KEY `user_chat_message_documents_2` (`user_chat_id`,`extension`),
  ADD KEY `user_chat_message_documents_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_chat_participants`
--
ALTER TABLE `tsk_user_chat_participants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_chat_participants_user_id_user_chat_id_unique` (`user_id`,`user_chat_id`),
  ADD KEY `user_chat_participants_created_at_index` (`created_at`),
  ADD KEY `user_chat_participants_status_user_index` (`user_id`),
  ADD KEY `user_chat_participants_user_chat_id_status_user_id_index` (`user_chat_id`,`status`,`user_id`);

--
-- Indexes for table `tsk_user_profiles`
--
ALTER TABLE `tsk_user_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_profiles_user_id_name_unique` (`user_id`,`name`),
  ADD KEY `user_profiles_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_profile_documents`
--
ALTER TABLE `tsk_user_profile_documents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_profile_documents_user_id_filename_unique` (`user_id`,`filename`),
  ADD KEY `user_profile_documents_document_category_id_foreign` (`document_category_id`),
  ADD KEY `user_profile_documents_user_id_public_access_unique` (`user_id`,`public_access`),
  ADD KEY `user_profile_documents_user_id_extension_unique` (`user_id`,`extension`),
  ADD KEY `user_profile_documents_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_skills`
--
ALTER TABLE `tsk_user_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_skills_created_at_index` (`created_at`),
  ADD KEY `user_skills_user_id_skill_index` (`user_id`,`skill`);

--
-- Indexes for table `tsk_user_task_types`
--
ALTER TABLE `tsk_user_task_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_task_types_name_unique` (`name`),
  ADD KEY `user_task_types_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_todos`
--
ALTER TABLE `tsk_user_todos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_todos_user_id_text_unique` (`user_id`,`text`),
  ADD KEY `user_todos_task_id_foreign` (`task_id`),
  ADD KEY `user_todos_user_id_completed` (`user_id`,`completed`),
  ADD KEY `user_todos_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_user_chat_new_messages`
--
ALTER TABLE `tsk_user_user_chat_new_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_user_chat_new_messages_user_chat_message_id_foreign` (`user_chat_message_id`),
  ADD KEY `user_user_chat_new_messages_created_at_index` (`created_at`),
  ADD KEY `user_user_chat_new_messages_user_id_user_chat_message_id_unique` (`user_id`,`user_chat_message_id`);

--
-- Indexes for table `tsk_user_weather_locations`
--
ALTER TABLE `tsk_user_weather_locations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_weather_locations_user_id_location_unique` (`user_id`,`location`),
  ADD KEY `user_weather_locations_created_at_index` (`created_at`),
  ADD KEY `user_weather_locations_user_id_ordering_index` (`user_id`,`ordering`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsk_categories`
--
ALTER TABLE `tsk_categories`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tsk_document_categories`
--
ALTER TABLE `tsk_document_categories`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tsk_events`
--
ALTER TABLE `tsk_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tsk_events_users`
--
ALTER TABLE `tsk_events_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tsk_groups`
--
ALTER TABLE `tsk_groups`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tsk_migrations`
--
ALTER TABLE `tsk_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `tsk_settings`
--
ALTER TABLE `tsk_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tsk_tasks`
--
ALTER TABLE `tsk_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `tsk_task_assigned_to_users`
--
ALTER TABLE `tsk_task_assigned_to_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `tsk_task_operations`
--
ALTER TABLE `tsk_task_operations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tsk_task_status_changes`
--
ALTER TABLE `tsk_task_status_changes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tsk_users`
--
ALTER TABLE `tsk_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tsk_users_groups`
--
ALTER TABLE `tsk_users_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tsk_user_chats`
--
ALTER TABLE `tsk_user_chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tsk_user_chats_last_visited`
--
ALTER TABLE `tsk_user_chats_last_visited`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tsk_user_chat_messages`
--
ALTER TABLE `tsk_user_chat_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT for table `tsk_user_chat_message_documents`
--
ALTER TABLE `tsk_user_chat_message_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `tsk_user_chat_participants`
--
ALTER TABLE `tsk_user_chat_participants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tsk_user_profiles`
--
ALTER TABLE `tsk_user_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tsk_user_profile_documents`
--
ALTER TABLE `tsk_user_profile_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `tsk_user_skills`
--
ALTER TABLE `tsk_user_skills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `tsk_user_task_types`
--
ALTER TABLE `tsk_user_task_types`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tsk_user_todos`
--
ALTER TABLE `tsk_user_todos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tsk_user_user_chat_new_messages`
--
ALTER TABLE `tsk_user_user_chat_new_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tsk_user_weather_locations`
--
ALTER TABLE `tsk_user_weather_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tsk_categories`
--
ALTER TABLE `tsk_categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `tsk_categories` (`id`);

--
-- Constraints for table `tsk_events`
--
ALTER TABLE `tsk_events`
  ADD CONSTRAINT `events_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tsk_tasks` (`id`);

--
-- Constraints for table `tsk_events_users`
--
ALTER TABLE `tsk_events_users`
  ADD CONSTRAINT `events_users_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `tsk_events` (`id`),
  ADD CONSTRAINT `events_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_tasks`
--
ALTER TABLE `tsk_tasks`
  ADD CONSTRAINT `tasks_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `tsk_categories` (`id`),
  ADD CONSTRAINT `tasks_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_task_assigned_to_users`
--
ALTER TABLE `tsk_task_assigned_to_users`
  ADD CONSTRAINT `task_assigned_to_users_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tsk_tasks` (`id`),
  ADD CONSTRAINT `task_assigned_to_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `task_assigned_to_users_user_task_type_id_foreign` FOREIGN KEY (`user_task_type_id`) REFERENCES `tsk_user_task_types` (`id`);

--
-- Constraints for table `tsk_task_operations`
--
ALTER TABLE `tsk_task_operations`
  ADD CONSTRAINT `task_operations_task_assigned_to_user_id_foreign` FOREIGN KEY (`task_assigned_to_user_id`) REFERENCES `tsk_task_assigned_to_users` (`id`),
  ADD CONSTRAINT `task_operations_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tsk_tasks` (`id`),
  ADD CONSTRAINT `task_operations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `task_operations_user_operation_id_foreign` FOREIGN KEY (`user_operation_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_task_status_changes`
--
ALTER TABLE `tsk_task_status_changes`
  ADD CONSTRAINT `task_status_changes_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tsk_tasks` (`id`),
  ADD CONSTRAINT `task_status_changes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_users_groups`
--
ALTER TABLE `tsk_users_groups`
  ADD CONSTRAINT `users_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `tsk_groups` (`id`),
  ADD CONSTRAINT `users_groups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_chats`
--
ALTER TABLE `tsk_user_chats`
  ADD CONSTRAINT `user_chats_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `user_chats_manager_id_foreign` FOREIGN KEY (`manager_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `user_chats_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tsk_tasks` (`id`);

--
-- Constraints for table `tsk_user_chats_last_visited`
--
ALTER TABLE `tsk_user_chats_last_visited`
  ADD CONSTRAINT `user_chats_last_visited_user_chat_id_foreign` FOREIGN KEY (`user_chat_id`) REFERENCES `tsk_user_chats` (`id`),
  ADD CONSTRAINT `user_chats_last_visited_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_chat_messages`
--
ALTER TABLE `tsk_user_chat_messages`
  ADD CONSTRAINT `user_chat_messages_updated_at_by_user_id_foreign` FOREIGN KEY (`updated_at_by_user_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `user_chat_messages_user_chat_id_foreign` FOREIGN KEY (`user_chat_id`) REFERENCES `tsk_user_chats` (`id`),
  ADD CONSTRAINT `user_chat_messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_chat_message_documents`
--
ALTER TABLE `tsk_user_chat_message_documents`
  ADD CONSTRAINT `user_chat_message_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `tsk_document_categories` (`id`),
  ADD CONSTRAINT `user_chat_message_documents_user_chat_id_foreign` FOREIGN KEY (`user_chat_id`) REFERENCES `tsk_user_chats` (`id`),
  ADD CONSTRAINT `user_chat_message_documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_chat_participants`
--
ALTER TABLE `tsk_user_chat_participants`
  ADD CONSTRAINT `user_chat_participants_user_chat_id_foreign` FOREIGN KEY (`user_chat_id`) REFERENCES `tsk_user_chats` (`id`),
  ADD CONSTRAINT `user_chat_participants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_profiles`
--
ALTER TABLE `tsk_user_profiles`
  ADD CONSTRAINT `user_profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_profile_documents`
--
ALTER TABLE `tsk_user_profile_documents`
  ADD CONSTRAINT `user_profile_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `tsk_document_categories` (`id`),
  ADD CONSTRAINT `user_profile_documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_skills`
--
ALTER TABLE `tsk_user_skills`
  ADD CONSTRAINT `user_skills_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_todos`
--
ALTER TABLE `tsk_user_todos`
  ADD CONSTRAINT `user_todos_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tsk_tasks` (`id`),
  ADD CONSTRAINT `user_todos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_user_chat_new_messages`
--
ALTER TABLE `tsk_user_user_chat_new_messages`
  ADD CONSTRAINT `user_user_chat_new_messages_user_chat_message_id_foreign` FOREIGN KEY (`user_chat_message_id`) REFERENCES `tsk_user_chat_messages` (`id`),
  ADD CONSTRAINT `user_user_chat_new_messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_weather_locations`
--
ALTER TABLE `tsk_user_weather_locations`
  ADD CONSTRAINT `user_weather_locations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
