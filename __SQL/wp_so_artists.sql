
CREATE TYPE type_active_status AS ENUM (
    'A',
    'I',
    'N'
);

DROP TABLE IF EXISTS so_artists;
CREATE TABLE so_artists (
    id             integer NOT NULL,
    name           character varying(50) NOT NULL,
    ordering       integer NOT NULL,
    is_active      type_active_status DEFAULT 'N'::type_active_status NOT NULL,
    has_concerts   boolean DEFAULT false,
    email          character varying(50) NOT NULL,
    birthday       date NOT NULL,
    site           character varying(100) NOT NULL,
    registered_at  timestamp NOT NULL,
    info           text,
  	created_at     timestamp NOT NULL DEFAULT now(),
	  updated_at     timestamp NULL,
    PRIMARY KEY (id)

);
CREATE INDEX ind_so_artists_created_at ON so_artists ( created_at );
CREATE INDEX ind_so_artists_is_active_has_concerts_ordering ON so_artists ( is_active, has_concerts, ordering );

CREATE UNIQUE INDEX ind_so_artists_name ON so_artists (name);
CREATE UNIQUE INDEX ind_so_artists_email ON so_artists (email);



INSERT INTO so_artists (id, name, ordering, is_active, has_concerts, email, site, birthday, registered_at, info, updated_at, created_at) VALUES
(1, 'Ella Fitzgerald', 2, 'A', false, 'ellafitzgerald@mail.com', 'http://EllaFitzgerald_site.com', '1982-02-07', '2014-12-23 00:00:00', 'Ella Fitzgerald  some long text 1',
'2015-03-12 08:31:21', '2015-03-12 08:31:21'),
(2, 'Demis Roussos', 1, 'A', false, 'demisroussos@mail.com', 'http://demis_roussos_site.com', '1988-01-11', '2012-11-24 00:00:00', 'DemisRoussos some long text 22', '2015-03-10 04:35:07', '2015-03-10 04:35:07'),
(3, 'Frank Sinatra', 4, 'A', false, 'franksinatra@mail.com', 'http://frank_sinatra_site.com', '1976-07-21', '2015-01-14 00:00:00', 'Frank Sinatra some long text 3', '2015-03-11 05:21:54', '2015-03-11 05:26:54'),
(4, 'Elvis Presley', 3, 'A', false, 'elvis_presley@mail.com', 'http://elvis_presley_site.com', '1978-12-01', '2013-06-13 00:00:00', 'Elvis Presley some long text 4', '2015-03-11 05:26:54', '2015-03-11 05:26:54'),
(5, 'Vopli Vidopliassova ', 5, 'A', true, 'Vopli_Vidopliassova@mail.com', 'http://Vopliv_idopliassova_site.com', '1986-11-11', '2013-06-13 00:00:00', 'Vopli Vdopliassova some long text 4', '2015-03-11 05:26:54', '2015-03-11 05:26:54');



CREATE DOMAIN type_location AS numeric(22,16)
	CONSTRAINT type_location_check CHECK ((VALUE >= (0)::numeric));






DROP TABLE IF EXISTS so_tours;
CREATE TABLE so_tours (
  id           integer NOT NULL,
  artist_id    integer NOT NULL,
  tour_name    character varying(100) NOT NULL,
  start_date   date NOT NULL,
  end_date     date NOT NULL,
  ordering     integer DEFAULT NULL,
  color        character varying(20) NOT NULL,
  description  text NOT NULL,
  created      timestamp NOT NULL DEFAULT now(),
  PRIMARY KEY  (id),
  CONSTRAINT   fk_so_tours_artist_id FOREIGN KEY (artist_id) REFERENCES so_artists (id) ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE INDEX ind_so_tours_artist_id_start_date_end_date ON so_tours ( artist_id, start_date, end_date );

INSERT INTO "so_tours" VALUES (1,5,'Small Tour of Vopli Vidopliassova ','2015-12-11','2015-12-22',1,'grey','Small Tour of Vopli Vidopliassova during 10 days ','2016-01-16 05:10:55'),(2,5,'Big Tour of Vopli Vidopliassova ','2016-01-12','2016-02-02',2,'blue','Big Tour of Vopli Vidopliassova during 20 days ','2015-12-12 11:57:03');


DROP TABLE IF EXISTS so_artist_concerts;
CREATE TABLE so_artist_concerts (
  id              integer  NOT NULL,
  artist_id       integer NOT NULL,
  place_name      character varying (100) NOT NULL,
  lat             type_location NOT NULL,
  lng             type_location NOT NULL,
  participants    integer DEFAULT NULL,
  event_date      timestamp NOT NULL,
  tour_id         integer DEFAULT NULL,
  country         character varying(30) NOT NULL,
  description     text NOT NULL,
 	created_at      timestamp NOT NULL DEFAULT now(),
  PRIMARY KEY    (id),
  CONSTRAINT     fk_so_artist_concerts_artist_id FOREIGN KEY (artist_id) REFERENCES so_artists (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT     fk_so_artist_concerts_tour_id FOREIGN KEY (tour_id) REFERENCES so_tours (id) ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE INDEX ind_so_artist_concerts_artist_id_country_event_date ON so_artist_concerts ( artist_id, country, event_date );

INSERT INTO "so_artist_concerts" VALUES (1,5,'Lviv',49.8974723815918000,24.1181907653808600,25500,'2015-12-18 18:30:00',NULL,'Ukraine','Some festival on <b>Lviv</b>. Join us and have fun! ','2016-01-07 03:29:59'),(2,5,'Mukacheve',48.4705543518066400,22.7685413360595700,3800,'2015-12-18 18:00:00',NULL,'Ukraine','Wine festival in Mukacheve','2015-11-25 21:59:24'),(3,5,'Uzhhorod',48.6603317260742200,22.3492813110351560,NULL,'2015-12-15 20:30:00',NULL,'Ukraine','Big rock concert on <b>Uzhhorod</b> !','2015-12-19 05:42:32'),(4,5,'Rakhiv',48.0499992370605500,24.2000007629394530,25,'2015-12-20 20:00:00',NULL,'Ukraine','Meeting of best people in Rakhiv','2015-11-21 09:27:55'),(5,5,'Pylypets',48.6699981689453100,23.3400001525878900,2200,'2015-11-19 19:45:00',NULL,'Ukraine','Wine festival on <b>Pylypets</b>. Not miss! ','2015-12-28 07:08:43'),(6,5,'Kolomyya',48.5305557250976560,25.0402774810791000,600,'2015-11-24 18:30:00',1,'Ukraine','Some theatrical festival in <b>Kolomyya</b>. Order tickets now!','2015-12-07 10:55:30'),(7,5,'Chernivtsi',48.2560806274414060,25.9518928527832030,19700,'2015-12-04 18:30:00',NULL,'Ukraine','Day of city festival in <b>Chernivtsi</b>. Free!','2015-12-15 08:08:34'),(8,5,'Khmilnyk',49.5558128356933600,27.9733772277832030,0,'2015-11-21 18:30:00',NULL,'Ukraine','Cure yourself in <b>Khmilnyk</b>. Expensive but usefull!','2015-12-25 08:57:15'),(9,5,'Truskavets',49.2692489624023440,23.5129280090332030,NULL,'2015-12-19 21:05:00',NULL,'Ukraine','Have rest in <b>Truskavets</b>','2016-01-08 11:03:34'),(10,5,'Zdanów',50.6962203979492200,23.2267208099365230,NULL,'2015-12-18 20:00:00',2,'Poland','Rock Festival in <b>Zdanów</b> , Poland','2015-11-26 23:26:40'),(11,5,'?ód?',51.7322082519531250,19.5203323364257800,NULL,'2015-12-21 20:30:00',2,'Poland','Rock Festival in <b>?ód?</b> , Poland','2015-12-19 06:25:33'),(12,5,'Warsaw, Poland',52.2157936096191400,21.0011157989501950,NULL,'2015-12-17 19:45:00',2,'Poland','Big Rock Concert in <b>Warsaw</b> , Poland','2015-11-28 11:22:06'),(13,5,'Koshice',48.7134933471679700,21.2703437805175780,NULL,'2015-12-21 20:30:00',2,'Slovakia','Rock Festival in <b>Koshice</b> , Slovakia','2015-12-18 09:09:01'),(14,5,'Poprad',49.0566482543945300,20.3035469055175780,NULL,'2015-12-13 21:00:00',2,'Slovakia','Day of city with Rock concert in <b>Poprad</b> , Slovakia','2015-12-04 07:40:39'),(15,5,'Ternopil',49.5544929504394500,25.5714893341064450,NULL,'2015-12-11 20:15:00',2,'Ukraine','Day of city with Rock concert in <b>Ternopil</b> , Ukraine','2015-11-28 19:42:23'),(16,5,'Lutsk',50.7404861450195300,25.3211135864257800,NULL,'2015-12-30 22:00:00',2,'Ukraine','Rock concert in <b>Lutsk</b> , Ukraine','2015-12-09 20:15:13'),(17,5,'Shepetivka',50.1809768676757800,27.0569534301757800,NULL,'2016-02-07 21:15:00',1,'Ukraine','Rock Tour in <b>Shepetivka</b> , Ukraine','2015-11-23 20:59:13'),(18,5,'Vinnytsia',49.2406692504882800,28.4671993255615230,NULL,'2016-02-12 19:40:00',1,'Ukraine','Rock Tour in <b>Vinnytsia</b> , Ukraine','2015-11-22 07:03:45'),(19,5,'Kraków',50.0330047607421900,20.0037307739257800,NULL,'2016-02-25 19:40:00',1,'Poland','Rock Tour in <b>Kraków</b> , Poland','2015-12-14 20:12:54'),(20,5,'Pozna?',52.4306678771972660,16.8546504974365230,NULL,'2016-02-24 19:40:00',1,'Poland','Rock Tour in <b>Pozna?</b> , Poland','2015-11-17 15:36:59');



LAST:

DROP TABLE IF EXISTS so_songs;
CREATE TABLE so_songs (
  id          integer NOT NULL,
  title       varchar(100) NOT NULL DEFAULT '',
  ordering    integer DEFAULT NULL,
  is_active   boolean DEFAULT false,
  created     timestamp NOT NULL DEFAULT now()
  PRIMARY KEY (id),
);
CREATE INDEX ind_so_songs_is_active_ordering ON so_songs ( is_active, ordering );
CREATE UNIQUE INDEX ind_so_songs_title ON so_songs (title);


INSERT INTO so_songs VALUES (1,'Summertime (High Quality - Remastered)',5,true,'2015-11-19 09:53:40'),(2,'Mack the Knife (in Berlin)',2,false,'2015-12-03 16:17:28'),(3,'Forget Domani',3,true,'2015-11-22 20:15:26'),(4,'Rain and Tears',1,false,'2016-01-15 11:20:27'),(5,'That\''s Life',4,true,'2015-12-27 04:59:32'),(6,'THE DOZENS',6,true,'2015-12-12 21:21:12'),(7,'Can\''t Hep Falling In Love',88,
true,'2015-12-19 10:57:46'),(8,'Always On My Mind',7,false,'2015-11-25 15:55:13'),(30,'Perd�name',3,false,'2015-12-06 16:52:20'),(31,'Can\''t Say How Much I LoveYou',23,false,
'2015-12-21 02:47:08'),(32,'Souvenirs',23,true,'2015-12-05 22:34:16'),(33,'My Only Fascination',34,true,'2015-12-23 18:43:32'),(34,'Goodbye My Love, Goodbye',32,true,'2015-12-08 15:03:29'),(35,'When Forever Has Gone',23,true,'2015-12-17 08:41:30'),(36,'Come Waltz With Me',23,true,'2015-11-29 13:16:59'),(37,'Lost In Love',23,true,'2015-12-18 01:12:39'),(38,'\nSomeday Somewhere ',23,true,'2015-11-24 23:58:36'),(39,'Happy To Be On An Island In The Sun',5,true,'2016-01-10 15:44:22'),(40,'Sailin\'' Home',34,true,'2015-12-24 04:37:23'),(41,'Only the Lonely',54,true,'2015-11-20 06:54:24'),(42,'Angel Eyes',45,true,'2015-11-28 20:51:48'),(43,'It\''s a Lonesome Old Town',65,true,'2015-12-23 10:18:20'),(44,'Willow Weep for Me',4,true,'2016-01-09 12:20:36'),(45,'Guess I\''ll Hang My Tears Out to Dry',34,true,'2015-12-07 10:14:46'),(46,'Blues in the Night',45,true,'2016-01-16 02:22:54'),(47,'One for My Baby (And One More for the Road)',43,true,'2015-11-30 00:16:45'),(48,'Sleep Warm',3,true,'2015-11-25 19:09:37'),(49,'Where or When',34,true,'2015-11-17 22:00:22'),(50,'Spring Is Here',45,true,'2015-12-30 19:08:25'),(51,'A Big Hunk O\'' Love',45,true,'2015-11-18 09:44:38'),(52,'A Boy Like Me, a Girl Like You',89,true,'2015-12-06 23:15:46'),(53,'A Cane and a High Starched Collar',6,true,'2016-01-07 09:32:17'),(54,'Elvis Presley lyrics -- Elvis A-Z -- SongDataBase',8,true,'2015-11-17 19:12:39'),(55,'America the Beautiful',8,true,'2015-12-28 09:24:26'),(56,'Amazing Grace',7,true,'2015-12-06 10:04:38'),(57,'And I Love You So',6,true,'2015-12-15 00:58:38'),(58,'Are You Lonesome Tonight',3,true,'2015-12-06 02:02:23'),(59,'Blue Christmas',8,true,'2016-01-13 17:00:38'),(60,'Crying In The Chapel',6,true,'2016-01-09 03:23:32'),(61,'Jailhouse Rock',34,true,'2016-01-12 20:49:26'),(62,'Return To Sender',8,true,'2015-11-30 23:05:51'),(63,'Undecided',63,true,'2015-11-19 09:17:50'),(64,'A-Tisket, A-Tasket',64,true,'2015-11-29 06:28:41'),(65,'Dream a Little Dream of Me',65,true,'2015-12-07 09:19:21'),(66,'Sugar Blues',66,true,'2015-12-22 07:43:15'),(67,'The Starlit Hour',67,true,'2015-11-17 16:52:03'),(68,'Carmen',68,true,'2015-12-23 05:35:44'),(69,'Car',69,true,'2015-12-31 08:10:03'),(70,'I\''ll Come',70,true,'2015-12-10 04:54:54'),(71,'Mussa',71,true,'2016-01-03 02:44:50'),(72,'You\''ve Gone',72,true,'2015-12-10 04:31:54'),(73,'Winter Again',73,true,'2015-12-23 18:15:22'),(74,'Galya, Come',74,true,'2016-01-08 21:37:23'),(75,'Dances',75,true,'2015-12-10 10:26:44'),(76,'Legends of Love',76,true,'2015-11-30 22:28:59'),(77,'Lida',77,true,'2015-12-14 13:02:51'),(78,'Mad Star',78,true,'2015-12-21 20:46:40'),(79,'Once',79,true,'2015-12-12 10:30:37');






DROP TABLE IF EXISTS so_song_jenres;
CREATE TABLE so_song_jenres (
  id            integer  NOT NULL,
  song_id       integer NOT NULL,
  jenre_id      integer NOT NULL,
  created       timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY   (id),
  KEY fk_so_song_jenres_song_id (song_id),
  CONSTRAINT fk_so_song_jenres_song_id FOREIGN KEY (song_id) REFERENCES so_songs (ID) ON DELETE NO ACTION ON UPDATE NO ACTION
);

INSERT INTO so_song_jenres VALUES (1,1,2,'2016-01-17 10:24:53'),(2,1,4,'2016-01-08 11:48:35'),(3,2,1,'2016-01-08 07:25:04'),(4,2,3,'2015-12-10 19:40:55'),(5,3,5,'2016-01-08 21:26:24'),(6,4,2,'2015-12-02 05:06:24'),(20,30,1,'2015-12-31 18:43:43'),(21,31,1,'2015-11-30 20:04:17'),(22,32,1,'2015-11-18 08:09:27'),(23,33,1,'2015-11-30 21:01:45'),(24,34,1,'2015-11-28 07:48:09'),(25,35,1,'2015-12-03 10:03:05'),(26,36,1,'2015-12-11 08:29:15'),(27,37,1,'2015-12-02 11:39:14'),(28,38,1,'2015-12-26 06:57:55'),(29,39,1,'2016-01-01 23:12:44'),(30,40,1,'2015-12-29 06:12:32'),(31,41,1,'2015-12-07 03:18:46'),(32,41,4,'2016-01-05 01:54:59'),(33,42,1,'2015-11-30 20:04:08'),(34,43,4,'2015-11-22 11:22:13'),(35,44,4,'2015-12-28 21:26:39'),(36,45,1,'2015-05-25 08:54:20'),(37,45,4,'2015-05-25 08:54:20'),(38,46,1,'2015-05-25 08:54:47'),(39,46,4,'2015-05-25 08:54:47'),(40,47,1,'2015-05-25 08:55:21'),(41,48,1,'2015-05-25 08:55:50'),(42,48,4,'2015-05-25 08:55:50'),(43,49,1,'2015-05-25 08:56:14'),(44,49,4,'2015-05-25 08:56:14'),(45,50,1,'2015-05-25 08:59:44'),(46,51,2,'2015-05-25 10:17:23'),(47,52,2,'2015-05-25 10:19:13'),(48,52,4,'2015-05-25 10:19:13'),(49,53,2,'2015-05-25 10:20:18'),(50,53,4,'2015-05-25 10:20:18'),(51,54,2,'2015-05-25 10:21:22'),(52,54,4,'2015-05-25 10:21:22'),(53,55,2,'2015-05-25 10:22:03'),(54,55,4,'2015-05-25 10:22:03'),(55,56,2,'2015-05-25 10:22:35'),(56,56,4,'2015-05-25 10:22:35'),(57,57,2,'2015-05-25 10:23:13'),(58,57,4,'2015-05-25 10:23:13'),(59,58,2,'2015-05-25 10:24:52'),(60,58,4,'2015-05-25 10:24:52'),(61,59,2,'2015-05-25 10:25:16'),(62,59,4,'2015-05-25 10:25:16'),(63,60,2,'2015-05-25 10:25:52'),(64,60,4,'2015-05-25 10:25:52'),(65,61,2,'2015-05-25 10:27:24'),(66,61,4,'2015-05-25 10:27:24'),(67,62,2,'2015-12-31 06:33:38'),(68,62,4,'2015-12-22 15:22:57'),(69,63,3,'2016-01-10 11:29:30'),(70,64,3,'2016-01-15 16:16:26'),(71,65,3,'2015-12-22 03:25:31'),(72,66,3,'2015-11-19 10:38:36'),(73,67,3,'2015-12-18 20:49:15');


--
-- Indexes for dumped tables
--

--
-- Indexes for table `so_artists`
--

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `so_artists`
--
ALTER TABLE `so_artists`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



CREATE TABLE public.pd_product (
	id int8 NOT NULL DEFAULT nextval('pd_product_id_seq'::regclass),
	title varchar(255) NOT NULL,
	status type_productstatus NOT NULL DEFAULT 'D'::type_productstatus,
	slug varchar(255) NULL,
	sku varchar(255) NOT NULL,
	user_id int4 NOT NULL,
	regular_price type_money NULL DEFAULT 0,
	sale_price type_money NULL DEFAULT 0,
	in_stock bool NOT NULL DEFAULT false,
	has_discount_prices bool NOT NULL DEFAULT false,
	stock_qty type_qty NOT NULL DEFAULT 0,
	is_backorder type_backorder NOT NULL DEFAULT 'N'::type_backorder,
	is_homepage bool NOT NULL DEFAULT false,
	is_featured bool NOT NULL DEFAULT false,
	weight int2 NULL,
	length int2 NULL,
	width int2 NULL,
	height int2 NULL,
	shipping_class_id int2 NULL,
	short_description varchar(255) NOT NULL,
	description text NULL,
	has_attributes bool NOT NULL,
	downloadable bool NOT NULL DEFAULT false,
	virtual bool NOT NULL DEFAULT false,
	rating_count int4 NOT NULL DEFAULT 0,
	rating_summary int4 NOT NULL DEFAULT 0,
	published_at timestamp NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NULL,
	CONSTRAINT ind_product_sku_unique UNIQUE (sku),
	CONSTRAINT ind_product_slug_unique UNIQUE (slug),
	CONSTRAINT pd_product_pkey PRIMARY KEY (id),
	CONSTRAINT pd_product_shipping_class_id_fkey FOREIGN KEY (shipping_class_id) REFERENCES public.pd_shipping_class(id),
	CONSTRAINT pd_product_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.pd_ion_users(id)
)
WITH (
	OIDS=FALSE
) ;
create
    index idx_product_created_at on
    pd_product
        using btree(created_at) ;
create
    index idx_product_in_stock_regular_price_sale_price on
    pd_product
        using btree(
        in_stock,
        regular_price,
        sale_price
    ) ;
create
    index idx_product_is_backorder on
    pd_product
        using btree(is_backorder) ;
create
    index idx_product_published_at on
    pd_product
        using btree(published_at) ;
create
    index idx_product_status_is_featured on
    pd_product
        using btree(
        status,
        is_featured
    ) ;
create
    index idx_product_status_is_homepage on
    pd_product
        using btree(
        status,
        is_homepage
    ) ;
create
    index idx_product_status_sku_title on
    pd_product
        using btree(
        status,
        sku,
        title
    ) ;
