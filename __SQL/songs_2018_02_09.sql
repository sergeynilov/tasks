--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: type_active_status; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE type_active_status AS ENUM (
    'A',
    'I',
    'N'
);


ALTER TYPE type_active_status OWNER TO postgres;

--
-- Name: type_location; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN type_location AS numeric(22,16)
	CONSTRAINT type_location_check CHECK ((VALUE >= (0)::numeric));


ALTER DOMAIN type_location OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: so_artist_concerts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE so_artist_concerts (
    id integer NOT NULL,
    artist_id integer NOT NULL,
    place_name character varying(100) NOT NULL,
    lat type_location NOT NULL,
    lng type_location NOT NULL,
    participants integer,
    event_date timestamp without time zone NOT NULL,
    tour_id integer,
    country character varying(30) NOT NULL,
    description text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE so_artist_concerts OWNER TO postgres;

--
-- Name: so_artists; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE so_artists (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    ordering integer NOT NULL,
    is_active type_active_status DEFAULT 'N'::type_active_status NOT NULL,
    has_concerts boolean DEFAULT false,
    email character varying(50) NOT NULL,
    birthday date NOT NULL,
    site character varying(100) NOT NULL,
    registered_at timestamp without time zone NOT NULL,
    info text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE so_artists OWNER TO postgres;

--
-- Name: so_songs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE so_songs (
    id integer NOT NULL,
    title character varying(100) DEFAULT ''::character varying NOT NULL,
    ordering integer,
    is_active boolean DEFAULT false,
    created timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE so_songs OWNER TO postgres;

--
-- Name: so_tours; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE so_tours (
    id integer NOT NULL,
    artist_id integer NOT NULL,
    tour_name character varying(100) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    ordering integer,
    color character varying(20) NOT NULL,
    description text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE so_tours OWNER TO postgres;

--
-- Data for Name: so_artist_concerts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY so_artist_concerts (id, artist_id, place_name, lat, lng, participants, event_date, tour_id, country, description, created_at) FROM stdin;
1	5	Lviv	49.8974723815918000	24.1181907653808600	25500	2015-12-18 18:30:00	\N	Ukraine	Some festival on <b>Lviv</b>. Join us and have fun! 	2016-01-07 03:29:59
2	5	Mukacheve	48.4705543518066400	22.7685413360595700	3800	2015-12-18 18:00:00	\N	Ukraine	Wine festival in Mukacheve	2015-11-25 21:59:24
3	5	Uzhhorod	48.6603317260742200	22.3492813110351560	\N	2015-12-15 20:30:00	\N	Ukraine	Big rock concert on <b>Uzhhorod</b> !	2015-12-19 05:42:32
4	5	Rakhiv	48.0499992370605500	24.2000007629394530	25	2015-12-20 20:00:00	\N	Ukraine	Meeting of best people in Rakhiv	2015-11-21 09:27:55
5	5	Pylypets	48.6699981689453100	23.3400001525878900	2200	2015-11-19 19:45:00	\N	Ukraine	Wine festival on <b>Pylypets</b>. Not miss! 	2015-12-28 07:08:43
6	5	Kolomyya	48.5305557250976560	25.0402774810791000	600	2015-11-24 18:30:00	1	Ukraine	Some theatrical festival in <b>Kolomyya</b>. Order tickets now!	2015-12-07 10:55:30
7	5	Chernivtsi	48.2560806274414060	25.9518928527832030	19700	2015-12-04 18:30:00	\N	Ukraine	Day of city festival in <b>Chernivtsi</b>. Free!	2015-12-15 08:08:34
8	5	Khmilnyk	49.5558128356933600	27.9733772277832030	0	2015-11-21 18:30:00	\N	Ukraine	Cure yourself in <b>Khmilnyk</b>. Expensive but usefull!	2015-12-25 08:57:15
9	5	Truskavets	49.2692489624023440	23.5129280090332030	\N	2015-12-19 21:05:00	\N	Ukraine	Have rest in <b>Truskavets</b>	2016-01-08 11:03:34
10	5	Zdanów	50.6962203979492200	23.2267208099365230	\N	2015-12-18 20:00:00	2	Poland	Rock Festival in <b>Zdanów</b> , Poland	2015-11-26 23:26:40
11	5	?ód?	51.7322082519531250	19.5203323364257800	\N	2015-12-21 20:30:00	2	Poland	Rock Festival in <b>?ód?</b> , Poland	2015-12-19 06:25:33
12	5	Warsaw, Poland	52.2157936096191400	21.0011157989501950	\N	2015-12-17 19:45:00	2	Poland	Big Rock Concert in <b>Warsaw</b> , Poland	2015-11-28 11:22:06
13	5	Koshice	48.7134933471679700	21.2703437805175780	\N	2015-12-21 20:30:00	2	Slovakia	Rock Festival in <b>Koshice</b> , Slovakia	2015-12-18 09:09:01
14	5	Poprad	49.0566482543945300	20.3035469055175780	\N	2015-12-13 21:00:00	2	Slovakia	Day of city with Rock concert in <b>Poprad</b> , Slovakia	2015-12-04 07:40:39
15	5	Ternopil	49.5544929504394500	25.5714893341064450	\N	2015-12-11 20:15:00	2	Ukraine	Day of city with Rock concert in <b>Ternopil</b> , Ukraine	2015-11-28 19:42:23
16	5	Lutsk	50.7404861450195300	25.3211135864257800	\N	2015-12-30 22:00:00	2	Ukraine	Rock concert in <b>Lutsk</b> , Ukraine	2015-12-09 20:15:13
17	5	Shepetivka	50.1809768676757800	27.0569534301757800	\N	2016-02-07 21:15:00	1	Ukraine	Rock Tour in <b>Shepetivka</b> , Ukraine	2015-11-23 20:59:13
18	5	Vinnytsia	49.2406692504882800	28.4671993255615230	\N	2016-02-12 19:40:00	1	Ukraine	Rock Tour in <b>Vinnytsia</b> , Ukraine	2015-11-22 07:03:45
19	5	Kraków	50.0330047607421900	20.0037307739257800	\N	2016-02-25 19:40:00	1	Poland	Rock Tour in <b>Kraków</b> , Poland	2015-12-14 20:12:54
20	5	Pozna?	52.4306678771972660	16.8546504974365230	\N	2016-02-24 19:40:00	1	Poland	Rock Tour in <b>Pozna?</b> , Poland	2015-11-17 15:36:59
\.


--
-- Data for Name: so_artists; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY so_artists (id, name, ordering, is_active, has_concerts, email, birthday, site, registered_at, info, created_at, updated_at) FROM stdin;
1	Ella Fitzgerald	2	A	f	ellafitzgerald@mail.com	1982-02-07	http://EllaFitzgerald_site.com	2014-12-23 00:00:00	Ella Fitzgerald  some long text 1	2015-03-12 08:31:21	2015-03-12 08:31:21
2	Demis Roussos	1	A	f	demisroussos@mail.com	1988-01-11	http://demis_roussos_site.com	2012-11-24 00:00:00	DemisRoussos some long text 22	2015-03-10 04:35:07	2015-03-10 04:35:07
3	Frank Sinatra	4	A	f	franksinatra@mail.com	1976-07-21	http://frank_sinatra_site.com	2015-01-14 00:00:00	Frank Sinatra some long text 3	2015-03-11 05:26:54	2015-03-11 05:21:54
4	Elvis Presley	3	A	f	elvis_presley@mail.com	1978-12-01	http://elvis_presley_site.com	2013-06-13 00:00:00	Elvis Presley some long text 4	2015-03-11 05:26:54	2015-03-11 05:26:54
5	Vopli Vidopliassova 	5	A	t	Vopli_Vidopliassova@mail.com	1986-11-11	http://Vopliv_idopliassova_site.com	2013-06-13 00:00:00	Vopli Vdopliassova some long text 4	2015-03-11 05:26:54	2015-03-11 05:26:54
\.


--
-- Data for Name: so_songs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY so_songs (id, title, ordering, is_active, created) FROM stdin;
1	Summertime (High Quality - Remastered)	5	t	2015-11-19 09:53:40
2	Mack the Knife (in Berlin)	2	f	2015-12-03 16:17:28
3	Forget Domani	3	t	2015-11-22 20:15:26
4	Rain and Tears	1	f	2016-01-15 11:20:27
5	That\\'s Life	4	t	2015-12-27 04:59:32
6	THE DOZENS	6	t	2015-12-12 21:21:12
7	Can\\'t Hep Falling In Love	88	t	2015-12-19 10:57:46
8	Always On My Mind	7	f	2015-11-25 15:55:13
30	Perd�name	3	f	2015-12-06 16:52:20
31	Can\\'t Say How Much I LoveYou	23	f	2015-12-21 02:47:08
32	Souvenirs	23	t	2015-12-05 22:34:16
33	My Only Fascination	34	t	2015-12-23 18:43:32
34	Goodbye My Love, Goodbye	32	t	2015-12-08 15:03:29
35	When Forever Has Gone	23	t	2015-12-17 08:41:30
36	Come Waltz With Me	23	t	2015-11-29 13:16:59
37	Lost In Love	23	t	2015-12-18 01:12:39
38	\\nSomeday Somewhere 	23	t	2015-11-24 23:58:36
39	Happy To Be On An Island In The Sun	5	t	2016-01-10 15:44:22
40	Sailin\\' Home	34	t	2015-12-24 04:37:23
41	Only the Lonely	54	t	2015-11-20 06:54:24
42	Angel Eyes	45	t	2015-11-28 20:51:48
43	It\\'s a Lonesome Old Town	65	t	2015-12-23 10:18:20
44	Willow Weep for Me	4	t	2016-01-09 12:20:36
45	Guess I\\'ll Hang My Tears Out to Dry	34	t	2015-12-07 10:14:46
46	Blues in the Night	45	t	2016-01-16 02:22:54
47	One for My Baby (And One More for the Road)	43	t	2015-11-30 00:16:45
48	Sleep Warm	3	t	2015-11-25 19:09:37
49	Where or When	34	t	2015-11-17 22:00:22
50	Spring Is Here	45	t	2015-12-30 19:08:25
51	A Big Hunk O\\' Love	45	t	2015-11-18 09:44:38
52	A Boy Like Me, a Girl Like You	89	t	2015-12-06 23:15:46
53	A Cane and a High Starched Collar	6	t	2016-01-07 09:32:17
54	Elvis Presley lyrics -- Elvis A-Z -- SongDataBase	8	t	2015-11-17 19:12:39
55	America the Beautiful	8	t	2015-12-28 09:24:26
56	Amazing Grace	7	t	2015-12-06 10:04:38
57	And I Love You So	6	t	2015-12-15 00:58:38
58	Are You Lonesome Tonight	3	t	2015-12-06 02:02:23
59	Blue Christmas	8	t	2016-01-13 17:00:38
60	Crying In The Chapel	6	t	2016-01-09 03:23:32
61	Jailhouse Rock	34	t	2016-01-12 20:49:26
62	Return To Sender	8	t	2015-11-30 23:05:51
63	Undecided	63	t	2015-11-19 09:17:50
64	A-Tisket, A-Tasket	64	t	2015-11-29 06:28:41
65	Dream a Little Dream of Me	65	t	2015-12-07 09:19:21
66	Sugar Blues	66	t	2015-12-22 07:43:15
67	The Starlit Hour	67	t	2015-11-17 16:52:03
68	Carmen	68	t	2015-12-23 05:35:44
69	Car	69	t	2015-12-31 08:10:03
70	I\\'ll Come	70	t	2015-12-10 04:54:54
71	Mussa	71	t	2016-01-03 02:44:50
72	You\\'ve Gone	72	t	2015-12-10 04:31:54
73	Winter Again	73	t	2015-12-23 18:15:22
74	Galya, Come	74	t	2016-01-08 21:37:23
75	Dances	75	t	2015-12-10 10:26:44
76	Legends of Love	76	t	2015-11-30 22:28:59
77	Lida	77	t	2015-12-14 13:02:51
78	Mad Star	78	t	2015-12-21 20:46:40
79	Once	79	t	2015-12-12 10:30:37
\.


--
-- Data for Name: so_tours; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY so_tours (id, artist_id, tour_name, start_date, end_date, ordering, color, description, created) FROM stdin;
1	5	Small Tour of Vopli Vidopliassova 	2015-12-11	2015-12-22	1	grey	Small Tour of Vopli Vidopliassova during 10 days 	2016-01-16 05:10:55
2	5	Big Tour of Vopli Vidopliassova 	2016-01-12	2016-02-02	2	blue	Big Tour of Vopli Vidopliassova during 20 days 	2015-12-12 11:57:03
\.


--
-- Name: so_artist_concerts so_artist_concerts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY so_artist_concerts
    ADD CONSTRAINT so_artist_concerts_pkey PRIMARY KEY (id);


--
-- Name: so_artists so_artists_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY so_artists
    ADD CONSTRAINT so_artists_pkey PRIMARY KEY (id);


--
-- Name: so_songs so_songs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY so_songs
    ADD CONSTRAINT so_songs_pkey PRIMARY KEY (id);


--
-- Name: so_tours so_tours_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY so_tours
    ADD CONSTRAINT so_tours_pkey PRIMARY KEY (id);


--
-- Name: ind_so_artist_concerts_artist_id_country_event_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_so_artist_concerts_artist_id_country_event_date ON so_artist_concerts USING btree (artist_id, country, event_date);


--
-- Name: ind_so_artists_created_at; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_so_artists_created_at ON so_artists USING btree (created_at);


--
-- Name: ind_so_artists_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ind_so_artists_email ON so_artists USING btree (email);


--
-- Name: ind_so_artists_is_active_has_concerts_ordering; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_so_artists_is_active_has_concerts_ordering ON so_artists USING btree (is_active, has_concerts, ordering);


--
-- Name: ind_so_artists_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ind_so_artists_name ON so_artists USING btree (name);


--
-- Name: ind_so_songs_is_active_ordering; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_so_songs_is_active_ordering ON so_songs USING btree (is_active, ordering);


--
-- Name: ind_so_songs_title; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ind_so_songs_title ON so_songs USING btree (title);


--
-- Name: ind_so_tours_artist_id_start_date_end_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_so_tours_artist_id_start_date_end_date ON so_tours USING btree (artist_id, start_date, end_date);


--
-- Name: so_artist_concerts fk_so_artist_concerts_artist_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY so_artist_concerts
    ADD CONSTRAINT fk_so_artist_concerts_artist_id FOREIGN KEY (artist_id) REFERENCES so_artists(id);


--
-- Name: so_artist_concerts fk_so_artist_concerts_tour_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY so_artist_concerts
    ADD CONSTRAINT fk_so_artist_concerts_tour_id FOREIGN KEY (tour_id) REFERENCES so_tours(id);


--
-- Name: so_tours fk_so_tours_artist_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY so_tours
    ADD CONSTRAINT fk_so_tours_artist_id FOREIGN KEY (artist_id) REFERENCES so_artists(id);


--
-- PostgreSQL database dump complete
--

