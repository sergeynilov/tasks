-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 10, 2018 at 12:29 PM
-- Server version: 5.7.20-0ubuntu0.17.10.1
-- PHP Version: 7.1.8-1ubuntu1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tasks_work`
--

-- --------------------------------------------------------

--
-- Table structure for table `tsk_categories`
--

CREATE TABLE `tsk_categories` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` smallint(5) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_categories`
--

INSERT INTO `tsk_categories` (`id`, `name`, `description`, `image`, `parent_id`, `created_at`) VALUES
(1, 'PHP Development', 'PHP Development description...', NULL, NULL, '2017-12-30 16:48:18'),
(2, 'Wordpress Development', 'Wordpress Development description...', NULL, 1, '2017-12-30 16:48:18'),
(3, 'Joomla Development', 'Joomla Development description...', NULL, 1, '2017-12-30 16:48:18'),
(4, 'Laravel Development', 'Laravel Development description...', NULL, 1, '2017-12-30 16:48:18'),
(5, 'Laravel/vue.js', 'Laravel/vue.js Development description...', NULL, 4, '2017-12-30 16:48:18'),
(6, 'Laravel/angular.js', 'Laravel/angular.js Development description...', NULL, 4, '2017-12-30 16:48:18'),
(7, 'Laravel/react.js', 'Laravel/react.js Development description...', NULL, 4, '2017-12-30 16:48:18');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_document_categories`
--

CREATE TABLE `tsk_document_categories` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('D','C','P','M','T') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT ' D=>Task Document, C=>Chat Document, P=>Profile Document, M=>Profile Main Image, T=>Profile Thumbnail Image',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_document_categories`
--

INSERT INTO `tsk_document_categories` (`id`, `name`, `alias`, `type`, `description`, `created_at`) VALUES
(1, 'Profile main image', NULL, 'M', 'Profile main image description...', '2017-12-30 16:55:33'),
(2, 'Profile thumbnail image', NULL, 'T', 'Profile thumbnail image description...', '2017-12-30 16:55:33'),
(3, 'User\'s documents (diploma, certificate, license)', NULL, 'P', 'User\'s documents (diploma, certificate, license) description...', '2017-12-30 16:55:33'),
(4, 'Task\'s specification documents', NULL, 'D', 'Task\'s specification documents description...', '2017-12-30 16:55:33'),
(5, 'Task\'s attached images', NULL, 'D', 'Task\'s attached images description...', '2017-12-30 16:55:33'),
(6, 'Task\'s attached schemes', NULL, 'D', 'Task\'s attached schemes description...', '2017-12-30 16:55:33'),
(7, 'Chat\'s attached files', NULL, 'C', 'Chat\'s attached files description...vvvvvvvv', '2017-12-30 16:55:33'),
(8, 'd', NULL, 'P', 'dddddd  **This is bold text**  example : ```git status``` ', '2018-01-05 15:24:00'),
(10, 'dd', NULL, 'C', 'scca', '2018-01-05 16:21:09');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_groups`
--

CREATE TABLE `tsk_groups` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_groups`
--

INSERT INTO `tsk_groups` (`id`, `name`, `description`, `created_at`) VALUES
(1, 'Admin', 'Administrator', '2017-12-30 16:47:41'),
(2, 'Main Manager', 'Main Manager description...', '2017-12-30 16:47:41'),
(3, 'Manager', 'Manager description...', '2017-12-30 16:47:41'),
(4, 'Employee', 'Employee description...', '2017-12-30 16:47:41');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_migrations`
--

CREATE TABLE `tsk_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_migrations`
--

INSERT INTO `tsk_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_22_155945_modify_users_table_status', 2),
(4, '2017_11_23_055108_create_users_groups_table', 2),
(5, '2017_11_23_080830_fill_users_groups_table_with_sapmle_data', 3),
(6, '2017_11_22_071254_create_tasks_table', 4),
(7, '2017_11_23_082235_create_categories_table', 5),
(8, '2017_11_23_084116_modify_tasks_table_status', 6),
(9, '2017_11_23_121432_modify_users_table_timestamp-', 7),
(10, '2017_11_23_145312_create_settings_table', 8),
(11, '2017_11_24_090056_modify_tasks_table_many_fields-', 9),
(12, '2017_11_24_124214_create_user_task_types_table', 10),
(13, '2017_11_24_131358_create_task_assigned_to_users_table', 11),
(14, '2017_11_27_084731_create_user_chats_table', 12),
(15, '2017_11_27_090448_create_user_chat_participants_table', 13),
(16, '2017_12_01_051343_modify_user_chats_table_task_id', 14),
(17, '2017_12_02_090642_modify_user_chat_participants_table', 15),
(18, '2017_12_02_114658_modify_user_chat_participants_table_status', 16),
(19, '2017_12_02_120953_modify_user_chats_table_manager_id', 17),
(20, '2017_12_04_112349_create_user_chat_messages_table', 18),
(21, '2017_12_05_051408_modify_user_chat_messages_table_updated_at', 19),
(22, '2017_12_08_065215_create_user_profile_table', 20),
(23, '2017_12_15_083529_create_document_categories_table', 21),
(24, '2017_12_17_081933_create_user_profile_documents_table', 22),
(25, '2017_12_26_061829_create_user_chats_last_visited_table', 23),
(26, '2017_12_26_083141_modify_user_chat_participants_table_unique_index', 24),
(27, '2017_12_27_133926_modify_tasks_description', 25),
(29, '2017_12_29_125721_modify_tsk_document_categories_alias', 26),
(30, '2017_12_29_154350_create_user_skills_table', 27),
(34, '2018_01_01_114651_create_user_chat_message_documents_table', 28);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_password_resets`
--

CREATE TABLE `tsk_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tsk_settings`
--

CREATE TABLE `tsk_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_settings`
--

INSERT INTO `tsk_settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'items_per_page', '20', '2017-12-30 16:49:42', NULL),
(2, 'site_name', 'Loan import', '2017-12-30 16:49:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_tasks`
--

CREATE TABLE `tsk_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `category_id` smallint(5) UNSIGNED NOT NULL,
  `priority` enum('0','1','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '  0-No, 1-Low, 2-Normal, 3-High, 4-Urgent, 5-Immediate  ',
  `status` enum('D','A','C','P','K','O') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'D => Draft, A=>Assigning, C => Cancelled, P => Processing, K=> Checking, O=> Completed',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `needs_reports` enum('0','1','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=>No, 1=>Hourly, 2=>Twice a day,3=>Daily,4=>Twice a week,5=>Weekly',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_tasks`
--

INSERT INTO `tsk_tasks` (`id`, `name`, `description`, `creator_id`, `category_id`, `priority`, `status`, `date_start`, `date_end`, `needs_reports`, `created_at`, `updated_at`) VALUES
(1, 'Mastering Laravel/vue.js', '', 1, 4, '3', 'P', '2017-12-30', '2018-03-02', '4', '2017-12-30 16:50:26', NULL),
(2, 'Develop Tasks management site using Laravel/vue.js', '', 2, 5, '4', 'A', '2017-12-30', '2018-04-30', '5', '2017-12-30 16:50:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_task_assigned_to_users`
--

CREATE TABLE `tsk_task_assigned_to_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_task_type_id` smallint(5) UNSIGNED NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_task_assigned_to_users`
--

INSERT INTO `tsk_task_assigned_to_users` (`id`, `task_id`, `user_id`, `user_task_type_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, 'You need to learn/control Mastering Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:51:12', NULL),
(2, 1, 5, 2, 'You need to learn Mastering Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:51:12', NULL),
(3, 2, 3, 1, 'You need to learn/control Develop Tasks management site using Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:51:12', NULL),
(4, 2, 5, 8, 'You need to learn Develop Tasks management site using Laravel/vue.js description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:51:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_users`
--

CREATE TABLE `tsk_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('N','A','I') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' N => New(Waiting activation), A=>Active, I=>Inactive',
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_start` date DEFAULT NULL,
  `contract_end` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_users`
--

INSERT INTO `tsk_users` (`id`, `name`, `email`, `password`, `remember_token`, `status`, `first_name`, `last_name`, `phone`, `website`, `contract_start`, `contract_end`, `created_at`, `updated_at`) VALUES
(1, 'Jon Glads', 'admin@site.com', '$2y$10$cH4J5WQBXdlzSV0hQB/MPOva8x.UFI3BmcmlYmCWjhKrPW13K554a', NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-30 16:49:31', NULL),
(2, 'Rod Bodrick', 'rod_bodrick@site.com', '$2y$10$Zz8705fDquwM7O5xvW5dregHygiVTjSUDYBhmdx4oHoHyk/loEXRy', NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-30 16:49:31', NULL),
(3, 'Tony Black', 'tony_black@site.com', '$2y$10$a04mlfvvPqt9qA1LsGjKtuSJkJjJTw6Urn2Ik8zN.fjwqpyS.WD62', NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-30 16:49:31', NULL),
(4, 'Adam Lang', 'adam_lang@site.com', '$2y$10$JuQOQyHaC9zGVuKUg3OcuOTQR3tvZI31Zck31ZxQrXpKn5Zhl7PBa', NULL, 'I', NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-30 16:49:31', NULL),
(5, 'admin', 'admin@mail.com', '$2y$10$qzgpltH4YRR2EwX1cHcL4uy47g9WaYc9XRJrBiCm1lyOlN1UzxMvW', 'YGtrdY1ShJfTPQxJnVuct0KHbmQk1K4BCEjQr00h1dh1JnGVlxyeJT1pHgXj', 'A', 'Mode', 'Pdode', '123-567-87', 'fdasdf.com', '2017-01-21', '2018-11-22', '2017-12-31 04:48:06', NULL),
(6, 'EmployeeUser', 'EmployeeUser@mail.com', '$2y$10$0jwh.8jtp/WaK3ZVKNITterBPXliN89bL6Qr44QixBbf6/tTLb1Nq', 'aiC6Le2hg2EyAjzb0dcU8Wy9uckcBR0QcqoNFzMgeLq4MxKPRbQTtsrIOnHW', 'A', 'gred', 'Tred', '00', '00', NULL, NULL, '2017-12-31 04:52:23', NULL),
(7, 'admindd', 'admindd@mail.com', '$2y$10$yrzLT8EvxAmhsH0gEfVY8eUF3AiyH5qe6F5eSWOeeMfz4WlsvDp5G', 'gmHVZVc1SzRCKDJErdW1xJFgRP8ZKLmk7F4e90F9fiD8j7JrNVIok1R74JFV', 'A', 'Fdes', 'Kjfe', 'klnl', 'kjk', NULL, NULL, '2018-01-02 13:12:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_users_groups`
--

CREATE TABLE `tsk_users_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` smallint(5) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_users_groups`
--

INSERT INTO `tsk_users_groups` (`id`, `user_id`, `group_id`, `created_at`) VALUES
(1, 1, 1, '2017-12-30 16:47:54'),
(2, 2, 2, '2017-12-30 16:47:54'),
(3, 3, 3, '2017-12-30 16:47:54'),
(4, 4, 4, '2017-12-30 16:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chats`
--

CREATE TABLE `tsk_user_chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('A','C') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT ' A=>Active, C=>Closed',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `task_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `manager_id` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chats`
--

INSERT INTO `tsk_user_chats` (`id`, `name`, `description`, `creator_id`, `status`, `created_at`, `task_id`, `updated_at`, `manager_id`) VALUES
(1, 'Greeting all employees!', 'Greeting all employees! and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 1, 'A', '2017-12-30 16:51:22', NULL, NULL, 5),
(2, 'People, get the first task description', 'First task description and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 2, 'A', '2017-12-30 16:51:22', NULL, NULL, 1),
(3, 'Let\'s discuss task Mastering Laravel/vue.js in this chat...', 'Let\'s discuss task Mastering Laravel/vue.js in this chat... and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 1, 'A', '2017-12-30 16:51:51', 1, NULL, 1),
(4, 'People, this is chat for Develop Tasks management site using Laravel/vue.js task  discussion', 'People, this is chat for Develop Tasks management site using Laravel/vue.js task  discussion and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', 2, 'A', '2017-12-30 16:51:51', 2, NULL, 1),
(5, 'a', 'a', 5, 'A', '2018-01-08 05:14:10', 1, NULL, 7),
(6, 'aaa', 'aaa', 5, 'A', '2018-01-08 05:15:18', 2, NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chats_last_visited`
--

CREATE TABLE `tsk_user_chats_last_visited` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_chat_id` int(10) UNSIGNED NOT NULL,
  `visited_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chats_last_visited`
--

INSERT INTO `tsk_user_chats_last_visited` (`id`, `user_id`, `user_chat_id`, `visited_at`) VALUES
(1, 5, 1, '2018-01-07 05:21:25'),
(2, 6, 1, '2018-01-07 05:18:06'),
(3, 1, 1, '2018-01-10 09:18:35'),
(4, 4, 1, '2018-01-10 10:03:32');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chat_messages`
--

CREATE TABLE `tsk_user_chat_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_chat_id` int(10) UNSIGNED NOT NULL,
  `is_top` tinyint(1) NOT NULL DEFAULT '0',
  `text` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chat_messages`
--

INSERT INTO `tsk_user_chat_messages` (`id`, `user_id`, `user_chat_id`, `is_top`, `text`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, ' That is first/top message on \"Greeting all employees!\" chan and Lorem ipsum ', '2017-12-30 16:55:16', NULL),
(2, 3, 1, 0, ' That is next message on \"Greeting all employees!\" chan and Lorem ipsum do', '2017-12-30 16:55:16', NULL),
(3, 1, 2, 1, ' That is first/top message on \"People, get the first task description\" chan and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:55:16', NULL),
(4, 4, 2, 0, ' That is next message on \"People, get the first task description\" chan and Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:55:16', NULL),
(8, 5, 1, 0, '2 files were uploaded : 1.png, 2.jpeg by Mode Pdode', '2018-01-10 12:12:01', NULL),
(9, 5, 1, 0, '2 files were uploaded : lender_table_2.xls, q.txt by Mode Pdode', '2018-01-10 12:12:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chat_message_documents`
--

CREATE TABLE `tsk_user_chat_message_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_chat_message_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_category_id` smallint(5) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chat_message_documents`
--

INSERT INTO `tsk_user_chat_message_documents` (`id`, `user_chat_message_id`, `user_id`, `filename`, `extension`, `info`, `document_category_id`, `created_at`) VALUES
(1, 8, 5, '1.png', 'png', 'Info text...', 7, '2018-01-10 12:12:01'),
(2, 8, 5, '2.jpeg', 'jpeg', 'Info text...', 7, '2018-01-10 12:12:01'),
(3, 9, 5, 'lender_table_2.xls', 'xls', 'Info text...', 7, '2018-01-10 12:12:16'),
(4, 9, 5, 'q.txt', 'txt', 'Info text...', 7, '2018-01-10 12:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_chat_participants`
--

CREATE TABLE `tsk_user_chat_participants` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_chat_id` int(10) UNSIGNED NOT NULL,
  `status` enum('M','W','R') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'R' COMMENT ' ''M''=>''Manage this chat'', ''W'' => ''Can write messages'', ''R'' => ''Can only read'' '
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_chat_participants`
--

INSERT INTO `tsk_user_chat_participants` (`id`, `user_id`, `created_at`, `user_chat_id`, `status`) VALUES
(1, 5, '2018-01-01 06:20:49', 1, 'M'),
(2, 6, '2018-01-01 12:18:05', 1, 'W'),
(3, 2, '2018-01-05 05:31:47', 1, 'W'),
(4, 3, '2018-01-05 05:31:52', 1, 'R'),
(5, 5, '2018-01-08 05:14:10', 5, 'W'),
(6, 7, '2018-01-08 05:14:10', 5, 'M'),
(7, 5, '2018-01-08 05:15:18', 6, 'M'),
(8, 7, '2018-01-08 05:15:18', 6, 'W');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_profiles`
--

CREATE TABLE `tsk_user_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_profiles`
--

INSERT INTO `tsk_user_profiles` (`id`, `user_id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 5, 'lang', 'en', '2018-01-02 12:27:22', NULL),
(2, 5, 'submit_message_by_enter', 'Y', '2018-01-02 12:27:22', NULL),
(3, 5, 'color', '#c9ba4c', '2018-01-02 12:27:22', NULL),
(4, 5, 'background_color', '#07288b', '2018-01-02 12:27:22', NULL),
(5, 5, 'subscription_to_newsletters', 'Y', '2018-01-02 12:27:22', NULL),
(6, 5, 'show_online_status', 'Y', '2018-01-02 12:27:22', NULL),
(7, 7, 'lang', 'en', '2018-01-02 14:36:01', NULL),
(8, 7, 'submit_message_by_enter', 'N', '2018-01-02 14:36:01', NULL),
(9, 7, 'color', '#dd3a3a', '2018-01-02 14:36:01', NULL),
(10, 7, 'background_color', '#bbff00', '2018-01-02 14:36:01', NULL),
(11, 7, 'subscription_to_newsletters', 'N', '2018-01-02 14:36:01', NULL),
(12, 7, 'show_online_status', 'N', '2018-01-02 14:36:01', NULL),
(13, 6, 'lang', 'en', '2018-01-06 14:48:03', NULL),
(14, 6, 'submit_message_by_enter', 'N', '2018-01-06 14:48:03', NULL),
(15, 6, 'color', '#de4b4b', '2018-01-06 14:48:03', NULL),
(16, 6, 'background_color', '#b8ea15', '2018-01-06 14:48:03', NULL),
(17, 6, 'subscription_to_newsletters', 'Y', '2018-01-06 14:48:03', NULL),
(18, 6, 'show_online_status', 'Y', '2018-01-06 14:48:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_profile_documents`
--

CREATE TABLE `tsk_user_profile_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_category_id` smallint(5) UNSIGNED NOT NULL,
  `public_access` enum('P','E') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'E' COMMENT '  P-Public, E-Personal',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_profile_documents`
--

INSERT INTO `tsk_user_profile_documents` (`id`, `user_id`, `filename`, `extension`, `document_category_id`, `public_access`, `info`, `created_at`) VALUES
(30, 7, '6.jpeg', 'jpeg', 3, 'P', 'Info text... Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis a', '2018-01-02 14:35:00'),
(73, 6, '2.jpeg', 'jpeg', 3, 'E', 'Info text...', '2018-01-06 13:09:09'),
(75, 6, 'short_original.csv', 'csv', 3, 'E', 'Info text...', '2018-01-06 13:09:35'),
(76, 6, 'q.txt', 'txt', 3, 'E', 'Info text...', '2018-01-06 13:09:35'),
(77, 6, '1.png', 'png', 3, 'E', 'Info text...', '2018-01-06 13:09:35'),
(78, 5, 'q.txt', 'txt', 3, 'E', 'Info text...', '2018-01-06 13:09:53'),
(81, 5, '1.png', 'png', 3, 'P', 'info AAA text...', '2018-01-07 08:45:01'),
(93, 5, '2.jpeg', 'jpeg', 3, 'P', 'info BBB text...', '2018-01-08 06:07:30'),
(94, 5, 'short_original.csv', 'csv', 3, 'E', 'Info text...', '2018-01-08 06:07:39');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_skills`
--

CREATE TABLE `tsk_user_skills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `skill` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_skills`
--

INSERT INTO `tsk_user_skills` (`id`, `user_id`, `skill`, `rating`, `created_at`) VALUES
(1, 1, 'PHP', 8, '2017-12-31 05:18:32'),
(2, 1, 'Laravel', 6, '2017-12-31 05:18:32'),
(3, 1, 'Vue.js', 0, '2017-12-31 05:18:32'),
(4, 1, 'HTML', 7, '2017-12-31 05:18:32'),
(5, 1, 'Team leader', 8, '2017-12-31 05:18:32'),
(6, 2, 'PHP', 6, '2017-12-31 05:18:32'),
(7, 2, 'Laravel', 5, '2017-12-31 05:18:32'),
(8, 2, 'Vue.js', 2, '2017-12-31 05:18:32'),
(9, 2, 'HTML', 9, '2017-12-31 05:18:32'),
(10, 3, 'PHP', 4, '2017-12-31 05:18:32'),
(11, 3, 'Laravel', 0, '2017-12-31 05:18:32'),
(12, 3, 'Vue.js', 7, '2017-12-31 05:18:32'),
(13, 3, 'Bootstrap', 9, '2017-12-31 05:18:32'),
(14, 3, 'HTML', 10, '2017-12-31 05:18:32'),
(15, 3, 'Remote administrator', 8, '2017-12-31 05:18:32'),
(16, 4, 'PHP', 6, '2017-12-31 05:18:32'),
(17, 4, 'Laravel', 5, '2017-12-31 05:18:32'),
(18, 4, 'Vue.js', 2, '2017-12-31 05:18:32'),
(19, 4, 'Bootstrap', 8, '2017-12-31 05:18:32'),
(20, 4, 'HTML', 9, '2017-12-31 05:18:32'),
(26, 6, 'PHP', 6, '2017-12-31 05:18:32'),
(27, 6, 'Laravel', 2, '2017-12-31 05:18:32'),
(28, 6, 'Vue.js', 2, '2017-12-31 05:18:32'),
(29, 6, 'Bootstrap', 5, '2017-12-31 05:18:32'),
(30, 6, 'HTML', 4, '2017-12-31 05:18:32'),
(31, 6, 'Tester', 7, '2017-12-31 05:18:32'),
(41, 5, 'Bootstrap', 5, '2018-01-05 12:12:37'),
(42, 5, 'HTML', 4, '2018-01-05 12:12:37'),
(43, 5, 'Laravel', 3, '2018-01-05 12:12:37'),
(44, 5, 'PHP', 2, '2018-01-05 12:12:37'),
(45, 5, 'Remote administrator and white rabits calculating', 1, '2018-01-05 12:12:37');

-- --------------------------------------------------------

--
-- Table structure for table `tsk_user_task_types`
--

CREATE TABLE `tsk_user_task_types` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tsk_user_task_types`
--

INSERT INTO `tsk_user_task_types` (`id`, `name`, `description`, `created_at`) VALUES
(1, 'Team Leader', 'Team Leader ... Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:50:59'),
(2, 'PHP developer', 'PHP developer Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:50:59'),
(3, 'HTML developer', 'HTML developer Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-12-30 16:50:59'),
(4, 'Javascript/Vue developer', 'Javascript/Vue developer Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-12-30 16:50:59'),
(5, 'Painter', 'Painter Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:50:59'),
(6, 'Tester', 'Tester Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n            \n            Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2017-12-30 16:50:59'),
(8, 'aa', 'a', '2018-01-04 13:29:07'),
(9, 'a', 'a', '2018-01-09 06:00:55'),
(10, 'azzx', 'a', '2018-01-09 06:03:04'),
(11, 'aazz', 'aaa', '2018-01-09 06:05:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsk_categories`
--
ALTER TABLE `tsk_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`),
  ADD KEY `categories_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_document_categories`
--
ALTER TABLE `tsk_document_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `document_categories_name_unique` (`name`),
  ADD UNIQUE KEY `document_categories_alias_unique` (`alias`),
  ADD KEY `document_categories_created_at_index` (`created_at`),
  ADD KEY `document_type_name_at_index` (`type`,`name`);

--
-- Indexes for table `tsk_groups`
--
ALTER TABLE `tsk_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groups_name_unique` (`name`),
  ADD UNIQUE KEY `groups_description_unique` (`description`),
  ADD KEY `groups_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_migrations`
--
ALTER TABLE `tsk_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tsk_password_resets`
--
ALTER TABLE `tsk_password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tsk_settings`
--
ALTER TABLE `tsk_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_name_unique` (`name`),
  ADD KEY `settings_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_tasks`
--
ALTER TABLE `tsk_tasks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tasks_name_unique` (`name`),
  ADD KEY `tasks_creator_id_foreign` (`creator_id`),
  ADD KEY `tasks_category_id_foreign` (`category_id`),
  ADD KEY `tasks_status_date_start_date_end_index` (`status`,`date_start`,`date_end`),
  ADD KEY `tasks_priority_category_id_date_end_index` (`priority`,`category_id`,`date_end`),
  ADD KEY `tasks_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_task_assigned_to_users`
--
ALTER TABLE `tsk_task_assigned_to_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_assigned_to_users_user_id_task_id_unique` (`user_id`,`task_id`),
  ADD KEY `task_assigned_to_users_task_id_foreign` (`task_id`),
  ADD KEY `task_assigned_to_users_user_task_type_id_foreign` (`user_task_type_id`),
  ADD KEY `task_assigned_to_users_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_users`
--
ALTER TABLE `tsk_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_status_login_index` (`status`),
  ADD KEY `users_created_at_index` (`created_at`),
  ADD KEY `users_status_contract_start_index` (`status`,`contract_start`);

--
-- Indexes for table `tsk_users_groups`
--
ALTER TABLE `tsk_users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_groups_user_id_group_id_unique` (`user_id`,`group_id`),
  ADD KEY `users_groups_group_id_foreign` (`group_id`),
  ADD KEY `users_groups_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_chats`
--
ALTER TABLE `tsk_user_chats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_chats_created_at_index` (`created_at`),
  ADD KEY `user_chats_creator_id_status_name_index` (`creator_id`,`status`,`name`),
  ADD KEY `user_chats_task_id_foreign` (`task_id`),
  ADD KEY `tasks_status_task_id_creator_id_index` (`status`,`task_id`,`creator_id`),
  ADD KEY `user_chats_manager_id_status_index` (`manager_id`,`status`);

--
-- Indexes for table `tsk_user_chats_last_visited`
--
ALTER TABLE `tsk_user_chats_last_visited`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_chats_last_visited_user_id_user_chat_id_unique` (`user_id`,`user_chat_id`),
  ADD KEY `user_chats_last_visited_user_chat_id_foreign` (`user_chat_id`),
  ADD KEY `user_chats_last_visited_visited_at_index` (`visited_at`);

--
-- Indexes for table `tsk_user_chat_messages`
--
ALTER TABLE `tsk_user_chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_chat_messages_user_id_user_chat_id_index` (`user_id`,`user_chat_id`),
  ADD KEY `user_chat_messages_user_chat_id_is_top_index` (`user_chat_id`,`is_top`),
  ADD KEY `user_chat_messages_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_chat_message_documents`
--
ALTER TABLE `tsk_user_chat_message_documents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_chat_message_documents_1_unique` (`user_chat_message_id`,`user_id`,`filename`),
  ADD KEY `user_chat_message_documents_user_id_foreign` (`user_id`),
  ADD KEY `user_chat_message_documents_document_category_id_foreign` (`document_category_id`),
  ADD KEY `user_chat_message_documents_2` (`user_chat_message_id`,`user_id`,`extension`),
  ADD KEY `user_chat_message_documents_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_chat_participants`
--
ALTER TABLE `tsk_user_chat_participants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_chat_participants_user_id_user_chat_id_unique` (`user_id`,`user_chat_id`),
  ADD KEY `user_chat_participants_created_at_index` (`created_at`),
  ADD KEY `user_chat_participants_status_user_index` (`user_id`),
  ADD KEY `user_chat_participants_user_chat_id_status_user_id_index` (`user_chat_id`,`status`,`user_id`);

--
-- Indexes for table `tsk_user_profiles`
--
ALTER TABLE `tsk_user_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_profiles_user_id_name_unique` (`user_id`,`name`),
  ADD KEY `user_profiles_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_profile_documents`
--
ALTER TABLE `tsk_user_profile_documents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_profile_documents_user_id_filename_unique` (`user_id`,`filename`),
  ADD KEY `user_profile_documents_document_category_id_foreign` (`document_category_id`),
  ADD KEY `user_profile_documents_user_id_public_access_unique` (`user_id`,`public_access`),
  ADD KEY `user_profile_documents_user_id_extension_unique` (`user_id`,`extension`),
  ADD KEY `user_profile_documents_created_at_index` (`created_at`);

--
-- Indexes for table `tsk_user_skills`
--
ALTER TABLE `tsk_user_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_skills_created_at_index` (`created_at`),
  ADD KEY `user_skills_user_id_skill_index` (`user_id`,`skill`);

--
-- Indexes for table `tsk_user_task_types`
--
ALTER TABLE `tsk_user_task_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_task_types_name_unique` (`name`),
  ADD KEY `user_task_types_created_at_index` (`created_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsk_categories`
--
ALTER TABLE `tsk_categories`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tsk_document_categories`
--
ALTER TABLE `tsk_document_categories`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tsk_groups`
--
ALTER TABLE `tsk_groups`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tsk_migrations`
--
ALTER TABLE `tsk_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tsk_settings`
--
ALTER TABLE `tsk_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tsk_tasks`
--
ALTER TABLE `tsk_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tsk_task_assigned_to_users`
--
ALTER TABLE `tsk_task_assigned_to_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tsk_users`
--
ALTER TABLE `tsk_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tsk_users_groups`
--
ALTER TABLE `tsk_users_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tsk_user_chats`
--
ALTER TABLE `tsk_user_chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tsk_user_chats_last_visited`
--
ALTER TABLE `tsk_user_chats_last_visited`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tsk_user_chat_messages`
--
ALTER TABLE `tsk_user_chat_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tsk_user_chat_message_documents`
--
ALTER TABLE `tsk_user_chat_message_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tsk_user_chat_participants`
--
ALTER TABLE `tsk_user_chat_participants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tsk_user_profiles`
--
ALTER TABLE `tsk_user_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tsk_user_profile_documents`
--
ALTER TABLE `tsk_user_profile_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `tsk_user_skills`
--
ALTER TABLE `tsk_user_skills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `tsk_user_task_types`
--
ALTER TABLE `tsk_user_task_types`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tsk_categories`
--
ALTER TABLE `tsk_categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `tsk_categories` (`id`);

--
-- Constraints for table `tsk_tasks`
--
ALTER TABLE `tsk_tasks`
  ADD CONSTRAINT `tasks_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `tsk_categories` (`id`),
  ADD CONSTRAINT `tasks_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_task_assigned_to_users`
--
ALTER TABLE `tsk_task_assigned_to_users`
  ADD CONSTRAINT `task_assigned_to_users_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tsk_tasks` (`id`),
  ADD CONSTRAINT `task_assigned_to_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `task_assigned_to_users_user_task_type_id_foreign` FOREIGN KEY (`user_task_type_id`) REFERENCES `tsk_user_task_types` (`id`);

--
-- Constraints for table `tsk_users_groups`
--
ALTER TABLE `tsk_users_groups`
  ADD CONSTRAINT `users_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `tsk_groups` (`id`),
  ADD CONSTRAINT `users_groups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_chats`
--
ALTER TABLE `tsk_user_chats`
  ADD CONSTRAINT `user_chats_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `user_chats_manager_id_foreign` FOREIGN KEY (`manager_id`) REFERENCES `tsk_users` (`id`),
  ADD CONSTRAINT `user_chats_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tsk_tasks` (`id`);

--
-- Constraints for table `tsk_user_chats_last_visited`
--
ALTER TABLE `tsk_user_chats_last_visited`
  ADD CONSTRAINT `user_chats_last_visited_user_chat_id_foreign` FOREIGN KEY (`user_chat_id`) REFERENCES `tsk_user_chats` (`id`),
  ADD CONSTRAINT `user_chats_last_visited_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_chat_messages`
--
ALTER TABLE `tsk_user_chat_messages`
  ADD CONSTRAINT `user_chat_messages_user_chat_id_foreign` FOREIGN KEY (`user_chat_id`) REFERENCES `tsk_user_chats` (`id`),
  ADD CONSTRAINT `user_chat_messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_chat_message_documents`
--
ALTER TABLE `tsk_user_chat_message_documents`
  ADD CONSTRAINT `user_chat_message_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `tsk_document_categories` (`id`),
  ADD CONSTRAINT `user_chat_message_documents_user_chat_message_id_foreign` FOREIGN KEY (`user_chat_message_id`) REFERENCES `tsk_user_chat_messages` (`id`),
  ADD CONSTRAINT `user_chat_message_documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_chat_participants`
--
ALTER TABLE `tsk_user_chat_participants`
  ADD CONSTRAINT `user_chat_participants_user_chat_id_foreign` FOREIGN KEY (`user_chat_id`) REFERENCES `tsk_user_chats` (`id`),
  ADD CONSTRAINT `user_chat_participants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_profiles`
--
ALTER TABLE `tsk_user_profiles`
  ADD CONSTRAINT `user_profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_profile_documents`
--
ALTER TABLE `tsk_user_profile_documents`
  ADD CONSTRAINT `user_profile_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `tsk_document_categories` (`id`),
  ADD CONSTRAINT `user_profile_documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

--
-- Constraints for table `tsk_user_skills`
--
ALTER TABLE `tsk_user_skills`
  ADD CONSTRAINT `user_skills_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tsk_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
